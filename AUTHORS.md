## AUTHORS

The ParaSkel++ platform has been conceived and is maintained by
* Simon LEMAIRE (INRIA, Univ. Lille): simon.lemaire@inria.fr

The main developers so far have been
* Laurence BEAUDE (INRIA, Univ. Lille): 02/2020--08/2021
* Thoma ZOTO (INRIA, Univ. Lille): 12/2022--06/2024
