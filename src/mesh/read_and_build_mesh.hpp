#ifndef READ_AND_BUILD_MESH_HPP
#define READ_AND_BUILD_MESH_HPP

#include <iostream>
#include <vector>
#include "mesh_reader_fvca.hpp"
#include "mesh_reader_gmsh.hpp"
#include "mesh_builder.hpp"


namespace MyMesh {
    /**
     * Read the meshfile and build the mesh : 
     * return the pointer to the mesh
     * 
     * @param mesh_file name of the textfile containing the mesh
     * @param build_edges boolean to avoid the construction of the edges when they are not necessary. By default, build edges.
    */
    template<size_t DIM>
    std::unique_ptr<Mesh<DIM>> read_and_build_mesh(std::string const mesh_file,
                                                   bool build_edges = true);

    /////////////////////////////////////////////////////////////////////////
    //                   Implementations
    /////////////////////////////////////////////////////////////////////////

    template<size_t DIM>
    std::unique_ptr<Mesh<DIM>> read_and_build_mesh(std::string const mesh_file,
                                                   bool build_edges) {
        
        std::vector<std::vector<double>> vertices;
        std::vector<std::vector<size_t>> vertices_by_cells;
        std::vector<std::vector<size_t>> faces_by_cells;
        std::vector<std::vector<size_t>> vertices_by_faces;
        std::vector<std::vector<double>>* centers(nullptr);

#ifdef DEBUG
        if(mesh_file.empty()) {
            throw "No mesh file has been provided, type ./exec -h to print help. ";
            return nullptr;
        }
#endif

        std::cout << "   ----------------------------------------------" << std::endl;
        std::cout << "   Reading mesh file: " << mesh_file << std::endl;

        if(DIM == 2) {

            if (std::regex_match(mesh_file, std::regex(".*\\.msh$") )) {
                // Mesh file is a 2D gmsh file
                read_gmsh_mesh(DIM, mesh_file, vertices, vertices_by_cells);

            } else if(std::regex_match(mesh_file, std::regex(".*\\.typ1$")) ) {
                // Mesh file is a 2D fvca5 file
                std::vector<std::vector<size_t>> vertices_by_bound_edges;
                read_2Dfvca_mesh(mesh_file, 1, vertices, vertices_by_cells, &vertices_by_bound_edges);
                
            } else if(std::regex_match(mesh_file, std::regex(".*\\.typ2$")) ) {
                // Mesh file is a 2D fvca5 file
                std::vector<std::vector<size_t>> vertices_by_bound_edges;
                read_2Dfvca_mesh(mesh_file, 2, vertices, vertices_by_cells, &vertices_by_bound_edges);

            } else {
                throw "Unknown extension (ie format) in read_and_build_mesh. ";
                return nullptr;
            }

            // Build the 2D mesh (with connectivity), vertices_by_faces is constructed
            return build_mesh<DIM>(vertices, vertices_by_cells);

        } else if(DIM == 3) {

            std::vector<std::vector<size_t>> edges_by_faces;
            std::vector<std::vector<size_t>> vertices_by_edges;
            std::string mesh_name(mesh_file);
    
            if (std::regex_match(mesh_file, std::regex(".*\\.msh$") )) {
                // Mesh file is a 3D gmsh file
                read_gmsh_mesh(DIM, mesh_file, vertices, vertices_by_cells,
                    &faces_by_cells, &vertices_by_faces, &edges_by_faces, &vertices_by_edges,
                    build_edges); // boolean to construct edges only if necessary

            } else if(std::regex_match(mesh_file, std::regex(".*\\.typ6$") )) {
                // Mesh file is a 3D fvca6 file
                read_3Dfvca_mesh(mesh_file, mesh_name, vertices, faces_by_cells, 
                    vertices_by_cells, edges_by_faces, vertices_by_faces, vertices_by_edges);

            } else {
                throw "Unknown extension (ie format) in read_and_build_mesh. ";
                return nullptr;
            }

            // Build the 3D mesh (with connectivity)
            return build_mesh<DIM>(vertices, vertices_by_cells,
                centers, &vertices_by_faces, &faces_by_cells,
                &edges_by_faces, &vertices_by_edges, mesh_name);
        }

        throw " Unknown dimension. ";
        return nullptr;

    } // read_and_build_mesh

} // MyMesh namespace
#endif /* READ_AND_BUILD_MESH_HPP */
