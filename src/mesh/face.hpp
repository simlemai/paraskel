#ifndef FACE_HPP
#define FACE_HPP

#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include "mesh_elemt_info.hpp"

namespace MyMesh { // forward declaration of template classes
    template<size_t DIM>
    class Cell;
    template<size_t DIM>
    class Edge;
    template<size_t DIM>
    class Vertex;
}
namespace MyMesh {
    /**
     * The class Face provides every specific informations about the faces. 
     * Face are always object of dimension DIM-1, 
     * so in 2D it represents the edges.
    */
    template<size_t DIM>
    class Face  : public MeshElemtInfo<DIM>
    {
    public:
        /**
         * Constructor
         * @param mesh pointer to the mesh
         * @param g_i global index of the face
         */
        Face(Mesh<DIM>* mesh, size_t g_i);
        ~Face() = default;   ///< Destructor

        // concerning the neighbouring cell(s) of the face
        size_t n_neighbour_cells() const;          ///<  returns the number of cells sharing the face (1 or 2)
        /**
         * add a neighbouring cell to the face list
         * @param cell pointer to the cell
        */
        inline void add_neighbour_cell(Cell<DIM> *cell);
        inline std::vector<Cell<DIM> *> get_neighbour_cells() const;  ///<  returns the list of neighbouring cells of the face (pointeur tabular)
        /**
         * add the local index of the face in the neighbouring cell C,
         * the local index is the position of this face in the list of faces of C.
         * The order in this list must be the same than in m_neighbour_cells
         * @param l_i local index
        */
        inline void add_local_index(size_t l_i);
        inline std::vector<size_t> get_local_indexes() const; ///< returns the list of local index(es) in the neighbouring cell(s)

        // concerning the edges of the face (useful only in 3D)
        inline size_t n_edges() const;               ///<  returns the number of edges of the face
        /**
         * add an edge to the face list
         * @param edge pointer to the edge
        */
        inline void add_edge(Edge<DIM> *edge);
        inline std::vector<Edge<DIM> *> get_edges() const;  ///<  returns the list of edges of the face

        // concerning the vertices of the face
        inline size_t n_vertices() const;               ///<  returns the number of vertices of the face
        /**
         * add an vertex to the face list
         * @param vertex pointer to the vertex
        */
        inline void add_vertex(Vertex<DIM> *vertex);
        inline std::vector<Vertex<DIM> *> get_vertices() const;  ///<  returns the list of vertices of the face
        inline Vertex<DIM>* vertex(size_t l_i) const;   ///< get a constant pointer to a vertex using its local index

        // // concerning the outer normal vector of the face
        // inline Eigen::VectorXd normal() const;         ///<  returns the outer normal vector of the face
        // /**
        //  * Set the normal of the mesh element with a vector
        //  * @param n vector value of normal
        //  */
        // inline void set_normal(const Eigen::VectorXd& n);
        // /**
        //  * Set the outer normal of the face with DIM=2 coordinates
        //  * @param x is the first coordinate
        //  * @param y is the second coordinate
        //  */
        // template<typename U>
        // inline void set_normal(const typename std::enable_if<DIM == 2, U>::type& x, const U& y);
        // /**
        //  * Set the outer normal of the face with DIM=3 coordinates
        //  * @param x is the first coordinate
        //  * @param y is the second coordinate
        //  * @param z is the third coordinate
        //  */
        // template<typename U>
        // inline void set_normal(const typename std::enable_if<DIM == 3, U>::type& x, const U& y, const U& z);


    private:
        // Eigen::VectorXd m_normal;           ///< outer normal to the face
        std::vector<Edge<DIM> *> m_edges;   ///<  list of edges' pointer of the face
        std::vector<Vertex<DIM> *> m_vertices;   ///<  list of vertices' pointer of the face
        std::vector<Cell<DIM> *> m_neighbour_cells;   ///<  list of cell(s) sharing the face
        std::vector<size_t> m_local_indexes;  ///< list of the local index(es) of this face in the neighbouring cell(s)

    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////
    template<size_t DIM>
    Face<DIM>::Face(Mesh<DIM>* mesh, size_t g_i) : 
    MeshElemtInfo<DIM>(mesh, g_i),
    m_edges(0),
    m_vertices(0),
    m_neighbour_cells(0),
    m_local_indexes(0) {}
    // m_normal(DIM) {m_normal.setZero();}

    template<size_t DIM>
    inline size_t Face<DIM>::n_edges() const {return m_edges.size();}

    template<size_t DIM>
    inline void Face<DIM>::add_edge(Edge<DIM> *edge) {m_edges.push_back(edge);}

    template<size_t DIM>
    inline std::vector<Edge<DIM> *> Face<DIM>::get_edges() const {return m_edges;}

    template<size_t DIM>
    inline size_t Face<DIM>::n_vertices() const {return m_vertices.size();}

    template<size_t DIM>
    inline void Face<DIM>::add_vertex(Vertex<DIM> *vertex) {m_vertices.push_back(vertex);}

    template<size_t DIM>
    inline std::vector<Vertex<DIM> *> Face<DIM>::get_vertices() const {return m_vertices;}

    template<size_t DIM>
    inline Vertex<DIM>* Face<DIM>::vertex(size_t l_i) const {
#ifdef DEBUG
        return m_vertices.at(l_i);
#else
        return m_vertices[l_i];
#endif
    }
    template<size_t DIM>
    inline size_t Face<DIM>::n_neighbour_cells() const {return m_neighbour_cells.size();}

    template<size_t DIM>
    inline void Face<DIM>::add_neighbour_cell(Cell<DIM> *cell) {m_neighbour_cells.push_back(cell);}

    template<size_t DIM>
    inline std::vector<Cell<DIM> *> Face<DIM>::get_neighbour_cells() const {return m_neighbour_cells;}

    template<size_t DIM>
    inline void Face<DIM>::add_local_index(size_t l_i) {m_local_indexes.push_back(l_i);}

    template<size_t DIM>
    inline std::vector<size_t> Face<DIM>::get_local_indexes() const {return m_local_indexes;}

    // template<size_t DIM>
    // inline Eigen::VectorXd Face<DIM>::normal() const {return m_normal;}

    // template<size_t DIM>
    // inline void Face<DIM>::set_normal(const Eigen::VectorXd& n) {m_normal = n;}

    // template<size_t DIM>
    // template<typename U>
    // inline void Face<DIM>::set_normal(const typename std::enable_if<DIM == 2, U>::type& x, const U& y) 
    // {m_normal<< x, y;}

    // template<size_t DIM>
    // template<typename U>
    // inline void Face<DIM>::set_normal(const typename std::enable_if<DIM == 3, U>::type& x, const U& y, const U& z) 
    // {m_normal<< x, y, z;}

} // MyMesh namespace
#endif /* FACE_HPP */
