#ifndef MESH_READER_GMSH_HPP
#define MESH_READER_GMSH_HPP

#include <iostream>
#include <fstream>
#include <regex>
#include <vector>
#include <cctype>
#include<algorithm> // for sort() 
#include "mesh_reader_common.hpp"

namespace MyMesh {
    /**
     * Reads the textfile which contains the mesh 
     * in gmsh format (can be 2D or 3D)
     * and fill some connectivity vectors
     * 
     * This format is like:
     * $MeshFormat
     *      version-number file-type data-size
     * $EndMeshFormat
     * $PhysicalNames
     *      number-of-names
     *      physical-dimension physical-number "physical-name"
     *               ...
     * $EndPhysicalNames
     * $Nodes
     *      nb of nodes
     *      num_vertex  x_coord  y_coord  z_coord
     *               ...
     * $EndNodes
     * $Elements
     *      nb of element
     *      elm-number elm-type number-of-tags < tag > ... node-number-list
     *       ...
     * $EndElements
     * 
     * @param dimension dimension (2 or 3)
     * @param filename name of the textfile containing the mesh
     * @param vertices vector with the vertices coordinates
     * @param vertices_by_cells vector with the vertices global indexes in each cell
     * @param faces_by_cells vector with the faces global indexes in each cell
     * @param vertices_by_faces vector with the vertices global indexes in each face
     * @param edges_by_faces vector with the edges global indexes in each face
     * @param vertices_by_edges vector with the vertices global indexes in each edge
     * @param build_edges boolean to avoid the construction of the edges when they are not necessary. By default, build edges.
    */
    void read_gmsh_mesh(int const dimension,
                    std::string const filename,
                    std::vector<std::vector<double> >& vertices,
                    std::vector<std::vector<size_t> >& vertices_by_cells,
                    std::vector<std::vector<size_t> >* faces_by_cells = nullptr,
                    std::vector<std::vector<size_t> >* vertices_by_faces = nullptr,
                    std::vector<std::vector<size_t> >* edges_by_faces = nullptr,
                    std::vector<std::vector<size_t> >* vertices_by_edges = nullptr,
                    bool build_edges = true); 

    /**
     * read a list from 2D gmsh file when format is
     * elm-number elm-type number-of-tags < tag > ... node-number-list
     *     ....
     * and fill different vectors thanks to "elm-type".
     * 
     * @param infile ifstream of file
     * @param n_elements number of elements to read
     * @param vertices_by_cells index of the vertices of each cell
     * @param vertices_by_bound_faces index of the vertices of each boundary face
    */
    static void read_2Dgmsh_list(std::ifstream* infile,
                size_t const n_elements,
                std::vector<std::vector<size_t> >& vertices_by_cells,
                std::vector<std::vector<size_t> >* vertices_by_bound_faces = nullptr);

    /**
     * read a list from 3D gmsh file when format is
     * elm-number elm-type number-of-tags < tag > ... node-number-list
     *     ....
     * and fill different vectors thanks to "elm-type".
     * 
     * @param infile ifstream of file
     * @param n_vertices total number of vertices in mesh
     * @param n_elements number of elements to read
     * @param vertices_by_cells index of the vertices of each cell
     * @param faces_by_cells index of the faces of each cell
     * @param vertices_by_faces index of the vertices of each face
     * @param edges_by_faces index of the vertices of each face
     * @param vertices_by_edges index of the vertices of each edge
     * @param build_edges boolean to avoid the construction of the edges when they are not necessary. By default, build edges.
    */
    static void read_3Dgmsh_list(std::ifstream* infile,
                size_t const n_vertices,
                size_t const n_elements,
                std::vector<std::vector<size_t> >& vertices_by_cells,
                std::vector<std::vector<size_t> >& faces_by_cells,
                std::vector<std::vector<size_t> >& vertices_by_faces,
                std::vector<std::vector<size_t> >& edges_by_faces,
                std::vector<std::vector<size_t> >& vertices_by_edges,
                bool build_edges);

    /**
     * The goal is to build only once each face and each edge
     * given each gmsh mesh element.
     * Build info about the faces : faces_by_cells and vertices_by_faces 
     *        and about the edges : edges_by_faces and vertices_by_edges 
     * when local_vertices_by_faces and partial vertices_by_faces,
     * local_vertices_by_edges, local_edges_by_faces and partial 
     * edges_by_faces and vertices_by_edges are provided.
     * 
     * @param local_vertices_by_faces vertices global indexes in all faces of one mesh element
     * @param local_vertices_by_edges vertices global indexes in all edges of one mesh element
     * @param local_edges_by_faces edges LOCAL indexes in all faces of one mesh element
     * @param edges_by_min_vertex edges global indexes stored with their min vertex global index
     * @param faces_by_min_vertex faces global indexes stored with their min vertex global index
     * @param sorted_vertices_by_faces vertices global indexes in each face in increasing index number
     * @param vertices_by_faces vertices global indexes in each face (face are in global number)
     * @param faces_by_cells faces global indexes in each cell
     * @param edges_by_faces edges global indexes in each face
     * @param vertices_by_edges vertices global indexes in each edge
     * @param is_boundary_face if the mesh elemt is a face, do not add it to the list of cells
    */
    static void build_edges_and_faces(const std::vector<std::vector<size_t> >& local_vertices_by_faces, 
                    const std::vector<std::vector<size_t> >& local_vertices_by_edges, 
                    const std::vector<std::vector<size_t> >& local_edges_by_faces,
                    std::vector<std::vector<size_t> >& edges_by_min_vertex, 
                    std::vector<std::vector<size_t> >& faces_by_min_vertex,
                    std::vector<std::vector<size_t> >& sorted_vertices_by_faces,
                    std::vector<std::vector<size_t> >& vertices_by_faces,
                    std::vector<std::vector<size_t> >& faces_by_cells,
                    std::vector<std::vector<size_t> >& edges_by_faces,
                    std::vector<std::vector<size_t> >& vertices_by_edges,
                    bool is_boundary_face = false);

    /**
     * The goal is to build only once each face
     * given each gmsh mesh element.
     * Build info about the faces : faces_by_cells and vertices_by_faces 
     * when local_vertices_by_faces and partial vertices_by_faces are provided.
     * Edges are not build.
     * 
     * @param local_vertices_by_faces vertices global indexes in all faces of one mesh element
     * @param faces_by_min_vertex faces global indexes stored with their min vertex global index
     * @param sorted_vertices_by_faces vertices global indexes in each face in increasing index number
     * @param vertices_by_faces vertices global indexes in each face (face are in global number)
     * @param faces_by_cells faces global indexes in each cell
     * @param is_boundary_face if the mesh elemt is a face, do not add it to the list of cells
    */
    static void build_faces(const std::vector<std::vector<size_t> >& local_vertices_by_faces, 
                    std::vector<std::vector<size_t> >& faces_by_min_vertex,
                    std::vector<std::vector<size_t> >& sorted_vertices_by_faces,
                    std::vector<std::vector<size_t> >& vertices_by_faces,
                    std::vector<std::vector<size_t> >& faces_by_cells,
                    bool is_boundary_face = false);

    /////////////////////////////////////////////////////////////////////////
    //                   Implementations
    /////////////////////////////////////////////////////////////////////////

    void read_gmsh_mesh(int const dimension,
                    std::string const filename,
                    std::vector<std::vector<double> >& vertices,
                    std::vector<std::vector<size_t> >& vertices_by_cells,
                    std::vector<std::vector<size_t> >* faces_by_cells,
                    std::vector<std::vector<size_t> >* vertices_by_faces,
                    std::vector<std::vector<size_t> >* edges_by_faces,
                    std::vector<std::vector<size_t> >* vertices_by_edges,
                    bool build_edges) {

        std::ifstream infile(filename);
        std::string keyword, str_line;
        std::string nodes_name("$Nodes");
        std::string end_nodes_name("$EndNodes");
        std::string elements_name("$Elements");
        std::string end_elements_name("$EndElements");
        size_t n_v(0), n_elmt(0);
        
#ifdef DEBUG
        std::cout << "Mesh format is gmsh" << std::endl;
#endif
        // open file
        if(!infile.is_open()) {
            throw "Error in opening file. ";
            return;
        }

        // -------------------- Header --------------------
        // avoid the first 3th lines
        for (size_t i=0; i<3; i++) { std::getline(infile, str_line); }

        // gmsh file contains parts not in order so use keyword switch
        while (!infile.eof()) {
            // read keyword
            keyword = get_and_clean_line(&infile);

            if(keyword.compare(nodes_name) == 0) {
                // -------------------- Vertices --------------------
                n_v = std::stoi(get_and_clean_line(&infile)); // nb vertices
                for (size_t i=0; i<n_v; i++) {
                    std::vector<double> x(3);
                    infile >> n_elmt >> x[0] >> x[1] >> x[2]; // 3 coords even in 2D
#ifdef DEBUG
                    if(n_elmt != i+1) {
                        throw "Error, non consecutive num of nodes in read gmsh file, not implemented yet. ";
                        return;
                    }
#endif
                    x.resize(dimension); // store only DIM coords (remove the last one in 2D)
                    vertices.push_back(x);
                }
                str_line = get_and_clean_line(&infile);
#ifdef DEBUG
                if(str_line.compare(end_nodes_name) != 0) {
                    throw "Error in gmsh file, not good keyword at end of vertices. ";
                    return;
                }
#endif
                continue;
            }
            if(keyword.compare(elements_name) == 0) { 
                // -------------------- Elements --------------------
                n_elmt = std::stoi(get_and_clean_line(&infile)); // nb elements in mesh
                if(dimension==2) {
                    read_2Dgmsh_list(&infile, n_elmt, vertices_by_cells);
                } else if(dimension==3) {
                    read_3Dgmsh_list(&infile, n_v, n_elmt, vertices_by_cells, *faces_by_cells,
                            *vertices_by_faces, *edges_by_faces, *vertices_by_edges, build_edges);
                }
                str_line = get_and_clean_line(&infile);
#ifdef DEBUG
                if(str_line.compare(end_elements_name) != 0) {
                    throw "Error in gmsh file, not good keyword at end of elements. ";
                    return;
                }
#endif
                break; 
            }
        }

        infile.close();
    } // read_gmsh_mesh

    // Read list with 2D gmsh format
    // elm-number elm-type number-of-tags < tag > ... node-number-list
    // when elm-type is :
    //     1 : 2-node line (face because 2D)
    //     2 : 3-node triangle (cell because 2D)
    //     3 : 4-node quadrangle (cell because 2D)
    //     15 : 1-node point (avoid it)
    static void read_2Dgmsh_list(std::ifstream* infile,
        size_t const n_elements,
        std::vector<std::vector<size_t> >& vertices_by_cells,
        std::vector<std::vector<size_t> >* vertices_by_bound_faces) {

        // local vector, necessary if vertices_by_bound_faces is not provided
        std::vector<std::vector<size_t>> local_vertices_by_bound_faces;

        size_t aux(0), type(0), nb_tags(0), tag(0);
        for (size_t i=0; i<n_elements; i++) {
            // first value is the index of the element, avoid it
            // second value is the element type, third value is the nb of tags to be read
            *infile >> aux >> type >> nb_tags;
            for (size_t j=0; j<nb_tags; j++) { *infile >> tag; } // avoid tags

            if(type == 1) { // 2-node line (face)
                read_list(infile, 1, local_vertices_by_bound_faces, 2);
            } else if(type == 2) { // 3-node triangle (cell)
                read_list(infile, 1, vertices_by_cells, 3); 
            } else if(type == 3) { // 4-node quadrangle (cell)
                read_list(infile, 1, vertices_by_cells, 4); 
            } else if(type == 15) { // 1-node point (avoid it)
                *infile >> aux; 
                continue; // skip it
            } else {
                throw "gmsh elm-type is not recognized.";
                return;
            }

        }
        if(vertices_by_bound_faces) {*vertices_by_bound_faces = local_vertices_by_bound_faces;}

    } // read_2Dgmsh_list

    // Read list with 3D gmsh format
    // elm-number elm-type number-of-tags < tag > ... node-number-list
    // when elm-type is :
    //     1  : 2-node line (edge because 3D)
    //     2  : 3-node triangle (face because 3D)
    //     3  : 4-node quadrangle (face because 3D)
    //     4  : 4-node tetrahedron (cell)
    //     5  : 8-node hexahedron (cell)
    //     6  : 6-node prism (cell)
    //     7  : 5-node pyramid (cell)
    //     15 : 1-node point (avoid it)
    static void read_3Dgmsh_list(std::ifstream* infile,
        size_t const n_vertices,
        size_t const n_elements,
        std::vector<std::vector<size_t> >& vertices_by_cells,
        std::vector<std::vector<size_t> >& faces_by_cells,
        std::vector<std::vector<size_t> >& vertices_by_faces,
        std::vector<std::vector<size_t> >& edges_by_faces,
        std::vector<std::vector<size_t> >& vertices_by_edges,
        bool build_edges) {

        size_t aux(0), type(0), nb_tags(0), tag(0);
        std::vector<std::vector<size_t> > vertices_by_bound_edges;
        std::vector<std::vector<size_t> > vertices_by_bound_faces;
        std::vector<std::vector<size_t> > sorted_vertices_by_faces;
#ifdef DEBUG
        if(n_vertices < 1) {
            throw "in 3D gmsh meshfile, nodes are defined after the elements.";
            return;
        }
#endif
        std::vector<std::vector<size_t> > edges_by_min_vertex(n_vertices - 1);
        std::vector<std::vector<size_t> > faces_by_min_vertex(n_vertices - 1);
        std::vector<size_t> local_v_by_c;

        for (size_t i=0; i<n_elements; i++) {
            // first value is the index of the element, avoid it
            // second value is the element type, third value is the nb of tags to be read
            *infile >> aux >> type >> nb_tags;
            for (size_t j=0; j<nb_tags; j++) { *infile >> tag; } // avoid tags

            if(type == 1) { // 2-node line (boundary edge)
                read_list(infile, 1, vertices_by_bound_edges, 2); 
            } else if(type == 2) { // 3-node triangle (boundary face)
                read_list(infile, 1, vertices_by_bound_faces, 3); 

                // get vertices of the face which has just been read
                std::vector<std::vector<size_t> > local_vertices_by_faces(1); // only one face in this element
                local_vertices_by_faces[0] = vertices_by_bound_faces[
                            vertices_by_bound_faces.size()-1];

                // extract the local edges
                std::vector<std::vector<size_t> > local_vertices_by_edges(3);
                std::vector<std::vector<size_t> > local_edges_by_faces(1); // only one face in this element
                local_edges_by_faces[0] = {0, 1, 2};
                for(size_t i = 0; i < 3; i++) {
                    local_vertices_by_edges[i] = 
                            {local_vertices_by_faces[0][i], local_vertices_by_faces[0][(i+1)%3]};
                    // store in increasing index of vertices (MUST BE DONE for build_edges_and_faces)
                    sort(local_vertices_by_edges[i].begin(), local_vertices_by_edges[i].end());  
                }
                
                if(build_edges) {
                    // Build faces_by_cells, vertices_by_faces,
                    // edges_by_faces and vertices_by_edges
                    // from local_vertices_by_faces, local_vertices_by_edges,
                    // vertices_by_faces (partially filled),
                    // edges_by_faces  (partially filled)
                    build_edges_and_faces(local_vertices_by_faces, local_vertices_by_edges, local_edges_by_faces,
                            edges_by_min_vertex, faces_by_min_vertex, sorted_vertices_by_faces,
                            vertices_by_faces, faces_by_cells, 
                            edges_by_faces, vertices_by_edges, true); // (is_boundary_face=true)
                } else {
                    // Build only faces : faces_by_cells, vertices_by_faces,
                    // from local_vertices_by_faces, and
                    // vertices_by_faces (partially filled),
                    build_faces(local_vertices_by_faces, faces_by_min_vertex, sorted_vertices_by_faces,
                        vertices_by_faces, faces_by_cells, true); // (is_boundary_face=true)
                } // are edges necessary ?

            } else if(type == 3) { // 4-node quadrangle (boundary face)
                read_list(infile, 1, vertices_by_bound_faces, 4);

                // get vertices of the face which has just been read
                std::vector<std::vector<size_t> > local_vertices_by_faces(1);
                local_vertices_by_faces[0] = vertices_by_bound_faces[
                            vertices_by_bound_faces.size()-1];

                // extract the local edges
                std::vector<std::vector<size_t> > local_vertices_by_edges(4);
                std::vector<std::vector<size_t> > local_edges_by_faces(1); // only one face in this element
                local_edges_by_faces[0] = {0, 1, 2, 3};
                for(size_t i = 0; i < 4; i++) {
                    local_vertices_by_edges[i] = 
                            {local_vertices_by_faces[0][i], local_vertices_by_faces[0][(i+1)%4]};
                    // store in increasing index of vertices (MUST BE DONE for build_edges_and_faces)
                    sort(local_vertices_by_edges[i].begin(), local_vertices_by_edges[i].end());  
                }

                if(build_edges) {
                    // Build faces_by_cells, vertices_by_faces,
                    // edges_by_faces and vertices_by_edges
                    // from local_vertices_by_faces, local_vertices_by_edges,
                    // vertices_by_faces (partially filled),
                    // edges_by_faces  (partially filled)
                    build_edges_and_faces(local_vertices_by_faces, local_vertices_by_edges, local_edges_by_faces,
                            edges_by_min_vertex, faces_by_min_vertex, sorted_vertices_by_faces,
                            vertices_by_faces, faces_by_cells, 
                            edges_by_faces, vertices_by_edges, true); // (is_boundary_face=true)
                } else {
                    // Build only faces : faces_by_cells, vertices_by_faces,
                    // from local_vertices_by_faces, and
                    // vertices_by_faces (partially filled),
                    build_faces(local_vertices_by_faces, faces_by_min_vertex, sorted_vertices_by_faces,
                        vertices_by_faces, faces_by_cells, true); // (is_boundary_face=true)
                } // are edges necessary ?


            } else if(type == 4) { // 4-node tetrahedron (cell)
                read_list(infile, 1, vertices_by_cells, 4); 
                // get vertices of the cell which has just been read
                local_v_by_c = vertices_by_cells[vertices_by_cells.size()-1];
                // extract the local faces and the local edges
                std::vector<std::vector<size_t> > local_vertices_by_faces(4); // 4 faces in this element
                std::vector<std::vector<size_t> > local_vertices_by_edges(6); // 6 edges in this element
                std::vector<std::vector<size_t> > local_edges_by_faces(4);

                local_vertices_by_edges[0] = {local_v_by_c[0], local_v_by_c[1]};
                local_vertices_by_edges[1] = {local_v_by_c[0], local_v_by_c[3]};
                local_vertices_by_edges[2] = {local_v_by_c[1], local_v_by_c[3]};
                local_vertices_by_edges[3] = {local_v_by_c[0], local_v_by_c[2]};
                local_vertices_by_edges[4] = {local_v_by_c[1], local_v_by_c[2]};
                local_vertices_by_edges[5] = {local_v_by_c[2], local_v_by_c[3]};
                // store in increasing index of vertices (MUST BE DONE for build_edges_and_faces)
                for(size_t i = 0, size = local_vertices_by_edges.size(); i < size; i++) {
                    sort(local_vertices_by_edges[i].begin(), local_vertices_by_edges[i].end());  
                }

                local_vertices_by_faces[0] = {local_v_by_c[0], local_v_by_c[1], local_v_by_c[2]};
                local_edges_by_faces[0] = {0, 3, 4};
                local_vertices_by_faces[1] = {local_v_by_c[0], local_v_by_c[1], local_v_by_c[3]};
                local_edges_by_faces[1] = {0, 1, 2};
                local_vertices_by_faces[2] = {local_v_by_c[0], local_v_by_c[2], local_v_by_c[3]};
                local_edges_by_faces[2] = {1, 3, 5};
                local_vertices_by_faces[3] = {local_v_by_c[1], local_v_by_c[2], local_v_by_c[3]};
                local_edges_by_faces[3] = {2, 4, 5};

                if(build_edges) {
                    // Build faces_by_cells, vertices_by_faces,
                    // edges_by_faces and vertices_by_edges
                    // from local_vertices_by_faces, local_vertices_by_edges,
                    // vertices_by_faces (partially filled),
                    // edges_by_faces  (partially filled)
                    build_edges_and_faces(local_vertices_by_faces, local_vertices_by_edges, local_edges_by_faces,
                            edges_by_min_vertex, faces_by_min_vertex, sorted_vertices_by_faces,
                            vertices_by_faces, faces_by_cells, 
                            edges_by_faces, vertices_by_edges);
                } else {
                    // Build only faces : faces_by_cells, vertices_by_faces,
                    // from local_vertices_by_faces, and
                    // vertices_by_faces (partially filled),
                    build_faces(local_vertices_by_faces, faces_by_min_vertex, sorted_vertices_by_faces,
                        vertices_by_faces, faces_by_cells);
                } // are edges necessary ?

            } else if(type == 5) { // 8-node hexahedron (cell)
                read_list(infile, 1, vertices_by_cells, 8); 
                // get vertices of the cell which has just been read
                local_v_by_c = vertices_by_cells[vertices_by_cells.size()-1];

                // extract the local faces and the local edges
                std::vector<std::vector<size_t> > local_vertices_by_faces(6); // 6 faces in this element
                std::vector<std::vector<size_t> > local_vertices_by_edges(12); // 12 edges in this element
                std::vector<std::vector<size_t> > local_edges_by_faces(6);

                local_vertices_by_edges[0] = {local_v_by_c[0], local_v_by_c[1]};
                local_vertices_by_edges[1] = {local_v_by_c[1], local_v_by_c[2]};
                local_vertices_by_edges[2] = {local_v_by_c[2], local_v_by_c[3]};
                local_vertices_by_edges[3] = {local_v_by_c[0], local_v_by_c[3]};
                local_vertices_by_edges[4] = {local_v_by_c[4], local_v_by_c[5]};
                local_vertices_by_edges[5] = {local_v_by_c[5], local_v_by_c[6]};
                local_vertices_by_edges[6] = {local_v_by_c[6], local_v_by_c[7]};
                local_vertices_by_edges[7] = {local_v_by_c[4], local_v_by_c[7]};
                local_vertices_by_edges[8] = {local_v_by_c[0], local_v_by_c[4]};
                local_vertices_by_edges[9] = {local_v_by_c[1], local_v_by_c[5]};
                local_vertices_by_edges[10] = {local_v_by_c[2], local_v_by_c[6]};
                local_vertices_by_edges[11] = {local_v_by_c[3], local_v_by_c[7]};
                // store in increasing index of vertices (MUST BE DONE for build_edges_and_faces)
                for(size_t i = 0, size = local_vertices_by_edges.size(); i < size; i++) {
                    sort(local_vertices_by_edges[i].begin(), local_vertices_by_edges[i].end());  
                }

                local_vertices_by_faces[0] = {local_v_by_c[0], local_v_by_c[1], local_v_by_c[2], local_v_by_c[3]};
                local_edges_by_faces[0] = {0, 1, 2, 3};
                local_vertices_by_faces[1] = {local_v_by_c[4], local_v_by_c[5], local_v_by_c[6], local_v_by_c[7]};
                local_edges_by_faces[1] = {4, 5, 6, 7};
                local_vertices_by_faces[2] = {local_v_by_c[0], local_v_by_c[4], local_v_by_c[5], local_v_by_c[1]};
                local_edges_by_faces[2] = {0, 8, 4, 9};
                local_vertices_by_faces[3] = {local_v_by_c[3], local_v_by_c[7], local_v_by_c[6], local_v_by_c[2]};
                local_edges_by_faces[3] = {2, 11, 6, 10};
                local_vertices_by_faces[4] = {local_v_by_c[0], local_v_by_c[4], local_v_by_c[7], local_v_by_c[3]};
                local_edges_by_faces[4] = {3, 8, 7, 11};
                local_vertices_by_faces[5] = {local_v_by_c[1], local_v_by_c[5], local_v_by_c[6], local_v_by_c[2]};
                local_edges_by_faces[5] = {1, 9, 5, 10};

                if(build_edges) {
                    // Build faces_by_cells, vertices_by_faces,
                    // edges_by_faces and vertices_by_edges
                    // from local_vertices_by_faces, local_vertices_by_edges,
                    // vertices_by_faces (partially filled),
                    // edges_by_faces  (partially filled)
                    build_edges_and_faces(local_vertices_by_faces, local_vertices_by_edges, local_edges_by_faces,
                            edges_by_min_vertex, faces_by_min_vertex, sorted_vertices_by_faces,
                            vertices_by_faces, faces_by_cells, 
                            edges_by_faces, vertices_by_edges);
                } else {
                    // Build only faces : faces_by_cells, vertices_by_faces,
                    // from local_vertices_by_faces, and
                    // vertices_by_faces (partially filled),
                    build_faces(local_vertices_by_faces, faces_by_min_vertex, sorted_vertices_by_faces,
                        vertices_by_faces, faces_by_cells);
                } // are edges necessary ?
            
            } else if(type == 6) { // 6-node prism (cell)
                read_list(infile, 1, vertices_by_cells, 6);
                // get vertices of the cell which has just been read
                local_v_by_c = vertices_by_cells[vertices_by_cells.size()-1];

                // extract the local faces and the local edges
                std::vector<std::vector<size_t> > local_vertices_by_faces(5); // 5 faces in this element
                std::vector<std::vector<size_t> > local_vertices_by_edges(9); // 9 edges in this element
                std::vector<std::vector<size_t> > local_edges_by_faces(5);

                local_vertices_by_edges[0] = {local_v_by_c[0], local_v_by_c[1]};
                local_vertices_by_edges[1] = {local_v_by_c[0], local_v_by_c[2]};
                local_vertices_by_edges[2] = {local_v_by_c[1], local_v_by_c[2]};
                local_vertices_by_edges[3] = {local_v_by_c[3], local_v_by_c[4]};
                local_vertices_by_edges[4] = {local_v_by_c[3], local_v_by_c[5]};
                local_vertices_by_edges[5] = {local_v_by_c[4], local_v_by_c[5]};
                local_vertices_by_edges[6] = {local_v_by_c[0], local_v_by_c[3]};
                local_vertices_by_edges[7] = {local_v_by_c[1], local_v_by_c[4]};
                local_vertices_by_edges[8] = {local_v_by_c[2], local_v_by_c[5]};
                // store in increasing index of vertices (MUST BE DONE for build_edges_and_faces)
                for(size_t i = 0, size = local_vertices_by_edges.size(); i < size; i++) {
                    sort(local_vertices_by_edges[i].begin(), local_vertices_by_edges[i].end());  
                }

                local_vertices_by_faces[0] = {local_v_by_c[0], local_v_by_c[1], local_v_by_c[2]};
                local_edges_by_faces[0] = {0, 1, 2};
                local_vertices_by_faces[1] = {local_v_by_c[3], local_v_by_c[4], local_v_by_c[5]};
                local_edges_by_faces[1] = {3, 4, 5};
                local_vertices_by_faces[2] = {local_v_by_c[0], local_v_by_c[3], local_v_by_c[4], local_v_by_c[1]};
                local_edges_by_faces[2] = {0, 6, 3, 7};
                local_vertices_by_faces[3] = {local_v_by_c[1], local_v_by_c[2], local_v_by_c[5], local_v_by_c[4]};
                local_edges_by_faces[3] = {2, 8, 5, 7};
                local_vertices_by_faces[4] = {local_v_by_c[0], local_v_by_c[2], local_v_by_c[5], local_v_by_c[3]};
                local_edges_by_faces[4] = {1, 8, 4, 6};

                if(build_edges) {
                    // Build faces_by_cells, vertices_by_faces,
                    // edges_by_faces and vertices_by_edges
                    // from local_vertices_by_faces, local_vertices_by_edges,
                    // vertices_by_faces (partially filled),
                    // edges_by_faces  (partially filled)
                    build_edges_and_faces(local_vertices_by_faces, local_vertices_by_edges, local_edges_by_faces,
                            edges_by_min_vertex, faces_by_min_vertex, sorted_vertices_by_faces,
                            vertices_by_faces, faces_by_cells, 
                            edges_by_faces, vertices_by_edges);
                } else {
                    // Build only faces : faces_by_cells, vertices_by_faces,
                    // from local_vertices_by_faces, and
                    // vertices_by_faces (partially filled),
                    build_faces(local_vertices_by_faces, faces_by_min_vertex, sorted_vertices_by_faces,
                        vertices_by_faces, faces_by_cells);
                } // are edges necessary ?

            } else if(type == 7) { // 5-node pyramid (cell)
                read_list(infile, 1, vertices_by_cells, 5); 
                // get vertices of the cell which has just been read
                local_v_by_c = vertices_by_cells[vertices_by_cells.size()-1];

                // extract the local faces and the local edges
                std::vector<std::vector<size_t> > local_vertices_by_faces(5); // 5 faces in this element
                std::vector<std::vector<size_t> > local_vertices_by_edges(8); // 8 edges in this element
                std::vector<std::vector<size_t> > local_edges_by_faces(5);

                local_vertices_by_edges[0] = {local_v_by_c[0], local_v_by_c[1]};
                local_vertices_by_edges[1] = {local_v_by_c[1], local_v_by_c[2]};
                local_vertices_by_edges[2] = {local_v_by_c[2], local_v_by_c[3]};
                local_vertices_by_edges[3] = {local_v_by_c[0], local_v_by_c[3]};
                local_vertices_by_edges[4] = {local_v_by_c[0], local_v_by_c[4]};
                local_vertices_by_edges[5] = {local_v_by_c[1], local_v_by_c[4]};
                local_vertices_by_edges[6] = {local_v_by_c[2], local_v_by_c[4]};
                local_vertices_by_edges[7] = {local_v_by_c[3], local_v_by_c[4]};
                // store in increasing index of vertices (MUST BE DONE for build_edges_and_faces)
                for(size_t i = 0, size = local_vertices_by_edges.size(); i < size; i++) {
                    sort(local_vertices_by_edges[i].begin(), local_vertices_by_edges[i].end());  
                }

                local_vertices_by_faces[0] = {local_v_by_c[0], local_v_by_c[1], local_v_by_c[4]};
                local_edges_by_faces[0] = {0, 4, 5};
                local_vertices_by_faces[1] = {local_v_by_c[1], local_v_by_c[2], local_v_by_c[4]};
                local_edges_by_faces[1] = {1, 6, 5};
                local_vertices_by_faces[2] = {local_v_by_c[2], local_v_by_c[3], local_v_by_c[4]};
                local_edges_by_faces[2] = {2, 6, 7};
                local_vertices_by_faces[3] = {local_v_by_c[3], local_v_by_c[0], local_v_by_c[4]};
                local_edges_by_faces[3] = {3, 4, 7};
                local_vertices_by_faces[4] = {local_v_by_c[0], local_v_by_c[1], local_v_by_c[2], local_v_by_c[3]};
                local_edges_by_faces[4] = {0, 1, 2, 3};

                if(build_edges) {
                    // Build faces_by_cells, vertices_by_faces,
                    // edges_by_faces and vertices_by_edges
                    // from local_vertices_by_faces, local_vertices_by_edges,
                    // vertices_by_faces (partially filled),
                    // edges_by_faces  (partially filled)
                    build_edges_and_faces(local_vertices_by_faces, local_vertices_by_edges, local_edges_by_faces,
                            edges_by_min_vertex, faces_by_min_vertex, sorted_vertices_by_faces,
                            vertices_by_faces, faces_by_cells, 
                            edges_by_faces, vertices_by_edges);
                } else {
                    // Build only faces : faces_by_cells, vertices_by_faces,
                    // from local_vertices_by_faces, and
                    // vertices_by_faces (partially filled),
                    build_faces(local_vertices_by_faces, faces_by_min_vertex, sorted_vertices_by_faces,
                        vertices_by_faces, faces_by_cells);
                } // are edges necessary ?

            } else if(type == 15) { // 1-node point (avoid it)
                *infile >> aux; 
            } else {
                throw "gmsh elm-type is not recognized.";
                return;
            }
        }
    } // read_3Dgmsh_list

    // called in 3D only
    static void build_edges_and_faces(const std::vector<std::vector<size_t> >& local_vertices_by_faces, 
                    const std::vector<std::vector<size_t> >& local_vertices_by_edges, 
                    const std::vector<std::vector<size_t> >& local_edges_by_faces,
                    std::vector<std::vector<size_t> >& edges_by_min_vertex, 
                    std::vector<std::vector<size_t> >& faces_by_min_vertex,
                    std::vector<std::vector<size_t> >& sorted_vertices_by_faces,
                    std::vector<std::vector<size_t> >& vertices_by_faces,
                    std::vector<std::vector<size_t> >& faces_by_cells,
                    std::vector<std::vector<size_t> >& edges_by_faces,
                    std::vector<std::vector<size_t> >& vertices_by_edges,
                    bool is_boundary_face) {

        size_t nb_local_faces(local_vertices_by_faces.size());
        std::vector<size_t> faces_by_cell;

        // for each local face ...
        for (size_t i = 0; i < nb_local_faces; i++) {
            bool found_face = false;

            // ... copy it to keep good vertices order ...
            std::vector<size_t> local_v_by_f(local_vertices_by_faces[i]);
            // ... sort the copy ...
            sort(local_v_by_f.begin(), local_v_by_f.end()); 

            // ... check if the face has already been stored by comparing with the faces 
            // which have the same min vertex index
            for(auto& j : faces_by_min_vertex[local_v_by_f[0]]) {

                // check if the vertices of the face is equal to sorted_vertices_by_faces[j]
                if(local_v_by_f == sorted_vertices_by_faces[j]) {
                    found_face = true;
                    // local face i is face with global index j
                    faces_by_cell.push_back(j);
                    // edges_by_faces and vertices_by_edges are 
                    // up to date wrt the edges of this face
                    break;
                }
            }
            // local face has not been found in sorted_vertices_by_faces, new face
            if(!found_face) {
                // keep sorted and original list of vertices
                size_t g_i = vertices_by_faces.size(); // global index of new face
                sorted_vertices_by_faces.push_back(local_v_by_f);
                vertices_by_faces.push_back(local_vertices_by_faces[i]);
                faces_by_min_vertex[local_v_by_f[0]].push_back(g_i);
                faces_by_cell.push_back(g_i);

                // ---------------------------------------
                // deal with the edges of the local face i
                // face did not exist so the list of its edges neither
                // but each edge may already exist
                size_t nb_local_edges(local_edges_by_faces[i].size());
                std::vector<size_t> edges_by_face;

                // for each local edge ...
                for (size_t e = 0; e < nb_local_edges; e++) {
                    // vertices global index of this edge (sorted)
                    std::vector<size_t> local_v_by_e(
                                local_vertices_by_edges[local_edges_by_faces[i][e]]);

                    // ... check if the edge has already been stored by comparing with the edges 
                    // which have the same min vertex index
                    bool found_edge = false;
                    for(auto& j : edges_by_min_vertex[local_v_by_e[0]]) {
                        // check if local_v_by_e is equal to vertices_by_edges[j]
                        if( local_v_by_e == vertices_by_edges[j]) {
                            found_edge = true;
                            // edge e is edge with global index j
                            edges_by_face.push_back(j);
                            break;
                        }
                    }
                    // local edge has not been found in vertices_by_edges, new edge
                    if(!found_edge) {
                        // keep list of vertices
                        size_t g_i = vertices_by_edges.size(); // global index of new edge
                        vertices_by_edges.push_back(local_v_by_e);
                        edges_by_face.push_back(g_i);
                        edges_by_min_vertex[local_v_by_e[0]].push_back(g_i);
                    }
                } // loop on local edges

                // face did not exist, so add its list of edges
                edges_by_faces.push_back(edges_by_face);

            } // face did not exist
        } // loop on local faces

        // add list of face to the new cell 
        // (if is_boundary_face: deals only with one face and not a cell)
        if(!is_boundary_face) faces_by_cells.push_back(faces_by_cell);

    } // build_edges_and_faces


    // called in 3D only (when edges are not build)
    static void build_faces(const std::vector<std::vector<size_t> >& local_vertices_by_faces, 
                    std::vector<std::vector<size_t> >& faces_by_min_vertex,
                    std::vector<std::vector<size_t> >& sorted_vertices_by_faces,
                    std::vector<std::vector<size_t> >& vertices_by_faces,
                    std::vector<std::vector<size_t> >& faces_by_cells,
                    bool is_boundary_face) {

        size_t nb_local_faces(local_vertices_by_faces.size());
        std::vector<size_t> faces_by_cell;

        // for each local face ...
        for (size_t i = 0; i < nb_local_faces; i++) {
            bool found_face = false;

            // ... copy it to keep good vertices order ...
            std::vector<size_t> local_v_by_f(local_vertices_by_faces[i]);
            // ... sort the copy ...
            sort(local_v_by_f.begin(), local_v_by_f.end()); 

            // ... check if the face has already been stored by comparing with the faces 
            // which have the same min vertex index
            for(auto& j : faces_by_min_vertex[local_v_by_f[0]]) {

                // check if the vertices of the face is equal to sorted_vertices_by_faces[j]
                if(local_v_by_f == sorted_vertices_by_faces[j]) {
                    found_face = true;
                    // local face i is face with global index j
                    faces_by_cell.push_back(j);
                    break;
                }
            }
            // local face has not been found in sorted_vertices_by_faces, new face
            if(!found_face) {
                // keep sorted and original list of vertices
                size_t g_i = vertices_by_faces.size(); // global index of new face
                sorted_vertices_by_faces.push_back(local_v_by_f);
                vertices_by_faces.push_back(local_vertices_by_faces[i]);
                faces_by_min_vertex[local_v_by_f[0]].push_back(g_i);
                faces_by_cell.push_back(g_i);
            } // face did not exist
        } // loop on local faces

        // add list of face to the new cell 
        // (if is_boundary_face: deals only with one face and not a cell)
        if(!is_boundary_face) faces_by_cells.push_back(faces_by_cell);

    } // build_faces

} // MyMesh namespace
#endif /* MESH_READER_GMSH_HPP */
