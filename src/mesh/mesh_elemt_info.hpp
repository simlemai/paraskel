#ifndef MESH_ELEMT_INFO_HPP
#define MESH_ELEMT_INFO_HPP

#include <Eigen/Dense>

namespace MyMesh { // forward declaration of template classes
    template<size_t DIM>
    class Mesh;
}

namespace MyMesh { 
        
    /**
     * The class MeshElemtInfo provides the informations 
     * commun for all elements (cell, face, (edge in 3D), vertex)
     * such as its global_index, is measure,...
    */
    template<size_t DIM>
    class MeshElemtInfo
    {
    public:
        /**
         * Constructor
         * @param mesh pointer to the mesh
         * @param g_i global index of the object
         *          */
        MeshElemtInfo(Mesh<DIM>* mesh, size_t g_i);
        MeshElemtInfo();    ///< Constructor
        virtual ~MeshElemtInfo() = default;   ///<  Default destructor
        /**
         * operator = overloading
         * @param other is the MeshElemtInfo to copy 
         */
        MeshElemtInfo& operator=(const MeshElemtInfo& other);

        /**
         * Get MeshElemtInfo informations
         */
        inline size_t global_index() const;
        inline bool is_boundary() const;
        inline double measure() const;
        inline double diam() const;
        inline Eigen::ArrayXd mass_center() const;
        inline int discrete_unknown_global_index(size_t var_index = 0) const;
        inline std::vector<bool> is_eliminated() const;
        inline int bc_type(int i) const;
        /**
         * Set the boundary booleans
         * @param b value of is_boundary
         */
        inline void set_boundary(bool const b);
        /**
         * Set the boolean "is_eliminated"
         * @param b value of is_eliminated
         */
        inline void set_is_eliminated(const std::vector<bool>& b);
        /**
         * Set the type of boundary of the element, 
         * from a vector containing one int by variable
         * @param b vector with the values of bc_type
         */
        inline void set_bc_type(const std::vector<int>& b);
        /**
         * Set the type of boundary of the element of only one variable
         * @param b value of bc_type
         * @param var_index index of the variable to set
         */
        inline void set_bc_type(int b, size_t var_index);
        /**
         * Set the measure of the mesh element
         * @param m value of measure 
         */
        inline void set_measure(double const m);
        /**
         * Set the diameter of the mesh element
         * @param d value of diam
         */
        inline void set_diam(double const d);
        /**
         * Set the mass center of the mesh element with a vector
         * @param pt vector value of mass center
         */
        inline void set_mass_center(const Eigen::ArrayXd& pt);
        /**
         * Set the mass center of the mesh element with DIM=2 coordinates
         * @param x is the first coordinate
         * @param y is the second coordinate
         */
        template<typename U>
        inline void set_mass_center(const typename std::enable_if<DIM == 2, U>::type& x, const U& y);
        /**
         * Set the mass center of the mesh element with DIM=3 coordinates
         * @param x is the first coordinate
         * @param y is the second coordinate
         * @param z is the third coordinate
         */
        template<typename U>
        inline void set_mass_center(const typename std::enable_if<DIM == 3, U>::type& x, const U& y, const U& z);

        inline void set_discrete_unknown_global_index(size_t var_index, int discr_unknown_gi);
        inline void set_discrete_unknown_global_index(const std::vector<int>& discr_unknown_gi);

    protected:
        Mesh<DIM>* m_mesh;      ///< pointer to the mesh containing this element 
        size_t m_global_index;  ///< global index of the element
        bool m_is_boundary;     ///< does the element belong to the boundary?
        double m_measure;       ///< measure of the element (length, surface, volume)
        double m_diam;          ///< diameter of the element (longest distance between two nodes)
        Eigen::ArrayXd m_mass_center;  ///< mass center of the element
        // Follows informations which are more related to MySolver, but useful to store them here.
        std::vector<int> m_discrete_unknown_global_index;  ///< global index of the first dof of the discrete unknown for each variable (vector of size n_variables) 
        std::vector<bool> m_is_eliminated;   ///< is this unknown eliminated ? (vector of size n_variables) 
        std::vector<int> m_bc_type;   ///< what type of BC is this element ? (vector of size n_variables) 
    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////
    template<size_t DIM>
    MeshElemtInfo<DIM>::MeshElemtInfo(Mesh<DIM>* mesh, size_t g_i) : 
    m_mesh(mesh),
    m_global_index(g_i), 
    m_is_boundary(false),
    m_measure(-1), 
    m_diam(-1), 
    m_mass_center(DIM)
    // m_discrete_unknown_global_index, m_is_eliminated and m_bc_type cannot 
    // be initialized because the size (n_variables) is unknown yet
    {
        m_mass_center.setZero();
    }

    template<size_t DIM>
    inline size_t MeshElemtInfo<DIM>::global_index() const {return m_global_index;}

    template<size_t DIM>
    inline bool MeshElemtInfo<DIM>::is_boundary() const {return m_is_boundary;}

    template<size_t DIM>
    inline double MeshElemtInfo<DIM>::measure() const {return m_measure;}

    template<size_t DIM>
    inline double MeshElemtInfo<DIM>::diam() const {return m_diam;}

    template<size_t DIM>
    inline Eigen::ArrayXd MeshElemtInfo<DIM>::mass_center() const {return m_mass_center;}

    template<size_t DIM>
    inline void MeshElemtInfo<DIM>::set_boundary(bool const b) {m_is_boundary = b;}

    template<size_t DIM>
    inline void MeshElemtInfo<DIM>::set_is_eliminated(const std::vector<bool>& b) {m_is_eliminated = b;}

    template<size_t DIM>
    inline void MeshElemtInfo<DIM>::set_bc_type(const std::vector<int>& b) {m_bc_type = b;}

    template<size_t DIM>
    inline void MeshElemtInfo<DIM>::set_bc_type(int b, size_t var_index) {m_bc_type[var_index] = b;}

    template<size_t DIM>
    inline void MeshElemtInfo<DIM>::set_measure(double const m) {m_measure = m;}

    template<size_t DIM>
    inline void MeshElemtInfo<DIM>::set_diam(double const d) {m_diam = d;}

    template<size_t DIM>
    inline void MeshElemtInfo<DIM>::set_mass_center(const Eigen::ArrayXd& pt) {m_mass_center = pt;}

    template<size_t DIM>
    template<typename U>
    inline void MeshElemtInfo<DIM>::set_mass_center(const typename std::enable_if<DIM == 2, U>::type& x, const U& y) 
    {m_mass_center << x, y;}

    template<size_t DIM>
    template<typename U>
    inline void MeshElemtInfo<DIM>::set_mass_center(const typename std::enable_if<DIM == 3, U>::type& x, const U& y, const U& z) 
    {m_mass_center << x, y, z;}

    template<size_t DIM>
    inline int MeshElemtInfo<DIM>::discrete_unknown_global_index(size_t var_index) const 
    {return m_discrete_unknown_global_index[var_index];}

    template<size_t DIM>
    inline std::vector<bool> MeshElemtInfo<DIM>::is_eliminated() const {return m_is_eliminated;}

    template<size_t DIM>
    inline int MeshElemtInfo<DIM>::bc_type(int i) const {return m_bc_type[i];}

    template<size_t DIM>
    inline void MeshElemtInfo<DIM>::set_discrete_unknown_global_index(size_t var_index, int discr_unknown_gi)
    {m_discrete_unknown_global_index[var_index] = discr_unknown_gi;}

    template<size_t DIM>
    inline void MeshElemtInfo<DIM>::set_discrete_unknown_global_index(const std::vector<int>& discr_unknown_gi)
    {m_discrete_unknown_global_index = discr_unknown_gi;}

} // MyMesh namespace
#endif /* Mesh_elemt_info_HPP */
