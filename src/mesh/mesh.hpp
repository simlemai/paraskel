#ifndef MESH_HPP
#define MESH_HPP

#include <iostream>
#include <string>
#include <vector>
#include <Eigen/Dense>

namespace MyMesh { // forward declaration of template classes
    template<size_t DIM>
    class Cell;
    template<size_t DIM>
    class Face;
    template<size_t DIM>
    class Edge;
    template<size_t DIM>
    class Vertex;
}
namespace MyMesh {
    /**
     * The class Mesh provides a description of the mesh. 
    */
    template<size_t DIM>
    class Mesh {
    public:
        Mesh() = default;    ///<  Default constructor
        ~Mesh();             ///<  Destructor (delete pointers)

        inline std::string get_name() const;   ///< get the mesh name
        inline size_t n_cells() const;         ///< number of cells in the mesh
        inline size_t n_faces() const;         ///< number of faces in the mesh
        inline size_t n_edges() const;         ///< number of edges in the mesh
        inline size_t n_vertices() const;      ///< number of vertices in the mesh
        inline size_t n_bound_cells() const;   ///< number of boundary cells
        inline size_t n_bound_faces() const;   ///< number of boundary faces
        inline size_t n_bound_edges() const;   ///< number of boundary edges
        inline size_t n_bound_vertices() const;  ///< number of boundary vertices
        inline size_t n_int_cells() const;     ///< number of boundary cells
        inline size_t n_int_faces() const;     ///< number of boundary faces
        inline size_t n_int_edges() const;     ///< number of boundary edges
        inline size_t n_int_vertices() const;  ///< number of boundary vertices
        inline double measure() const;         ///< sum of the measure of all cells in mesh
        inline double h_min() const;           ///< min of diameters of cells
        inline double h_max() const;           ///< max of diameters of cells
        inline double h_mean() const;          ///< mean value of diameters of cells
        inline double min_anisotropy() const;  ///< min value of anisotropy of cells
        inline double max_anisotropy() const;  ///< max value of anisotropy of cells

        inline const std::vector<Cell<DIM>*>& get_cells() const;       ///< list of the cells in the mesh
        inline const std::vector<Face<DIM>*>& get_faces() const;       ///< list of the faces in the mesh
        inline const std::vector<Edge<DIM>*>& get_edges() const;       ///< list of the edges in the mesh
        inline const std::vector<Vertex<DIM>*>& get_vertices() const;  ///< list of the vertices in the mesh
        inline const std::vector<Cell<DIM>*>& get_bound_cells() const; ///< list of the boundary cells in the mesh
        inline const std::vector<Face<DIM>*>& get_bound_faces() const; ///< list of the boundary faces in the mesh
        inline const std::vector<Edge<DIM>*>& get_bound_edges() const; ///< list of the boundary edges in the mesh
        inline const std::vector<Vertex<DIM>*>& get_bound_vertices() const;  ///< list of the boundary vertices in the mesh
        inline const std::vector<Cell<DIM>*>& get_int_cells() const;   ///< list of the interior cells in the mesh
        inline const std::vector<Face<DIM>*>& get_int_faces() const;   ///< list of the interior faces in the mesh
        inline const std::vector<Edge<DIM>*>& get_int_edges() const;   ///< list of the interior edges in the mesh
        inline const std::vector<Vertex<DIM>*>& get_int_vertices() const;    ///< list of the interior vertices in the mesh

        inline Cell<DIM>* cell(size_t g_i) const;       ///< get a pointer to a cell using its global index
        inline Face<DIM>* face(size_t g_i) const;       ///< get a pointer to a face using its global index
        inline Edge<DIM>* edge(size_t g_i) const;       ///< get a pointer to an edge using its global index
        inline Vertex<DIM>* vertex(size_t g_i) const;   ///< get a pointer to a vertex using its global index
        inline Cell<DIM>* bound_cell(size_t iC) const;  ///< get a pointer to the iC-th boundary cell
        inline Face<DIM>* bound_face(size_t iF) const;  ///< get a pointer to the iF-th boundary face
        inline Edge<DIM>* bound_edge(size_t iE) const;  ///< get a pointer to the iE-th boundary edge
        inline Vertex<DIM>* bound_vertex(size_t iV) const;   ///< get a pointer to the iV-th boundary vertex
        inline Cell<DIM>* int_cell(size_t iC) const;    ///< get a pointer to the iC-th interior cell
        inline Face<DIM>* int_face(size_t iF) const;    ///< get a pointer to the iF-th interior face
        inline Edge<DIM>* int_edge(size_t iE) const;    ///< get a pointer to the iE-th interior edge
        inline Vertex<DIM>* int_vertex(size_t iV) const;     ///< get a pointer to the iV-th interior vertex

        inline void set_name(std::string name);    ///< set a name to the mesh
        inline void set_measure();          ///< set the domain measure (surface in 2D, volume in 3D)
        inline void set_min_diam(const double h_min);    ///< set the min diameter of mesh (min diam of all cells)
        inline void set_max_diam(const double h_max);    ///< set the max diameter of mesh (max diam of all cells)
        inline void set_mean_diam(const double h_mean);    ///< set the mean value of all diameters of the mesh
        inline void set_min_anisotropy(const double min_anisotropy);    ///< set the min value of the anisotropy (diam/smallest edge) of all cells
        inline void set_max_anisotropy(const double max_anisotropy);    ///< set the max value of the anisotropy (diam/smallest edge) of all cells
        inline void add_cell(Cell<DIM>* cell);     ///< add a cell to the list of cells
        inline void add_face(Face<DIM>* face);     ///< add a face to the list of faces
        inline void add_edge(Edge<DIM>* edge);     ///< add an edge to the list of edges
        inline void add_vertex(Vertex<DIM>* vertex);  ///< add a vertex to the list of vertices
        inline void add_bound_cell(Cell<DIM>* cell);     ///< add a cell to the list of boundary cells
        inline void add_bound_face(Face<DIM>* face);     ///< add a face to the list of boundary faces
        inline void add_bound_edge(Edge<DIM>* edge);     ///< add an edge to the list of boundary edges
        inline void add_bound_vertex(Vertex<DIM>* vertex);  ///< add a vertex to the list of boundary vertices
        inline void add_int_cell(Cell<DIM>* cell);     ///< add a cell to the list of interior cells
        inline void add_int_face(Face<DIM>* face);     ///< add a face to the list of interior faces
        inline void add_int_edge(Edge<DIM>* edge);     ///< add an edge to the list of interior edges
        inline void add_int_vertex(Vertex<DIM>* vertex);  ///< add a vertex to the list of interior vertices

        inline void find_domain_extremities();   ///< extract dimensions of the mesh (xmin, xmax, ...)
        inline const std::vector<double>& min_domain_extremities() const;   ///< get min dimensions of the mesh (xmin, ymin, ...)
        inline const std::vector<double>& max_domain_extremities() const;   ///< get max dimensions of the mesh (xmax, ymax, ...)
        inline void deduce_boundaries_from_connectivity();  ///< deduce boundary elements from face which have only one neighbouring cell

    private:
        std::string m_mesh_name;
        std::vector<Cell<DIM>*> m_cells;         ///< list of cells' pointers
        std::vector<Face<DIM>*> m_faces;         ///< list of faces' pointers
        std::vector<Edge<DIM>*> m_edges;         ///< list of edges' pointers
        std::vector<Vertex<DIM>*> m_vertices;    ///< list of vertices' pointers
        std::vector<Cell<DIM>*> m_bound_cells;   ///< list of cells which contains at least one boundary element in its elements
        std::vector<Face<DIM>*> m_bound_faces;   ///< list of boundary faces' pointers
        std::vector<Edge<DIM>*> m_bound_edges;   ///< list of boundary edges' pointers
        std::vector<Vertex<DIM>*> m_bound_vertices;   ///< list of boundary vertices' pointers
        std::vector<Cell<DIM>*> m_int_cells;     ///< list of interior cells' pointers
        std::vector<Face<DIM>*> m_int_faces;     ///< list of interior faces' pointers
        std::vector<Edge<DIM>*> m_int_edges;     ///< list of interior edges' pointers
        std::vector<Vertex<DIM>*> m_int_vertices;     ///< list of interior vertices' pointers
        double m_measure;                     ///< surface or volume of the global geometry (sum over all cells of there measure)
        double m_h_min;                     ///< min diameter of the cells
        double m_h_max;                     ///< max diameter of the cells
        double m_h_mean;                    ///< mean value of the diameters of the cells
        double m_min_anisotropy;            ///< min value of the anisotropy of the cells (diam/smallest edge)
        double m_max_anisotropy;            ///< max value of the anisotropy of the cells (diam/smallest edge)
        std::vector<double> m_mesh_min;     ///< dimension of the geometry : min extremities of the mesh
        std::vector<double> m_mesh_max;     ///< dimension of the geometry : max extremities of the mesh
    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////
    template<size_t DIM>
    Mesh<DIM>::~Mesh() {
            for(auto& cell : m_cells) { delete cell; cell = 0; }
            for(auto& face : m_faces) { delete face; face = 0; }
            for(auto& edge : m_edges) { delete edge; edge = 0; }
            for(auto& vertex : m_vertices) { delete vertex; vertex = 0; }
    }

    template<size_t DIM>
    inline std::string Mesh<DIM>::get_name() const {return m_mesh_name;}

    template<size_t DIM>
    inline size_t Mesh<DIM>::n_cells() const {return m_cells.size();}
    template<size_t DIM>
    inline size_t Mesh<DIM>::n_faces() const {return m_faces.size();}
    template<size_t DIM>
    inline size_t Mesh<DIM>::n_edges() const {return m_edges.size();}
    template<size_t DIM>
    inline size_t Mesh<DIM>::n_vertices() const {return m_vertices.size();}
    template<size_t DIM>
    inline size_t Mesh<DIM>::n_bound_cells() const {return m_bound_cells.size();}
    template<size_t DIM>
    inline size_t Mesh<DIM>::n_bound_faces() const {return m_bound_faces.size();}
    template<size_t DIM>
    inline size_t Mesh<DIM>::n_bound_edges() const {return m_bound_edges.size();}
    template<size_t DIM>
    inline size_t Mesh<DIM>::n_bound_vertices() const {return m_bound_vertices.size();}
    template<size_t DIM>
    inline size_t Mesh<DIM>::n_int_cells() const {return m_int_cells.size();}
    template<size_t DIM>
    inline size_t Mesh<DIM>::n_int_faces() const {return m_int_faces.size();}
    template<size_t DIM>
    inline size_t Mesh<DIM>::n_int_edges() const {return m_int_edges.size();}
    template<size_t DIM>
    inline size_t Mesh<DIM>::n_int_vertices() const {return m_int_vertices.size();}
    template<size_t DIM>
    inline double Mesh<DIM>::measure() const { return m_measure; }
    template<size_t DIM>
    inline double Mesh<DIM>::h_min() const { return m_h_min; }
    template<size_t DIM>
    inline double Mesh<DIM>::h_max() const { return m_h_max; }
    template<size_t DIM>
    inline double Mesh<DIM>::h_mean() const { return m_h_mean; }
    template<size_t DIM>
    inline double Mesh<DIM>::min_anisotropy() const { return m_min_anisotropy; }
    template<size_t DIM>
    inline double Mesh<DIM>::max_anisotropy() const { return m_max_anisotropy; }

    template<size_t DIM>
    inline const std::vector<Cell<DIM>*>& Mesh<DIM>::get_cells() const {return m_cells;}
    template<size_t DIM>
    inline const std::vector<Face<DIM>*>& Mesh<DIM>::get_faces() const {return m_faces;}
    template<size_t DIM>
    inline const std::vector<Edge<DIM>*>& Mesh<DIM>::get_edges() const {return m_edges;}
    template<size_t DIM>
    inline const std::vector<Vertex<DIM>*>& Mesh<DIM>::get_vertices() const {return m_vertices;}
    template<size_t DIM>
    inline const std::vector<Cell<DIM>*>& Mesh<DIM>::get_bound_cells() const {return m_bound_cells;}
    template<size_t DIM>
    inline const std::vector<Face<DIM>*>& Mesh<DIM>::get_bound_faces() const {return m_bound_faces;}
    template<size_t DIM>
    inline const std::vector<Edge<DIM>*>& Mesh<DIM>::get_bound_edges() const {return m_bound_edges;}
    template<size_t DIM>
    inline const std::vector<Vertex<DIM>*>& Mesh<DIM>::get_bound_vertices() const {return m_bound_vertices;}
    template<size_t DIM>
    inline const std::vector<Cell<DIM>*>& Mesh<DIM>::get_int_cells() const {return m_int_cells;}
    template<size_t DIM>
    inline const std::vector<Face<DIM>*>& Mesh<DIM>::get_int_faces() const {return m_int_faces;}
    template<size_t DIM>
    inline const std::vector<Edge<DIM>*>& Mesh<DIM>::get_int_edges() const {return m_int_edges;}
    template<size_t DIM>
    inline const std::vector<Vertex<DIM>*>& Mesh<DIM>::get_int_vertices() const {return m_int_vertices;}
    
    template<size_t DIM>
    inline Cell<DIM>* Mesh<DIM>::cell(size_t g_i) const {
#ifdef DEBUG
        return m_cells.at(g_i);
#else
        return m_cells[g_i];
#endif
    }
    template<size_t DIM>
    inline Face<DIM>* Mesh<DIM>::face(size_t g_i) const {
#ifdef DEBUG
        return m_faces.at(g_i);
#else
        return m_faces[g_i];
#endif
    }
    template<size_t DIM>
    inline Edge<DIM>* Mesh<DIM>::edge(size_t g_i) const {
#ifdef DEBUG
        return m_edges.at(g_i);
#else
        return m_edges[g_i];
#endif
    }
    template<size_t DIM>
    inline Vertex<DIM>* Mesh<DIM>::vertex(size_t g_i) const {
#ifdef DEBUG
        return m_vertices.at(g_i);
#else
        return m_vertices[g_i];
#endif
    }
    template<size_t DIM>
    inline Cell<DIM>* Mesh<DIM>::bound_cell(size_t g_i) const {
#ifdef DEBUG
        return m_bound_cells.at(g_i);
#else
        return m_bound_cells[g_i];
#endif
    }
    template<size_t DIM>
    inline Face<DIM>* Mesh<DIM>::bound_face(size_t g_i) const {
#ifdef DEBUG
        return m_bound_faces.at(g_i);
#else
        return m_bound_faces[g_i];
#endif
    }
    template<size_t DIM>
    inline Edge<DIM>* Mesh<DIM>::bound_edge(size_t g_i) const {
#ifdef DEBUG
        return m_bound_edges.at(g_i);
#else
        return m_bound_edges[g_i];
#endif
    }
    template<size_t DIM>
    inline Vertex<DIM>* Mesh<DIM>::bound_vertex(size_t g_i) const {
#ifdef DEBUG
        return m_bound_vertices.at(g_i);
#else
        return m_bound_vertices[g_i];
#endif
    }
    template<size_t DIM>
    inline Cell<DIM>* Mesh<DIM>::int_cell(size_t g_i) const {
#ifdef DEBUG
        return m_int_cells.at(g_i);
#else
        return m_int_cells[g_i];
#endif
    }
    template<size_t DIM>
    inline Face<DIM>* Mesh<DIM>::int_face(size_t g_i) const {
#ifdef DEBUG
        return m_int_faces.at(g_i);
#else
        return m_int_faces[g_i];
#endif
    }
    template<size_t DIM>
    inline Edge<DIM>* Mesh<DIM>::int_edge(size_t g_i) const {
#ifdef DEBUG
        return m_int_edges.at(g_i);
#else
        return m_int_edges[g_i];
#endif
    }
    template<size_t DIM>
    inline Vertex<DIM>* Mesh<DIM>::int_vertex(size_t g_i) const {
#ifdef DEBUG
        return m_int_vertices.at(g_i);
#else
        return m_int_vertices[g_i];
#endif
    }

    template<size_t DIM>
    inline void Mesh<DIM>::set_name(std::string name) {m_mesh_name = name;}
    template<size_t DIM>
    inline void Mesh<DIM>::set_measure() {
        m_measure = 0;
        for(auto& c : m_cells) {
            m_measure += c->measure();
        }
    }
    template<size_t DIM>
    inline void Mesh<DIM>::set_min_diam(const double h_min) {m_h_min = h_min;}
    template<size_t DIM>
    inline void Mesh<DIM>::set_max_diam(const double h_max) {m_h_max = h_max;}
    template<size_t DIM>
    inline void Mesh<DIM>::set_mean_diam(const double h_mean) {m_h_mean = h_mean;}
    template<size_t DIM>
    inline void Mesh<DIM>::set_min_anisotropy(const double min_anisotropy) {m_min_anisotropy = min_anisotropy;}
    template<size_t DIM>
    inline void Mesh<DIM>::set_max_anisotropy(const double max_anisotropy) {m_max_anisotropy = max_anisotropy;}

    template<size_t DIM>
    inline void Mesh<DIM>::add_cell(Cell<DIM>* cell) {m_cells.push_back(cell);}
    template<size_t DIM>
    inline void Mesh<DIM>::add_face(Face<DIM>* face) {m_faces.push_back(face);}
    template<size_t DIM>
    inline void Mesh<DIM>::add_edge(Edge<DIM>* edge) {m_edges.push_back(edge);}
    template<size_t DIM>
    inline void Mesh<DIM>::add_vertex(Vertex<DIM>* vertex) {m_vertices.push_back(vertex);}
    template<size_t DIM>
    inline void Mesh<DIM>::add_bound_cell(Cell<DIM>* cell) {m_bound_cells.push_back(cell);}
    template<size_t DIM>
    inline void Mesh<DIM>::add_bound_face(Face<DIM>* face) {m_bound_faces.push_back(face);}
    template<size_t DIM>
    inline void Mesh<DIM>::add_bound_edge(Edge<DIM>* edge) {m_bound_edges.push_back(edge);}
    template<size_t DIM>
    inline void Mesh<DIM>::add_bound_vertex(Vertex<DIM>* vertex) {m_bound_vertices.push_back(vertex);}
    template<size_t DIM>
    inline void Mesh<DIM>::add_int_cell(Cell<DIM>* cell) {m_int_cells.push_back(cell);}
    template<size_t DIM>
    inline void Mesh<DIM>::add_int_face(Face<DIM>* face) {m_int_faces.push_back(face);}
    template<size_t DIM>
    inline void Mesh<DIM>::add_int_edge(Edge<DIM>* edge) {m_int_edges.push_back(edge);}
    template<size_t DIM>
    inline void Mesh<DIM>::add_int_vertex(Vertex<DIM>* vertex) {m_int_vertices.push_back(vertex);}
    
    template<size_t DIM>
    inline void Mesh<DIM>::find_domain_extremities() {
        std::vector<double> mesh_min(DIM, std::numeric_limits<double>::max());
        std::vector<double> mesh_max(DIM, std::numeric_limits<double>::min());

        for(auto& v : m_vertices) {
            for(int i=0; i<DIM; i++) {
                mesh_min[i] = std::min(mesh_min[i], v->coords()[i]);
                mesh_max[i] = std::max(mesh_max[i], v->coords()[i]);
            }
        }
        m_mesh_min = mesh_min; 
        m_mesh_max = mesh_max; 
    } // find_domain_extremities

    template<size_t DIM>
    inline const std::vector<double>& Mesh<DIM>::min_domain_extremities() const { return m_mesh_min; }
    
    template<size_t DIM>
    inline const std::vector<double>& Mesh<DIM>::max_domain_extremities() const { return m_mesh_max; }

    template<size_t DIM>
    inline void Mesh<DIM>::deduce_boundaries_from_connectivity() {
        // deduce skeletal boundary elements based on the faces
        // which have only 1 neighbouring cell
        for(auto& f : m_faces) {
            if( f->n_neighbour_cells()==1 ) {
                
                // set f as BC
                f->set_boundary(true);
                m_bound_faces.push_back(f);

                if(DIM == 3) {  // not necessary but optimize compiler
                    // set all edges of face as BC (except if already stored)
                    for(auto& e : f->get_edges()) {
                        if(!e->is_boundary()) {
                            e->set_boundary(true);
                            m_bound_edges.push_back(e);
                        }
                    }
                }

                // set all vertices of face as BC (except if already stored)
                for(auto& v : f->get_vertices()) {
                    if(!v->is_boundary()) {
                        v->set_boundary(true);
                        m_bound_vertices.push_back(v);
                    }
                }
            } // if BC face
        } // for face loop

        // label as bound_cells all cells which have at least one boundary vertex
        for(auto& v : m_bound_vertices) {
            // set all neighbouring cells as BC (except if already stored)
            for(auto& c : v->get_neighbour_cells()) {
                if(!c->is_boundary()) {
                    c->set_boundary(true);
                    m_bound_cells.push_back(c);
                }
            }
        }
        return;
    } // deduce_boundaries_from_connectivity

} // MyMesh namespace
#endif /* MESH_HPP */
