#ifndef CELL_HPP
#define CELL_HPP

#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include "mesh_elemt_info.hpp"

namespace MyMesh { // forward declaration of template classes
    template<size_t DIM>
    class Face;
    template<size_t DIM>
    class Edge;
    template<size_t DIM>
    class Vertex;}
namespace MyMesh {
    /**
     * The class Cell provides every specific informations about the cells. 
    */
    template<size_t DIM>
    class Cell : public MeshElemtInfo<DIM>
    {
    public:
        /**
         * Constructor
         * @param mesh pointer to the mesh
         * @param g_i global index of the cell
         */
        Cell(Mesh<DIM>* mesh, size_t g_i);
        ~Cell() = default;   ///<  Default destructor

        // concerning the faces of the cell
        inline size_t n_faces() const;          ///<  returns the number of faces of the cell
        /**
         * add a face to the cell list
         * @param face pointer to a face
         */
        inline void add_face(Face<DIM>* face);
        inline std::vector<Face<DIM> *> get_faces() const;  ///<  returns the list of the faces' pointer of the cell

        // concerning the edges of the cell
        inline size_t n_edges() const;          ///<  returns the number of edges of the cell
        /**
         * add an edge to the cell list
         * @param edge pointer to an edge
         */
        inline void add_edge(Edge<DIM>* edge);
        inline std::vector<Edge<DIM> *> get_edges() const;  ///<  returns the list of the edges' pointer of the cell

        // concerning the vertices of the cell
        inline size_t n_vertices() const;         ///<  returns the number of vertices of the cell
        /**
         * add a vertex to the cell list
         * @param vertex pointer to a vertex
         */
        inline void add_vertex(Vertex<DIM>* vertex);
        inline std::vector<Vertex<DIM> *> get_vertices() const;  ///<  returns the list of the vertices' pointer of the cell
        inline Vertex<DIM>* vertex(size_t l_i) const;   ///< get a constant pointer to a vertex using its local index


    private:
        std::vector<Face<DIM> *> m_faces;    ///<  list of faces' pointer of the cell
        std::vector<Edge<DIM> *> m_edges;    ///<  list of edges' pointer of the cell
        std::vector<Vertex<DIM> *> m_vertices;    ///<  list of vertices' pointer of the cell
    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////
    template<size_t DIM>
    Cell<DIM>::Cell(Mesh<DIM>* mesh, size_t g_i) : 
    MeshElemtInfo<DIM>(mesh, g_i),
    m_faces(0),
    m_edges(0),
    m_vertices(0) {}

    template<size_t DIM>
    inline size_t Cell<DIM>::n_faces() const {return m_faces.size();}

    template<size_t DIM>
    inline void Cell<DIM>::add_face(Face<DIM> *face) {m_faces.push_back(face);}

    template<size_t DIM>
    inline std::vector<Face<DIM> *> Cell<DIM>::get_faces() const {return m_faces;}

    template<size_t DIM>
    inline size_t Cell<DIM>::n_edges() const {return m_edges.size();}

    template<size_t DIM>
    inline void Cell<DIM>::add_edge(Edge<DIM> *edge) {m_edges.push_back(edge);}

    template<size_t DIM>
    inline std::vector<Edge<DIM> *> Cell<DIM>::get_edges() const {return m_edges;}

    template<size_t DIM>
    inline size_t Cell<DIM>::n_vertices() const {return m_vertices.size();}

    template<size_t DIM>
    inline void Cell<DIM>::add_vertex(Vertex<DIM> *vertex) {m_vertices.push_back(vertex);}

    template<size_t DIM>
    inline std::vector<Vertex<DIM> *> Cell<DIM>::get_vertices() const {return m_vertices;}

    template<size_t DIM>
    inline Vertex<DIM>* Cell<DIM>::vertex(size_t l_i) const {
#ifdef DEBUG
        return m_vertices.at(l_i);
#else
        return m_vertices[l_i];
#endif
    }

} // MyMesh namespace
#endif /* CELL_HPP */
