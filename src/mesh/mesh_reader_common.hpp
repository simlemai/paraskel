
     // Common functions to process file names 
     // and to read mesh file for very formats 

#ifndef MESH_READER_COMMON_HPP
#define MESH_READER_COMMON_HPP

#include <iostream>
#include <string>
#include <fstream>
#include <regex>
#include <vector>

namespace MyMesh {

    /**
     * read a line from file and remove blanks
     * at the beginning and at the end
     * @param infile ifstream of file
    */
    inline std::string get_and_clean_line(std::ifstream* infile);

    /**
     * read a list from file when format is
     * (n_pt1) l_1 .... l_n_pt1
     * (n_pt2) l_1 .... l_n_pt2
     *     ....
     * (n_pt_n_list) l_1 .... l_n_pt_n_list
     * 
     * @param infile ifstream of file
     * @param n_list number of lines to read
     * @param list store the value
     * @param n_element number of elements in each line (if provided, first line value belongs to the list)
     * @param offset difference of index in file and in cpp (by default is 1 to start at 0 in cpp, 
     *                but need to be 0 if coordinates are read !)
    */
    template<typename T>
    inline void read_list(std::ifstream* infile,
                size_t const n_list,
                std::vector<std::vector<T> >& list,
                size_t const n_element = 0,
                size_t const offset = 1);

    /**
     * read keyword and compare with the
     * expected one, then read the number of elements
     * and the whole list
     * 
     * @param infile ifstream of file
     * @param expected_keyword expected keyword
     * @param list store the value of the list
     * @param n_element number of elements in each line (if provided, first line value belongs to the list)
     * @param offset difference of index in file and in cpp (by default is 1 to start at 0 in cpp, 
     *                but need to be 0 if coordinates are read !)
    */
    template<typename T>
    inline size_t read_keyword_and_list(std::ifstream* infile,
                std::string const expected_keyword,
                std::vector<std::vector<T> >& list,
                size_t const n_element = 0,
                size_t const offset = 1);

    /////////////////////////////////////////////////////////////////////////
    //                   Implementations
    /////////////////////////////////////////////////////////////////////////

    inline std::string get_and_clean_line(std::ifstream* infile) {
        std::string str_line;

        if((*infile).eof()) {return "eof";}

        // read line
        std::getline(*infile, str_line);

        while(str_line.empty()) {str_line = get_and_clean_line(infile);} // remove possible empty line
        // clean begin of line
        while (std::isspace(str_line[0])) { 
            str_line.erase(0, 1);
            if(str_line.size()==0) { // str_line contained only spaces, read next line
                str_line = get_and_clean_line(infile);
            }
        }
        // clean end of line
        while (std::isspace(*(str_line.end() - 1))) { str_line.erase(str_line.end() - 1);}
        return str_line;
    } // get_and_clean_line

    template<typename T>
    inline void read_list(std::ifstream* infile,
        size_t const n_list,
        std::vector<std::vector<T> >& list,
        size_t const n_element,
        size_t const offset) {

        size_t n_pt(n_element);
        T l(0);
        for (size_t i=0; i<n_list; i++) {
            // IF n_element NOT GIVEN,
            // first value is the number of values to be read
            if(n_element == 0) { 
                *infile >> n_pt; 
            }
            std::vector<T> value_in_line;
            value_in_line.reserve(n_pt);

            // Store the value into a temporary vector value_in_line
            for (size_t j=0; j<n_pt; j++) {
                *infile >> l;
                value_in_line.push_back(l - offset); // by default offset=1 so indexes start at 0
            }
            // Store every informations into list
            list.push_back(value_in_line);
        }
    } // read_list


    template<typename T>
    inline size_t read_keyword_and_list(std::ifstream* infile,
                std::string const expected_keyword,
                std::vector<std::vector<T> >& list,
                size_t const n_element,
                size_t const offset) {

        std::string keyword = get_and_clean_line(infile);

        // read keyword and compare with expected one
        if(keyword.compare(expected_keyword) != 0) {
            (*infile).close();
            throw "Not the expected keyword in read_keyword_and_list. ";
            return -1;
        }
        // Read the nb of objects in the list
        size_t n_list = std::stoi(get_and_clean_line(infile));
        list.reserve(list.size() + n_list);
        read_list<T>(infile, n_list, list, n_element, offset);

        return n_list;
    } // read_keyword_and_list


} // MyMesh namespace
#endif /* MESH_READER_COMMON_HPP */
