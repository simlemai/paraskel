#ifndef VERTEX_HPP
#define VERTEX_HPP

#include <iostream>
#include <Eigen/Dense>
#include "mesh_elemt_info.hpp"

namespace MyMesh {
    /**
     * The class Vertex provides every specific informations about the vertices. 
    */
    template<size_t DIM>
    class Vertex : public MeshElemtInfo<DIM>
    {
    public:
        /**
         * Constructor
         * @param mesh pointer to the mesh
         * @param g_i global index of the vertex
         * @param coord  coordinates of the vertex (eigen array type)
         */
        Vertex(Mesh<DIM>* mesh, size_t g_i, Eigen::ArrayXd& coords);
        ~Vertex() = default;    ///<  Default destructor

        inline Eigen::ArrayXd coords() const;         ///<  returns an array with the vertex coordinates
        /**
         * Set the coordinates of the vertex with an array
         * @param pt array with the coordinates
         */
        inline void set_coords(const Eigen::ArrayXd& pt);
        /**
         * Set the coordinates of the vertex with DIM=2 coordinates
         * @param x is the first coordinate
         * @param y is the second coordinate
         */
        template<typename U>
        inline void set_coords(const typename std::enable_if<DIM == 2, U>::type& x, const U& y);
        /**
         * Set the coordinates of the vertex with DIM=3 coordinates
         * @param x is the first coordinate
         * @param y is the second coordinate
         * @param z is the third coordinate
         */
        template<typename U>
        inline void set_coords(const typename std::enable_if<DIM == 3, U>::type& x, const U& y, const U& z);

        inline void add_neighbour_cell(Cell<DIM> *cell);
        inline std::vector<Cell<DIM> *> get_neighbour_cells() const;  ///<  returns the list of neighbouring cells of the vertex (pointeur tabular)

    private:
        Eigen::ArrayXd m_coords;   ///< Coordinates of the vertex
        std::vector<Cell<DIM> *> m_neighbour_cells;   ///<  list of cell(s) sharing the face
    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////
    template<size_t DIM>
    Vertex<DIM>::Vertex(Mesh<DIM>* mesh, size_t g_i, Eigen::ArrayXd& coords) :
    MeshElemtInfo<DIM>(mesh, g_i),
    m_neighbour_cells(0),
    m_coords(DIM) {
        m_coords = coords;
    }

    template<size_t DIM>
    inline Eigen::ArrayXd Vertex<DIM>::coords() const {return m_coords;}

    template<size_t DIM>
    inline void Vertex<DIM>::set_coords(const Eigen::ArrayXd& pt) {m_coords = pt;}

    template<size_t DIM>
    template<typename U>
    inline void Vertex<DIM>::set_coords(const typename std::enable_if<DIM == 2, U>::type& x, const U& y) 
    {m_coords<< x, y;}

    template<size_t DIM>
    template<typename U>
    inline void Vertex<DIM>::set_coords(const typename std::enable_if<DIM == 3, U>::type& x, const U& y, const U& z) 
    {m_coords<< x, y, z;}

    template<size_t DIM>
    inline void Vertex<DIM>::add_neighbour_cell(Cell<DIM> *cell) {m_neighbour_cells.push_back(cell);}

    template<size_t DIM>
    inline std::vector<Cell<DIM> *> Vertex<DIM>::get_neighbour_cells() const {return m_neighbour_cells;}

} // MyMesh namespace
#endif /* VERTEX_HPP */
