#ifndef EDGE_HPP
#define EDGE_HPP

#include <iostream>
#include <vector>
#include "mesh_elemt_info.hpp"

namespace MyMesh { // forward declaration of template classes
    template<size_t DIM>
    class Vertex;
}
namespace MyMesh {
    /**
     * The class Edge provides every specific informations about the edges
     * useful only in 3D 
    */
    template<size_t DIM>
    class Edge  : public MeshElemtInfo<DIM>
    {
    public:
        /**
         * Constructor
         * @param mesh pointer to the mesh
         * @param g_i global index of the edge
         */
        Edge(Mesh<DIM>* mesh, size_t g_i);
        ~Edge() = default;    ///<  Default destructor

        // concerning the vertices of the edge
        inline size_t n_vertices() const;            ///<  returns the number of vertices of the edge
        /**
         * add a vertex to the edge list
         * @param vertex pointer to the vertex
        */ 
        inline void add_vertex(Vertex<DIM> *vertex);
        inline std::vector<Vertex<DIM> *> get_vertices() const;  ///<  returns the list of vertices' pointer of the edge

        // concerning the neighbouring cell(s) of the edge
        inline size_t n_neighbour_cells() const;          ///<  returns the number of cells sharing the edge
        /**
         * add a neighbouring cell to the edge list
         * @param cell pointer to the cell
        */
        inline void add_neighbour_cell(Cell<DIM> *cell);
        inline std::vector<Cell<DIM> *> get_neighbour_cells() const;  ///<  returns the list of neighbouring cells of the edge
        /**
         * add the local index of the edge in the neighbouring cell C,
         * the local index is the position of this edge in the list of edges of C.
         * The order in this list must be the same than in m_neighbour_cells
         * @param l_i local index
        */
        inline void add_local_index(size_t l_i);
        inline std::vector<size_t> get_local_indexes() const; ///< returns the list of local index(es) in the neighbouring cell(s)

    private:
        std::vector<Vertex<DIM> *> m_vertices;    ///<  list of vertices' pointer of the edge
        std::vector<Cell<DIM> *> m_neighbour_cells;   ///<  list of cell(s) sharing the edge
        std::vector<size_t> m_local_indexes;  ///< list of the local index(es) of this face in the neighbouring cell(s)

    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////
    template<size_t DIM>
    Edge<DIM>::Edge(Mesh<DIM>* mesh, size_t g_i) : 
    MeshElemtInfo<DIM>(mesh, g_i),
    m_local_indexes(0),
    m_vertices(0) {}

    template<>
    inline size_t Edge<3>::n_vertices() const { return m_vertices.size(); }
    template<>
    inline size_t Edge<2>::n_vertices() const { return 0; }

    template<>
    inline void Edge<3>::add_vertex(Vertex<3> *vertex) { m_vertices.push_back(vertex); }
    template<>
    inline void Edge<2>::add_vertex(Vertex<2> *vertex) { throw " Try to add a vertex to edge in 2D. "; }

    template<size_t DIM>
    inline std::vector<Vertex<DIM> *> Edge<DIM>::get_vertices() const { return m_vertices; }
    
    template<size_t DIM>
    inline size_t Edge<DIM>::n_neighbour_cells() const { return m_neighbour_cells.size(); }

    template<>
    inline void Edge<3>::add_neighbour_cell(Cell<3> *cell) { m_neighbour_cells.push_back(cell); }
    template<>
    inline void Edge<2>::add_neighbour_cell(Cell<2> *cell) { throw " Try to add a neighbouring cell to edge in 2D. "; }

    template<size_t DIM>
    inline std::vector<Cell<DIM> *> Edge<DIM>::get_neighbour_cells() const { return m_neighbour_cells; }
    
    template<>
    inline void Edge<3>::add_local_index(size_t l_i) { m_local_indexes.push_back(l_i); }
    template<>
    inline void Edge<2>::add_local_index(size_t l_i) { throw " Try to add a local index to edge in 2D. "; }

    template<size_t DIM>
    inline std::vector<size_t> Edge<DIM>::get_local_indexes() const { return m_local_indexes; }

} // MyMesh namespace
#endif /* EDGE_HPP */
