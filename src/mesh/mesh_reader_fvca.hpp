#ifndef MESH_READER_FVCA_HPP
#define MESH_READER_FVCA_HPP

#include <iostream>
#include <fstream>
#include <regex>
#include <vector>
#include "mesh_reader_common.hpp"

namespace MyMesh {
    /**
     * Reads the textfile which contains the mesh 
     * in FVCA format : 
     *     - typ1 (2D triangular or quadrangular control volumes) 
     *     - typ2 (2D polygonal control volumes) 
     * and fill the vectors vertices, vertices_by_cells,
     * vertices_by_bound_edges and cells_centers.
     * See https://github.com/wareHHOuse/fvca-meshes/tree/master/FVCA5
     * for more explanations on the format.
     * 
     * *************************** *
     * The typ1 format contains:
     * Vertices
     *   nb_vertices
     *   coordinates of the vertices
     *      ...
     * triangles
     *   nb_triangles
     *   the number of each of their three vertices ordered clockwise
     *       ...
     * quadrangles
     *   nb_quadrangles
     *   the number of each of their four vertices ordered clockwise
     *       ...
     * pentagons
     *   nb_pentagons
     *   the number of each of their five vertices ordered clockwise
     *       ...
     * hexagons
     *   nb_hexagons
     *   the number of each of their six vertices ordered clockwise
     *       ...
     * edges of the boundary
     *   nb_bound_edges
     *   the number of the vertices that define such edge
     *       ...
     * all edges
     *   nb_edges
     *   vertex1 vertex2 control_vol1 control_vol2 (for bound edge control_vol2=0)
     *       ...
     * *************************** *
     * The typ2 format contains:
     * Vertices
     *   nb_vertices
     *   coordinates of the vertices
     *      ...
     * cells
     *   nb_cells
     *   nb_face_in_cell    face1   face2   face...
     *       ...
     * centers
     *   x_coord  y_coord
     *       ...
     * *************************** *
     * 
     * @param filename name of the textfile containing the mesh
     * @param typ 1 for typ1, 2 for typ2
     * @param vertices vector with the vertices coordinates
     * @param vertices_by_cells vector with the vertices indexes in each cell
     * @param vertices_by_bound_edges vector with the vertices indexes in each boundary edge
     * @param cells_centers vector with the cell centers coordinates
    */
    void read_2Dfvca_mesh(
                    std::string const filename,
                    int const typ,
                    std::vector<std::vector<double> >& vertices,
                    std::vector<std::vector<size_t> >& vertices_by_cells,
                    std::vector<std::vector<size_t> >* vertices_by_bound_edges = nullptr,
                    std::vector<std::vector<double> >* cells_centers = nullptr);

    /**
     * Reads the textfile which contains the mesh 
     * in fvca typ6 format (it is a 3D general mesh)
     * and fill some connectivity vectors
     * 
     * This format is like:
     * 4 commentary lines
     *      "version number"
     * commentary line
     *      "name of mesh"
     * 2 commentary lines
     *      nb of vertices
     * commentary line
     *      nb of control volumes
     * commentary line
     *      nb of faces
     * commentary line
     *      nb of edges
     * commentary line (follow the vertices coordinates)
     *   x_coord  y_coord z_coord
     *      ...
     * commentary line (follow the )
     *   nb_cells
     *   nb_face_in_cell    face1   face2   face...
     *       ...
     * centers
     *   x_coord  y_coord
     *       ...
     * 
     * @param filename name of the textfile containing the mesh
     * @param meshname name of the mesh
     * @param vertices vector with the vertices coordinates
     * @param faces_by_cells vector with the faces global indexes in each cell
     * @param vertices_by_cells vector with the vertices global indexes in each cell
     * @param edges_by_faces vector with the edges global indexes in each face
     * @param vertices_by_faces vector with the vertices global indexes in each face
     * @param vertices_by_edges vector with the vertices global indexes in each edge
     * @param cells_centers vector with the cell centers coordinates
    */
    void read_3Dfvca_mesh(
                    std::string const filename,
                    std::string meshname,
                    std::vector<std::vector<double> >& vertices,
                    std::vector<std::vector<size_t> >& faces_by_cells,
                    std::vector<std::vector<size_t> >& vertices_by_cells,
                    std::vector<std::vector<size_t> >& edges_by_faces,
                    std::vector<std::vector<size_t> >& vertices_by_faces,
                    std::vector<std::vector<size_t> >& vertices_by_edges,
                    std::vector<std::vector<double> >* cells_centers = nullptr); 

    /////////////////////////////////////////////////////////////////////////
    //                   Implementations
    /////////////////////////////////////////////////////////////////////////

    void read_2Dfvca_mesh(
                    std::string const filename,
                    int const typ,
                    std::vector<std::vector<double> >& vertices,
                    std::vector<std::vector<size_t> >& vertices_by_cells,
                    std::vector<std::vector<size_t> >* vertices_by_bound_edges,
                    std::vector<std::vector<double> >* cells_centers) {

        std::ifstream infile(filename);
        size_t nb_all_cells(0);
        
        // open file
        if(!infile.is_open()) {
            throw "Error in opening file in read_2Dfvca_mesh. ";
            return;
        }
        // ----------------------------------------------
        // check if typ=1 (typ1)
        if (typ==1)
        {
#ifdef DEBUG
            std::cout << "Mesh format is typ1 (FVCA5 2D mesh)" << std::endl;
#endif
            // read first part which should be "vertices", fill vertices with
            // the coordinates
            read_keyword_and_list(&infile, "vertices", vertices, 2, 0);

            // Read next part which should be "triangles", fill vertices_by_cells
            // with vertices global index
            nb_all_cells = read_keyword_and_list(&infile, "triangles", vertices_by_cells, 3);

            // Read next part which should be "quadrangles", fill vertices_by_cells
            nb_all_cells += read_keyword_and_list(&infile, "quadrangles", vertices_by_cells, 4);

            // Read next part which should be "pentagons", fill vertices_by_cells
            nb_all_cells += read_keyword_and_list(&infile, "pentagons", vertices_by_cells, 5);

            // Read next part which should be "hexagons", fill vertices_by_cells
            nb_all_cells += read_keyword_and_list(&infile, "hexagons", vertices_by_cells, 6);

            // Read next part which should be "edges of the boundary"
            read_keyword_and_list(&infile, "edges of the boundary", *vertices_by_bound_edges, 2);

            // Read next part which should be "all edges" (avoid it)
            std::vector<std::vector<size_t> > tmp_vect;
            read_keyword_and_list(&infile, "all edges", tmp_vect, 4);

        // ----------------------------------------------
        // check if typ=2 (typ2)
        } else if (typ==2) {
#ifdef DEBUG
            std::cout << "Mesh format is typ2 (FVCA5 2D mesh)" << std::endl;
#endif
            // read first part which should be "Vertices", fill vertices with
            // the coordinates
            read_keyword_and_list(&infile, "Vertices", vertices, 2, 0);

            // Read next part which should be "cells", fill vertices_by_cells with
            // the global index of the vertices forming each cell
            nb_all_cells = read_keyword_and_list(&infile, "cells", vertices_by_cells);

        // ----------------------------------------------
        } else { // if typ1 or typ2
            infile.close();
            throw "Mesh format is not recognized, only typ1 and typ2 \
            can be read with this function.";
            return;
        }

        // the file may also contain the center of cells
        if (cells_centers) {
            // Read the following line which may contain "centers"
            std::string keyword = get_and_clean_line(&infile);
            if(keyword.compare("centers") == 0) {
#ifdef DEBUG
                std::cout << "Cell centers have been read from mesh file" << std::endl;
#endif
                // cannot use read_keyword_and_list because nb_all_cells is not part of the file
                read_list<double>(&infile, nb_all_cells, *cells_centers, 2, 0);
            }
        }

        infile.close();

    } // read_2Dfvca_mesh

    void read_3Dfvca_mesh(
                    std::string const filename,
                    std::string meshname,
                    std::vector<std::vector<double> >& vertices,
                    std::vector<std::vector<size_t> >& faces_by_cells,
                    std::vector<std::vector<size_t> >& vertices_by_cells,
                    std::vector<std::vector<size_t> >& edges_by_faces,
                    std::vector<std::vector<size_t> >& vertices_by_faces,
                    std::vector<std::vector<size_t> >& vertices_by_edges,
                    std::vector<std::vector<double> >* cells_centers) {

        std::ifstream infile(filename);
        std::string str_line;
        size_t n_v(0), n_c(0), n_f(0), n_e(0);
        
#ifdef DEBUG
        std::cout << "Mesh format is 3D fvca (typ6)" << std::endl;
#endif
        // open file
        if(!infile.is_open()) {
            throw "Error in opening file. ";
            return;
        }

        // -------------------- Header --------------------
        // avoid the first 6th lines
        for (size_t i=0; i<6; i++) { std::getline(infile, str_line); }
        // read mesh name
        meshname = get_and_clean_line(&infile);
        for (size_t i=0; i<2; i++) { std::getline(infile, str_line); } // avoid 2 lines
        n_v = std::stoi(get_and_clean_line(&infile)); // nb vertices
        std::getline(infile, str_line); // avoid 1 line
        n_c = std::stoi(get_and_clean_line(&infile)); // nb cells
        std::getline(infile, str_line); // avoid 1 line
        n_f = std::stoi(get_and_clean_line(&infile)); // nb faces
        std::getline(infile, str_line); // avoid 1 line
        n_e = std::stoi(get_and_clean_line(&infile)); // nb edges

        // -------------------- Vertices --------------------
        std::getline(infile, str_line); // avoid 1 line
        // Read all vertices lines
        read_list(&infile, n_v, vertices, 3, 0); // read the 3 coords and store them into vertices
        std::getline(infile, str_line); // avoid 1 line
        
        // -------------------- Faces in each cell --------------------
        std::getline(infile, str_line); // avoid 1 line
        read_list(&infile, n_c, faces_by_cells); // read the lists and store them into faces_by_cells
        std::getline(infile, str_line);
        
        // -------------------- Vertices in each cell --------------------
        std::getline(infile, str_line); // avoid 1 line
        read_list(&infile, n_c, vertices_by_cells); // read the lists and store them into vertices_by_cells
        std::getline(infile, str_line);
        
        // -------------------- Edges in each face --------------------
        std::getline(infile, str_line); // avoid 1 line
        read_list(&infile, n_f, edges_by_faces); // read the lists and store them into edges_by_faces
        std::getline(infile, str_line);

        // -------------------- Vertices in each face --------------------
        std::getline(infile, str_line); // avoid 1 line
        read_list(&infile, n_f, vertices_by_faces); // read the lists and store them into vertices_by_faces
        std::getline(infile, str_line);

        // -------------------- avoid the next n_f+1 lines --------------------
        for (size_t i=0; i<n_f+1; i++) { std::getline(infile, str_line); }

        // -------------------- Vertices in each edge --------------------
        std::getline(infile, str_line); // avoid 1 line
        // read the 2 values for each edge and store them into vertices_by_edges
        read_list(&infile, n_e, vertices_by_edges, 2);

        infile.close();
    } // read_3Dfvca_mesh

} // MyMesh namespace
#endif /* MESH_READER_FVCA_HPP */
