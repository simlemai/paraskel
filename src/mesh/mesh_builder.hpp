#ifndef MESH_BUILDER_HPP
#define MESH_BUILDER_HPP

#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include <memory>
#include "common_geometry.hpp"
#include "mesh.hpp"
#include "cell.hpp"
#include "face.hpp"
#include "edge.hpp"
#include "vertex.hpp"

#include <typeinfo>

namespace MyMesh {
    /**
     * Build the mesh : construct the connectivity and create the mesh objects
     * after the file mesh has been read.
     * 
     * @param vertices vertices coordinates
     * @param vertices_by_cells vertices global indexes in each cell
     * @param faces_by_cells faces global indexes in each cell
     * @param edges_by_faces edges global indexes in each face
     * @param vertices_by_faces vertices global indexes in each face
     * @param vertices_by_edges vertices global indexes in each edge
     * @param cells_centers cell centers coordinates
     * @param meshname name given to the mesh
    */
    template<size_t DIM>
    std::unique_ptr<Mesh<DIM>> build_mesh( 
                    std::vector<std::vector<double> >& vertices,
                    std::vector<std::vector<size_t> >& vertices_by_cells,
                    std::vector<std::vector<double> >* cells_centers = nullptr,
                    std::vector<std::vector<size_t> >* vertices_by_faces = nullptr,
                    std::vector<std::vector<size_t> >* faces_by_cells = nullptr,
                    std::vector<std::vector<size_t> >* edges_by_faces = nullptr,
                    std::vector<std::vector<size_t> >* vertices_by_edges = nullptr,
                    std::string meshname = "");

    /**
     * Compute the measure and mass center of each cell and face.
     * @param mesh mesh pointer
    */
    template<size_t DIM>
    static inline void build_measure_and_mass_centers(Mesh<DIM>* mesh);

    /**
     * Volume of a cell (only 3D by definition)
     * @param cell 
    */
    static inline double build_cell_volume(Cell<3>* cell);

    /**
     * Init the diameter of all elements of a mesh
     * @param mesh pointer to the mesh
    */
    template<size_t DIM>
    static inline void build_diameters(Mesh<DIM>* mesh);

    /**
     * Init the diameter of one element
     * @param element one element (cell, face, edge)
    */
    template<typename ELEMT>
    static inline void build_diameters_element(ELEMT element);

    /**
     * The connectivity has been read from mesh file,
     * creates mesh objects and fill connectivity.
     * 
     * @param mesh mesh pointer
     * @param vertices_by_cells vertices global indexes in each cell
     * @param vertices_by_faces vertices global indexes in each face
     * @param faces_by_cells faces global indexes in each cell
     * @param edges_by_faces edges global indexes in each face
     * @param vertices_by_edges vertices global indexes in each edge
    */
    template<size_t DIM>
    static void build_mesh_with_provided_connectivity(
                    Mesh<DIM>* mesh,
                    std::vector<std::vector<size_t> >& vertices_by_cells,
                    std::vector<std::vector<size_t> >& vertices_by_faces,
                    std::vector<std::vector<size_t> >& faces_by_cells,
                    std::vector<std::vector<size_t> >* edges_by_faces = nullptr,
                    std::vector<std::vector<size_t> >* vertices_by_edges = nullptr);

    /**
     * In 2D, creates mesh objects and fill connectivity
     * when only vertices_by_cells has been read from mesh file.
     * 
     * @param mesh mesh pointer
     * @param vertices_by_cells vertices global indexes in each cell
    */
    template<size_t DIM>
    static void build_mesh_from_vertices_by_cells(
                    Mesh<DIM>* mesh,
                    const std::vector<std::vector<size_t> >& vertices_by_cells);

    /**
     * In 2D, with two consecutives vertices of the cell,
     * get the pointer of the face (creates or finds the face)
     * and add the vertices to this face.
     * 
     * @param mesh mesh pointer
     * @param vertex vertices describing the face
     * @param nb_total_faces total number of face which have already been stored
     * @param face pointer of the face mesh object 
     * @param connectivity vector with the connectivity (all vertices connected to one vertex)
     * @param stored_face vector with the face pointers connectivity
    */
    inline void get_face_pt_from_2vertices(Mesh<2>* mesh,
                    size_t const vertex1, size_t const vertex2, 
                    size_t* nb_total_faces, Face<2>** face,
                    std::vector<std::vector<size_t> >& connectivity,
                    std::vector<std::vector<Face<2>*> >& stored_face);

    // /**
    //  * In 3D, creates edges objects and fill connectivity
    //  * when only vertices_by_faces has been given.
    //  * 
    //  * @param mesh mesh pointer
    //  * @param vertices_by_faces vertices global indexes in each face
    // */
    // template<size_t DIM>
    // void build_edges_from_vertices_by_faces(
    //                 Mesh<DIM>* mesh,
    //                 std::vector<std::vector<size_t> > vertices_by_faces);

    // /**
    //  * In 3D, with two consecutives vertices of the cell,
    //  * get the pointer of the edge (creates or finds the face)
    //  * and add the vertices to this edge.
    //  * 
    //  * @param mesh mesh pointer
    //  * @param vertex vertices describing the face
    //  * @param nb_total_edges total number of edge which have already been stored
    //  * @param edge pointer of the edge mesh object 
    //  * @param connectivity vector with the connectivity (all vertices connected to one vertex)
    //  * @param stored_edge vector with the face pointers connectivity
    // */
    // inline void get_edge_pt_from_2vertices(Mesh<3>* mesh,
    //                 size_t const vertex1, size_t const vertex2, 
    //                 size_t* nb_total_edges, Edge<3>** edge,
    //                 std::vector<std::vector<size_t> >& connectivity,
    //                 std::vector<std::vector<Edge<3>*> >& stored_edge);

    /////////////////////////////////////////////////////////////////////////
    //                   Implementations
    /////////////////////////////////////////////////////////////////////////

    template<size_t DIM>
    std::unique_ptr<Mesh<DIM>> build_mesh(
                    std::vector<std::vector<double> >& vertices,
                    std::vector<std::vector<size_t> >& vertices_by_cells,
                    std::vector<std::vector<double> >* cells_centers,
                    std::vector<std::vector<size_t> >* vertices_by_faces,
                    std::vector<std::vector<size_t> >* faces_by_cells,
                    std::vector<std::vector<size_t> >* edges_by_faces,
                    std::vector<std::vector<size_t> >* vertices_by_edges,
                    std::string meshname) {
#ifdef DEBUG
        if(vertices.size() == 0 || vertices_by_cells.size() == 0) {
            throw " In build_mesh, vertices or/and vertices_by_cells is/are empty. ";
            return nullptr;
        }
#endif
        // Pointer to the mesh
        // unique_ptr so it outlives the function
        std::unique_ptr<Mesh<DIM>> mesh_pt(std::make_unique<Mesh<DIM>>());

        // Set name of the mesh
        mesh_pt->set_name(meshname);

        // Creates the vertices and adds them to the mesh
        size_t g_iV(0);
        for(auto& v : vertices) {
            Eigen::ArrayXd array_v(Eigen::Map<Eigen::Array<double, DIM, 1>>(v.data()));
            Vertex<DIM>* vertex(new Vertex<DIM>(mesh_pt.get(), g_iV++, array_v));
            mesh_pt->add_vertex(vertex);
        }

        // The next strategy depends on which vectors are provided
        //      ------ in 2D ------
        if(DIM == 2 && faces_by_cells && vertices_by_faces) {
            // The connectivity is already known, creates mesh objects and fills connectivity
            build_mesh_with_provided_connectivity(mesh_pt.get(), vertices_by_cells,
                         *vertices_by_faces, *faces_by_cells);
        } else if (DIM == 2) {
            // The connectivity is build from vertices_by_cells
            build_mesh_from_vertices_by_cells(mesh_pt.get(), vertices_by_cells);
        
        //      ------ in 3D ------
        } else if (DIM == 3 && faces_by_cells && vertices_by_faces) {
            // The connectivity is already known, creates mesh objects and fills connectivity
            build_mesh_with_provided_connectivity(mesh_pt.get(), vertices_by_cells,
                        *vertices_by_faces, *faces_by_cells, edges_by_faces, vertices_by_edges);
        } else {
            throw " Unknown way to build mesh ";
            return nullptr;
        }

        // deduce BC elements from the connectivity
        mesh_pt->deduce_boundaries_from_connectivity();

        if(cells_centers && !(*cells_centers).empty()) {
            // add cell centers if provided
            size_t g_iC(0);
            // add cells_centers to the cell
            for(auto& c : mesh_pt->get_cells()) {
                Eigen::ArrayXd array_cc(Eigen::Map<Eigen::Array<double, DIM, 1>>((*cells_centers)[g_iC++].data()));
                c->set_mass_center(array_cc);
            }

            throw " In mesh_builder, cell centers provided by user is not implemented yet ";
            return nullptr;
            
        } else { // build them
            build_measure_and_mass_centers(mesh_pt.get());
        }
        // compute the diameters of the mesh elements 
        // (biggest distance between 2 vertices)
        build_diameters(mesh_pt.get());


        std::cout << "   The " << DIM << "D mesh contains : " << std::endl;
        std::cout << "                     " << mesh_pt->n_vertices() << 
                    " vertices " << std::endl;
        if(DIM == 3) { std::cout << "                     " << mesh_pt->n_edges() << 
                    " edges " << std::endl; }
        std::cout << "                     " << mesh_pt->n_faces() << 
                    " faces " << std::endl;
        std::cout << "                     " << mesh_pt->n_cells() << 
                    " cells " << std::endl;
#ifdef DEBUG
        std::cout << std::endl << "                     " << mesh_pt->n_bound_vertices() << 
                    " boundary vertices " << std::endl;
        if(DIM == 3) { std::cout << "                     " << mesh_pt->n_bound_edges() << 
                    " boundary edges " << std::endl; }
        std::cout << "                     " << mesh_pt->n_bound_faces() << 
                    " boundary faces " << std::endl;
        
        if(cells_centers && !(*cells_centers).empty()) {
            std::cout << " Cell centers have been build from mesh file" << 
            std::endl;
        }
#endif
        std::cout << "   ----------------------------------------------" << std::endl;

        if(mesh_pt->n_cells() == 0) {
            throw " There is no cell in the mesh, is the good dimension precised in main ? ";
        }

        return mesh_pt;

    } // build_mesh

    template<size_t DIM>
    static void build_mesh_with_provided_connectivity(
                    Mesh<DIM>* mesh,
                    std::vector<std::vector<size_t> >& vertices_by_cells,
                    std::vector<std::vector<size_t> >& vertices_by_faces,
                    std::vector<std::vector<size_t> >& faces_by_cells,
                    std::vector<std::vector<size_t> >* edges_by_faces,
                    std::vector<std::vector<size_t> >* vertices_by_edges) {

        // Use vertices_by_faces to create FACES and fill connectivity
        size_t g_iF(0);
        for(auto& f : vertices_by_faces) {
            Face<DIM>* face(new Face<DIM>(mesh, g_iF++));
            mesh->add_face(face);
            for (auto& v : f) {
                // Vertices already exist
                face->add_vertex(mesh->vertex(v));
            }
        } 

        // Use faces_by_cells to create CELLS and fill connectivity
        size_t g_iC(0);
        // Creates the cells and fills connectivity
        for(auto& c : faces_by_cells) { // loop over the cells
            Cell<DIM>* cell(new Cell<DIM>(mesh, g_iC++));
            mesh->add_cell(cell);
            for (auto& f : c) {  // loop over the faces in cell
                // Face already exist, fill connectivity : face belongs to cell
                cell->add_face(mesh->face(f));
                // fill connectivity: cell is neighbour of face
                mesh->face(f)->add_neighbour_cell(cell);
                // in cell, local index of face f is cell->n_faces()-1
                mesh->face(f)->add_local_index(cell->n_faces()-1);
            }
        } 

        // Use vertices_by_cells to build connectivity
        g_iC = 0;
        // Cells and Vertices already exist
        for(auto& c : vertices_by_cells) {
            for(auto& v : c) {
                // from cell global index, get cell and add the vertex whose global index is v
                mesh->cell(g_iC)->add_vertex(mesh->vertex(v));
                // add the cell to the neighbouring cells of the vertex whose global index is v
                mesh->vertex(v)->add_neighbour_cell(mesh->cell(g_iC));
            }
            g_iC++;
        }

        if(DIM == 3 && !(*edges_by_faces).empty()) {
#ifdef DEBUG
            if(!vertices_by_edges) {
                throw " In build_mesh, edges_by_faces is provided but not vertices_by_edges. ";
                mesh = nullptr;
                return;
            }
#endif
            // Creates the EDGES and fills connectivity
            size_t g_iE(0);
            for(auto& e : *vertices_by_edges) {
                Edge<DIM>* edge(new Edge<DIM>(mesh, g_iE++));
                mesh->add_edge(edge);
                edge->add_vertex(mesh->vertex(e[0]));
                edge->add_vertex(mesh->vertex(e[1]));
            }
            g_iF = 0;
            // Edges and Faces already exist, build connectivity
            for(auto& f : *edges_by_faces) {
                for(auto& e : f) {
                    Edge<DIM>* edge_e(mesh->edge(e));
                    // from face global index, get face pointer and add the edge whose global index is e
                    mesh->face(g_iF)->add_edge(edge_e);
                    // fills the connectivity: edge e belongs to cells
                    // loop over the neighbouring cells of the face
                    for(auto& cf : mesh->face(g_iF)->get_neighbour_cells()) {
                        bool already_stored(false);
                        // loop over the neighbouring cells of the edge
                        for(auto& ce : edge_e->get_neighbour_cells()) {
                            if(cf == ce) {
                                // the neighbouring cell is already stored
                                already_stored = true;
                                break;
                            }
                        }
                        if(!already_stored) {
                            // add edge e into the edges belonging to the cell cf
                            cf->add_edge(edge_e);
                            // add cell cf into the neighbouring cells of edge e
                            edge_e->add_neighbour_cell(cf);
                            // store local index of edge e (index in the list of edges belonging to cell cf)
                            edge_e->add_local_index(cf->n_edges()-1);
                        }
                    }
                }
                g_iF++;
            }
        } else if(DIM == 3 && false) { // TODO: add a bool to do it only if necessary

            // build_edges_from_vertices_by_faces(mesh, vertices_by_faces);

            std::cout << " ---------------------------------------- " << std::endl;
            std::cout << " WARNING : 3D Edges not tested ! " << std::endl;
            std::cout << " ---------------------------------------- " << std::endl;
        }

        return;

    } // build_mesh_with_provided_connectivity

    static void build_mesh_from_vertices_by_cells(
                    Mesh<3>* mesh,
                    const std::vector<std::vector<size_t> >& vertices_by_cells) {

        throw " build_mesh_from_vertices_by_cells called with 3D ";
        return;
    }

    static void build_mesh_from_vertices_by_cells(
                    Mesh<2>* mesh,
                    const std::vector<std::vector<size_t> >& vertices_by_cells) {

        // In 2D only :
        // Creates the Faces
        // with two consecutives vertices index in each cell,
        // and add the connectivity (global index of vertices and neighbouring cell(s)).
        // Creates the Cells of the mesh
        // and add the connectivity (global index of faces and vertices)
        std::vector<std::vector<size_t> > 
                    connectivity_pts(mesh->n_vertices(), std::vector<size_t>());
        std::vector<std::vector<Face<2>*> > 
                    num_face_storage(mesh->n_vertices(), std::vector<Face<2>*>());
        size_t g_iC(0);
        size_t nb_faces(0);
        Face<2>* f(nullptr);
        for (auto& vertices_in_cell : vertices_by_cells) {
            // Creates cell
            Cell<2>* c(new Cell<2>(mesh, g_iC++));
            mesh->add_cell(c);
            // Add the vertices to the cell, and the cell to the neighbouring cells of the vertex
            for (size_t v_gi : vertices_in_cell) { // vertices_in_cell[i] is global index of vertex
                c->add_vertex(mesh->vertex(v_gi)); 
                mesh->vertex(v_gi)->add_neighbour_cell(c);
            }
            // For two consecutives vertices of the cell
            // get the pointer of the face (creates or finds the face)
            // and add the vertices to this face
            for (size_t i = 0, n_v = vertices_in_cell.size(); i < n_v; i++) {
                get_face_pt_from_2vertices(mesh, vertices_in_cell[i], vertices_in_cell[(i+1)%n_v], 
                        &nb_faces, &f, connectivity_pts, num_face_storage); 

                // add the face to the cell
                c->add_face(f);
                // fill connectivity: face f belongs to cell c
                f->add_neighbour_cell(c);
                // in cell c, local index of face f is c->n_faces()-1
                f->add_local_index(c->n_faces()-1);

            }
        }
        return;
    } // build_mesh_from_vertices_by_cells 2D


    inline void get_face_pt_from_2vertices(Mesh<2>* mesh,
                    size_t const vertex1, size_t const vertex2, 
                    size_t* nb_total_faces, Face<2>** face,
                    std::vector<std::vector<size_t> >& connectivity,
                    std::vector<std::vector<Face<2>*> >& stored_face) {

        // order index of the two points
        size_t v_min(std::min(vertex1, vertex2));
        size_t v_max(std::max(vertex1, vertex2));

        // Are the two points already stored, does the face already has a pointer ?
        for (size_t j = 0; j < connectivity[v_min].size(); j++) {
            if(connectivity[v_min][j] == v_max) {
                // get face pointer
                *face = stored_face[v_min][j];
                // remove (v_min, v_max) from connectivity and stored_face,
                // because each face is in at most 2 cells :
                // fist time, the face is created, the second time, remove from list.
                connectivity[v_min].erase(connectivity[v_min].begin()+j);
                stored_face[v_min].erase(stored_face[v_min].begin()+j);
                return;
            }
        }
        // The face has not been found,
        // creates Face which does not exist yet
        size_t g_i(*nb_total_faces);
        *face = new Face<2>(mesh, g_i);
        mesh->add_face(*face);
        connectivity[v_min].push_back(v_max);
        stored_face[v_min].push_back(*face);

        // add the vertices to the face 
        (*face)->add_vertex(mesh->vertex(vertex1));
        (*face)->add_vertex(mesh->vertex(vertex2));

        *nb_total_faces = ++g_i;
        return;

    } // get_face_pt_from_2vertices


    // template<size_t DIM>
    // void build_edges_from_vertices_by_faces(
    //                 Mesh<DIM>* mesh,
    //                 const std::vector<std::vector<size_t> >& vertices_by_faces) {

    //     // In 3D only :
    //     // Creates the Edges
    //     // with two consecutives vertices index in each face.
    //     std::vector<std::vector<size_t> > 
    //                 connectivity_pts(mesh->n_vertices(), std::vector<size_t>());
    //     std::vector<std::vector<Edge<DIM>*> > 
    //                 num_edge_storage(mesh->n_vertices(), std::vector<Edge<DIM>*>());
    //     size_t g_iF(0);
    //     size_t nb_edges(0);
    //     Edge<DIM>* e(nullptr);
    //     for (auto& vertices_in_face : vertices_by_faces) {
    //         // For two consecutives vertices of the face
    //         // get the pointer of the edge (creates or finds it)
    //         // and add the vertices to this edge
    //         for (size_t i = 0, n_v = vertices_in_face.size(); i < n_v; i++) {
    //             get_edge_pt_from_2vertices(mesh, vertices_in_face[i], vertices_in_face[(i+1)%n_v], 
    //                     &nb_edges, &e, connectivity_pts, num_edge_storage); 

    //             // add the edge to the face
    //             // from face global index, get face and add the edge
    //             mesh->face(g_iF)->add_edge(e);
    //             throw " Add the cell-edge connectivity (neighbouring cells, local list and index of edges) ";
    //         }
    //         g_iF++;
    //     }
    // } // build_edges_from_vertices_by_faces


    // inline void get_edge_pt_from_2vertices(Mesh<3>* mesh,
    //                 size_t const vertex1, size_t const vertex2, 
    //                 size_t* nb_total_edges, Edge<3>** edge,
    //                 std::vector<std::vector<size_t> >& connectivity,
    //                 std::vector<std::vector<Edge<3>*> >& stored_edge) {

    //     // order index of the two points
    //     size_t v_min(std::min(vertex1, vertex2));
    //     size_t v_max(std::max(vertex1, vertex2));

    //     // Are the two points already stored, does the edge already has a pointer ?
    //     for (size_t j = 0, nc = connectivity[v_min].size(); j < nc; j++) {
    //         if(connectivity[v_min][j] == v_max) {
    //             // get edge pointer
    //             *edge = stored_edge[v_min][j];
    //             return;
    //         }
    //     }
    //     // The edge has not been found,
    //     // creates Edge which does not exist yet
    //     size_t g_i(*nb_total_edges);
    //     *edge = new Edge<3>(mesh, g_i);
    //     mesh->add_edge(*edge);
    //     connectivity[v_min].push_back(v_max);
    //     stored_edge[v_min].push_back(*edge);

    //     // add the vertices to the edge 
    //     (*edge)->add_vertex(mesh->vertex(vertex1));
    //     (*edge)->add_vertex(mesh->vertex(vertex2));

    //     *nb_total_edges = ++g_i;
    //     return;

    // } // get_edge_pt_from_2vertices

    // 2D
    static inline void build_measure_and_mass_centers(Mesh<2>* mesh) {

        // Face is an edge
        for (auto& f : mesh->get_faces()) {

            // Store vertices coordinates
            std::vector<Eigen::ArrayXd> vertices_coords;
            for(auto& v : f->get_vertices()) {
                vertices_coords.push_back(v->coords());
            }

            // mass center is located at the middle of the 2 vertices
            f->set_mass_center((vertices_coords[0] + vertices_coords[1]) / 2.); 
            // measure is the norm of the edge
            Eigen::VectorXd v(vertices_coords[1] - vertices_coords[0]);
            f->set_measure(v.norm());

        } // end loop f

        // build cell mass center and cell measure
        // in 2D
        for (auto& c : mesh->get_cells()) {

            // Store vertices coordinates
            size_t n_v(c->n_vertices());
            std::vector<Eigen::ArrayXd> vertices_coords;
            vertices_coords.reserve(n_v);
            for(auto& v : c->get_vertices()) {
                vertices_coords.push_back(v->coords());
            }

            Eigen::ArrayXd cell_mass_center;
            double surface;
            build_surface_and_mass_center_polygon<2>(n_v, vertices_coords, surface, cell_mass_center);
            c->set_mass_center(cell_mass_center);
            c->set_measure(surface);
        }

        // compute the measure of the mesh :
        // sum over the cells of there measure
        mesh->set_measure();

        return;
    } // build_measure_and_mass_centers 2D


    // 3D
    static inline void build_measure_and_mass_centers(Mesh<3>* mesh) {

        for (auto& f : mesh->get_faces()) {

            // Store vertices coordinates
            size_t n_v(f->n_vertices());
            std::vector<Eigen::ArrayXd> vertices_coords;
            vertices_coords.reserve(n_v);
            for(auto& v : f->get_vertices()) {
                vertices_coords.push_back(v->coords());
            }

            // Face is a polygon
            Eigen::ArrayXd face_mass_center;
            double surface;
            build_surface_and_mass_center_polygon<3>(n_v, vertices_coords, surface, face_mass_center);
            f->set_mass_center(face_mass_center);
            f->set_measure(surface);

        } // end loop f

        // build cell mass center and cell measure
        // in 3D
        for (auto& c : mesh->get_cells()) {
            Eigen::ArrayXd cell_mass_center(Eigen::ArrayXd::Zero(3));
            double total_measure(0);

            // in 3D, cell mass center is build with all faces informations
            for (auto& f : c->get_faces()) {
                // add face contribution to the cell mass center and measure
                cell_mass_center += f->measure() * f->mass_center();
                total_measure += f->measure();
            }
            cell_mass_center /= total_measure;
            c->set_mass_center(cell_mass_center);
            c->set_measure(build_cell_volume(c));
        }

        // for each edge, construct the centroid (located at the middle of the 2 vertices)
        for (auto& e : mesh->get_edges()) {
            // Store vertices coordinates
            std::vector<Eigen::ArrayXd> vertices_coords;
            Eigen::ArrayXd edge_centroid(Eigen::ArrayXd::Zero(3));
            for(auto& v : e->get_vertices()) {
                vertices_coords.push_back(v->coords());
                edge_centroid += v->coords();
            }
            edge_centroid /= 2;
            e->set_mass_center(edge_centroid);
            Eigen::VectorXd v(vertices_coords[1] - vertices_coords[0]);
            e->set_measure(v.norm());
        } // edge loop

        // compute the measure of the mesh :
        // sum over the cells of there measure
        mesh->set_measure();

        return;
    } // build_measure_and_mass_centers 3D

    // build cell volume in 3D
    static inline double build_cell_volume(Cell<3>* cell) {

        // adapt to the shape of the cell
        size_t n_v(cell->n_vertices());
        double volume(0);

        if(n_v == 4) {
            // the cell is a tetrahedron
            // get the vertices of the cell
            std::vector<MyMesh::Vertex<3>*> pts(cell->get_vertices());

            return tetra_volume(pts[0]->coords(), pts[1]->coords(), 
                                  pts[2]->coords(), pts[3]->coords());
            
        } else {
            // Generic : split into tetrahedra
            const Eigen::ArrayXd cell_center(cell->mass_center());
        
            for (auto& f : cell->get_faces()) {
                std::vector<MyMesh::Vertex<3>*> pts(f->get_vertices());
                size_t n_v(pts.size());

                if(n_v == 3) {
                    // face is a triangle, creates a tetrahedron with cell_center
                    volume += tetra_volume(pts[0]->coords(), pts[1]->coords(), 
                                        pts[2]->coords(), cell_center);

                } else if(n_v == 4) {
                    // face is a quadrangle, split it in 2 to create tetrahedra with cell_center
                    // triangle of pts[0], pts[1] and pts[2]
                    volume += tetra_volume(pts[0]->coords(), pts[1]->coords(), 
                                        pts[2]->coords(), cell_center);

                    // triangle of pts[0], pts[2] and pts[3]
                    volume += tetra_volume(pts[0]->coords(), pts[2]->coords(), 
                                        pts[3]->coords(), cell_center);

                } else {
                    // face is a polygon, split it into triangles
                    // each tetra is a sub-domain added to volume
                    for(size_t i = 0; i < n_v; i++) {
                        // triangle of pts[i], pts[i+1] and face_center
                        volume += tetra_volume(pts[i]->coords(), pts[(i+1)%n_v]->coords(), 
                                               f->mass_center(), cell_center);
                    }
                } // shape of face

            } // loop over faces

            return volume;
        } // shape of cell
    } // build_cell_volume 3D


    template<size_t DIM>
    static inline void build_diameters(Mesh<DIM>* mesh) {

        double mean_diam(0.0); // mean of all diameters
        double max_diam(0.0);
        double min_diam(1.e16);
        double max_anisotropy(0.);
        double min_anisotropy(1.e16);

        // Faces
        for (auto& f : mesh->get_faces()) {
            build_diameters_element(f);
        }
        if(DIM == 3) {
            // Edges
            for (auto& e : mesh->get_edges()) {
                build_diameters_element(e);
            }
        }

        // Cells
        for (auto& c : mesh->get_cells()) {
            // compute the diameter of the cell
            build_diameters_element(c);
            
            // find the min and the max diameters of the mesh
            min_diam = std::min(min_diam, c->diam());
            max_diam = std::max(max_diam, c->diam());

            // find the smallest edge of the cell to compute the anisotropy
            double smallest_edge(1.e16);
            for(auto& e : c->get_edges()) {
                smallest_edge = std::min(smallest_edge, e->diam());
            }

            mean_diam += c->diam();
            min_anisotropy = std::min(min_anisotropy, c->diam() / smallest_edge);
            max_anisotropy = std::max(max_anisotropy, c->diam() / smallest_edge);
        }
        mean_diam /= mesh->n_cells();

        // store the mesh sizes
        mesh->set_min_diam(min_diam);
        mesh->set_max_diam(max_diam);
        mesh->set_mean_diam(mean_diam);
        mesh->set_min_anisotropy(min_anisotropy);
        mesh->set_max_anisotropy(max_anisotropy);

        return;
    } // build_diameters

    template<typename ELEMT>
    static inline void build_diameters_element(ELEMT element) {

        std::vector<Eigen::VectorXd> vertices_coords;
        for(auto& v : element->get_vertices()) {
            vertices_coords.push_back(v->coords().matrix());
        }
        element->set_diam(element_diameter(vertices_coords)); // common_geometry

        return;
    } // build_diameters_element

} // MyMesh namespace
#endif /* MESH_BUILDER_HPP */
