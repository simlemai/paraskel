#ifndef QUADRATURES_HPP
#define QUADRATURES_HPP

#include <iostream>
#include <vector>
#include <Eigen/Dense>

namespace MyQuadra {

    /**
     * This class describes one quadrature point: 
     * its coordinates (method .point()) and its weight (method .weight())
     * 
     * The function "integrate" constructs all the quadrature points 
     * of one element (edge, face, cell)
    */
    template<size_t DIM>
    class QuadraturePoint
    {
    public:
        /**
         * Constructor
         * @param p point's coordinates
         * @param w weight
         */
        QuadraturePoint(const Eigen::ArrayXd& p, const double w);
        ~QuadraturePoint() = default;   ///<  Default destructor

        inline Eigen::ArrayXd point() const; ///< get the coordinates of the quadrature point
        inline double weight() const; ///< get the weight of the quadrature point

    private:
        Eigen::ArrayXd m_coords;   ///< Coordinates of the quadrature point
        double m_weight;    ///< Weight of the quadrature point
    };


    template<size_t DIM>
    class QuadraturePoints
    {
    public:
        /**
         * Constructor
         * @param qps vector with the quadrature points
         */
        QuadraturePoints(const std::vector<QuadraturePoint<DIM>*> qps);
        ~QuadraturePoints();   ///<  Destructor : delete all element of vector because created with "new"

        inline std::vector<QuadraturePoint<DIM>*> list() const; ///< get all the quadrature points
        inline size_t size() const; ///< get number of quadrature points

    private:
        std::vector<QuadraturePoint<DIM>*> m_qps;   ///< vector containing the quadrature points
    };


    /////////////////////////////////////////////////////////////////////////
    //                   Classes Methods implementations
    /////////////////////////////////////////////////////////////////////////
    template<size_t DIM>
    QuadraturePoint<DIM>::QuadraturePoint(const Eigen::ArrayXd& p, const double w) :
    m_weight(w),
    m_coords(DIM) {
        m_coords = p;
    }

    template<size_t DIM>
    inline Eigen::ArrayXd QuadraturePoint<DIM>::point() const { return m_coords; }

    template<size_t DIM>
    inline double QuadraturePoint<DIM>::weight() const { return m_weight; }

    template<size_t DIM>
    QuadraturePoints<DIM>::QuadraturePoints(const std::vector<QuadraturePoint<DIM>*> qps) :
    m_qps(qps) {}

    template<size_t DIM>
    QuadraturePoints<DIM>::~QuadraturePoints() { 
        for(auto& qp : m_qps) { delete qp; qp = 0; } // "new" in creation of this class
    } 

    template<size_t DIM>
    inline std::vector<QuadraturePoint<DIM>*> QuadraturePoints<DIM>::list() const { return m_qps; }

    template<size_t DIM>
    inline size_t QuadraturePoints<DIM>::size() const { return m_qps.size(); }


    /////////////////////////////////////////////////////////////////////////
    //          declarations which do not belong to the class
    /////////////////////////////////////////////////////////////////////////
    /**
     * The function "integrate" constructs all the quadrature points 
     * of one element (edge, face, cell)
     * 
     * Definitions depending on the element and the dimension are in gauss_legendre.hpp
    */
    template<size_t DIM, typename ELEMT>
    inline std::vector<QuadraturePoint<DIM>*> integrate(const ELEMT element, const int degree) {
        std::vector<QuadraturePoint<DIM>*> qp;
        throw " integrate is not implemented in this case ";
        return qp;
    }

} // MyQuadra namespace

#include "quad_common.hpp"
// contains definition of integrate for Gauss Legendre quadrature rules (1D elements)
#include "quad_gauss_legendre.hpp" 
// contains definition of integrate for Dunavant quadrature rules (2D elements)
#include "quad_dunavant.hpp"
// contains definition of integrate for quadrature rules of 3D cells 
#include "quad_volume.hpp" 


#endif /* QUADRATURES_HPP */
