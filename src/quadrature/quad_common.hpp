#ifndef QUAD_BONES_HPP
#define QUAD_BONES_HPP

#include "mesh.hpp"

#ifndef QUADRATURES_HPP
    throw "You must NOT include this file. Include quadratures.hpp"
#endif

/**
 * When deg = 0, the integral is approximated at the centroid of the element 
 * whith a weight equal to its measure
*/
template<size_t DIM, typename ELEMT>
static inline std::vector<MyQuadra::QuadraturePoint<DIM>*> integrate_degree0(const ELEMT element)
{
    std::vector<MyQuadra::QuadraturePoint<DIM>*> qp;

    // only one point thus no loop
    Eigen::ArrayXd pt(element->mass_center());
    MyQuadra::QuadraturePoint<DIM>* quadra_e(new MyQuadra::QuadraturePoint<DIM>(pt, element->measure()));
    qp.push_back(quadra_e);

    return qp;
}


#endif /* QUAD_BONES_HPP */
