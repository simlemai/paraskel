#ifndef QUAD_DUNAVANT_HPP
#define QUAD_DUNAVANT_HPP

#include "mesh.hpp"
#include "triangle_dunavant_rule.hpp"

#ifndef QUADRATURES_HPP
    throw " You must NOT include this file. Include quadratures.hpp"
#endif

/**
 * Return a QuadraturePoint vector with 
 * the quadrature points and weights 
 * to be able to integrate over
 * any TRIANGLE
 * Dunavant rule
 * integrate polynomials of degree <= 20 EXACTLY
 * uses the jburkardt's code
*/
template<size_t DIM>
static inline std::vector<MyQuadra::QuadraturePoint<DIM>*> 
integrate_triangle(size_t degree,
            const Eigen::ArrayXd p0,
            const Eigen::ArrayXd p1,
            const Eigen::ArrayXd p2) {

    // get the Dunavant quadrature points qp and weights qw
    const size_t nq(dunavant_order_num(degree));
    std::vector<double> qw(nq);
    std::vector<double> qp(2*nq); // 2D element

    //    Input, int RULE, the index of the rule.
    //    Input, int ORDER_NUM, the order (number of points) of the rule.
    //    Output, double XY[2*ORDER_NUM], the points of the rule.
    //    Output, double W[ORDER_NUM], the weights of the rule.
    dunavant_rule(degree, nq, &qp[0], &qw[0]);
    std::vector<MyQuadra::QuadraturePoint<DIM>*> tri_quadra;
    tri_quadra.reserve(nq);

    double area_triangle(triangle_surface<DIM>(p0, p1, p2));

    // set all quadrature points and weights
    for(size_t i=0; i<nq; i++) {
        const double w(qw[i]* area_triangle);
        Eigen::ArrayXd p(DIM);
        // for(size_t j=0; j<DIM; j++) { // each coordinate of the quadrature point p
        //     p[j] = qp[2*i] * p0[j] + qp[2*i+1] * p1[j] + (1. - qp[2*i] - qp[2*i+1]) * p2[j];
        // }
        p = qp[2*i] * p0 + qp[2*i+1] * p1 + (1. - qp[2*i] - qp[2*i+1]) * p2;

        MyQuadra::QuadraturePoint<DIM>* quadra_pt(new MyQuadra::QuadraturePoint<DIM>(p,w));
        tri_quadra.push_back(quadra_pt);
    }

    return tri_quadra;
} // integrate_triangle


template<size_t DIM, typename ELEMT>
static inline std::vector<MyQuadra::QuadraturePoint<DIM>*> 
integrate_polygon(const ELEMT element, size_t degree) {

    // Dunavant gives the quadrature rules over a triangle,
    // so if necessary, decompose the element into triangles.
    const size_t nq(dunavant_order_num(degree));
    std::vector<MyQuadra::QuadraturePoint<DIM>*> quadra_P;

    // get the vertices of the 2D element
    std::vector<MyMesh::Vertex<DIM>*> pts(element->get_vertices());
    size_t n_v(pts.size());

    if(n_v == 3) { // already a triangle
        quadra_P.reserve(nq);
        quadra_P = integrate_triangle<DIM>(degree, 
                pts[0]->coords(), pts[1]->coords(), pts[2]->coords());

        return quadra_P;

    } else if(n_v == 4) { // quadrangle : split it into 2 triangles
        quadra_P.reserve(2 * nq);

        std::vector<MyQuadra::QuadraturePoint<DIM>*> quadra_T;
        quadra_T.reserve(nq);

        // each triangle is a sub-domain added to quadra_P
        // triangle of pts[0], pts[1] and pts[2]
        quadra_T = integrate_triangle<DIM>(degree, 
                pts[0]->coords(), pts[1]->coords(), pts[2]->coords());
        quadra_P.insert(quadra_P.begin(), quadra_T.begin(), quadra_T.end());
        // triangle of pts[0], pts[2] and pts[3]
        quadra_T = integrate_triangle<DIM>(degree, 
                pts[0]->coords(), pts[2]->coords(), pts[3]->coords());
        quadra_P.insert(quadra_P.begin() + nq, quadra_T.begin(), quadra_T.end());

        return quadra_P;

    } else { // polygon : split it into triangles using center of mass
        // reserve enough place in quadra_P
        quadra_P.reserve(n_v * nq);

        // divide polygon into triangles
        const Eigen::ArrayXd elemt_center(element->mass_center());
        std::vector<MyQuadra::QuadraturePoint<DIM>*> quadra_T;
        quadra_T.reserve(nq);

        // each triangle is a sub-domain added to quadra_P
        for(size_t i = 0; i < n_v; i++) {
            // triangle of pts[i], pts[i+1] and elemt_center
            quadra_T = integrate_triangle<DIM>(degree, 
                    pts[i]->coords(), pts[(i+1)%n_v]->coords(), elemt_center);
            quadra_P.insert(quadra_P.begin() + i * nq, quadra_T.begin(), quadra_T.end());
        }

        return quadra_P;
    }

    throw " Could not determine shape of face in integrate_polygon ";
    return quadra_P;
} // integrate_polygon

namespace MyQuadra {
    /**
     * The function "integrate" constructs all the quadrature points 
     * of one element : a 3D Face (polygon)
    */
    inline std::vector<QuadraturePoint<3>*> integrate(const MyMesh::Face<3>* face, const int degree) {

#ifdef DEBUG
        if(degree > 20) {
            throw " Quadrature rule for 3D face of degree higher than 20. ";
        }
#endif

        if(degree <= 0) {
            return integrate_degree0<3>(face);
        }

        return integrate_polygon<3>(face, degree);

    }

    /**
     * The function "integrate" constructs all the quadrature points 
     * of one element : a 2D Cell (polygon)
    */
    inline std::vector<QuadraturePoint<2>*> integrate(const MyMesh::Cell<2>* cell, const int degree) {

#ifdef DEBUG
        if(degree > 20) {
            throw " Quadrature rule for 2D cell of degree higher than 20. ";
        }
#endif

        if(degree <= 0) {
            return integrate_degree0<2>(cell);
        }

        return integrate_polygon<2>(cell, degree);

    }

} // MyQuadra namespace


#endif /* QUAD_DUNAVANT_HPP */
