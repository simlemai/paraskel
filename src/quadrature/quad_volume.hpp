#ifndef QUAD_VOLUME_HPP
#define QUAD_VOLUME_HPP

#include "common_geometry.hpp"
#include "quad_tetra.hpp"

#ifndef QUADRATURES_HPP
    throw " You must NOT include this file. Include quadratures.hpp"
#endif

/**
 * Return a QuadraturePoint vector with 
 * the quadrature points and weights 
 * to be able to integrate over
 * any Tetrahedron
 * uses the jburkardt's code
 * integrate polynomials of degree <= 14
*/
static inline std::vector<MyQuadra::QuadraturePoint<3>*> 
integrate_tetra(const size_t degree,
            const size_t nq,
            const std::vector<double>& q0, 
            const std::vector<double>& q1, 
            const std::vector<double>& q2, 
            const std::vector<double>& q3, 
            const std::vector<double>& qw,
            const Eigen::ArrayXd& p0,
            const Eigen::ArrayXd& p1,
            const Eigen::ArrayXd& p2,
            const Eigen::ArrayXd& p3,
            const double vol_T) {

    std::vector<MyQuadra::QuadraturePoint<3>*> quadra_T;
    quadra_T.reserve(nq);
    
    for (size_t iq = 0; iq < nq; iq++) {
        const double w(qw[iq]* vol_T);
        Eigen::ArrayXd p(3);
        p = q0[iq] * p0 + q1[iq] * p1 + q2[iq] * p2 + q3[iq] * p3;

        MyQuadra::QuadraturePoint<3>* quadra_pt(new MyQuadra::QuadraturePoint<3>(p,w));
        quadra_T.push_back(quadra_pt);
    }
    return quadra_T;

} // integrate_tetra


/**
 * Construct the quadrature points and weights of a 3D cell
 * If necessary, split the cell to construct the quadrature points
 * over tetrahedra and calling integrate_tetra
*/
static inline std::vector<MyQuadra::QuadraturePoint<3>*> 
integrate_volume(const MyMesh::Cell<3>* cell, const size_t degree) {

    std::vector<MyQuadra::QuadraturePoint<3>*> quadra_C;
    // nb of quadrature pts in each tetrahedron
    const size_t nq(tetra_unit_size(degree)); 

    // barycentric coordinates of UNIT tetrahedron's vertices
    std::vector<double> q0(nq), q1(nq), q2(nq), q3(nq);
    // weights
    std::vector<double> qw(nq);
    // get qadra points and weights over the unit tetrahedron
    tetra_unit_set(degree, nq, q0, q1, q2, qw);
    for (size_t iq = 0; iq < nq; iq++) {
      q3[iq] = 1. - q0[iq] - q1[iq] - q2[iq];
    }


    // adapt to the shape of the cell
    size_t n_v(cell->n_vertices());

    if(n_v == 4) {
        // the cell is a tetrahedron
        // get the vertices of the cell
        std::vector<MyMesh::Vertex<3>*> pts(cell->get_vertices());

        quadra_C = integrate_tetra(degree, nq,
                q0, q1, q2, q3, qw,
                pts[0]->coords(), pts[1]->coords(), pts[2]->coords(), pts[3]->coords(),
                cell->measure());
        
        return quadra_C;

    } else if(n_v == 5 && cell->n_faces() == 5) {
        // Square pyramid : split the square base in 2, together with apex creates a tetrahedron
        // first : find the base
        std::vector<MyMesh::Vertex<3>*> pts_base(4);

        size_t base_i(0);
        std::vector<MyMesh::Face<3>*> faces_c(cell->get_faces());
        for (auto& f : faces_c) {
            if(f->n_vertices() == 4) {
                // the face is the base, get its vertices
                pts_base = f->get_vertices();
                break;
            }
            base_i++; //
        }
        // then : find the apex of the pyramid using vertices of an other face
        size_t other_i;
        if(base_i == 0) {
            other_i = 1;
        } else {
            other_i = base_i-1;
        }
        // other face should have 3 vertices, stored in pts_other
        std::vector<MyMesh::Vertex<3>*> pts_other(faces_c[other_i]->get_vertices());
        MyMesh::Vertex<3>* apex;
        for(auto& v : pts_other) {
            // if the vertex is not found in pts_base, this is the apex
            if(std::find(pts_base.begin(), pts_base.end(), v) == pts_base.end()) {
                // the vertex is not found in pts_base, this is the apex
                apex = v;
                break;
            }
        }

        // finally : split the cell in 2 tetrahedra
        std::vector<MyQuadra::QuadraturePoint<3>*> quadra_T;
        // tetrahedra of pts_base[0], pts_base[1], pts_base[2] and apex
        quadra_T = integrate_tetra(degree, nq,
            q0, q1, q2, q3, qw,
            pts_base[0]->coords(), pts_base[1]->coords(), pts_base[2]->coords(), apex->coords(),
            tetra_volume(pts_base[0]->coords(), pts_base[1]->coords(), pts_base[2]->coords(), apex->coords()));

        quadra_C.insert(quadra_C.begin(), quadra_T.begin(), quadra_T.end());

        // tetrahedra of pts_base[0], pts_base[2], pts_base[3] and apex
        quadra_T = integrate_tetra(degree, nq,
            q0, q1, q2, q3, qw,
            pts_base[0]->coords(), pts_base[2]->coords(), pts_base[3]->coords(), apex->coords(),
            tetra_volume(pts_base[0]->coords(), pts_base[2]->coords(), pts_base[3]->coords(), apex->coords()));

        quadra_C.insert(quadra_C.begin() + nq, quadra_T.begin(), quadra_T.end());

        return quadra_C;

    } else {
        // Generic : split into tetrahedra
        const Eigen::ArrayXd cell_center(cell->mass_center());
        std::vector<MyQuadra::QuadraturePoint<3>*> quadra_T;
        
        size_t compt_quadra_pts(0);
        for (auto& f : cell->get_faces()) {
            std::vector<MyMesh::Vertex<3>*> pts(f->get_vertices());
            size_t n_v(pts.size());

            if(n_v == 3) {
                // face is a triangle, creates a tetrahedron with cell_center
                quadra_T = integrate_tetra(degree, nq,
                        q0, q1, q2, q3, qw,
                        pts[0]->coords(), pts[1]->coords(), pts[2]->coords(), cell_center,
                        tetra_volume(pts[0]->coords(), pts[1]->coords(), pts[2]->coords(), cell_center));

                quadra_C.insert(quadra_C.begin() + compt_quadra_pts, quadra_T.begin(), quadra_T.end());
                compt_quadra_pts += nq;

            } else if(n_v == 4) {
                // face is a quadrangle, split it in 2 to create tetrahedra with cell_center
                // triangle of pts[0], pts[1] and pts[2]
                quadra_T = integrate_tetra(degree, nq,
                        q0, q1, q2, q3, qw,
                        pts[0]->coords(), pts[1]->coords(), pts[2]->coords(), cell_center,
                        tetra_volume(pts[0]->coords(), pts[1]->coords(), pts[2]->coords(), cell_center));

                quadra_C.insert(quadra_C.begin() + compt_quadra_pts, quadra_T.begin(), quadra_T.end());
                compt_quadra_pts += nq;

                // triangle of pts[0], pts[2] and pts[3]
                quadra_T = integrate_tetra(degree, nq,
                        q0, q1, q2, q3, qw,
                        pts[0]->coords(), pts[2]->coords(), pts[3]->coords(), cell_center,
                        tetra_volume(pts[0]->coords(), pts[2]->coords(), pts[3]->coords(), cell_center));

                quadra_C.insert(quadra_C.begin() + compt_quadra_pts, quadra_T.begin(), quadra_T.end());
                compt_quadra_pts += nq;
            
            } else {
                // face is a polygon, split it into triangles
                // each tetra is a sub-domain added to quadra_C
                for(size_t i = 0; i < n_v; i++) {
                    // triangle of pts[i], pts[i+1] and face_center
                    quadra_T = integrate_tetra(degree, nq,
                            q0, q1, q2, q3, qw,
                            pts[i]->coords(), pts[(i+1)%n_v]->coords(), f->mass_center(), cell_center,
                            tetra_volume(pts[i]->coords(), pts[(i+1)%n_v]->coords(), f->mass_center(), cell_center));

                    quadra_C.insert(quadra_C.begin() + compt_quadra_pts, quadra_T.begin(), quadra_T.end());
                    compt_quadra_pts += nq;
                }

            } // shape of face
        } // loop over faces

        return quadra_C;

    } // shape of cell

} // integrate_volume


namespace MyQuadra {
    /**
     * The function "integrate" constructs all the quadrature points 
     * of one element : a 3D Cell
    */
    inline std::vector<QuadraturePoint<3>*> integrate(const MyMesh::Cell<3>* cell, const int degree) {

#ifdef DEBUG
        if(degree > 14) {
            throw " Quadrature rule for 3D cell of degree higher than 14. ";
        }
#endif

        if(degree <= 0) {
            return integrate_degree0<3>(cell);
        }

        return integrate_volume(cell, degree);

    }

} // MyQuadra namespace


#endif /* QUAD_VOLUME_HPP */
