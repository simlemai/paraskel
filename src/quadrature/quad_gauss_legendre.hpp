#ifndef QUAD_GAUSS_LEGENDRE_HPP
#define QUAD_GAUSS_LEGENDRE_HPP

#include "mesh.hpp"

#ifndef QUADRATURES_HPP
    throw " You must NOT include this file. Include quadratures.hpp"
#endif

/**
 * get the points and weights of the Gauss Legendre quadrature
 * on the reference 1D element [-1, 1]
*/
template<size_t DIM>
static inline std::vector<MyQuadra::QuadraturePoint<DIM>*> gauss_legendre(size_t degree) {

    size_t npts(std::ceil((degree + 1)/ 2.0));

    std::vector<MyQuadra::QuadraturePoint<DIM>*> ret;
    ret.reserve(npts);

    Eigen::ArrayXd qp(Eigen::ArrayXd::Zero(DIM));
    double qw(0.), a1(0.), a2(0.);
    MyQuadra::QuadraturePoint<DIM>* quadra_pt;
    
    switch (npts) {
        case 1:
            // qp remains at 0 because middle of [-1, 1]
            qw = 2.0;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            return ret;

        case 2:
            qp[0] = 1.0 / std::sqrt(3.0);
            qw = 1.0;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            return ret;

        case 3:
            // for one point qp remains at 0
            qw = 8.0 / 9.0;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);

            qp[0] = std::sqrt(3.0 / 5.0);
            qw = 5.0 / 9.0;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            return ret;

        case 4:
            a1 = 15;
            a2 = 2.0 * std::sqrt(30.0);

            qp = std::sqrt((a1 - a2) / 35.0);
            qw = (18.0 + std::sqrt(30.0)) / 36.0;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = std::sqrt((a1 + a2) / 35.0);
            qw = (18.0 - std::sqrt(30.0)) / 36.0;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            return ret;

        case 5:
            // for one point qp remains at 0
            qw = 128.0 / 225.0;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            a1 = 5.0;
            a2 = 2.0 * std::sqrt(10.0 / 7.0);
            qp = std::sqrt(a1 - a2) / 3.0;
            qw = (322 + 13.0 * std::sqrt(70.0)) / 900.0;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = std::sqrt(a1 + a2) / 3.0;
            qw = (322 - 13.0 * std::sqrt(70.0)) / 900.0;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));
            return ret;

        case 6:
            qp = 0.9324695142031521;
            qw = 0.1713244923791704;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.6612093864662645;
            qw = 0.3607615730481386;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.2386191860831969;
            qw = 0.4679139345726910;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));
            return ret;

        case 7:
            // for one point qp remains at 0
            qw = 0.4179591836734694;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.9491079123427585;
            qw = 0.1294849661688697;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.7415311855993945;
            qw = 0.2797053914892766;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.4058451513773972;
            qw = 0.3818300505051189;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));
            return ret;

        case 8:
            qp = 0.9602898564975363;
            qw = 0.1012285362903763;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.7966664774136267;
            qw = 0.2223810344533745;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.5255324099163290;
            qw = 0.3137066458778873;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.1834346424956498;
            qw = 0.3626837833783620;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));
            return ret;

        case 9:
            // for one point qp remains at 0
            qw = 0.3302393550012598;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.9681602395076261;
            qw = 0.0812743883615744;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.8360311073266358;
            qw = 0.1806481606948574;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.6133714327005904;
            qw = 0.2606106964029354;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.3242534234038089;
            qw = 0.3123470770400029;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));
            return ret;

        case 10:
            qp = 0.9739065285171717;
            qw = 0.0666713443086881;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.8650633666889845;
            qw = 0.1494513491505806;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.6794095682990244;
            qw = 0.2190863625159820;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.4333953941292472;
            qw = 0.2692667193099963;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.1488743389816312;
            qw = 0.2955242247147529;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));
            return ret;

        case 11:
            // for one point qp remains at 0
            qw = 0.2729250867779006;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.9782286581460570;
            qw = 0.0556685671161737;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.8870625997680953;
            qw = 0.1255803694649046;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.7301520055740494;
            qw = 0.1862902109277343;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.5190961292068118;
            qw = 0.2331937645919905;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));

            qp = 0.2695431559523450;
            qw = 0.2628045445102467;
            quadra_pt = new MyQuadra::QuadraturePoint<DIM>(qp,qw);
            ret.push_back(quadra_pt);
            qp *= -1;
            ret.push_back(new MyQuadra::QuadraturePoint<DIM>(qp, qw));
            return ret;

        default:
            throw "Can't integrate 1D element to degree ";
            return ret;
    }
} // gauss_legendre

/**
 * Return a QuadraturePoint vector with 
 * the Gauss Legendre points and weights 
 * to be able to integrate over the 1D element
 * with a certain degree.
*/
template<size_t DIM, typename ELEMT>
static inline std::vector<MyQuadra::QuadraturePoint<DIM>*> 
integrate_1D_element(const ELEMT element, size_t degree) {

    // get the points and weights of the Gauss Legendre quadrature
    // on the reference 1D element [-1, 1]
    const std::vector<MyQuadra::QuadraturePoint<DIM>*> qps(gauss_legendre<DIM>(degree));

    // get the 2 vertices of the 1D element
    const std::vector<MyMesh::Vertex<DIM>*> pts(element->get_vertices()); 

    const double meas(element->measure());

    std::vector<MyQuadra::QuadraturePoint<DIM>*> ret;
    ret.reserve(qps.size());

    for (auto& qp : qps)
    {
        // in the following, we need the x coordinate 
        // of the quadrature point qp because it contains 
        // its coordinate in the reference edge [-1,1]
        const double t(qp->point()[0]); 
        // 1D element, so a line ... living in a 2D or 3D world
        Eigen::ArrayXd p( 0.5 * (1 - t) * pts[0]->coords() 
                                + 0.5 * (1 + t) * pts[1]->coords() );
        const double w(qp->weight() * meas * 0.5);

        ret.push_back(new MyQuadra::QuadraturePoint<DIM>(p,w));
    }

    return ret;
} // integrate_1D_element

namespace MyQuadra {

    // ---------------------- 2D ----------------------------------------
    /**
     * The function "integrate" constructs all the quadrature points 
     * of one element : a 2D Face (edge)
    */
    inline std::vector<QuadraturePoint<2>*> integrate(const MyMesh::Face<2>* face, const int degree) {

        return integrate_1D_element<2>(face, std::max(degree, 0));
    }


    // ---------------------- 3D ----------------------------------------
    /**
     * The function "integrate" constructs all the quadrature points 
     * of one element : a 3D Edge
    */
    inline std::vector<QuadraturePoint<3>*> integrate(const MyMesh::Edge<3>* edge, const int degree) {
    
        return integrate_1D_element<3>(edge, std::max(degree, 0));
    }
    /**
     * 2D Edge does not exist, but need to define an integrate fonction 
     * with 2D edge for compilation generality
    */
    inline std::vector<QuadraturePoint<2>*> integrate(const MyMesh::Edge<2>* edge, const int degree) {
        std::vector<QuadraturePoint<2>*> qp;
        throw " integrate is called over 2D Edge ";
        return qp;
    }

} // MyQuadra namespace



#endif /* QUAD_GAUSS_LEGENDRE_HPP */
