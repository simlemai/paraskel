#ifndef COMMON_IO_HPP
#define COMMON_IO_HPP

#include <iostream>
#include <unistd.h>
#include <ctype.h>
#include <stdio.h>
#include <cstring>
#include <sys/stat.h>
/**
 * Process the arguments given at the execution
 * ./exec -m full_meshfile -v (full_output_file)
 * 
 * @param argc 
 * @param argv 
 * @param dimension dimension of the geometry (2 or 3)
 * @param bulk_degree degree of the bulk unknowns
 * @param skeletal_degree degree of the skeletal unknowns
 * @param full_meshfile full name of the file containing the mesh (careful with the extension) 
 * @param output_vtk_directory vtk output directory (is created if it does not exist)
 * @param output_vtk_meshfile name (without dir) of the vtk output file
 * @param output_vtk_numsol_file name (without dir) of the vtk numerical solution output file
 * @param output_vtk_exactsol_file name (without dir) of the vtk exact solution output file
 * @param output_tex_directory tex output directory (is created if it does not exist)
 * @param output_tex_meshfile name (without dir) of the tex output file
 * @param output_error_directory error output directory (is created if it does not exist)
 * @param output_error_textfile name (without dir) of the textfile with some characteristic numbers
 * @param output_solver_textfile name (without dir) of the textfile with some solver info
*/
void parse_execution_options(int argc, char** argv, 
        int dimension,
        int& bulk_degree,
        int& skeletal_degree,
        std::string& full_meshfile,
        std::string& output_vtk_directory,
        std::string& output_vtk_meshfile,
        std::string& output_vtk_numsol_file,
        std::string& output_vtk_exactsol_file,
        std::string& output_tex_directory,
        std::string& output_tex_meshfile,
        std::string& output_error_directory,
        std::string& output_error_textfile,
        std::string& output_solver_textfile);

/**
 * From a string containing path/filename.extension,
 * extract the path and the filename.
 * @param full_name string containing path/filename.extension
 * @param path extract the path
 * @param filename extract the filename
*/
void extract_path_name(std::string const full_name,
            std::string& path,
            std::string& filename);

/**
 * Print all possible options
*/
void print_help();

/**
 * write in textfile some characteristic numbers
 * @param directory directory name (is created if it does not exist)
 * @param textfile filename (is created if it does not exist, otherwise append values)
 * @param cv_order convergence order of the method
 * @param meshsize diameter of the mesh
 * @param solver_time time to solve the global linear system (in second)
 * @param total_time total time of the simulation (in second)
 * @param n_cells number of cells in mesh
 * @param n_dof number of degrees of freedom in the resolution
 * @param l2_error l2 error between the numerical sol and the exact sol
 * @param h1_error h1 error between the numerical sol and the exact sol
*/
void output_simu_characteristic_num(
        std::string& directory,
        std::string& textfile,
        const int cv_order,
        const double meshsize,
        const double solver_time,
        const double total_time,
        const size_t n_cells,
        const size_t n_dof,
        const double l2_error,
        const double h1_error);

/**
 * write in textfile some informations about the solver
 * @param directory directory name (is created if it does not exist)
 * @param textfile filename (is created if it does not exist, otherwise append values)
 * @param cv_order convergence order of the method
 * @param meshsize diameter of the mesh
 * @param solver_time time to solve the linear system (in second)
 * @param n_cells number of cells in mesh
 * @param n_dof number of degrees of freedom in the resolution
 * @param n_iterations number of iterations
 * @param error estimation error
*/
void output_simu_solver(
        std::string& directory,
        std::string& textfile,
        const int cv_order,
        const double meshsize,
        const double solver_time,
        const size_t n_cells,
        const size_t n_dof,
        const size_t n_iterations,
        const double error);


/////////////////////////////////////////////////////////////////////////
//                   Implementations
/////////////////////////////////////////////////////////////////////////

void parse_execution_options(int argc, char** argv, 
        int dimension,
        int& bulk_degree,
        int& skeletal_degree,
        std::string& full_meshfile,
        std::string& output_vtk_directory,
        std::string& output_vtk_meshfile,
        std::string& output_vtk_numsol_file,
        std::string& output_vtk_exactsol_file,
        std::string& output_tex_directory,
        std::string& output_tex_meshfile,
        std::string& output_error_directory,
        std::string& output_error_textfile,
        std::string& output_solver_textfile) {

    std::string full_output_file;
    bool default_vtk_output(false), default_tex_output(false),
            default_error_output(false);

    // deal with option arguments given when executing the file
    // ./exec -m full_input_mesh_filename 
    //        -s skeletal_degree_value
    //        -b bulk_degree_value
    //        -v (optional: full_output_vtk_filename) 
    //        -t (optional: full_output_tex_filename)
    //        -e (optional: full_output_filename for error and solver info)
    // -h print help message
    // -D print the dimension
    //
    // only "-m full_input_mesh_filename" is mandatory,
    // the following formats of the meshfile are allowed 
    // (the extensions are important):
    //      in 2D : gmsh "*.msh" or the FVCA5 format "*.typ1" or "*.typ2"
    //      in 3D : gmsh "*.msh" or the FVCA6 format "*.typ6".
    char optstring[]=":hDm:s:b:v:t::e:";
    int c_arg = getopt(argc, argv, optstring);
    if(c_arg == -1) c_arg = 'h'; // if no argument, print help
    do { // read and process arguments
        switch (c_arg) {
            case 'h':
                print_help();
                throw " Code is aborted. ";
                return;
            case 'D':
                std::cout << "   ----------------------------------------------" << std::endl;
                std::cout << "                     Dimension is " << dimension << std::endl;
                std::cout << "   ----------------------------------------------" << std::endl;
                break;
            case 'm':
                full_meshfile = optarg;
                break;
            case 's':
                skeletal_degree = atoi(optarg);
                std::cout << "   ----------------------------------------------" << std::endl;
                std::cout << "       Skeletal degree is fixed at " << skeletal_degree << std::endl;
                std::cout << "   ----------------------------------------------" << std::endl;
                break;
            case 'b':
                bulk_degree = atoi(optarg);
                std::cout << "   ----------------------------------------------" << std::endl;
                std::cout << "       Bulk degree is fixed at " << bulk_degree << std::endl;
                std::cout << "   ----------------------------------------------" << std::endl;
                break;
            case 'v':
                if(optarg[0] == '-') {
                    std::cout << " Not good comprehension of arguments, " << optarg <<
                        " is considered as the filename for vtk visualization. " << std::endl;
                    throw "";
                    return;
                }
                full_output_file = optarg;
                // extract path and filename from full name
                extract_path_name(full_output_file, output_vtk_directory, output_vtk_meshfile);
                output_vtk_directory += "/";
                output_vtk_numsol_file = output_vtk_meshfile + "_num_sol.vtu";
                output_vtk_exactsol_file = output_vtk_meshfile + "_exact_sol.vtu";
                output_vtk_meshfile += ".vtu";
                break;
            case 't':
                if(optarg[0] == '-') {
                    std::cout << " Not good comprehension of arguments, " << optarg <<
                        " is considered as the filename for error and solver informations. " << std::endl;
                    throw "";
                    return;
                }
                full_output_file = optarg;
                // extract path and filename from full name
                extract_path_name(full_output_file, output_tex_directory, output_tex_meshfile);
                output_tex_directory += "/";
                output_tex_meshfile += ".tikz";
                break;
            case 'e':
                if(optarg[0] == '-') {
                    std::cout << " Not good comprehension of arguments, " << optarg <<
                        " is considered as the filename for tex visualization. " << std::endl;
                    throw "";
                    return;
                }
                // write some characteristic numbers in textfile
                full_output_file = optarg;
                // extract path and filename from full name
                extract_path_name(full_output_file, output_error_directory, output_error_textfile);
                output_error_directory += "/";
                output_solver_textfile = output_error_textfile + "_solver.dat";
                output_error_textfile += ".dat";
                break;
            case ':': // missing argument
                if (optopt == 'm') {
                    throw "Option -m requires the meshfile name (with full path) as argument, type ./exec -h to print help.";
                    return;
                } else if (optopt == 'v') {
#ifdef DEBUG
                    std::cout << "Option -v has been passed without argument, default vtk output is used." << std::endl;
#endif
                    default_vtk_output = true;
                    break;
                } else if (optopt == 't') {
#ifdef DEBUG
                    std::cout << "Option -t has been passed without argument, default tex output is used." << std::endl;
#endif
                    default_tex_output = true;
                    break;
                } else if (optopt == 'e') {
#ifdef DEBUG
                    std::cout << "Option -e has been passed without argument, default error and solver textfiles output is used." << std::endl;
#endif
                    default_error_output = true;
                    break;
                } else {
                    throw " Unexplained missing argument in parse_execution_options ";
                    return;
                }
            case '?': // unrecognized option
                if (std::isprint (optopt)) { // check if optopt is printable
                    std::cerr << "Unknown option " << (char)optopt << ", avoid it. "<< std::endl;
                    break;
                }
            default:
                throw "Error while reading arguments. Type ./exec -h to print help. ";
                return;
        } // end of switch and while
    }
    while ( (c_arg = getopt(argc, argv, optstring)) != -1 );


    if(default_vtk_output) {
        // extract path and filename from mesh name
        std::string path, mesh_file;
        extract_path_name(full_meshfile, path, mesh_file);
        // extract executable name
        std::string exestring(argv[0]); // find executable name and convert into string
        size_t found(exestring.find_last_of("/\\"));
        std::string exename(exestring.substr(found+1)); // keep everything after the last "/"

        output_vtk_directory = path + "/paraskel_output/";
        // filenames = executable name + mesh name + skeletal degree + extension
        output_vtk_numsol_file = exename + "_ks" + std::to_string(skeletal_degree) 
            + "_" + mesh_file + "_num_sol.vtu";
        output_vtk_exactsol_file = exename + "_ks" + std::to_string(skeletal_degree) 
            + "_" + mesh_file + "_exact_sol.vtu";
        output_vtk_meshfile = mesh_file + ".vtu";
    }
    if(default_tex_output) {
        // extract path and filename from mesh name
        std::string path, mesh_file;
        extract_path_name(full_meshfile, path, mesh_file);
        output_tex_directory = path + "/paraskel_output_visu/";
        output_tex_meshfile = mesh_file + ".tikz";
    }
    if(default_error_output) {
        // extract path and filename from mesh name
        std::string path, mesh_file;
        extract_path_name(full_meshfile, path, mesh_file);
        // extract executable name
        std::string exestring(argv[0]); // find executable name and convert into string
        size_t found(exestring.find_last_of("/\\"));
        std::string exename(exestring.substr(found+1)); // keep everything after the last "/"

        output_error_directory = path + "/paraskel_cv_info/";
        found = mesh_file.find_last_of("_\\"); // mesh name is usualy like mymesh_1, mymesh_2, ...
        mesh_file = mesh_file.substr(0,found); // keep everything before the last "_"
        output_error_textfile = exename + "_ks" + std::to_string(skeletal_degree) 
            + "_" + mesh_file + ".dat";
        output_solver_textfile = exename + "_ks" + std::to_string(skeletal_degree) 
            + "_" + mesh_file + "_solver.dat";
    }

    return;
} // parse_execution_options

void extract_path_name(std::string const full_name,
            std::string& path,
            std::string& filename) {

    if(full_name.empty()) {
        throw "Could not extract path and filename because global name is empty. ";
        return;
    }
    
    size_t found;
    found=full_name.find_last_of("/\\");
    path = full_name.substr(0,found); // keep everything before the last "/"
    filename = full_name.substr(found+1); // keep everything after the last "/", the extension is still present
    found=filename.find_last_of(".\\");
    filename = filename.substr(0,found); // keep everything before the last "."

    return;
} // extract_path_name

void print_help() {

    std::cout << std::endl;
    std::cout << " To execute this code: " << std::endl;
    std::cout << "      \"-D\" to print the dimension, " << std::endl;
    std::cout << "      \"-m path/meshfile\" to provide mesh file, " << std::endl;
    std::cout << "      \"-v (output_path/vtk_file)\"  to write a VTK file for visualization" << std::endl;
    std::cout << "                 (directory will be created if it does not exist," << std::endl;
    std::cout << "                 and default filename is used if no name is provided), " << std::endl;
    std::cout << "      \"-t (output_path/tex_file)\" to write a TEX file (tikz format), " << std::endl;
    std::cout << "      \"-e (output_path/characteristic_num_file)\" to write a textfile with characteristic numbers (norm of error,...), " << std::endl;
    std::cout << std::endl;

    return;
} // print_help

void output_simu_characteristic_num(
        std::string& directory,
        std::string& textfile,
        const int cv_order,
        const double meshsize,
        const double solver_time,
        const double total_time,
        const size_t n_cells,
        const size_t n_dof,
        const double l2_error,
        const double h1_error) {

    // convert string to char to check if directory already exists
    char tmp_dir[directory.length()+1];
    strcpy(tmp_dir,directory.c_str()); 

    // check if directory exists, if not create it
    if( access(tmp_dir, 0) != 0 ) {
        if( mkdir(directory.c_str(), S_IRWXU) != 0) { // Read + Write + Execute for User
            throw "Could not access nor create output directory for characteristic numbers. ";
        } 
        std::cout << " Create output directory " << directory << std::endl;
    }

    // check if file exists, if not, open file and add header
    struct stat buffer;  
    FILE* output_file; 
    if(stat ((directory+textfile).c_str(), &buffer) != 0) {
        output_file = fopen((directory+textfile).c_str(), "w");

        // add legend
        fprintf(output_file, "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n", 
                "cv_order", "meshsize", "solver_time", "tot_execution_time", "n_cell", 
                "n_DOFs", "L2error",  "H1error");
    } else {
        // open file to append values at end of file
        output_file = fopen((directory+textfile).c_str(), "a");
    }

    fprintf(output_file, "%i\t%e\t%e\t%e\t%lu\t%lu\t%e\t%e\n", 
        cv_order, meshsize, solver_time, total_time, n_cells, n_dof, l2_error,  h1_error);

#ifdef DEBUG
    if(output_file == NULL) {
        throw "Could not open output file for characteristic numbers. ";
    }
#endif

    // close file
    fclose(output_file);

    return;
} // output_simu_characteristic_num

void output_simu_solver(
        std::string& directory,
        std::string& textfile,
        const int cv_order,
        const double meshsize,
        const double solver_time,
        const size_t n_cells,
        const size_t n_dof,
        const size_t n_iterations,
        const double estimated_error) {

    // convert string to char to check if directory already exists
    char tmp_dir[directory.length()+1];
    strcpy(tmp_dir,directory.c_str()); 

    // check if directory exists, if not create it
    if( access(tmp_dir, 0) != 0 ) {
        if( mkdir(directory.c_str(), S_IRWXU) != 0) { // Read + Write + Execute for User
            throw "Could not access nor create output directory for characteristic numbers. ";
        } 
        std::cout << " Create output directory " << directory << std::endl;
    }

    struct stat buffer;  
    FILE* output_file; 
    // check if file exists, if not, create file and add header
    if(stat ((directory+textfile).c_str(), &buffer) != 0) {
        output_file = fopen((directory+textfile).c_str(), "w");

        // add legend
        fprintf(output_file, "%s\t%s\t%s\t%s\t%s\t%s\t%s\n", 
            "cv_order", "meshsize", "solver_time", "n_cell", 
            "n_DOFs", "n_iterations", "estimated_error");
    } else {
        // open file to append values at end of file
        output_file = fopen((directory+textfile).c_str(), "a");
    }

    // append values
    fprintf(output_file, "%i\t%e\t%e\t%lu\t%lu\t%lu\t%e\n", 
        cv_order, meshsize, solver_time, n_cells, n_dof, n_iterations, estimated_error);

#ifdef DEBUG
    if(output_file == NULL) {
        throw "Could not open output file to write solver informations. ";
    }
#endif

    // close file
    fclose(output_file);

    return;
} // output_simu_solver

#endif /* COMMON_IO_HPP */
