#ifndef COMMON_GEOMETRY_HPP
#define COMMON_GEOMETRY_HPP

#include <iostream>
#include <unistd.h>
#include <ctype.h>
#include <Eigen/Dense>

/**
 * Compute the measure and mass center of a triangle.
 * 
 * @param p1 1st point's coordinates
 * @param p2 2nd point's coordinates
 * @param p3 3rd point's coordinates
 * @param surface surface of the triangle
 * @param mass_center mass_center of the triangle
*/
template<size_t DIM>
inline void build_surface_and_mass_center_triangle(
                const Eigen::ArrayXd& p1, const Eigen::ArrayXd& p2, const Eigen::ArrayXd& p3, 
                double& surface, Eigen::ArrayXd& mass_center);

/**
 * Compute the measure and mass center of any polygon
 * using build_surface_and_mass_center_triangle.
 * 
 * @param np number of points in the surface
 * @param p_coords all points' coordinates
 * @param surface surface
 * @param mass_center mass_center of the surface
*/
template<size_t DIM>
inline void build_surface_and_mass_center_polygon(
                size_t np, std::vector<Eigen::ArrayXd>& p_coords,
                double& surface, Eigen::ArrayXd& mass_center);

/**
 * Compute the surface of a triangle.
 * 
 * @param p1 1st point's coordinates
 * @param p2 2nd point's coordinates
 * @param p3 3rd point's coordinates
 * @return surface of the triangle
*/
template<size_t DIM>
inline double triangle_surface(
                const Eigen::ArrayXd& p1, const Eigen::ArrayXd& p2, const Eigen::ArrayXd& p3);

/**
 * Compute the surface of a polygon (2D object) living in a 2D or 3D geometry
 * 
 * @param vertices_coords list of all points' coordinates
 * @param centroid tesselation to decompose the polygon into triangles using this central point
*/
template<size_t DIM>
inline double polygon_surface(const std::vector<Eigen::ArrayXd>& vertices_coords, 
                const Eigen::ArrayXd& centroid);

/**
 * Volume of a tetrahedron (only in 3D)
 * @param p0 coordinates of 1st vertex
 * @param p1 coordinates of 2nd vertex
 * @param p2 coordinates of 3rd vertex
 * @param p3 coordinates of 4th vertex
 * @return volume of the tetrahedron
*/
inline double tetra_volume(const Eigen::ArrayXd& p0, 
                           const Eigen::ArrayXd& p1, 
                           const Eigen::ArrayXd& p2, 
                           const Eigen::ArrayXd& p3);

/**
 * Diameter of an element knowning the coords of it's vertices
 * (max distance between the vertices)
 * @param vertices_coords coordinates of all vertices of the element
 * @return diameter of the element
*/
inline double element_diameter(const std::vector<Eigen::ArrayXd>& vertices_coords);

/**
 * compute the outward normal to the object
 * wrt the centroid
 * 2 or 3 vertices coordinates are necessary
 * depending on DIM
 * @param vertices_coords list of vertices' coordinates
 * @param centroid point to decide the orientation of the normal
 */
inline Eigen::Vector2d outward_normal(const std::vector<Eigen::Array2d>& vertices_coords, 
                const Eigen::Array2d& centroid);
inline Eigen::Vector3d outward_normal(const std::vector<Eigen::Array3d>& vertices_coords, 
                const Eigen::Array3d& centroid);


    /////////////////////////////////////////////////////////////////////////
    //                   Implementations
    /////////////////////////////////////////////////////////////////////////

template<size_t DIM>
inline void build_surface_and_mass_center_triangle(
                const Eigen::ArrayXd& p1, const Eigen::ArrayXd& p2, const Eigen::ArrayXd& p3, 
                double& surface, Eigen::ArrayXd& mass_center) {

    surface = triangle_surface<DIM>(p1, p2, p3);
    mass_center = (p1 + p2 + p3) / 3.;
    return;
} // build_surface_and_mass_center_triangle

template<size_t DIM>
inline void build_surface_and_mass_center_polygon(
                size_t np, std::vector<Eigen::ArrayXd>& p_coords,
                double& surface, Eigen::ArrayXd& mass_center) {

    if(np == 3) { // polygon is a triangle
        build_surface_and_mass_center_triangle<DIM>(p_coords[0], p_coords[1], p_coords[2],
                surface, mass_center);
        return;

    } else if (np == 4) { // polygon is a rectangle, cut in 2 triangles
        Eigen::ArrayXd tri_mass_center;
        double tri_surface;
        // triangle of vertices_coords[0], vertices_coords[1] and vertices_coords[2]
        // compute surface and mass center
        build_surface_and_mass_center_triangle<DIM>(p_coords[0], p_coords[1], p_coords[2],
            surface, tri_mass_center);
        // add contribution to the polygon surface and mass center
        mass_center = surface * tri_mass_center;

        // triangle of pts[0], pts[2] and pts[3]
        // compute surface and mass center
        build_surface_and_mass_center_triangle<DIM>(p_coords[0], p_coords[2], p_coords[3],
            tri_surface, tri_mass_center);
        // add contribution to the polygon surface and mass center
        surface += tri_surface;
        mass_center += tri_surface * tri_mass_center;
        mass_center /= surface;

        return;

    } else { // any polygon
        // Split the polygon into a set of triangles using the centroid
        Eigen::ArrayXd centroid(Eigen::ArrayXd::Zero(DIM));
        for(auto& coords : p_coords) {
            centroid += coords;
        }
        centroid /= np;
        // compute the surface and the mass center for each triangle
        mass_center = Eigen::ArrayXd::Zero(DIM);
        surface = 0;
        // for each triangle with 2 points and the centroid
        for(size_t i = 0; i < np; i++) {
            Eigen::ArrayXd tri_mass_center;
            double tri_surface;
            // compute surface and mass center
            build_surface_and_mass_center_triangle<DIM>(centroid, p_coords[i], p_coords[(i+1)%np],
                tri_surface, tri_mass_center);
            // add contribution to the face surface and mass center
            mass_center += tri_surface * tri_mass_center;
            surface += tri_surface;
        }
        mass_center /= surface;
        return;
    }

} // build_surface_and_mass_center_polygon

// 2D
template<>
inline double triangle_surface<2>(
                const Eigen::ArrayXd& p1, const Eigen::ArrayXd& p2, const Eigen::ArrayXd& p3) {

    Eigen::Vector2d v1(p2 - p1);
    Eigen::Vector2d v2(p3 - p2);

    return std::abs(v1[0]*v2[1]-v2[0]*v1[1])/2.;
} // triangle_surface

// 3D
template<>
inline double triangle_surface<3>(
                const Eigen::ArrayXd& p1, const Eigen::ArrayXd& p2, const Eigen::ArrayXd& p3) {

    // Area is half the cross product of vectors (p1 p2) and (p2 p3)
    Eigen::Vector3d v1(p2 - p1);
    Eigen::Vector3d v2(p3 - p2);

    return 0.5 * v1.cross(v2).norm(); // cross exists only with 3D objects
} // triangle_surface

template<size_t DIM>
inline double polygon_surface(const std::vector<Eigen::ArrayXd>& vertices_coords, 
                const Eigen::ArrayXd& centroid) {

    size_t n_v(vertices_coords.size());
    double surface(0.);

    if (n_v == 3) { // polygon is a triangle
        surface = triangle_surface<DIM>(vertices_coords[0], vertices_coords[1],
            vertices_coords[2]);

    } else if (n_v == 4) { // polygon is a rectangle, split in 2 triangles
        // triangle of v[0], v[1] and v[2]
        surface = triangle_surface<DIM>(vertices_coords[0], vertices_coords[1],
            vertices_coords[2]);

        // triangle of v[0], v[2] and v[3]
        surface += triangle_surface<DIM>(vertices_coords[0], vertices_coords[2],
            vertices_coords[3]);

    } else { // a polygon
        // Split the face into a set of triangles, and then compute the surface using the mass center
        for(size_t i = 0; i < n_v; i++) {
            surface += triangle_surface<DIM>(vertices_coords[i], vertices_coords[(i+1)%n_v],
                centroid);
        }
    } // n_v 

    return surface;
} // polygon_surface

inline double tetra_volume(const Eigen::ArrayXd& p0, 
                           const Eigen::ArrayXd& p1, 
                           const Eigen::ArrayXd& p2, 
                           const Eigen::ArrayXd& p3) {

  return std::abs((p1[2] - p0[2]) * (p3[1] * p2[0] - p2[1] * p3[0]) +
                  (p2[2] - p0[2]) * (p3[0] * p1[1] - p1[0] * p3[1]) +
                  (p3[2] - p0[2]) * (p1[0] * p2[1] - p2[0] * p1[1]) +
                  (p2[2] - p1[2]) * (p3[1] * p0[0] - p0[1] * p3[0]) +
                  (p3[2] - p1[2]) * (p2[0] * p0[1] - p0[0] * p2[1]) +
                  (p3[2] - p2[2]) * (p0[0] * p1[1] - p1[0] * p0[1])) / 6.0;

} // tetra_volume

inline double element_diameter(const std::vector<Eigen::VectorXd>& vertices_coords) {
    
    double diam(0.);
    size_t n_v(vertices_coords.size());
    for (size_t i0 = 0; i0 < n_v; i0++) {
        for (size_t i1 = i0 + 1; i1 < n_v; i1++) {
            diam = std::max(diam, (vertices_coords[i0] - vertices_coords[i1]).norm());
        }
    }
    return diam;

} // element_diameter

inline Eigen::Vector2d outward_normal(const std::vector<Eigen::Array2d>& vertices_coords, 
                const Eigen::Array2d& centroid) {

    Eigen::Vector2d v0(vertices_coords[0]);
    Eigen::Vector2d v1(vertices_coords[1]);

    // compute a normal vector
    Eigen::Vector2d normal((v1 - v0).y(), -(v1 - v0).x());
    // normalize it
    normal.normalize();
    // and make it outward to the cell
    if (normal.dot(v0-Eigen::Vector2d(centroid)) < 0) normal *= -1;

    return normal;
} // 2D outward_normal

inline Eigen::Vector3d outward_normal(const std::vector<Eigen::Array3d>& vertices_coords, 
                const Eigen::Array3d& centroid) {

    Eigen::Vector3d v0(vertices_coords[0]);
    Eigen::Vector3d v1(vertices_coords[1]);

    if(vertices_coords.size() > 2) { // not an edge

        Eigen::Vector3d v2(vertices_coords[2]);

        Eigen::Vector3d normal((v1 - v0).cross(v2 - v1)); // cross exists only with 3D objects
        // normalize it
        normal.normalize();
        // and make it outward to the cell
        if (normal.dot(v0-Eigen::Vector3d(centroid)) < 0) normal *= -1;

        return normal;

    } else { // compute the unit outward normal vector of an edge in 3D meshes

        Eigen::Vector3d edge(v1 - v0);

        double k( edge.dot(Eigen::Vector3d(centroid) - v0) );
        k /= edge.squaredNorm();

        Eigen::Vector3d normal( k * edge - Eigen::Vector3d(centroid) + v0 );
        // normalize it
        normal.normalize();
        
        return normal;
    }

} // 3D outward_normal


#endif /* COMMON_GEOMETRY_HPP */
