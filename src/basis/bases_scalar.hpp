#ifndef BASES_SCALAR_HPP
#define BASES_SCALAR_HPP

#ifndef BASES_HPP
    throw "You must NOT include this file. Include bases.hpp";
#endif


namespace MyBasis {

    /**
     * This class describes the scaled monomial scalar basis. 
     * Careful : the monomial vector basis call the scalar basis.
    */

    // Generic template for scalar bases.
    template<typename ELEMT>
    class ScaledMonomialScalarBasis : public Basis<ELEMT>
    {
    public:
        /**
         * Default constructor prints error
         */
        ScaledMonomialScalarBasis(const ELEMT element, const size_t degree);
        ~ScaledMonomialScalarBasis() = default;   ///<  Default destructor
    };

    //---------------------------------------------
    //---------------------------------------------

// assert deg > 0 ??????
    /// Specialization for cells in 2D meshes
    template<>
    class ScaledMonomialScalarBasis<MyMesh::Cell<2>*> : public Basis<MyMesh::Cell<2>*>
    {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param degree maximum polynomial degree to be considered
         */
        ScaledMonomialScalarBasis(const MyMesh::Cell<2>* cell, const size_t degree);
        ~ScaledMonomialScalarBasis() = default;   ///<  Default destructor

        /**
         * Evaluate the bases functions of the cell at point x
         * @param x point where to evaluate the bases functions
         */
        Eigen::MatrixXd eval_functions(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the gradients of the bases functions of the cell at point x
         * @param x point where to evaluate the gradients of the bases functions
         */
        std::vector<Eigen::MatrixXd> eval_gradients(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the derivatives of the bases functions of the face at point x
         * WITH RESPECT TO the local coordinate (f'(tau))
         * @param x point where to evaluate the derivatives of the bases functions
         */
        Eigen::MatrixXd eval_derivatives(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the second derivatives of the bases functions of the face at point x
         * WITH RESPECT TO the local coordinate (f''(tau))
         * @param x point where to evaluate the second derivatives of the bases functions
         */
        Eigen::MatrixXd eval_sec_derivatives(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the divergences of the bases functions of the element at point x
         * (is defined for each basis and for each element)
         * @param x point where to evaluate the divergences of the bases functions
         */
        Eigen::RowVectorXd eval_divergences(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the laplacian of the bases functions of the element at point x
         * @param x point where to evaluate the laplacian of the bases functions
         */
        Eigen::MatrixXd eval_laplacians(const Eigen::VectorXd& x) const;

    private:
        std::vector<Eigen::Vector2i> m_powers; ///< powers of each coordinates for all basis function
        Eigen::Vector2d m_bar; ///< barycenter of cell
        double m_scale; ///< scale = 1 / 0.5 / diam
    };

    /// Specialization for faces in 2D meshes (face is a 1D object)
    template<>
    class ScaledMonomialScalarBasis<MyMesh::Face<2>*> : public Basis<MyMesh::Face<2>*> 
    {
    public:
        /**
         * Constructor
         * @param face pointer to the face (is a segment)
         * @param degree maximum polynomial degree to be considered
         */
        ScaledMonomialScalarBasis(const MyMesh::Face<2>* face, const size_t degree);
        ~ScaledMonomialScalarBasis() = default;   ///<  Default destructor

        /**
         * Evaluate the bases functions of the face at point x
         * @param x point where to evaluate the bases functions
         */
        Eigen::MatrixXd eval_functions(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the gradients of the bases functions of the face at point x
         * wrt to the global coordinates (df/dx; df/dy)
         * @param x point where to evaluate the gradients of the bases functions
         */
        std::vector<Eigen::MatrixXd> eval_gradients(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the derivatives of the bases functions of the face at point x
         * WITH RESPECT TO the local coordinate (f'(tau))
         * @param x point where to evaluate the derivatives of the bases functions
         */
        Eigen::MatrixXd eval_derivatives(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the second derivatives of the bases functions of the face at point x
         * WITH RESPECT TO the local coordinate (f''(tau))
         * @param x point where to evaluate the second derivatives of the bases functions
         */
        Eigen::MatrixXd eval_sec_derivatives(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the divergences of the bases functions of the element at point x
         * (is defined for each basis and for each element)
         * @param x point where to evaluate the divergences of the bases functions
         */
        Eigen::RowVectorXd eval_divergences(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the laplacian of the bases functions of the element at point x
         * @param x point where to evaluate the laplacian of the bases functions
         */
        Eigen::MatrixXd eval_laplacians(const Eigen::VectorXd& x) const;

    private:
        Eigen::Vector2d m_bar; ///< barycenter of face
        Eigen::Vector2d m_tangent; ///< normalized tangent of the 1D face
        double m_scale; ///< scale = 1 / 0.5 / diam
    };

    //---------------------------------------------
    //---------------------------------------------

    /// Specialization for cells in 3D meshes
    template<>
    class ScaledMonomialScalarBasis<MyMesh::Cell<3>*> : public Basis<MyMesh::Cell<3>*>
    {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param degree maximum polynomial degree to be considered
         */
        ScaledMonomialScalarBasis(const MyMesh::Cell<3>* cell, const size_t degree);
        ~ScaledMonomialScalarBasis() = default;   ///<  Default destructor

        /**
         * Evaluate the bases functions of the cell at point x
         * @param x point where to evaluate the bases functions
         */
        Eigen::MatrixXd eval_functions(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the gradients of the bases functions of the cell at point x
         * @param x point where to evaluate the gradients of the bases functions
         */
        std::vector<Eigen::MatrixXd> eval_gradients(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the derivatives of the bases functions of the face at point x
         * WITH RESPECT TO the local coordinate (f'(tau))
         * @param x point where to evaluate the derivatives of the bases functions
         */
        Eigen::MatrixXd eval_derivatives(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the second derivatives of the bases functions of the face at point x
         * WITH RESPECT TO the local coordinate (f''(tau))
         * @param x point where to evaluate the second derivatives of the bases functions
         */
        Eigen::MatrixXd eval_sec_derivatives(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the divergences of the bases functions of the element at point x
         * (is defined for each basis and for each element)
         * @param x point where to evaluate the divergences of the bases functions
         */
        Eigen::RowVectorXd eval_divergences(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the laplacian of the bases functions of the element at point x
         * @param x point where to evaluate the laplacian of the bases functions
         */
        Eigen::MatrixXd eval_laplacians(const Eigen::VectorXd& x) const;

    private:
        std::vector<Eigen::Vector3i> m_powers; ///< powers of each coordinates for all basis function
        Eigen::Vector3d m_bar; ///< barycenter of cell
        double m_scale; ///< scale = 1 / 0.5 / diam
    };

    /// Specialization for faces in 3D meshes
    template<>
    class ScaledMonomialScalarBasis<MyMesh::Face<3>*> : public Basis<MyMesh::Face<3>*>
    {
    public:
        /**
         * Constructor
         * @param face pointer to the face
         * @param degree maximum polynomial degree to be considered
         */
        ScaledMonomialScalarBasis(const MyMesh::Face<3>* face, const size_t degree);
        ~ScaledMonomialScalarBasis() = default;   ///<  Default destructor

        /**
         * Evaluate the bases functions of the face at point x
         * @param x point where to evaluate the bases functions
         */
        Eigen::MatrixXd eval_functions(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the gradients of the bases functions of the face at point x
         * @param x point where to evaluate the gradients of the bases functions
         */
        std::vector<Eigen::MatrixXd> eval_gradients(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the derivatives of the bases functions of the face at point x
         * WITH RESPECT TO the local coordinate (f'(tau))
         * @param x point where to evaluate the derivatives of the bases functions
         */
        Eigen::MatrixXd eval_derivatives(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the second derivatives of the bases functions of the face at point x
         * WITH RESPECT TO the local coordinate (f''(tau))
         * @param x point where to evaluate the second derivatives of the bases functions
         */
        Eigen::MatrixXd eval_sec_derivatives(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the divergences of the bases functions of the element at point x
         * (is defined for each basis and for each element)
         * @param x point where to evaluate the divergences of the bases functions
         */
        Eigen::RowVectorXd eval_divergences(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the laplacian of the bases functions of the element at point x
         * @param x point where to evaluate the laplacian of the bases functions
         */
        Eigen::MatrixXd eval_laplacians(const Eigen::VectorXd& x) const;

    private:
        std::vector<Eigen::Vector2i> m_powers; ///< powers of each coordinates for all basis function
        Eigen::Vector3d m_bar; ///< barycenter of face
        Eigen::Vector3d m_e0, m_e1; ///< coordinate axis of a 2D reference system
        double m_scale; ///< scale = 1 / 0.5 / diam
    };

    /// Specialization for edges (they exist only in 3D meshes)
    template<size_t DIM>
    class ScaledMonomialScalarBasis<MyMesh::Edge<DIM>*> : public Basis<MyMesh::Edge<DIM>*>
    {
    public:
        /**
         * Constructor
         * @param edge pointer to the edge (is a segment)
         * @param degree maximum polynomial degree to be considered
         */
        ScaledMonomialScalarBasis(const MyMesh::Edge<DIM>* edge, const size_t degree);
        ~ScaledMonomialScalarBasis() = default;   ///<  Default destructor

        /**
         * Evaluate the bases functions of the edge at point x
         * @param x point where to evaluate the bases functions
         */
        Eigen::MatrixXd eval_functions(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the gradients of the bases functions of the edge at point x
         * @param x point where to evaluate the gradients of the bases functions
         */
        std::vector<Eigen::MatrixXd> eval_gradients(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the derivatives of the bases functions of the edge at point x
         * WITH RESPECT TO the local coordinate (f'(tau))
         * @param x point where to evaluate the derivatives of the bases functions
         */
        Eigen::MatrixXd eval_derivatives(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the second derivatives of the bases functions of the edge at point x
         * WITH RESPECT TO the local coordinate (f''(tau))
         * @param x point where to evaluate the second derivatives of the bases functions
         */
        Eigen::MatrixXd eval_sec_derivatives(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the divergences of the bases functions of the element at point x
         * (is defined for each basis and for each element)
         * @param x point where to evaluate the divergences of the bases functions
         */
        Eigen::RowVectorXd eval_divergences(const Eigen::VectorXd& x) const;

        /**
         * Evaluate the laplacian of the bases functions of the element at point x
         * @param x point where to evaluate the laplacian of the bases functions
         */
        Eigen::MatrixXd eval_laplacians(const Eigen::VectorXd& x) const;

    private:
        Eigen::Vector3d m_bar; ///< barycenter of edge
        Eigen::Vector3d m_tangent; ///< normalized tangent of the edge
        double m_scale; ///< scale = 1 / 0.5 / diam
    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////

    /// general constructor gives error, only specialization are useful
    template<typename ELEMT>
    ScaledMonomialScalarBasis<ELEMT>::ScaledMonomialScalarBasis(const ELEMT element, const size_t degree) {
        throw " Scaled Monomial Scalar Basis not implemented for this element. ";
    }

    //---------------------------------------------
    /// constructor for cells in 2D meshes
    ScaledMonomialScalarBasis<MyMesh::Cell<2>*>::ScaledMonomialScalarBasis(const MyMesh::Cell<2>* cell, const size_t degree) :
    Basis(degree, (degree + 1) * (degree + 2) / 2),
    m_bar(cell->mass_center()),
    m_scale(1. / 0.5 / cell->diam()) {
        /// init m_power with i,j such that i+j = l with 0 <= l <= degree
        m_powers.reserve(m_basis_dimension);
        for (size_t l = 0; l <= degree; l++) {
            for (size_t i = 0; i <= l; i++) {
                m_powers.push_back(Eigen::Vector2i(l-i, i));
            }
        }

    }
    
    // evaluate the basis functions at point x for cells in 2D meshes
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Cell<2>*>::eval_functions(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::VectorXd ret(this->m_basis_dimension);
        const Eigen::Vector2d y((x - m_bar) * m_scale); // transform coordinates to scale
        for(size_t i = 0; i < this->m_basis_dimension; i++) {
            const Eigen::Vector2i powers(m_powers[i]);
            ret(i) = std::pow(y[0], powers[0]) * std::pow(y[1], powers[1]);
        }
        return ret.transpose();
    } // eval_functions

    // evaluate the gradients of the basis functions at point x for cells in 2D meshes
    std::vector<Eigen::MatrixXd> 
    ScaledMonomialScalarBasis<MyMesh::Cell<2>*>::eval_gradients(const Eigen::VectorXd& x) const {
        // there are this->m_basis_dimension basis functions
        std::vector<Eigen::MatrixXd> ret;
        ret.reserve(this->m_basis_dimension);
        // two partial derivatives by basis function
        Eigen::MatrixXd grad_function(1, 2);

        const Eigen::Vector2d y((x - m_bar) * m_scale); // transform coordinates to scale
        // for each basis function
        for(size_t i = 0; i < this->m_basis_dimension; i++) {
            const Eigen::Vector2i powers(m_powers[i]);
            const double d0((powers[0] == 0) ? 0 : powers[0] * m_scale * std::pow(y[0], powers[0]-1));
            const double d1((powers[1] == 0) ? 0 : powers[1] * m_scale * std::pow(y[1], powers[1]-1));
            grad_function(0,0) = d0 * std::pow(y[1], powers[1]);
            grad_function(0,1) = std::pow(y[0], powers[0]) * d1;
            ret.push_back(grad_function);
        }
        return ret;
    } // eval_gradients

    // evaluate the derivatives of the basis functions at point x for cells in 2D meshes
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Cell<2>*>::eval_derivatives(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::RowVectorXd ret(this->m_basis_dimension);

        throw " eval_derivatives called in cells in 2D meshes in ScaledMonomialScalarBasis";
        return ret;
    } // eval_derivatives

    // evaluate the second derivatives of the basis functions at point x for cells in 2D meshes
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Cell<2>*>::eval_sec_derivatives(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::RowVectorXd ret(this->m_basis_dimension);

        throw " eval_sec_derivatives called in cells in 2D meshes in ScaledMonomialScalarBasis";
        return ret;
    } // eval_sec_derivatives

    // evaluate the divergences of the basis functions at point x for cells in 2D meshes
    Eigen::RowVectorXd 
    ScaledMonomialScalarBasis<MyMesh::Cell<2>*>::eval_divergences(const Eigen::VectorXd& x) const {

        // one column by basis function
        Eigen::RowVectorXd ret(this->m_basis_dimension);

        throw " eval_divergences called in ScaledMonomialScalarBasis ";
        return ret;
    } // eval_divergences

    // evaluate the laplacians of the basis functions at point x for cells in 2D meshes
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Cell<2>*>::eval_laplacians(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::RowVectorXd ret(Eigen::RowVectorXd::Zero(this->m_basis_dimension));

        const Eigen::Vector2d y((x - m_bar) * m_scale); // transform coordinates to scale
        const double factor(std::pow(m_scale, 2));
        // for each basis function
        for(size_t i = 0; i < this->m_basis_dimension; i++) {
            const Eigen::Vector2i powers(m_powers[i]);
            // + d2f / dx2
            ret[i] += (powers[0] < 2) ? 0 : 
                powers[0] * (powers[0]-1) * factor * std::pow(y[0], powers[0]-2) * std::pow(y[1], powers[1]);
            // + d2f / dy2
            ret[i] += (powers[1] < 2) ? 0 : 
                powers[1] * (powers[1]-1) * factor * std::pow(y[1], powers[1]-2) * std::pow(y[0], powers[0]);
        }
        return ret;
    } // eval_laplacians

    //---------------------------------------------
    // constructor for faces in 2D meshes (1D objects)
    ScaledMonomialScalarBasis<MyMesh::Face<2>*>::ScaledMonomialScalarBasis(const MyMesh::Face<2>* face, const size_t degree) :
    Basis(degree, degree + 1),
    m_bar(face->mass_center()),
    m_scale(1. / 0.5 / face->diam()) {
        // Fill the tangent with the normalized vector given by the 2 points of the face
        std::vector<MyMesh::Vertex<2> *> vertices(face->get_vertices());
        m_tangent = vertices[1]->coords() - vertices[0]->coords();
        m_tangent /= m_tangent.norm();
    }

    // evaluate functions at point x for faces in 2D meshes
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Face<2>*>::eval_functions(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::VectorXd ret(this->m_basis_dimension);
        // transform coordinates along the segment
        double y((x - m_bar).dot(m_tangent) * m_scale);
        for(size_t i = 0; i < this->m_basis_dimension; i++) {
            ret(i) = std::pow(y, i);
        }
        return ret.transpose();
    } // eval_functions

    // evaluate the gradients of the basis functions at point x for faces in 2D meshes
    std::vector<Eigen::MatrixXd>
    ScaledMonomialScalarBasis<MyMesh::Face<2>*>::eval_gradients(const Eigen::VectorXd& x) const {
        // there are this->m_basis_dimension basis functions
        std::vector<Eigen::MatrixXd> ret;
        ret.reserve(this->m_basis_dimension);

        // transform coordinates along the segment
        double y((x - m_bar).dot(m_tangent) * m_scale);

        // for the constant basis function, grad is null
        ret.push_back(Eigen::RowVectorXd::Zero(2));
        // for each basis function (except constant basis function)
        for(size_t i = 1; i < this->m_basis_dimension; i++) {
            Eigen::RowVectorXd grad_function(i * m_scale * std::pow(y, i-1) * m_tangent.transpose());
            ret.push_back(grad_function);
        }
        return ret;
    } // eval_gradients

    // evaluate the derivatives of the basis functions at point x for faces in 2D meshes
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Face<2>*>::eval_derivatives(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::RowVectorXd ret(this->m_basis_dimension);
        // transform coordinates along the segment
        double y((x - m_bar).dot(m_tangent) * m_scale);
        // for constant basis function
        ret(0) = 0.;
        // for each basis function (except constant basis function)
        for(size_t i = 1; i < this->m_basis_dimension; i++) {
            ret(i) = i * m_scale * std::pow(y, i-1);
        }
        return ret;
    } // eval_derivatives

    // evaluate the second derivatives of the basis functions at point x for faces in 2D meshes
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Face<2>*>::eval_sec_derivatives(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::RowVectorXd ret(Eigen::RowVectorXd::Zero(this->m_basis_dimension));
        // transform coordinates along the segment
        double y((x - m_bar).dot(m_tangent) * m_scale);
        const double factor(std::pow(m_scale, 2));
        // for each basis function when i > 1 (others are already init with 0)
        for(size_t i = 2; i < this->m_basis_dimension; i++) {
            ret(i) = i * (i - 1) * factor * std::pow(y, i-2);
        }
        return ret;
    } // eval_sec_derivatives

    // evaluate the divergences of the basis functions at point x for faces in 2D meshes
    Eigen::RowVectorXd 
    ScaledMonomialScalarBasis<MyMesh::Face<2>*>::eval_divergences(const Eigen::VectorXd& x) const {

        // one value by basis function
        Eigen::RowVectorXd ret(this->m_basis_dimension);

        throw " eval_divergences called in ScaledMonomialScalarBasis ";
        return ret;
    } // eval_divergences

    // evaluate the laplacians of the basis functions at point x for faces in 2D meshes
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Face<2>*>::eval_laplacians(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::RowVectorXd ret(this->m_basis_dimension);

        throw " eval_laplacians called in ScaledMonomialScalarBasis ";
        return ret;
    } // eval_laplacians

    //---------------------------------------------
    // constructor for cells in 3D meshes
    ScaledMonomialScalarBasis<MyMesh::Cell<3>*>::ScaledMonomialScalarBasis(const MyMesh::Cell<3>* cell, const size_t degree) :
// (m_degree + 1) * (m_degree + 2) * (m_degree + 3) / 6 = 
// m_degree * (m_degree + 1) * (2 * m_degree + 1) + 9 * m_degree * (m_degree + 1) + 12 * (m_degree + 1)) / 12
    Basis(degree, (degree + 1) * (degree + 2) * (degree + 3) / 6),
    m_bar(cell->mass_center()),
    m_scale(1. / 0.5 / cell->diam()) {

        // init m_power with i,j,k such that i+j+k = l with 0 <= l <= degree
        m_powers.reserve(m_basis_dimension);
        for (size_t l = 0; l <= degree; l++) {
            for (size_t i = 0; i <= l; i++) {
                for (size_t j = 0; i + j <= l; j++) {
                    m_powers.push_back(Eigen::Vector3i(l-i-j, j, i));
                }
            }
        }
    }

    // evaluate functions at point x for cells in 3D meshes
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Cell<3>*>::eval_functions(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::VectorXd ret(this->m_basis_dimension);
        const Eigen::Vector3d y((x - m_bar) * m_scale); // transform coordinates to scale
        for(size_t i = 0; i < this->m_basis_dimension; i++) {
            const Eigen::Vector3i powers(m_powers[i]);
            ret(i) = std::pow(y[0], powers[0]) * std::pow(y[1], powers[1]) * std::pow(y[2], powers[2]);
        }
        return ret.transpose();
    } // eval_functions

    // evaluate the gradients of the basis functions at point x for cells in 3D meshes
    std::vector<Eigen::MatrixXd>
    ScaledMonomialScalarBasis<MyMesh::Cell<3>*>::eval_gradients(const Eigen::VectorXd& x) const {
        // there are this->m_basis_dimension basis functions
        std::vector<Eigen::MatrixXd> ret;
        ret.reserve(this->m_basis_dimension);
        // three partial derivatives by basis function
        Eigen::MatrixXd grad_function(1, 3);

        const Eigen::Vector3d y((x - m_bar) * m_scale); // transform coordinates to scale
        // for each basis function
        for(size_t i = 0; i < this->m_basis_dimension; i++) {
            const Eigen::Vector3i powers(m_powers[i]);
            const double d0((powers[0] == 0) ? 0 : powers[0] * m_scale * std::pow(y[0], powers[0]-1));
            const double d1((powers[1] == 0) ? 0 : powers[1] * m_scale * std::pow(y[1], powers[1]-1));
            const double d2((powers[2] == 0) ? 0 : powers[2] * m_scale * std::pow(y[2], powers[2]-1));

            grad_function(0,0) = d0 * std::pow(y[1], powers[1]) * std::pow(y[2], powers[2]);
            grad_function(0,1) = std::pow(y[0], powers[0]) * d1 * std::pow(y[2], powers[2]);
            grad_function(0,2) = std::pow(y[0], powers[0]) * std::pow(y[1], powers[1]) * d2;

            ret.push_back(grad_function);
        }
        return ret;
    } // eval_gradients

    // evaluate the derivatives of the basis functions at point x for cells in 3D meshes
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Cell<3>*>::eval_derivatives(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::RowVectorXd ret(this->m_basis_dimension);

        throw " eval_derivatives called in cells in 3D meshes in ScaledMonomialScalarBasis";
        return ret;
    } // eval_derivatives

    // evaluate the second derivatives of the basis functions at point x for cells in 3D meshes
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Cell<3>*>::eval_sec_derivatives(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::RowVectorXd ret(this->m_basis_dimension);
        
        throw " eval_sec_derivatives called in cells in 3D meshes in ScaledMonomialScalarBasis";
        return ret;
    } // eval_sec_derivatives

    // evaluate the divergences of the basis functions at point x for cells in 3D meshes
    Eigen::RowVectorXd 
    ScaledMonomialScalarBasis<MyMesh::Cell<3>*>::eval_divergences(const Eigen::VectorXd& x) const {

        // one column by basis function
        Eigen::RowVectorXd ret(this->m_basis_dimension);

        throw " eval_divergences called in ScaledMonomialScalarBasis ";
        return ret;
    } // eval_divergences

    // evaluate the laplacians of the basis functions at point x for cells in 3D meshes
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Cell<3>*>::eval_laplacians(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::RowVectorXd ret(Eigen::RowVectorXd::Zero(this->m_basis_dimension));

        const Eigen::Vector3d y((x - m_bar) * m_scale); // transform coordinates to scale
        const double factor(std::pow(m_scale, 2));
        // for each basis function
        for(size_t i = 0; i < this->m_basis_dimension; i++) {
            const Eigen::Vector3i powers(m_powers[i]);
            // + d2f / dx2
            ret[i] += (powers[0] < 2) ? 0 : 
                powers[0] * (powers[0]-1) * factor * std::pow(y[0], powers[0]-2) 
                * std::pow(y[1], powers[1]) * std::pow(y[2], powers[2]);
            // + d2f / dy2
            ret[i] += (powers[1] < 2) ? 0 : 
                powers[1] * (powers[1]-1) * factor * std::pow(y[1], powers[1]-2) 
                * std::pow(y[0], powers[0]) * std::pow(y[2], powers[2]);
            // + d2f / dz2
            ret[i] += (powers[2] < 2) ? 0 : 
                powers[2] * (powers[2]-1) * factor * std::pow(y[2], powers[2]-2) 
                * std::pow(y[0], powers[0]) * std::pow(y[1], powers[1]);
        }
        return ret;
    } // eval_laplacians

    //---------------------------------------------
    /// constructor for faces in 3D meshes (2D objects)
    ScaledMonomialScalarBasis<MyMesh::Face<3>*>::ScaledMonomialScalarBasis(const MyMesh::Face<3>* face, const size_t degree) :
    Basis(degree, (degree + 1) * (degree + 2) / 2),
    m_bar(face->mass_center()),
    m_scale(1. / 0.5 / face->diam()) {
        /// It is necessary to create a basis of a 2D reference system.
        // Uses two edges of the face as the coordinate
        // axis of the 2D reference system. Those two edges are accepted 
        // only if they have an angle between them greater than 8 degrees,
        // then they are orthonormalized via Gram–Schmidt.
        const std::vector<MyMesh::Vertex<3> *> vertices(face->get_vertices());
        const size_t n_v(vertices.size());
        bool done(false);

        for(size_t i0 = 1; i0 <= n_v; i0++) {
            const size_t i1((i0 + 1) % n_v);
            const size_t i2((i0 - 1) % n_v);
            const Eigen::Vector3d v0(vertices[i1]->coords() - vertices[i0]->coords());
            const Eigen::Vector3d v1(vertices[i2]->coords() - vertices[i0]->coords());
            // normalize vector to check if angle between them greater than 8 degrees 
            m_e0 = v0 / v0.norm();
            const Eigen::Vector3d v1n(v1 / v1.norm());
            if (m_e0.dot(v1n) < 0.99) { 
                // build m_e1 via Gram-Schmidt projection of v1
                m_e1 = v1 - (v1.dot(v0) * v0) / (v0.dot(v0));
                m_e1 = m_e1 / m_e1.norm();
                done = true; 
                break; 
            }
        }
        if(!done) {
            throw " Degenerate polyhedron, could not build basis for the 3D face";
        }

        /// init m_power with i,j such that i+j = l with 0 <= l <= degree
        m_powers.reserve(m_basis_dimension);
        for (size_t l = 0; l <= degree; l++) {
            for (size_t i = 0; i <= l; i++) {
                m_powers.push_back(Eigen::Vector2i(l-i, i));
            }
        }

    }

    // evaluate functions at point x for faces in 3D meshes
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Face<3>*>::eval_functions(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::VectorXd ret(this->m_basis_dimension);
        // transform the 3D coordinates into the 2D basis of the face
        Eigen::Vector3d xn((x - m_bar) * m_scale);
        Eigen::Vector2d y;
        y << xn.dot(m_e0), xn.dot(m_e1);
        // Evaluate functions 
        for(size_t i = 0; i < this->m_basis_dimension; i++) {
            const Eigen::Vector2i powers(m_powers[i]);
            ret(i) = std::pow(y[0], powers[0]) * std::pow(y[1], powers[1]);
        }
        return ret.transpose();
    } // eval_functions

    // evaluate the gradients of the basis functions at point x for faces in 3D meshes
    std::vector<Eigen::MatrixXd>
    ScaledMonomialScalarBasis<MyMesh::Face<3>*>::eval_gradients(const Eigen::VectorXd& x) const {
        // there are this->m_basis_dimension basis functions
        std::vector<Eigen::MatrixXd> ret;
        ret.reserve(this->m_basis_dimension);
        // three partial derivatives by basis function
        Eigen::MatrixXd grad_function(1, 3);

        // transform the 3D coordinates into the 2D basis of the face
        Eigen::Vector3d xn((x - m_bar) * m_scale);
        Eigen::Vector2d y;
        y << xn.dot(m_e0), xn.dot(m_e1);

        // for each basis function
        for(size_t i = 0; i < this->m_basis_dimension; i++) {
            const Eigen::Vector2i powers(m_powers[i]);

            const double d0((powers[0] == 0) ? 0 : powers[0] * m_scale * std::pow(y[0], powers[0]-1) * std::pow(y[1], powers[1]));
            const double d1((powers[1] == 0) ? 0 : std::pow(y[0], powers[0]) * powers[1] * m_scale * std::pow(y[1], powers[1]-1));

            grad_function(0,0) = m_e0[0] * d0 + m_e1[0] * d1;
            grad_function(0,1) = m_e0[1] * d0 + m_e1[1] * d1;
            grad_function(0,2) = m_e0[2] * d0 + m_e1[2] * d1;

            ret.push_back(grad_function);
        }
        return ret;
    } // eval_gradients

    // evaluate the derivatives of the basis functions at point x for faces in 3D meshes
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Face<3>*>::eval_derivatives(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::RowVectorXd ret(this->m_basis_dimension);

        throw " eval_derivatives called in faces in 3D meshes in ScaledMonomialScalarBasis";
        return ret;
    } // eval_derivatives

    // evaluate the second derivatives of the basis functions at point x for faces in 3D meshes
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Face<3>*>::eval_sec_derivatives(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::RowVectorXd ret(this->m_basis_dimension);
        
        throw " eval_sec_derivatives called in faces in 3D meshes in ScaledMonomialScalarBasis";
        return ret;
    } // eval_sec_derivatives

    // evaluate the divergences of the basis functions at point x for faces in 3D meshes
    Eigen::RowVectorXd 
    ScaledMonomialScalarBasis<MyMesh::Face<3>*>::eval_divergences(const Eigen::VectorXd& x) const {

        // one column by basis function
        Eigen::RowVectorXd ret(this->m_basis_dimension);

        throw " eval_divergences called in ScaledMonomialScalarBasis ";
        return ret;
    } // eval_divergences

    // evaluate the laplacians of the basis functions at point x for faces in 3D meshes
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Face<3>*>::eval_laplacians(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::RowVectorXd ret(Eigen::RowVectorXd::Zero(this->m_basis_dimension));

        // transform the 3D coordinates into the 2D basis of the face
        Eigen::Vector3d xn((x - m_bar) * m_scale);
        Eigen::Vector2d y;
        y << xn.dot(m_e0), xn.dot(m_e1);
        const double factor(std::pow(m_scale, 2));
        Eigen::Vector3d e0_2, e1_2, e01; ///< square of m_e0, m_e1 and product
        for(size_t j = 0; j < 3; j++) {
            e0_2[j] = std::pow(m_e0[j], 2); 
            e1_2[j] = std::pow(m_e1[j], 2); 
            e01[j] = 2. * m_e0[j] * m_e1[j];
        }

        // for each basis function
        for(size_t i = 0; i < this->m_basis_dimension; i++) {
            const Eigen::Vector2i powers(m_powers[i]);

            const double d0_2((powers[0] < 2) ? 0 : 
                powers[0] * (powers[0] - 1) * factor * std::pow(y[0], powers[0]-2) * std::pow(y[1], powers[1]));
            const double d1_2((powers[1] < 2) ? 0 : 
                std::pow(y[0], powers[0]) * powers[1] * (powers[1] - 1) * factor * std::pow(y[1], powers[1]-2));
            const double d01((powers[0] == 0 || powers[1] == 0) ? 0 : 
                factor * powers[0] * std::pow(y[0], powers[0]-1) * powers[1] * std::pow(y[1], powers[1]-1));

            for(size_t j = 0; j < 3; j++) {
                ret[i] += e0_2[j] * d0_2 + e1_2[j] * d1_2 + e01[j] * d01;
            }
        }

        return ret;
    } // eval_laplacians

    //---------------------------------------------
    // constructor for edges (1D objects)
    // (edges exist only in 3D meshes)
    template<size_t DIM>
    ScaledMonomialScalarBasis<MyMesh::Edge<DIM>*>::ScaledMonomialScalarBasis(const MyMesh::Edge<DIM>* edge, const size_t degree) :
    Basis<MyMesh::Edge<DIM>*>(degree, degree + 1),
    m_bar(edge->mass_center()),
    m_scale(1. / 0.5 / edge->diam()) {
        // Fill the tangent with the normalized vector given by the 2 points of the edge
        std::vector<MyMesh::Vertex<DIM> *> vertices(edge->get_vertices());
        m_tangent = vertices[1]->coords() - vertices[0]->coords();
        m_tangent /= m_tangent.norm();
    }

    // evaluate functions at point x for edges
    // (edges exist only in 3D meshes)
    template<size_t DIM>
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Edge<DIM>*>::eval_functions(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::VectorXd ret(this->m_basis_dimension);
        // transform coordinates along the segment
        double y((x - m_bar).dot(m_tangent) * m_scale);
        for(size_t i = 0; i < this->m_basis_dimension; i++) {
            ret(i) = std::pow(y, i);
        }
        return ret.transpose();
    } // eval_functions

    // evaluate the gradients of the basis functions at point x for edges
    // (edges exist only in 3D meshes)
    template<size_t DIM>
    std::vector<Eigen::MatrixXd>
    ScaledMonomialScalarBasis<MyMesh::Edge<DIM>*>::eval_gradients(const Eigen::VectorXd& x) const {
        // there are this->m_basis_dimension basis functions
        std::vector<Eigen::MatrixXd> ret;
        ret.reserve(this->m_basis_dimension);

        // transform coordinates along the segment
        double y((x - m_bar).dot(m_tangent) * m_scale);

        // for the constant basis function, grad is null
        ret.push_back(Eigen::RowVectorXd::Zero(DIM));
        // for each basis function (except constant basis function)
        for(size_t i = 1; i < this->m_basis_dimension; i++) {
            Eigen::RowVectorXd grad_function(i * m_scale * std::pow(y, i-1) * m_tangent.transpose());
            ret.push_back(grad_function);
        }
        return ret;
    } // eval_gradients

    // evaluate the derivatives of the basis functions at point x for edges in 3D meshes
    // (edges exist only in 3D meshes)
    template<size_t DIM>
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Edge<DIM>*>::eval_derivatives(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::RowVectorXd ret(this->m_basis_dimension);
        // transform coordinates along the segment
        double y((x - m_bar).dot(m_tangent) * m_scale);
        // constant basis function
        ret(0) = 0.;
        // for each basis function
        for(size_t i = 1; i < this->m_basis_dimension; i++) {
            ret(i) = i * m_scale * std::pow(y, i-1);
        }
        return ret;
    } // eval_derivatives

    // evaluate the second derivatives of the basis functions at point x for edges in 3D meshes
    // (edges exist only in 3D meshes)
    template<size_t DIM>
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Edge<DIM>*>::eval_sec_derivatives(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::RowVectorXd ret(Eigen::RowVectorXd::Zero(this->m_basis_dimension));
        // transform coordinates along the segment
        double y((x - m_bar).dot(m_tangent) * m_scale);
        const double factor(std::pow(m_scale, 2));
        // for each basis function when i > 1 (others are already init with 0)
        for(size_t i = 2; i < this->m_basis_dimension; i++) {
            ret(i) = i * (i - 1) * factor * std::pow(y, i-2);
        }
        return ret;
    } // eval_sec_derivatives

    // evaluate the divergences of the basis functions at point x for edges
    // (edges exist only in 3D meshes)
    template<size_t DIM>
    Eigen::RowVectorXd 
    ScaledMonomialScalarBasis<MyMesh::Edge<DIM>*>::eval_divergences(const Eigen::VectorXd& x) const {

        // one column by basis function
        Eigen::RowVectorXd ret(this->m_basis_dimension);

        throw " eval_divergences called in ScaledMonomialScalarBasis ";
        return ret;
    } // eval_divergences

    // evaluate the laplacians of the basis functions at point x for edges
    // (edges exist only in 3D meshes)
    template<size_t DIM>
    Eigen::MatrixXd 
    ScaledMonomialScalarBasis<MyMesh::Edge<DIM>*>::eval_laplacians(const Eigen::VectorXd& x) const {
        // one value by basis function
        Eigen::RowVectorXd ret(this->m_basis_dimension);

        throw " eval_laplacians called in ScaledMonomialScalarBasis ";
        return ret;
    } // eval_laplacians


} // MyBasis namespace

#endif /* BASES_SCALAR_HPP */
