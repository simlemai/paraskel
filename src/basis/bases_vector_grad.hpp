namespace MyBasis {

	//---------------------------------------------
	//---------------------------------------------
	/// Gradient of the scaled monomial scalar basis : \nabla P^k_Td(T)
	//---------------------------------------------
	//---------------------------------------------

	/// dimension of the vector basis of polynomials of degree <= k : \nabla P^k_Td(T)
	static size_t grad_vector_basis_dimension(size_t k, size_t Td, size_t dim)
	{
	    size_t num = 1;
	    size_t den = 1;

	    for (size_t i = 1; i <= Td; i++)
	    {
		num *= k + i;
		den *= i;
	    }

	    return num / den - 1;
	}

	/// Specialization for cells in 2D meshes
	template<>
	class ScaledMonomialVectorBasis<MyMesh::Cell<2>*, GRAD> : public Basis<MyMesh::Cell<2>*> 
	{
	public:
	    /**
	     * Constructor
	     * @param cell pointer to the cell
	     * @param degree maximum polynomial degree to be considered
	     */
	    ScaledMonomialVectorBasis(const MyMesh::Cell<2>* cell, const size_t degree);
	    ~ScaledMonomialVectorBasis() = default;   ///<  Default destructor

	    /**
	     * Evaluate the bases functions of the cell at point x
	     * @param x point where to evaluate the bases functions
	     */
	    Eigen::MatrixXd eval_functions(const Eigen::VectorXd& x) const;

	    std::vector<Eigen::MatrixXd> eval_gradients(const Eigen::VectorXd& x) const {
		std::vector<Eigen::MatrixXd> ret;
		throw " eval_gradients is not implemented for this basis ";
		return ret;
	    }

	    Eigen::MatrixXd eval_derivatives(const Eigen::VectorXd& x) const {
		Eigen::MatrixXd ret;
		throw " eval_derivatives is not implemented for this basis ";
		return ret;
	    }

	    Eigen::MatrixXd eval_sec_derivatives(const Eigen::VectorXd& x) const {
		Eigen::MatrixXd ret;
		throw " eval_sec_derivatives is not implemented for this basis ";
		return ret;
	    }

	    Eigen::RowVectorXd eval_divergences(const Eigen::VectorXd& x) const {
		Eigen::RowVectorXd ret;
		throw " eval_divergences is not implemented for this basis ";
		return ret;
	    }

	    Eigen::MatrixXd eval_laplacians(const Eigen::VectorXd& x) const {
		Eigen::MatrixXd ret;
		throw " eval_laplacians is not implemented for this basis ";
		return ret;
	    }


	private:
	    ScaledMonomialScalarBasis<MyMesh::Cell<2>*> m_scalar_basis; ///< scaled monomial scalar basis associated
	};

	/// Specialization for faces in 2D meshes (face is a 1D object)
	template<>
	class ScaledMonomialVectorBasis<MyMesh::Face<2>*, GRAD> : public Basis<MyMesh::Face<2>*>
	{
	public:
	    /**
	     * Constructor
	     * @param face pointer to the face (is a segment)
	     * @param degree maximum polynomial degree to be considered
	     */
	    ScaledMonomialVectorBasis(const MyMesh::Face<2>* face, const size_t degree);
	    ~ScaledMonomialVectorBasis() = default;   ///<  Default destructor

	    /**
	     * Evaluate the bases functions of the face at point x
	     * @param x point where to evaluate the bases functions
	     */
	    Eigen::MatrixXd eval_functions(const Eigen::VectorXd& x) const;

	    std::vector<Eigen::MatrixXd> eval_gradients(const Eigen::VectorXd& x) const {
		std::vector<Eigen::MatrixXd> ret;
		throw " eval_gradients is not implemented for this basis ";
		return ret;
	    }

	    Eigen::MatrixXd eval_derivatives(const Eigen::VectorXd& x) const {
		Eigen::MatrixXd ret;
		throw " eval_derivatives is not implemented for this basis ";
		return ret;
	    }

	    Eigen::MatrixXd eval_sec_derivatives(const Eigen::VectorXd& x) const {
		Eigen::MatrixXd ret;
		throw " eval_sec_derivatives is not implemented for this basis ";
		return ret;
	    }

	    Eigen::RowVectorXd eval_divergences(const Eigen::VectorXd& x) const {
		Eigen::RowVectorXd ret;
		throw " eval_divergences is not implemented for this basis ";
		return ret;
	    }

	    Eigen::MatrixXd eval_laplacians(const Eigen::VectorXd& x) const {
		Eigen::MatrixXd ret;
		throw " eval_laplacians is not implemented for this basis ";
		return ret;
	    }

	private:
	    ScaledMonomialScalarBasis<MyMesh::Face<2>*> m_scalar_basis; ///< scaled monomial scalar basis associated
	};

	//---------------------------------------------
	//---------------------------------------------

	/// Specialization for cells in 3D meshes
	template<>
	class ScaledMonomialVectorBasis<MyMesh::Cell<3>*, GRAD>  : public Basis<MyMesh::Cell<3>*> 
	{
	public:
	    /**
	     * Constructor
	     * @param cell pointer to the cell
	     * @param degree maximum polynomial degree to be considered
	     */
	    ScaledMonomialVectorBasis(const MyMesh::Cell<3>* cell, const size_t degree);
	    ~ScaledMonomialVectorBasis() = default;   ///<  Default destructor

	    /**
	     * Evaluate the bases functions of the cell at point x
	     * @param x point where to evaluate the bases functions
	     */
	    Eigen::MatrixXd eval_functions(const Eigen::VectorXd& x) const;

	    std::vector<Eigen::MatrixXd> eval_gradients(const Eigen::VectorXd& x) const {
		std::vector<Eigen::MatrixXd> ret;
		throw " eval_gradients is not implemented for this basis ";
		return ret;
	    }

	    Eigen::MatrixXd eval_derivatives(const Eigen::VectorXd& x) const {
		Eigen::MatrixXd ret;
		throw " eval_derivatives is not implemented for this basis ";
		return ret;
	    }

	    Eigen::MatrixXd eval_sec_derivatives(const Eigen::VectorXd& x) const {
		Eigen::MatrixXd ret;
		throw " eval_sec_derivatives is not implemented for this basis ";
		return ret;
	    }

	    Eigen::RowVectorXd eval_divergences(const Eigen::VectorXd& x) const {
		Eigen::RowVectorXd ret;
		throw " eval_divergences is not implemented for this basis ";
		return ret;
	    }

	    Eigen::MatrixXd eval_laplacians(const Eigen::VectorXd& x) const {
		Eigen::MatrixXd ret;
		throw " eval_laplacians is not implemented for this basis ";
		return ret;
	    }

	private:
	    ScaledMonomialScalarBasis<MyMesh::Cell<3>*> m_scalar_basis; ///< scaled monomial scalar basis associated
	};

	/// Specialization for faces in 3D meshes
	template<>
	class ScaledMonomialVectorBasis<MyMesh::Face<3>*, GRAD>  : public Basis<MyMesh::Face<3>*> 
	{
	public:
	    /**
	     * Constructor
	     * @param face pointer to the face (is a segment)
	     * @param degree maximum polynomial degree to be considered
	     */
	    ScaledMonomialVectorBasis(const MyMesh::Face<3>* face, const size_t degree);
	    ~ScaledMonomialVectorBasis() = default;   ///<  Default destructor

	    /**
	     * Evaluate the bases functions of the face at point x
	     * @param x point where to evaluate the bases functions
	     */
	    Eigen::MatrixXd eval_functions(const Eigen::VectorXd& x) const;

	    std::vector<Eigen::MatrixXd> eval_gradients(const Eigen::VectorXd& x) const {
		std::vector<Eigen::MatrixXd> ret;
		throw " eval_gradients is not implemented for this basis ";
		return ret;
	    }

	    Eigen::MatrixXd eval_derivatives(const Eigen::VectorXd& x) const {
		Eigen::MatrixXd ret;
		throw " eval_derivatives is not implemented for this basis ";
		return ret;
	    }

	    Eigen::MatrixXd eval_sec_derivatives(const Eigen::VectorXd& x) const {
		Eigen::MatrixXd ret;
		throw " eval_sec_derivatives is not implemented for this basis ";
		return ret;
	    }

	    Eigen::RowVectorXd eval_divergences(const Eigen::VectorXd& x) const {
		Eigen::RowVectorXd ret;
		throw " eval_divergences is not implemented for this basis ";
		return ret;
	    }

	    Eigen::MatrixXd eval_laplacians(const Eigen::VectorXd& x) const {
		Eigen::MatrixXd ret;
		throw " eval_laplacians is not implemented for this basis ";
		return ret;
	    }

	private:
	    ScaledMonomialScalarBasis<MyMesh::Face<3>*> m_scalar_basis; ///< scaled monomial scalar basis associated
	};

	/////////////////////////////////////////////////////////////////////////
	//                   Class Methods implementations
	/////////////////////////////////////////////////////////////////////////

	//---------------------------------------------
	// constructor for cells in 2D meshes
	ScaledMonomialVectorBasis<MyMesh::Cell<2>*, GRAD>::ScaledMonomialVectorBasis(const MyMesh::Cell<2>* cell, const size_t degree) :
	Basis(degree, grad_vector_basis_dimension(degree, 2, 2)),
	m_scalar_basis(cell, degree) {}

	// evaluate the basis functions at point x for cells in 2D meshes
	Eigen::MatrixXd  
	ScaledMonomialVectorBasis<MyMesh::Cell<2>*, GRAD>::eval_functions(const Eigen::VectorXd& x) const {

	    Eigen::MatrixXd ret(Eigen::MatrixXd::Zero(2, m_basis_dimension));

	    const std::vector<Eigen::MatrixXd> phi(m_scalar_basis.eval_gradients(x));

	    for (size_t i = 0; i < m_scalar_basis.dimension() - 1; i++) { ret.col(i) = phi[i+1]; }
	    throw " to be tested ";

	    return ret;
	} // eval_functions

	//---------------------------------------------
	// constructor for faces in 2D meshes (1D objects)
	ScaledMonomialVectorBasis<MyMesh::Face<2>*, GRAD>::ScaledMonomialVectorBasis(const MyMesh::Face<2>* face, const size_t degree) :
	Basis(degree, grad_vector_basis_dimension(degree, 1, 2)),
	m_scalar_basis(face, degree) {}

	// evaluate functions at point x for faces in 2D meshes
	Eigen::MatrixXd   
	ScaledMonomialVectorBasis<MyMesh::Face<2>*, GRAD>::eval_functions(const Eigen::VectorXd& x) const {

	    Eigen::MatrixXd ret(Eigen::MatrixXd::Zero(2, m_basis_dimension));

	    const std::vector<Eigen::MatrixXd> phi(m_scalar_basis.eval_gradients(x));

	    for (size_t i = 0; i < m_scalar_basis.dimension() - 1; i++) { ret.col(i) = phi[i+1]; }
	    throw " to be tested ";

	    return ret;
	} // eval_functions

	//---------------------------------------------
	// constructor for cells in 3D meshes
	ScaledMonomialVectorBasis<MyMesh::Cell<3>*, GRAD>::ScaledMonomialVectorBasis(const MyMesh::Cell<3>* cell, const size_t degree) :
	Basis(degree, grad_vector_basis_dimension(degree, 3, 3)),
	m_scalar_basis(cell, degree) {}

	// evaluate functions at point x for cells in 3D meshes
	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Cell<3>*, GRAD>::eval_functions(const Eigen::VectorXd& x) const {
	    
	    Eigen::MatrixXd ret(Eigen::MatrixXd::Zero(3, m_basis_dimension));
	    
	    const std::vector<Eigen::MatrixXd> phi(m_scalar_basis.eval_gradients(x));

	    for (size_t i = 0; i < m_scalar_basis.dimension() - 1; i++) { ret.col(i) = phi[i+1]; }
	    throw " to be tested ";

	    return ret;
	} // eval_functions

	//---------------------------------------------
	// constructor for faces in 3D meshes (2D objects)
	ScaledMonomialVectorBasis<MyMesh::Face<3>*, GRAD>::ScaledMonomialVectorBasis(const MyMesh::Face<3>* face, const size_t degree) :
	Basis(degree, grad_vector_basis_dimension(degree, 2, 3)),
	m_scalar_basis(face, degree) {}
	// The basis depends on the coordinate axis of the 2D reference system, is it useful to store them here ?

	// evaluate functions at point x for faces in 3D meshes
	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Face<3>*, GRAD>::eval_functions(const Eigen::VectorXd& x) const {

	    Eigen::MatrixXd ret(Eigen::MatrixXd::Zero(3, this->m_basis_dimension));

	    const std::vector<Eigen::MatrixXd> phi(m_scalar_basis.eval_gradients(x));

	    for (size_t i = 0; i < m_scalar_basis.dimension() - 1; i++) { ret.col(i) = phi[i+1]; }
	    throw " to be tested ";

	    return ret;
	} // eval_functions

} // MyBasis namespace
