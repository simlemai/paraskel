#ifndef BASES_HPP
#define BASES_HPP

#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include "mesh.hpp"

namespace MyBasis {

    /**
     * The abstract class Bases contains 
     * all the common informations about
     * the basis used in one object. 
    */

    // Generic template for scalar bases.
    template<typename ELEMT>
    class Basis {
    public:
        /**
         * Default constructors prints error
         */
        Basis() = default;
        Basis(const size_t degree, const size_t dimension);
        virtual ~Basis() = default;   ///<  Virtual destructor for basis class

        /**
         * @return the dimension of the basis
         */
        size_t dimension() const { return m_basis_dimension; }
        /**
         * @return the degree of the basis
         */
        size_t degree() const { return m_basis_degree; }

        /**
         * Evaluate the bases functions of the element at point x
         * (is defined for each basis and for each element)
         * @param x point where to evaluate the bases functions
         */
        virtual Eigen::MatrixXd eval_functions(const Eigen::VectorXd& x) const = 0;

        /**
         * Evaluate the gradients of the bases functions of the element at point x
         * (is defined for each basis and for each element)
         * @param x point where to evaluate the gradients of the bases functions
         */
        virtual std::vector<Eigen::MatrixXd> eval_gradients(const Eigen::VectorXd& x) const = 0;

        /**
         * Evaluate the derivatives of the bases functions of the edge at point x
         * WITH RESPECT TO the local coordinate (f'(tau))
         * @param x point where to evaluate the derivatives of the bases functions
         */
        virtual Eigen::MatrixXd eval_derivatives(const Eigen::VectorXd& x) const = 0;

        /**
         * Evaluate the second derivatives of the bases functions of the edge at point x
         * WITH RESPECT TO the local coordinate (f''(tau))
         * @param x point where to evaluate the second derivatives of the bases functions
         */
        virtual Eigen::MatrixXd eval_sec_derivatives(const Eigen::VectorXd& x) const = 0;

        /**
         * Evaluate the divergences of the bases functions of the element at point x
         * (is defined for each basis and for each element)
         * @param x point where to evaluate the divergences of the bases functions
         */
        virtual Eigen::RowVectorXd eval_divergences(const Eigen::VectorXd& x) const = 0;

        /**
         * Evaluate the laplacian of the bases functions of the element at point x
         * @param x point where to evaluate the laplacian of the bases functions
         */
        virtual Eigen::MatrixXd eval_laplacians(const Eigen::VectorXd& x) const = 0;

    protected:
        size_t m_basis_degree; ///< maximum polynomial degree to be considered
        size_t m_basis_dimension; ///< dimension of the basis

    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////

    // general constructor
    template<typename ELEMT>
    Basis<ELEMT>::Basis(const size_t degree, const size_t dimension) :
    m_basis_degree(degree),
    m_basis_dimension(dimension) {}

} // MyBasis namespace

#include "bases_scalar.hpp"
#include "bases_vector.hpp"


#endif /* BASES_HPP */
