#ifndef BASES_VECTOR_HPP
#define BASES_VECTOR_HPP

#ifndef BASES_HPP
    throw "You must NOT include this file. Include bases.hpp";
#endif


namespace MyBasis {

    /**
     * This class describes the scaled monomial vector basis. 
    */
    
    /**
     * The enum VectorBasisType contains
     * all the possible Basis types
    */
    enum VectorBasisType { FULL, GRAD };

    // Generic template for vector bases.
    template<typename ELEMT, int BT>
    class ScaledMonomialVectorBasis : public Basis<ELEMT> 
    {
    public:
        /**
         * General constructor prints error, only specializations are useful
         */
        ScaledMonomialVectorBasis(const ELEMT element, const size_t degree);
        ~ScaledMonomialVectorBasis() = default;   ///<  Default destructor
    };

    // general constructor gives error, only specializations are useful
    template<typename ELEMT, int BT>
    ScaledMonomialVectorBasis<ELEMT, BT>::ScaledMonomialVectorBasis(const ELEMT element, const size_t degree) {
        throw " Scaled Monomial Vector Basis not implemented for this element and this VectorBasisType. ";
    }
    
} // MyBasis namespace

// contains the FULL scaled monomial vector basis : P^k_Td(T, R^dim) 
#include "bases_vector_full.hpp"
// contains the Gradient of the scaled monomial scalar basis : \nabla P^k_Td(T) 
#include "bases_vector_grad.hpp"

#endif /* BASES_VECTOR_HPP */
