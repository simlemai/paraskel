namespace MyBasis {

	//---------------------------------------------
	//---------------------------------------------
	/// FULL scaled monomial vector basis : P^k_ed(T, R^dim)
	//---------------------------------------------
	//---------------------------------------------

	/// dimension of the vector basis of polynomials of degree <= k : P^k_ed(T, R^dim)
	static size_t full_vector_basis_dimension(size_t k, size_t ed, size_t dim)
	{
	    size_t num = 1;
	    size_t den = 1;

	    for (size_t i = 1; i <= ed; i++)
	    {
		num *= k + i;
		den *= i;
	    }

	    return dim * (num / den);
	}

	/// Specialization for cells in 2D meshes
	template<>
	class ScaledMonomialVectorBasis<MyMesh::Cell<2>*, FULL> : public Basis<MyMesh::Cell<2>*> 
	{
	public:
	    /**
	     * Constructor
	     * @param cell pointer to the cell
	     * @param degree maximum polynomial degree to be considered
	     */
	    ScaledMonomialVectorBasis(const MyMesh::Cell<2>* cell, const size_t degree);
	    ~ScaledMonomialVectorBasis() = default;   ///<  Default destructor

	    /**
	     * Evaluate the bases functions of the cell at point x
	     * @param x point where to evaluate the bases functions
	     */
	    Eigen::MatrixXd eval_functions(const Eigen::VectorXd& x) const;

	    std::vector<Eigen::MatrixXd> eval_gradients(const Eigen::VectorXd& x) const;

	    Eigen::MatrixXd eval_derivatives(const Eigen::VectorXd& x) const;
	    Eigen::MatrixXd eval_sec_derivatives(const Eigen::VectorXd& x) const;
	    Eigen::RowVectorXd eval_divergences(const Eigen::VectorXd& x) const;
	    Eigen::MatrixXd eval_laplacians(const Eigen::VectorXd& x) const;

	private:
	    ScaledMonomialScalarBasis<MyMesh::Cell<2>*> m_scalar_basis; ///< scaled monomial scalar basis associated
	};

	/// Specialization for faces in 2D meshes (face is a 1D object)
	template<>
	class ScaledMonomialVectorBasis<MyMesh::Face<2>*, FULL> : public Basis<MyMesh::Face<2>*>
	{
	public:
	    /**
	     * Constructor
	     * @param face pointer to the face (is a segment)
	     * @param degree maximum polynomial degree to be considered
	     */
	    ScaledMonomialVectorBasis(const MyMesh::Face<2>* face, const size_t degree);
	    ~ScaledMonomialVectorBasis() = default;   ///<  Default destructor

	    /**
	     * Evaluate the bases functions of the face at point x
	     * @param x point where to evaluate the bases functions
	     */
	    Eigen::MatrixXd eval_functions(const Eigen::VectorXd& x) const;

	    std::vector<Eigen::MatrixXd> eval_gradients(const Eigen::VectorXd& x) const;

	    Eigen::MatrixXd eval_derivatives(const Eigen::VectorXd& x) const;
	    Eigen::MatrixXd eval_sec_derivatives(const Eigen::VectorXd& x) const;
	    Eigen::RowVectorXd eval_divergences(const Eigen::VectorXd& x) const;
	    Eigen::MatrixXd eval_laplacians(const Eigen::VectorXd& x) const;

	private:
	    ScaledMonomialScalarBasis<MyMesh::Face<2>*> m_scalar_basis; ///< scaled monomial scalar basis associated
	};

	//---------------------------------------------
	//---------------------------------------------

	/// Specialization for cells in 3D meshes
	template<>
	class ScaledMonomialVectorBasis<MyMesh::Cell<3>*, FULL> : public Basis<MyMesh::Cell<3>*> 
	{
	public:
	    /**
	     * Constructor
	     * @param cell pointer to the cell
	     * @param degree maximum polynomial degree to be considered
	     */
	    ScaledMonomialVectorBasis(const MyMesh::Cell<3>* cell, const size_t degree);
	    ~ScaledMonomialVectorBasis() = default;   ///<  Default destructor

	    /**
	     * Evaluate the bases functions of the cell at point x
	     * @param x point where to evaluate the bases functions
	     */
	    Eigen::MatrixXd eval_functions(const Eigen::VectorXd& x) const;

	    std::vector<Eigen::MatrixXd> eval_gradients(const Eigen::VectorXd& x) const;

	    Eigen::MatrixXd eval_derivatives(const Eigen::VectorXd& x) const;
	    Eigen::MatrixXd eval_sec_derivatives(const Eigen::VectorXd& x) const;
	    Eigen::RowVectorXd eval_divergences(const Eigen::VectorXd& x) const;
	    Eigen::MatrixXd eval_laplacians(const Eigen::VectorXd& x) const;

	private:
	    ScaledMonomialScalarBasis<MyMesh::Cell<3>*> m_scalar_basis; ///< scaled monomial scalar basis associated
	};

	/// Specialization for faces in 3D meshes
	template<>
	class ScaledMonomialVectorBasis<MyMesh::Face<3>*, FULL> : public Basis<MyMesh::Face<3>*> 
	{
	public:
	    /**
	     * Constructor
	     * @param face pointer to the face
	     * @param degree maximum polynomial degree to be considered
	     */
	    ScaledMonomialVectorBasis(const MyMesh::Face<3>* face, const size_t degree);
	    ~ScaledMonomialVectorBasis() = default;   ///<  Default destructor

	    /**
	     * Evaluate the bases functions of the face at point x
	     * @param x point where to evaluate the bases functions
	     */
	    Eigen::MatrixXd eval_functions(const Eigen::VectorXd& x) const;

	    std::vector<Eigen::MatrixXd> eval_gradients(const Eigen::VectorXd& x) const;

	    Eigen::MatrixXd eval_derivatives(const Eigen::VectorXd& x) const;
	    Eigen::MatrixXd eval_sec_derivatives(const Eigen::VectorXd& x) const;
	    Eigen::RowVectorXd eval_divergences(const Eigen::VectorXd& x) const;
	    Eigen::MatrixXd eval_laplacians(const Eigen::VectorXd& x) const;

	private:
	    ScaledMonomialScalarBasis<MyMesh::Face<3>*> m_scalar_basis; ///< scaled monomial scalar basis associated
	};

	/// Specialization for edges (they exist only in 3D meshes)
	template<size_t DIM>
	class ScaledMonomialVectorBasis<MyMesh::Edge<DIM>*, FULL> : public Basis<MyMesh::Edge<DIM>*> 
	{
	public:
	    /**
	     * Constructor
	     * @param edge pointer to the edge (is a segment)
	     * @param degree maximum polynomial degree to be considered
	     */
	    ScaledMonomialVectorBasis(const MyMesh::Edge<DIM>* edge, const size_t degree);
	    ~ScaledMonomialVectorBasis() = default;   ///<  Default destructor

	    /**
	     * Evaluate the bases functions of the edge at point x
	     * @param x point where to evaluate the bases functions
	     */
	    Eigen::MatrixXd eval_functions(const Eigen::VectorXd& x) const;

	    std::vector<Eigen::MatrixXd> eval_gradients(const Eigen::VectorXd& x) const;

	    Eigen::MatrixXd eval_derivatives(const Eigen::VectorXd& x) const;
	    Eigen::MatrixXd eval_sec_derivatives(const Eigen::VectorXd& x) const;
	    Eigen::RowVectorXd eval_divergences(const Eigen::VectorXd& x) const;
	    Eigen::MatrixXd eval_laplacians(const Eigen::VectorXd& x) const;

	private:
	    ScaledMonomialScalarBasis<MyMesh::Edge<DIM>*> m_scalar_basis; ///< scaled monomial scalar basis associated
	};


	/////////////////////////////////////////////////////////////////////////
	//                   Class Methods implementations
	/////////////////////////////////////////////////////////////////////////

	//---------------------------------------------
	// constructor for cells in 2D meshes
	ScaledMonomialVectorBasis<MyMesh::Cell<2>*, FULL>::ScaledMonomialVectorBasis(const MyMesh::Cell<2>* cell, const size_t degree) :
	Basis(degree, full_vector_basis_dimension(degree, 2, 2)),
	m_scalar_basis(cell, degree) {}


	// evaluate the basis functions at point x for cells in 2D meshes
	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Cell<2>*, FULL>::eval_functions(const Eigen::VectorXd& x) const {

	    Eigen::MatrixXd ret(Eigen::MatrixXd::Zero(2, this->m_basis_dimension));

	    const Eigen::RowVectorXd phi(m_scalar_basis.eval_functions(x));

	    for (size_t i = 0, basis_dim = m_scalar_basis.dimension(); i < basis_dim; i++) {
		ret(0, 2 * i)     = phi(i);
		ret(1, 2 * i + 1) = phi(i);
	    }

	    return ret;
	} // eval_functions

	std::vector<Eigen::MatrixXd> 
	ScaledMonomialVectorBasis<MyMesh::Cell<2>*, FULL>::eval_gradients(const Eigen::VectorXd& x) const {

	    // there are this->m_basis_dimension basis functions, 
	    // there is a matrix containing the gradients for each basis function
	    std::vector<Eigen::MatrixXd> ret;
	    ret.reserve(this->m_basis_dimension);

	    // the vectorial basis is based on a scalar basis,
	    // so need to obtain the gradients of these scalar functions
	    const std::vector<Eigen::MatrixXd> grads_phi(m_scalar_basis.eval_gradients(x));

	    // store the gradients of each scalar function at the corresponding place
	    Eigen::MatrixXd grad_vect_phi(2, 2);
	    for(auto& grad_phi : grads_phi) {
		// vectorial basis function is ( Phi  ; 0 )
		// the gradients is ( dPhi/dx  dPhi/dy  ;  0   0 )
		grad_vect_phi = Eigen::MatrixXd::Zero(2, 2);
		grad_vect_phi.row(0) = grad_phi;
		ret.push_back(grad_vect_phi);

		// vectorial basis function is ( 0  ;  Phi )
		// the gradients is (  0   0 ; dPhi/dx  dPhi/dy )
		grad_vect_phi = Eigen::MatrixXd::Zero(2, 2);
		grad_vect_phi.row(1) = grad_phi;
		ret.push_back(grad_vect_phi);
	    }

	    return ret;
	} // eval_gradients

	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Cell<2>*, FULL>::eval_derivatives(const Eigen::VectorXd& x) const {
	    // one value by basis function
	    Eigen::RowVectorXd ret(this->m_basis_dimension);
	    throw " eval_derivatives called in FULL ScaledMonomialVectorBasis";
	    return ret;
	} // eval_derivatives

	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Cell<2>*, FULL>::eval_sec_derivatives(const Eigen::VectorXd& x) const {
	    // one value by basis function
	    Eigen::RowVectorXd ret(this->m_basis_dimension);
	    throw " eval_sec_derivatives called in FULL ScaledMonomialVectorBasis";
	    return ret;
	}

	Eigen::RowVectorXd 
	ScaledMonomialVectorBasis<MyMesh::Cell<2>*, FULL>::eval_divergences(const Eigen::VectorXd& x) const {

	    // one value by basis function
	    Eigen::RowVectorXd ret(this->m_basis_dimension);

	    // the vectorial basis is based on a scalar basis,
	    // so need to obtain the gradients of these scalar functions
	    const std::vector<Eigen::MatrixXd> grads_phi(m_scalar_basis.eval_gradients(x));

	    // compute the divergence
	    size_t i(0);
	    for(auto& grad_phi : grads_phi) {
		// vectorial basis function is ( Phi  ; 0 )
		// the divergence is dPhi/dx
		ret[2 * i] = grad_phi(0);

		// vectorial basis function is ( 0  ;  Phi )
		// the divergence is dPhi/dy
		ret[2 * i + 1] = grad_phi(1);
		i++; // next scalar function
	    }

	    return ret;
	} // eval_divergences

	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Cell<2>*, FULL>::eval_laplacians(const Eigen::VectorXd& x) const {
	    // one value by basis function and by variable
	    Eigen::MatrixXd ret(Eigen::MatrixXd::Zero(2, this->m_basis_dimension));

	    // the vectorial basis is based on a scalar basis,
	    // so need to obtain the laplacians of these scalar functions
	    const Eigen::RowVectorXd lap_phi(m_scalar_basis.eval_laplacians(x));

	    // fill the vectorial laplacian
	    for (size_t i = 0, basis_dim = m_scalar_basis.dimension(); i < basis_dim; i++) {
		// vectorial basis function is ( Phi  ; 0 )
		// the laplacian is ( lap(Phi) ; 0 )
		ret(0, 2 * i)     = lap_phi(i);
		// vectorial basis function is ( 0 ; Phi )
		// the laplacian is ( 0 ; lap(Phi) )
		ret(1, 2 * i + 1) = lap_phi(i);
	    }

	    return ret;
	} // eval_laplacians


	//---------------------------------------------
	// constructor for faces in 2D meshes (1D objects)
	ScaledMonomialVectorBasis<MyMesh::Face<2>*, FULL>::ScaledMonomialVectorBasis(const MyMesh::Face<2>* face, const size_t degree) :
	Basis(degree, full_vector_basis_dimension(degree, 1, 2)),
	m_scalar_basis(face, degree) {}

	// evaluate functions at point x for faces in 2D meshes
	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Face<2>*, FULL>::eval_functions(const Eigen::VectorXd& x) const {

	    Eigen::MatrixXd ret(Eigen::MatrixXd::Zero(2, this->m_basis_dimension));

	    const Eigen::RowVectorXd phi(m_scalar_basis.eval_functions(x));

	    for (size_t i = 0, basis_dim = m_scalar_basis.dimension(); i < basis_dim; i++) {
		ret(0, 2 * i)     = phi(i);
		ret(1, 2 * i + 1) = phi(i);
	    }

	    return ret;
	} // eval_functions

	std::vector<Eigen::MatrixXd> 
	ScaledMonomialVectorBasis<MyMesh::Face<2>*, FULL>::eval_gradients(const Eigen::VectorXd& x) const {

	    // there are this->m_basis_dimension basis functions, 
	    // there is a matrix containing the gradients for each basis function
	    std::vector<Eigen::MatrixXd> ret;
	    ret.reserve(this->m_basis_dimension);

	    // the vectorial basis is based on a scalar basis,
	    // so need to obtain the gradients of these scalar functions
	    const std::vector<Eigen::MatrixXd> grads_phi(m_scalar_basis.eval_gradients(x));

	    // store the gradients of each scalar function at the corresponding place
	    Eigen::MatrixXd grad_vect_phi(2, 2);
	    for(auto& grad_phi : grads_phi) {
		// vectorial basis function is ( Phi  ; 0 )
		// the gradients is ( dPhi/dx  dPhi/dy  ;  0   0 )
		grad_vect_phi = Eigen::MatrixXd::Zero(2, 2);
		grad_vect_phi.row(0) = grad_phi;
		ret.push_back(grad_vect_phi);

		// vectorial basis function is ( 0  ;  Phi )
		// the gradients is (  0   0 ; dPhi/dx  dPhi/dy )
		grad_vect_phi = Eigen::MatrixXd::Zero(2, 2);
		grad_vect_phi.row(1) = grad_phi;
		ret.push_back(grad_vect_phi);
	    }

	    return ret;
	} // eval_gradients

	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Face<2>*, FULL>::eval_derivatives(const Eigen::VectorXd& x) const {

	    Eigen::MatrixXd ret(Eigen::MatrixXd::Zero(2, this->m_basis_dimension));

	    const Eigen::RowVectorXd phi_derivatives(m_scalar_basis.eval_derivatives(x));

	    for (size_t i = 0, basis_dim = m_scalar_basis.dimension(); i < basis_dim; i++) {
		ret(0, 2 * i)     = phi_derivatives(i);
		ret(1, 2 * i + 1) = phi_derivatives(i);
	    }

	    return ret;
	} // eval_derivatives

	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Face<2>*, FULL>::eval_sec_derivatives(const Eigen::VectorXd& x) const {

	    Eigen::MatrixXd ret(Eigen::MatrixXd::Zero(2, this->m_basis_dimension));

	    const Eigen::RowVectorXd phi_sec_derivatives(m_scalar_basis.eval_sec_derivatives(x));

	    for (size_t i = 0, basis_dim = m_scalar_basis.dimension(); i < basis_dim; i++) {
		ret(0, 2 * i)     = phi_sec_derivatives(i);
		ret(1, 2 * i + 1) = phi_sec_derivatives(i);
	    }

	    return ret;
	} // eval_sec_derivatives

	Eigen::RowVectorXd 
	ScaledMonomialVectorBasis<MyMesh::Face<2>*, FULL>::eval_divergences(const Eigen::VectorXd& x) const {

	    // one column by basis function
	    Eigen::RowVectorXd ret(this->m_basis_dimension);

	    // the vectorial basis is based on a scalar basis,
	    // so need to obtain the gradients of these scalar functions
	    const std::vector<Eigen::MatrixXd> grads_phi(m_scalar_basis.eval_gradients(x));

	    // compute the divergence
	    size_t i(0);
	    for(auto& grad_phi : grads_phi) {
		// vectorial basis function is ( Phi  ; 0 )
		// the divergence is dPhi/dx
		ret[2 * i] = grad_phi(0);

		// vectorial basis function is ( 0  ;  Phi )
		// the divergence is dPhi/dy
		ret[2 * i + 1] = grad_phi(1);
		i++; // next scalar function
	    }

	    return ret;
	} // eval_divergences

	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Face<2>*, FULL>::eval_laplacians(const Eigen::VectorXd& x) const {
	    // one value by basis function
	    Eigen::MatrixXd ret(2, this->m_basis_dimension);

	    throw " eval_laplacians called in FULL ScaledMonomialVectorBasis ";
	    return ret;
	} // eval_laplacians


	//---------------------------------------------
	// constructor for cells in 3D meshes
	ScaledMonomialVectorBasis<MyMesh::Cell<3>*, FULL>::ScaledMonomialVectorBasis(const MyMesh::Cell<3>* cell, const size_t degree) :
	Basis(degree, full_vector_basis_dimension(degree, 3, 3)),
	m_scalar_basis(cell, degree) {}

	// evaluate functions at point x for cells in 3D meshes
	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Cell<3>*, FULL>::eval_functions(const Eigen::VectorXd& x) const {
	    
	    Eigen::MatrixXd ret(Eigen::MatrixXd::Zero(3, this->m_basis_dimension));
	    
	    const Eigen::RowVectorXd phi(m_scalar_basis.eval_functions(x));

	    for (size_t i = 0, basis_dim = m_scalar_basis.dimension(); i < basis_dim; i++) {
		ret(0, 3 * i)     = phi(i);
		ret(1, 3 * i + 1) = phi(i);
		ret(2, 3 * i + 2) = phi(i);
	    }

	    return ret;
	} // eval_functions

	std::vector<Eigen::MatrixXd> 
	ScaledMonomialVectorBasis<MyMesh::Cell<3>*, FULL>::eval_gradients(const Eigen::VectorXd& x) const {

	    // there are this->m_basis_dimension basis functions, 
	    // there is a matrix containing the gradients for each basis function
	    std::vector<Eigen::MatrixXd> ret;
	    ret.reserve(this->m_basis_dimension);

	    // the vectorial basis is based on a scalar basis,
	    // so need to obtain the gradients of these scalar functions
	    const std::vector<Eigen::MatrixXd> grads_phi(m_scalar_basis.eval_gradients(x));

	    // store the gradients of each scalar function at the corresponding place
	    Eigen::MatrixXd grad_vect_phi(3, 3);
	    for(auto& grad_phi : grads_phi) {
		// vectorial basis function is ( Phi ; 0 ; 0)
		// the gradients is ( dPhi/dx  dPhi/dy  dPhi/dz ; 0   0  0 ; 0   0  0 )
		grad_vect_phi = Eigen::MatrixXd::Zero(3, 3);
		grad_vect_phi.row(0) = grad_phi;
		ret.push_back(grad_vect_phi);

		// vectorial basis function is ( 0 ; Phi ; 0 )
		// the gradients is (  0   0  0 ; dPhi/dx  dPhi/dy  dPhi/dz ;  0   0  0 )
		grad_vect_phi = Eigen::MatrixXd::Zero(3, 3);
		grad_vect_phi.row(1) = grad_phi;
		ret.push_back(grad_vect_phi);

		// vectorial basis function is ( 0 ; 0 ; Phi )
		// the gradients is (  0   0  0 ;  0   0  0 ; dPhi/dx  dPhi/dy  dPhi/dz )
		grad_vect_phi = Eigen::MatrixXd::Zero(3, 3);
		grad_vect_phi.row(2) = grad_phi;
		ret.push_back(grad_vect_phi);
	    }

	    return ret;
	} // eval_gradients

	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Cell<3>*, FULL>::eval_derivatives(const Eigen::VectorXd& x) const {
	    // one value by basis function
	    Eigen::RowVectorXd ret(this->m_basis_dimension);
	    throw " eval_derivatives called in FULL ScaledMonomialVectorBasis";
	    return ret;
	} // eval_derivatives

	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Cell<3>*, FULL>::eval_sec_derivatives(const Eigen::VectorXd& x) const {
	    // one value by basis function
	    Eigen::RowVectorXd ret(this->m_basis_dimension);
	    throw " eval_sec_derivatives called in FULL ScaledMonomialVectorBasis";
	    return ret;
	}

	Eigen::RowVectorXd 
	ScaledMonomialVectorBasis<MyMesh::Cell<3>*, FULL>::eval_divergences(const Eigen::VectorXd& x) const {

	    // one column by basis function
	    Eigen::RowVectorXd ret(this->m_basis_dimension);

	    // the vectorial basis is based on a scalar basis,
	    // so need to obtain the gradients of these scalar functions
	    const std::vector<Eigen::MatrixXd> grads_phi(m_scalar_basis.eval_gradients(x));

	    // compute the divergence
	    size_t i(0);
	    for(auto& grad_phi : grads_phi) {
		// vectorial basis function is ( Phi ; 0 ; 0)
		// the divergence is dPhi/dx
		ret[3 * i] = grad_phi(0);

		// vectorial basis function is ( 0 ; Phi ; 0 )
		// the divergence is dPhi/dy
		ret[3 * i + 1] = grad_phi(1);

		// vectorial basis function is ( 0 ; 0 ; Phi )
		// the divergence is dPhi/dz
		ret[3 * i + 2] = grad_phi(2);

		i++; // next scalar function
	    }

	    return ret;
	} // eval_divergences

	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Cell<3>*, FULL>::eval_laplacians(const Eigen::VectorXd& x) const {
	    // one value by basis function and by variable
	    Eigen::MatrixXd ret(Eigen::MatrixXd::Zero(3, this->m_basis_dimension));

	    // the vectorial basis is based on a scalar basis,
	    // so need to obtain the laplacians of these scalar functions
	    const Eigen::RowVectorXd lap_phi(m_scalar_basis.eval_laplacians(x));

	    // fill the vectorial laplacian
	    for (size_t i = 0, basis_dim = m_scalar_basis.dimension(); i < basis_dim; i++) {
		// vectorial basis function is ( Phi  ; 0 ; 0 )
		// the laplacian is ( lap(Phi) ; 0 ; 0 )
		ret(0, 3 * i)     = lap_phi(i);
		// vectorial basis function is ( 0 ; Phi ; 0 )
		// the laplacian is ( 0 ; lap(Phi) ; 0 )
		ret(1, 3 * i + 1) = lap_phi(i);
		// vectorial basis function is ( 0 ; 0 ; Phi)
		// the laplacian is ( 0 ; 0 ; lap(Phi) )
		ret(2, 3 * i + 2) = lap_phi(i);
	    }

	    return ret;
	} // eval_laplacians


	//---------------------------------------------
	// constructor for faces in 3D meshes (2D objects)
	ScaledMonomialVectorBasis<MyMesh::Face<3>*, FULL>::ScaledMonomialVectorBasis(const MyMesh::Face<3>* face, const size_t degree) :
	Basis(degree, full_vector_basis_dimension(degree, 2, 3)),
	m_scalar_basis(face, degree) {}

	// evaluate functions at point x for faces in 3D meshes
	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Face<3>*, FULL>::eval_functions(const Eigen::VectorXd& x) const {

	    Eigen::MatrixXd ret(Eigen::MatrixXd::Zero(3, this->m_basis_dimension));

	    const Eigen::RowVectorXd phi(m_scalar_basis.eval_functions(x));

	    for (size_t i = 0, basis_dim = m_scalar_basis.dimension(); i < basis_dim; i++) {
		ret(0, 3 * i)     = phi(i);
		ret(1, 3 * i + 1) = phi(i);
		ret(2, 3 * i + 2) = phi(i);
	    }

	    return ret;
	} // eval_functions

	std::vector<Eigen::MatrixXd> 
	ScaledMonomialVectorBasis<MyMesh::Face<3>*, FULL>::eval_gradients(const Eigen::VectorXd& x) const {

	    // there are this->m_basis_dimension basis functions, 
	    // there is a matrix containing the gradients for each basis function
	    std::vector<Eigen::MatrixXd> ret;
	    ret.reserve(this->m_basis_dimension);

	    // the vectorial basis is based on a scalar basis,
	    // so need to obtain the gradients of these scalar functions
	    const std::vector<Eigen::MatrixXd> grads_phi(m_scalar_basis.eval_gradients(x));

	    // store the gradients of each scalar function at the corresponding place
	    Eigen::MatrixXd grad_vect_phi(3, 3);
	    for(auto& grad_phi : grads_phi) {
		// vectorial basis function is ( Phi ; 0 ; 0)
		// the gradients is ( dPhi/dx  dPhi/dy  dPhi/dz ; 0   0  0 ; 0   0  0 )
		grad_vect_phi = Eigen::MatrixXd::Zero(3, 3);
		grad_vect_phi.row(0) = grad_phi;
		ret.push_back(grad_vect_phi);

		// vectorial basis function is ( 0 ; Phi ; 0 )
		// the gradients is (  0   0  0 ; dPhi/dx  dPhi/dy  dPhi/dz ;  0   0  0 )
		grad_vect_phi = Eigen::MatrixXd::Zero(3, 3);
		grad_vect_phi.row(1) = grad_phi;
		ret.push_back(grad_vect_phi);

		// vectorial basis function is ( 0 ; 0 ; Phi )
		// the gradients is (  0   0  0 ;  0   0  0 ; dPhi/dx  dPhi/dy  dPhi/dz )
		grad_vect_phi = Eigen::MatrixXd::Zero(3, 3);
		grad_vect_phi.row(2) = grad_phi;
		ret.push_back(grad_vect_phi);
	    }

	    return ret;
	} // eval_gradients

	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Face<3>*, FULL>::eval_derivatives(const Eigen::VectorXd& x) const {
	    // one value by basis function
	    Eigen::RowVectorXd ret(this->m_basis_dimension);
	    throw " eval_derivatives called in FULL ScaledMonomialVectorBasis";
	    return ret;
	} // eval_derivatives

	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Face<3>*, FULL>::eval_sec_derivatives(const Eigen::VectorXd& x) const {
	    // one value by basis function
	    Eigen::RowVectorXd ret(this->m_basis_dimension);
	    throw " eval_sec_derivatives called in FULL ScaledMonomialVectorBasis";
	    return ret;
	}

	Eigen::RowVectorXd 
	ScaledMonomialVectorBasis<MyMesh::Face<3>*, FULL>::eval_divergences(const Eigen::VectorXd& x) const {

	    // one column by basis function
	    Eigen::RowVectorXd ret(this->m_basis_dimension);

	    // the vectorial basis is based on a scalar basis,
	    // so need to obtain the gradients of these scalar functions
	    const std::vector<Eigen::MatrixXd> grads_phi(m_scalar_basis.eval_gradients(x));

	    // compute the divergence
	    size_t i(0);
	    for(auto& grad_phi : grads_phi) {
		// vectorial basis function is ( Phi ; 0 ; 0)
		// the divergence is dPhi/dx
		ret[3 * i] = grad_phi(0);

		// vectorial basis function is ( 0 ; Phi ; 0 )
		// the divergence is dPhi/dy
		ret[3 * i + 1] = grad_phi(1);

		// vectorial basis function is ( 0 ; 0 ; Phi )
		ret[3 * i + 2] = grad_phi(2);

		i++; // next scalar function
	    }

	    return ret;
	} // eval_divergences

	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Face<3>*, FULL>::eval_laplacians(const Eigen::VectorXd& x) const {
	    // one value by basis function and by variable
	    Eigen::MatrixXd ret(Eigen::MatrixXd::Zero(3, this->m_basis_dimension));

	    // the vectorial basis is based on a scalar basis,
	    // so need to obtain the laplacians of these scalar functions
	    const Eigen::RowVectorXd lap_phi(m_scalar_basis.eval_laplacians(x));

	    // fill the vectorial laplacian
	    for (size_t i = 0, basis_dim = m_scalar_basis.dimension(); i < basis_dim; i++) {
		// vectorial basis function is ( Phi  ; 0 ; 0 )
		// the laplacian is ( lap(Phi) ; 0 ; 0 )
		ret(0, 3 * i)     = lap_phi(i);
		// vectorial basis function is ( 0 ; Phi ; 0 )
		// the laplacian is ( 0 ; lap(Phi) ; 0 )
		ret(1, 3 * i + 1) = lap_phi(i);
		// vectorial basis function is ( 0 ; 0 ; Phi)
		// the laplacian is ( 0 ; 0 ; lap(Phi) )
		ret(2, 3 * i + 2) = lap_phi(i);
	    }

	    return ret;
	} // eval_laplacians


	//---------------------------------------------
	// constructor for edges, they exist only in 3D meshes and are 1D objects
	template<size_t DIM>
	ScaledMonomialVectorBasis<MyMesh::Edge<DIM>*, FULL>::ScaledMonomialVectorBasis(const MyMesh::Edge<DIM>* edge, const size_t degree) :
	Basis<MyMesh::Edge<DIM>*>(degree, full_vector_basis_dimension(degree, 1, DIM)),
	m_scalar_basis(edge, degree) {}

	// evaluate functions at point x for edges
	template<size_t DIM>
	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Edge<DIM>*, FULL>::eval_functions(const Eigen::VectorXd& x) const {

	    Eigen::MatrixXd ret(Eigen::MatrixXd::Zero(DIM, this->m_basis_dimension));

	    const Eigen::RowVectorXd phi(m_scalar_basis.eval_functions(x));

	    for (size_t i = 0, basis_dim = m_scalar_basis.dimension(); i < basis_dim; i++) { 
		// DIM = 3
		ret(0, 3 * i)     = phi(i);
		ret(1, 3 * i + 1) = phi(i);
		ret(2, 3 * i + 2) = phi(i);
	    }

	    return ret;
	} // eval_functions

	template<size_t DIM>
	std::vector<Eigen::MatrixXd> 
	ScaledMonomialVectorBasis<MyMesh::Edge<DIM>*, FULL>::eval_gradients(const Eigen::VectorXd& x) const {

	    // there are this->m_basis_dimension basis functions, 
	    // there is a matrix containing the gradients for each basis function
	    std::vector<Eigen::MatrixXd> ret;
	    ret.reserve(this->m_basis_dimension);

	    // the vectorial basis is based on a scalar basis,
	    // so need to obtain the gradients of these scalar functions
	    const std::vector<Eigen::MatrixXd> grads_phi(m_scalar_basis.eval_gradients(x));

	    // store the gradients of each scalar function at the corresponding place
	    Eigen::MatrixXd grad_vect_phi(3, 3);
	    for(auto& grad_phi : grads_phi) {
		// vectorial basis function is ( Phi ; 0 ; 0)
		// the gradients is ( dPhi/dx  dPhi/dy  dPhi/dz ; 0   0  0 ; 0   0  0 )
		grad_vect_phi = Eigen::MatrixXd::Zero(3, 3);
		grad_vect_phi.row(0) = grad_phi;
		ret.push_back(grad_vect_phi);

		// vectorial basis function is ( 0 ; Phi ; 0 )
		// the gradients is (  0   0  0 ; dPhi/dx  dPhi/dy  dPhi/dz ;  0   0  0 )
		grad_vect_phi = Eigen::MatrixXd::Zero(3, 3);
		grad_vect_phi.row(1) = grad_phi;
		ret.push_back(grad_vect_phi);

		// vectorial basis function is ( 0 ; 0 ; Phi )
		// the gradients is (  0   0  0 ;  0   0  0 ; dPhi/dx  dPhi/dy  dPhi/dz )
		grad_vect_phi = Eigen::MatrixXd::Zero(3, 3);
		grad_vect_phi.row(2) = grad_phi;
		ret.push_back(grad_vect_phi);
	    }

	    return ret;
	} // eval_gradients

	template<size_t DIM>
	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Edge<DIM>*, FULL>::eval_derivatives(const Eigen::VectorXd& x) const {

	    Eigen::MatrixXd ret(Eigen::MatrixXd::Zero(DIM, this->m_basis_dimension));

	    const Eigen::RowVectorXd phi_derivatives(m_scalar_basis.eval_derivatives(x));

	    for (size_t i = 0, basis_dim = m_scalar_basis.dimension(); i < basis_dim; i++) {
		// DIM = 3
		ret(0, 3 * i)     = phi_derivatives(i);
		ret(1, 3 * i + 1) = phi_derivatives(i);
		ret(2, 3 * i + 2) = phi_derivatives(i);
	    }

	    return ret;
	} // eval_derivatives

	template<size_t DIM>
	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Edge<DIM>*, FULL>::eval_sec_derivatives(const Eigen::VectorXd& x) const {

	    Eigen::MatrixXd ret(Eigen::MatrixXd::Zero(3, this->m_basis_dimension)); // DIM = 3

	    const Eigen::RowVectorXd phi_sec_derivatives(m_scalar_basis.eval_sec_derivatives(x));

	    for (size_t i = 0, basis_dim = m_scalar_basis.dimension(); i < basis_dim; i++) {
		ret(0, 3 * i)     = phi_sec_derivatives(i);
		ret(1, 3 * i + 1) = phi_sec_derivatives(i);
		ret(2, 3 * i + 2) = phi_sec_derivatives(i);
	    }

	    return ret;
	}

	template<size_t DIM>
	Eigen::RowVectorXd 
	ScaledMonomialVectorBasis<MyMesh::Edge<DIM>*, FULL>::eval_divergences(const Eigen::VectorXd& x) const {

	    // one column by basis function
	    Eigen::RowVectorXd ret(this->m_basis_dimension);

	    // the vectorial basis is based on a scalar basis,
	    // so need to obtain the gradients of these scalar functions
	    const std::vector<Eigen::MatrixXd> grads_phi(m_scalar_basis.eval_gradients(x));

	    // compute the divergence
	    size_t i(0);
	    for(auto& grad_phi : grads_phi) {
		// vectorial basis function is ( Phi ; 0 ; 0)
		// the divergence is dPhi/dx
		ret[3 * i] = grad_phi(0);

		// vectorial basis function is ( 0 ; Phi ; 0 )
		// the divergence is dPhi/dy
		ret[3 * i + 1] = grad_phi(1);

		// vectorial basis function is ( 0 ; 0 ; Phi )
		ret[3 * i + 2] = grad_phi(2);

		i++; // next scalar function
	    }
	    
	    return ret;
	} // eval_divergences

	template<size_t DIM>
	Eigen::MatrixXd 
	ScaledMonomialVectorBasis<MyMesh::Edge<DIM>*, FULL>::eval_laplacians(const Eigen::VectorXd& x) const {
	    // one value by basis function
	    Eigen::RowVectorXd ret(this->m_basis_dimension);

	    throw " eval_laplacians called in FULL ScaledMonomialVectorBasis ";
	    return ret;
	} // eval_laplacians

} // MyBasis namespace
