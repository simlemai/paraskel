#ifndef TEX_WRITER
#define TEX_WRITER

#include <iostream>
#include <vector>
#include "mesh.hpp"

/**
 * Write the textfile which contains the mesh 
 * in TEX format (tikz)
 * 
 * @param mesh pointer of the mesh
 * @param dir directory where to write the TEX file
 * @param filename name of the textfile to write to
 * @param plot_centers plot cell centers or not
*/
template<size_t DIM>
void write_tex_mesh(MyMesh::Mesh<DIM>* const mesh, std::string const dir,
                std::string const filename, bool plot_centers = false);

    /////////////////////////////////////////////////////////////////////////
    //                   Implementations
    /////////////////////////////////////////////////////////////////////////

template<size_t DIM>
void write_tex_mesh(MyMesh::Mesh<DIM>* const mesh, std::string const dir,
                std::string const filename, bool plot_centers) {

    // check if directory exists, if not create it.
    char tmp_dir[dir.length()+1];
    strcpy(tmp_dir,dir.c_str()); // convert string to char
    if( access(tmp_dir, 0) != 0 ) {
        if( mkdir(dir.c_str(), S_IRWXU) != 0) { // Read + Write + Execute for User
            throw "Could not access nor create output directory for visualization. ";
            return;
        } 
        std::cout << " Create output directory " << dir << std::endl;
    }

    // open file
    FILE* output_file = fopen((dir+filename).c_str(), "w");
#ifdef DEBUG
    if(output_file == NULL) {
        throw "Could not open output file for visualization. ";
        return;
    }
#endif
    std::cout << "   ----------------------------------------------" << std::endl;
    std::cout << "   Write the TEX file " << (dir+filename) << std::endl;
    std::cout << "   ----------------------------------------------" << std::endl;

    // --------------------- Header ---------------------
    fprintf(output_file, "%s\n", "\\begin{tikzpicture}");

    // --------------------- Cells ---------------------
    // write cells connectivity (all vertices) 
    for (auto& c : mesh->get_cells()) {
        // Draw cell contour
        fprintf(output_file, "%s", "  \\draw plot coordinates {");
        for(auto& v : c->get_vertices()) {
            // Eigen::Array2d v_coords = v->coords();
            fprintf(output_file, "%s %5.2f %s %5.2f %s", 
                    "(", v->coords()[0], ",", v->coords()[1], ") ");
        }
        // put again the first vertex, to close the cell
        const auto& v0 = c->get_vertices()[0];
        fprintf(output_file, "%s %5.2f %s %5.2f %s", 
                "(", v0->coords()[0], ",", v0->coords()[1], ")};\n");
    } // end cell loop
    
    // Draw cell centers if required
    if(plot_centers) {
        for (auto& c : mesh->get_cells()) {
            const auto& cc = c->mass_center();
            fprintf(output_file, "%s %5.2f %s %5.2f %s", 
                    "  \\fill (", cc[0], ",", 
                    cc[1], ") circle (0.02);\n");
        }
    }
    
    // --------------------- Footer ---------------------
    fprintf(output_file, "%s\n", "\\end{tikzpicture}");

    // close file
    fclose(output_file);

} // write_tex_mesh

#endif /* TEX_WRITER */
