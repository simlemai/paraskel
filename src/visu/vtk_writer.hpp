#ifndef VTK_WRITER
#define VTK_WRITER

#include <iostream>
#include <stdio.h>
#include <cstring>
#include <vector>
#include <unistd.h>
#include <sys/stat.h>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include "mesh.hpp"

namespace MyVisu {
    /**
     * The class VtkVisu stores the vectors with the data 
     * and write it into a vtk file
    */
    template<size_t DIM> 
    class VtkVisu
    {
    public:
        /**
         * Constructor
         * @param mesh pointer to the mesh
         */
        VtkVisu(MyMesh::Mesh<DIM>* mesh, bool conforming_method = false);
        ~VtkVisu() = default;   ///<  Default destructor

        /**
         * store the vertices values of the variable and its name
         * @param vertices_data data of all the vertices for one (scalar or vectorial) variable
         * @param data_name name of the variable
         */
        void add_var(const Eigen::MatrixXd& vertices_data, std::string const data_name = "num_sol");

        /**
         * store the vertices values of a continuous scalar function and its name
         * @param continuous_data continuous scalar function fromm which extract vertices values
         * @param data_name name of the variable
         */
        void add_var(double continuous_data(Eigen::ArrayXd), std::string const data_name = "exact_sol");

        /**
         * store the vertices values of a continuous vectorial function and its name
         * @param continuous_data continuous vectorial function fromm which extract vertices values
         * @param data_name name of the variable
         */
        void add_var(std::vector<double> continuous_data(Eigen::ArrayXd), 
                     std::string const data_name = "exact_sol");

        /**
         * Write the textfile which contains the mesh in VTK XML format
         * 
         * @param dir directory where to write the VTK file
         * @param filename name of the textfile to write to
        */
        bool write_mesh(std::string const dir, std::string const filename);

        /**
         * Write the textfile which contains all the data of the instance
         * 
         * @param dir directory where to write the VTK file
         * @param filename name of the textfile to write to
        */
        bool write(std::string const dir, std::string const filename);

    private:
        MyMesh::Mesh<DIM>* m_mesh;                ///< pointer to the mesh
        size_t m_n_visu_points;                   ///< number of points to visualize (= nb of vertices if the method is conforming)
        bool m_conforming_method;                    ///< is the method conforming? (if true, simplification of the visu file)
        std::vector<Eigen::MatrixXd> m_vertices_data; ///< list of the values at the vertices (in each matrix, one col is one vertex)
        std::vector<std::string> m_data_name;     ///< list of names of the variables

        /**
         * write header (VTK XML format)
         * @param output_file pointer of the output file (already open)
         * @param n_visu_points number of points 
         */
        void write_vtk_header(FILE* const output_file, size_t n_visu_points);
        
        /**
         * write footer (VTK XML format)
         * @param output_file pointer of the output file (already open)
         */
        void write_vtk_footer(FILE* const output_file);

        /**
         * Write the coordinates of vertices in VTK XML format
         * @param output_file ptr of the textfile to write to
        */
        void write_vtk_vertices_coordinates(FILE* const output_file);
        /**
         * Write the coordinates of all the visu points 
         * (each vertex is written several times in good order !)
         * in VTK XML format
         * @param output_file ptr of the textfile to write to
        */
        void write_vtk_visu_points_coordinates(FILE* const output_file);

        /**
         * Write the connectivity of the cells and faces
         * @param output_file ptr of the textfile to write to
        */
        void write_vtk_mesh_connectivity(FILE* const output_file);
        /**
         * Write the connectivity of the cells and faces
         * for non conforming method : the vertices have new global indexes
         * @param output_file ptr of the textfile to write to
        */
        void write_vtk_non_conforming_connectivity(FILE* const output_file);

        /**
         * Same beginning for all write_vtk_visu
         * 
         * @param dir directory where to write the VTK file
         * @param filename name of the textfile to write to
         * @param n_visu_points number of points in the visualization
        */
        FILE* write_vtk_pre_processing(std::string const dir,
                        std::string const filename, size_t n_visu_points);

        /**
         * Same post processing for all write_vtk_visu
         * 
         * @param output_file ptr of the textfile to write to
         * @param conforming_method do we write the data as a conforming method (only one value by vertex)? 
        */
        void write_vtk_post_processing(FILE* const output_file, bool conforming_method);

        /**
         * Write the scalar or vectorial data of vertices in VTK XML format
         * @param output_file ptr of the textfile to write to
         * @param discrete_data data at the vertices
         * @param name description of the scalar data
         * @param components size of the variable (scalar = 1, vect > 1)
        */
        void write_vtk_data_vertices(FILE* const output_file, Eigen::MatrixXd discrete_data, 
                                        std::string const name, size_t components = 1);

    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////
    template<size_t DIM>
    VtkVisu<DIM>::VtkVisu(MyMesh::Mesh<DIM>* mesh, bool conforming_method) :
    m_mesh(mesh),
    m_conforming_method(conforming_method),
    m_n_visu_points(0) {
        if(m_conforming_method) {
            // visu one value by vertex
            m_n_visu_points = m_mesh->n_vertices();
        } else {
            // visu one value by vertex BY CELL
            for(auto& c : m_mesh->get_cells()) {
                m_n_visu_points += c->n_vertices();
            }
        }
    }

    // add discrete values to the list
    // each column of vertices_data corresponds to one visualization point
    template<size_t DIM>
    void VtkVisu<DIM>::add_var(const Eigen::MatrixXd& vertices_data, std::string const data_name) {
        // check if the name contains space
        std::string name(data_name);
        std::replace(name.begin(), name.end(), ' ', '_');

#ifdef DEBUG
        if(vertices_data.cols() != m_n_visu_points) {
            throw " In VtkVisu::add_var, the size of the data matrix does not correspond to \
the number of visualization points. \n Does the type of method correspond in the creation \
of the VtkVisu instance and in the extraction of data at the vertices? \n";
            return;
        }
#endif

        // store name and data
        m_data_name.push_back(name);
        m_vertices_data.push_back(vertices_data);

        return;
    } // add_var discrete values

    // extract scalar continuous values to fill the list
    template<size_t DIM>
    void VtkVisu<DIM>::add_var(double continuous_data(Eigen::ArrayXd), std::string const data_name) {
        // transform data into a vector (one column by point to visualize)
        Eigen::RowVectorXd discrete_data(m_n_visu_points);
        if(m_conforming_method) {
            // if the method is conforming, store only one value by vertex, in vertex global index order
            for (size_t v_gi = 0; v_gi < m_n_visu_points; v_gi++) {
                discrete_data[v_gi] = continuous_data(m_mesh->vertex(v_gi)->coords());
            }
        } else {
            size_t visu_point_gi(0);
            // visu one value by vertex BY CELL
            for(auto& c : m_mesh->get_cells()) {
                for(auto& v : c->get_vertices()) {
                    discrete_data[visu_point_gi++] = continuous_data(v->coords());
                }
            }
        }

        return add_var(discrete_data, data_name);
    } // add_var scalar continuous values

    // extract vectorial continuous values to fill the list
    template<size_t DIM>
    void VtkVisu<DIM>::add_var(std::vector<double> continuous_data(Eigen::ArrayXd), std::string const data_name) {

        // look for the size of the variable, TODO: find a better way
        size_t var_size(continuous_data(m_mesh->vertex(0)->coords()).size());

        // transform continuous data into a matrix
        Eigen::MatrixXd discrete_data(var_size, m_n_visu_points);

        if(m_conforming_method) {
            // if the method is conforming, store only one value by vertex, in vertex global index order
            for (size_t v_gi = 0; v_gi < m_n_visu_points; v_gi++) {
                std::vector<double> f_v(continuous_data(m_mesh->vertex(v_gi)->coords()));
                discrete_data.col(v_gi) = Eigen::Map<Eigen::VectorXd> (f_v.data(), f_v.size()); // map into eigen vector
            }
        } else {
            size_t visu_point_gi(0);
            // visu one value by vertex BY CELL
            for(auto& c : m_mesh->get_cells()) {
                for(auto& v : c->get_vertices()) {
                    std::vector<double> f_v(continuous_data(v->coords()));
                    discrete_data.col(visu_point_gi++) = Eigen::Map<Eigen::VectorXd> (f_v.data(), f_v.size()); // map into eigen vector
                }
            }

        }

        return add_var(discrete_data, data_name);
    } // add_var vectorial continuous values


    template<size_t DIM>
    void VtkVisu<DIM>::write_vtk_header(FILE* const output_file, size_t n_visu_points) {
        // write header (VTK XML format)
        fprintf(output_file, "%s\n", "<?xml version=\"1.0\"?>");
        fprintf(output_file, "%s\n", "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">");
        fprintf(output_file, "%s\n", "   <UnstructuredGrid>");
        fprintf(output_file, "%s", "      <Piece  ");
        fprintf(output_file, "%s%zu%s", "NumberOfPoints=\"", n_visu_points, "\"  ");
        fprintf(output_file, "%s%zu%s\n", "NumberOfCells=\"", m_mesh->n_cells(), "\">");

        return;
    } // write_vtk_header

    template<size_t DIM>
    void VtkVisu<DIM>::write_vtk_footer(FILE* const output_file) {
        // write footer (VTK XML format)
        fprintf(output_file, "%s\n", "      </Piece>  ");
        fprintf(output_file, "%s\n", "   </UnstructuredGrid>");
        fprintf(output_file, "%s\n", "</VTKFile>");

        return;
    } // write_vtk_footer

    template<size_t DIM>
    void VtkVisu<DIM>::write_vtk_vertices_coordinates(FILE* const output_file) {

        // --------------------- Vertices ---------------------
        // write vertices coordinates
        // x(0) y(0) z(0) 
        // x(1) y(1) z(1) 
        // ... 
        // x(nb_vertices-1) y(nb_vertices-1) z(nb_vertices-1)
        fprintf(output_file, "%s\n", "         <Points>");
        fprintf(output_file, "%s\n",
                "            <DataArray type=\"Float32\" Name=\"Coordinates\" NumberOfComponents=\"3\" format=\"ascii\">");
        if(DIM == 2) {
            for (auto& v : m_mesh->get_vertices()) {
                Eigen::Array2d v_coords = v->coords();
                fprintf(output_file, "%e %e %e\n", v_coords(0), v_coords(1), 0.0);
            }
        } else if(DIM == 3)  {
            for (auto& v : m_mesh->get_vertices()) {
                Eigen::Array3d v_coords = v->coords();
                fprintf(output_file, "%e %e %e\n", v_coords(0), v_coords(1), v_coords(2));
            }
        }
        fprintf(output_file, "%s\n", "            </DataArray>");
        fprintf(output_file, "%s\n", "         </Points>");

        return;
    } // write_vtk_vertices_coordinates

    template<size_t DIM>
    void VtkVisu<DIM>::write_vtk_visu_points_coordinates(FILE* const output_file) {

        // ---------- Vertices coordinates in specific order ---------------------

        fprintf(output_file, "%s\n", "         <Points>");
        fprintf(output_file, "%s\n",
                "            <DataArray type=\"Float32\" Name=\"Coordinates\" NumberOfComponents=\"3\" format=\"ascii\">");
        if(DIM == 2) {
            for(auto& c : m_mesh->get_cells()) {
                for(auto& v : c->get_vertices()) {
                    Eigen::Array2d v_coords = v->coords();
                    fprintf(output_file, "%e %e %e\n", v_coords(0), v_coords(1), 0.0);
                }
            }
        } else if(DIM == 3)  {
            for(auto& c : m_mesh->get_cells()) {
                for(auto& v : c->get_vertices()) {
                    Eigen::Array3d v_coords = v->coords();
                    fprintf(output_file, "%e %e %e\n", v_coords(0), v_coords(1), v_coords(2));
                }
            }
        }

        fprintf(output_file, "%s\n", "            </DataArray>");
        fprintf(output_file, "%s\n", "         </Points>");

        return;
    } // write_vtk_visu_points_coordinates

    template<size_t DIM>
    void VtkVisu<DIM>::write_vtk_mesh_connectivity(FILE* const output_file) {
        // --------------------- Cells ---------------------
        // write cells connectivity (all vertices indexes) in CSR storage
        fprintf(output_file, "%s\n", "         <Cells>");
        fprintf(output_file, "%s\n",
                "            <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">");
        for (auto& c : m_mesh->get_cells()) {
            for(auto& v : c->get_vertices()) {
                fprintf(output_file, "%zu ", v->global_index());
            }
        }
        fprintf (output_file, "\n%s\n", "            </DataArray>");
        // cell offsets (CSR storage) : 
        // write end localisation of vertices in each cell
        // (to know which vertices belong to which cell)
        size_t end_storage = 0;
        fprintf (output_file, "%s\n", "            <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">");
        for (auto& c : m_mesh->get_cells()) {
            fprintf(output_file, "%zu ", end_storage += c->n_vertices());
        }
        fprintf (output_file, "\n%s\n", "            </DataArray>");
        // cell types
        fprintf (output_file, "%s\n", "            <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\">");
        int type_storage(0);
        if(DIM == 2) { type_storage = 7; }
        if(DIM == 3) { type_storage = 42; }
        for (size_t i = 0; i < m_mesh->n_cells(); i++){
            fprintf (output_file, "%d ", type_storage);
        }
        fprintf (output_file, "\n%s\n", "            </DataArray>");

        // ---- Faces in cells ----
        // write face connectivity (all vertices) in CSR storage
        // and compute the offset
        std::vector<size_t> cell_offset(m_mesh->n_cells(), 0);
        fprintf (output_file, "%s\n", "            <DataArray type=\"Int32\" Name=\"faces\" format=\"ascii\">");
        for (auto& c : m_mesh->get_cells()) {
            size_t g_i = c->global_index();
            // nb faces in cell c
            fprintf (output_file, "%zu ", c->n_faces());
            cell_offset[g_i] +=1;

            for (auto& f : c->get_faces()) {
                // nb of vertices in face f
                fprintf (output_file, "%zu ", f->n_vertices());

                for (auto& v : f->get_vertices()) {
                    // list of vertices (global index) in face f
                    fprintf (output_file, "%zu ", v->global_index());
                }
                cell_offset[g_i] += f->n_vertices();
            }
            cell_offset[g_i] += c->n_faces();
        }
        fprintf (output_file, "\n%s\n", "            </DataArray>");
        // face offsets (CSR storage)
        // write end localisation of faces in each cell
        fprintf (output_file, "%s\n", "            <DataArray type=\"Int32\" Name=\"faceoffsets\" format=\"ascii\">");
        end_storage = 0;
        for (auto& i : cell_offset) {
            fprintf(output_file, "%zu ", end_storage += i);
        }
        fprintf (output_file, "\n%s\n", "            </DataArray>");
        fprintf (output_file, "%s\n", "         </Cells>");

        return;
    } // write_vtk_mesh_connectivity

    template<size_t DIM>
    void VtkVisu<DIM>::write_vtk_non_conforming_connectivity(FILE* const output_file) {
        // to plot the non conforming method, 
        // the vertices are duplicated so they have new global indexes
        // it complicates the face connectivity

        // --------------------- Cells ---------------------
        // write cells connectivity (all vertices global visu index) in CSR storage
        fprintf(output_file, "%s\n", "         <Cells>");
        fprintf(output_file, "%s\n",
                "            <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">");
        size_t visu_point_gi(0);
        for (auto& c : m_mesh->get_cells()) {
            for(auto& v : c->get_vertices()) {
                fprintf(output_file, "%zu ", visu_point_gi++);
            }
        }
        fprintf (output_file, "\n%s\n", "            </DataArray>");
        // cell offsets (CSR storage) : 
        // write end localisation of vertices in each cell
        // (to know which vertices belong to which cell)
        size_t end_storage = 0;
        fprintf (output_file, "%s\n", "            <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">");
        for (auto& c : m_mesh->get_cells()) {
            fprintf(output_file, "%zu ", end_storage += c->n_vertices());
        }
        fprintf (output_file, "\n%s\n", "            </DataArray>");
        // cell types
        fprintf (output_file, "%s\n", "            <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\">");
        int type_storage(0);
        if(DIM == 2) { type_storage = 7; }
        if(DIM == 3) { type_storage = 42; }
        for (size_t i = 0; i < m_mesh->n_cells(); i++){
            fprintf (output_file, "%d ", type_storage);
        }
        fprintf (output_file, "\n%s\n", "            </DataArray>");

        // ---- Faces in cells ----
        // write face connectivity (all vertices global visu index) in CSR storage
        // and compute the offset
        std::vector<size_t> cell_offset(m_mesh->n_cells(), 0);
        fprintf (output_file, "%s\n", "            <DataArray type=\"Int32\" Name=\"faces\" format=\"ascii\">");
        visu_point_gi = 0; // only to fill the mapping between the mesh vertex global index
                           // and the vertex visu global index
        for (auto& c : m_mesh->get_cells()) {
            size_t g_i = c->global_index();

            // sparse vector to map the mesh vertex global index 
            // to the vertex visu global index which depend on each cell
            Eigen::SparseVector<size_t> map_indexes(m_mesh->n_vertices());
            for (auto& v : c->get_vertices()) {
                map_indexes.coeffRef(v->global_index()) = visu_point_gi++;
            }

            // nb faces in cell c
            fprintf (output_file, "%zu ", c->n_faces());
            cell_offset[g_i] +=1;

            for (auto& f : c->get_faces()) {
                // nb of vertices in face f
                fprintf (output_file, "%zu ", f->n_vertices());

                for (auto& v : f->get_vertices()) {
                    // list of vertices (visu global index) in face f
                    fprintf (output_file, "%zu ", map_indexes.coeffRef(v->global_index()));
                }
                cell_offset[g_i] += f->n_vertices();
            }
            cell_offset[g_i] += c->n_faces();
        }
        fprintf (output_file, "\n%s\n", "            </DataArray>");
        // face offsets (CSR storage)
        // write end localisation of faces in each cell
        fprintf (output_file, "%s\n", "            <DataArray type=\"Int32\" Name=\"faceoffsets\" format=\"ascii\">");
        end_storage = 0;
        for (auto& i : cell_offset) {
            fprintf(output_file, "%zu ", end_storage += i);
        }
        fprintf (output_file, "\n%s\n", "            </DataArray>");
        fprintf (output_file, "%s\n", "         </Cells>");

        return;
    } // write_vtk_non_conforming_connectivity

    // Same beginning for write and write_mesh
    template<size_t DIM>
    FILE* VtkVisu<DIM>::write_vtk_pre_processing(std::string const dir,
                    std::string const filename, size_t n_visu_points) {

        // convert string to char to check if directory already exists ?
        char tmp_dir[dir.length()+1];
        strcpy(tmp_dir,dir.c_str()); 

        // check if directory exists, if not create it.
        if( access(tmp_dir, 0) != 0 ) {
            if( mkdir(dir.c_str(), S_IRWXU) != 0) { // Read + Write + Execute for User
                throw "Could not access nor create output directory for visualization. ";
            } 
            std::cout << " Create output directory " << dir << std::endl;
        }

        // open file
        FILE* output_file(fopen((dir+filename).c_str(), "w"));
#ifdef DEBUG
        if(output_file == NULL) {
            throw "Could not open output file for visualization. ";
        }
#endif

        std::cout << std::endl;
        std::cout << "   ----------------------------------------------" << std::endl;
        std::cout << "   Write the VTK file " << (dir+filename) << std::endl;

        // write header (VTK XML format)
        write_vtk_header(output_file, n_visu_points);

        return output_file;

    } // write_vtk_pre_processing

    // Same post processing for write and write_mesh
    template<size_t DIM>
    void VtkVisu<DIM>::write_vtk_post_processing(FILE* const output_file, bool conforming_method) {

        if(conforming_method) {
            // write vertices
            write_vtk_vertices_coordinates(output_file);
            // write cells
            write_vtk_mesh_connectivity(output_file);
        } else {
            // write vertices
            write_vtk_visu_points_coordinates(output_file); // more than n_vertices, the points are duplicated
            // write cells
            write_vtk_non_conforming_connectivity(output_file);
        }
        // write footer
        write_vtk_footer(output_file);

        // close file
        fclose(output_file);

        std::cout << "   ----------------------------------------------" << std::endl;

        return;
    } // write_vtk_post_processing

    // Write the textfile which contains the mesh in VTK XML format
    template<size_t DIM>
    bool VtkVisu<DIM>::write_mesh(std::string const dir, std::string const filename) {

        // whatever the method, write the mesh the simplest way (n_vertices points, true for conforming_method)
        FILE* output_file(write_vtk_pre_processing(dir, filename, m_mesh->n_vertices()));
        write_vtk_post_processing(output_file, true);

        return true;
    } // write_mesh

    // Write the textfile which contains the data in VTK XML format
    template<size_t DIM>
    bool VtkVisu<DIM>::write(std::string const dir, std::string const filename) {

        FILE* output_file(write_vtk_pre_processing(dir, filename, m_n_visu_points));

        fprintf(output_file, "%s\n", "         <PointData Scalars=\"scalars\">");
        for(size_t i = 0, size = m_data_name.size(); i < size; i++) {
            // write the scalar or vectorial data at the vertices
            write_vtk_data_vertices(output_file, m_vertices_data[i], m_data_name[i], m_vertices_data[i].rows());
        }
        fprintf(output_file, "%s\n", "         </PointData>");

        write_vtk_post_processing(output_file, m_conforming_method);

        return true;
    } // write


    // Write the scalar (components = 1) or vectorial (components > 1) data of vertices in VTK XML format
    template<size_t DIM>
    void VtkVisu<DIM>::write_vtk_data_vertices(FILE* const output_file, Eigen::MatrixXd discrete_data, 
                                        std::string const name, size_t components) {

        // --------------------- Vertices data ---------------------
        // write scalar data at vertices
        // u(0)
        // u(1)
        // ... 
        // u(nb_vertices-1)

        fprintf(output_file, "%s%zu%s\n",
                ("            <DataArray type=\"Float32\" Name=\""+name+"\" NumberOfComponents=\"").c_str(), 
                components, 
                "\" format=\"ascii\">");
        
        for (size_t i = 0; i < m_n_visu_points; i++) {
            for(size_t comp = 0; comp < components; comp++) {
                fprintf(output_file, "%e ", discrete_data(comp, i));
            }
            fprintf(output_file, "\n");
        }

        fprintf(output_file, "%s\n", "            </DataArray>");

        return;
    } // write_vtk_data_vertices

} // MyVisu namespace

#endif /* VTK_WRITER */
