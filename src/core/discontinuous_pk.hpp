/**
  * @{
*/

#ifndef DISCONTINUOUS_PK_CPP
#define DISCONTINUOUS_PK_CPP

#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include "common_geometry.hpp"
#include "polytopal_element.hpp"
#include "cell.hpp"

namespace MyDiscreteSpace {
    /**
     * The DiscontinuousPk class contains 
     * the specific informations for the DG
     * method where the solution is approximated 
     * by a polynomial of order k in each cell.
     * It inherits of the PolytopalElement class.
    */

    template<size_t DIM, int... CONTINUOUS_SPACES> 
    class DiscontinuousPk : public PolytopalElement<DIM>
    {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param k order of the bulk unknowns v_T (k must be equal to ks)
         * @param ks order of the skeletal unknowns (k must be equal to ks)
         * @param var_sizes array with the dimensions of all the variables
         */
        DiscontinuousPk(MyMesh::Cell<DIM>* cell, int k, int ks, const Eigen::ArrayXi& var_sizes);
        ~DiscontinuousPk() = default;   ///<  Default destructor

        /**
         * Compute the reconstruction operator
         * @param var_sizes array with the dimension of each variable
         */
        void set_reconstruction_operator(const Eigen::ArrayXi& var_sizes);
        /**
         * Compute the reconstruction operator
         * with a specific basis (is called in P1 Crouzeix-Raviart P0 for example)
         * @param var_size dimension of variable
         * @param cell_basis specific cell_basis
         * @param var_index index of the variable (0 by default)
         */
        inline void set_reconstruction_operator(size_t var_size,
                MyBasis::Basis<MyMesh::Cell<DIM>*>* cell_basis, size_t var_index = 0);
        /**
         * Using the stiffness matrix, add
         * k * \int_T grad(r_T u_T) * grad(r_T v_T) dx
         * to the local matrix
         * @param var_index index of the variable to add the stiffness contribution
         * @param mult_cst multiplicative constant
         */
        inline void add_stiffness_contribution(size_t var_index = 0, double mult_cst = 1.);
        /**
         * Using the mass matrix, add
         * k * \int_T r_T u_T * r_T v_T dx
         * to the local matrix
         * @param var_index index of the variable to add the stiffness contribution
         * @param mult_cst multiplicative constant
         */
        inline void add_mass_contribution(size_t var_index = 0, double mult_cst = 1.);
        /**
         * Compute the reduction operator (scalar variable)
         * @param func continuous function to which extract reduction
         * @param only_bound_elemt boolean, if true apply reduction operator only on boundary elements
         */
        Eigen::VectorXd apply_reduction_operator(double func (Eigen::ArrayXd),
                size_t var_index = 0,
                bool only_bound_elemt = false);
        /**
         * Compute the reduction operator (vectorial variable)
         * @param func continuous function to which extract reduction
         * @param only_bound_elemt boolean, if true apply reduction operator only on boundary elements
         */
        Eigen::VectorXd apply_reduction_operator(std::vector<double> func (Eigen::ArrayXd),
                size_t var_index = 0,
                bool only_bound_elemt = false);

        /**
         * Add a divergence term knowing two variable indexes
         * @param var_index1 index of the first variable
         * @param var_index2 index of the second variable
         * @param mesh_measure measure of the global domain
         * @param mult_cst multiplicative constant
         */
        void add_divergence_contribution(
                size_t var_index1, 
                size_t var_index2, 
                double mesh_measure,
                double mult_cst = 1.) {
            throw " add_divergence_contribution not defined in DiscontinuousPk ";
        }

        /**
         * Set the local right-hand-side
         * from a continuous function
         * with specific quadrature order 
         * (6 by default)
         * For a scalar variable or vectorial one
         * @param func
         * @param quadra_order
         * @param var_index
         */
        void set_rhs(double func (Eigen::ArrayXd), int quadra_order, size_t var_index);
        void set_rhs(std::vector<double> func (Eigen::ArrayXd), int quadra_order, size_t var_index);

    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations                     //
    /////////////////////////////////////////////////////////////////////////

    // constructor for DiscontinuousPk (has access to the constructor of PolytopalElement).
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    DiscontinuousPk<DIM, CONTINUOUS_SPACES...>::DiscontinuousPk(
                       MyMesh::Cell<DIM>* cell, 
                       int k, 
                       int ks, 
                       const Eigen::ArrayXi& var_sizes) :
    PolytopalElement<DIM>(cell, var_sizes * 0, var_sizes * 0, var_sizes * 0, 
    // number of bulk dof = dim(P^{k}(T))
                       var_sizes * ( DIM == 2 ? (k+1)*(k+2)/2 : (k+1)*(k+2)*(k+3)/6 ), 
    // no static cond is possible, necessary to precise it because bulk dof exist
                       var_sizes * 0) { 

#ifdef DEBUG
        if(ks != k) throw " ks is not equal to k in discontinuous Pk ";
#endif

        this->m_cell_degree = k;
        if(var_sizes[0] == 1) { // one scalar variable
            MyBasis::Basis< MyMesh::Cell<DIM>* >* cell_basis(
                new MyBasis::ScaledMonomialScalarBasis<MyMesh::Cell<DIM>*>(cell, this->m_cell_degree));
            this->m_cell_bases.push_back(cell_basis); 
        } else if(var_sizes[0] == DIM) { // one vectorial variable
            // vectorial full basis
            MyBasis::Basis< MyMesh::Cell<DIM>* >* cell_basis(
                new MyBasis::ScaledMonomialVectorBasis<MyMesh::Cell<DIM>*, MyBasis::FULL>(cell, this->m_cell_degree));
            this->m_cell_bases.push_back(cell_basis);
        }
    }

    template<size_t DIM, int... CONTINUOUS_SPACES> 
    void DiscontinuousPk<DIM, CONTINUOUS_SPACES...>::set_reconstruction_operator(
            const Eigen::ArrayXi& var_sizes) {

        // loop over all the variables
        for(size_t var_index = 0, nb_var = var_sizes.size(); var_index < nb_var; var_index++) {
            // set the reconstruction operator for each variable
            set_reconstruction_operator(var_sizes[var_index], this->m_cell_bases[var_index], var_index);
        }

        return;
    } // set_reconstruction_operator

    // definition of set_reconstruction_operator with a specific cell basis
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    inline void DiscontinuousPk<DIM, CONTINUOUS_SPACES...>::set_reconstruction_operator(
        size_t var_size,
        MyBasis::Basis<MyMesh::Cell<DIM>*>* cell_basis,
        size_t var_index) {

        size_t start_bulk(this->m_bulk_dof[var_index][0]);
        size_t basis_dim(cell_basis->dimension());
        Eigen::MatrixXd rec_op(Eigen::MatrixXd::Zero(basis_dim, this->m_n_cumulate_local_dof));

        rec_op.block(0, start_bulk, basis_dim, basis_dim) = Eigen::MatrixXd::Identity(basis_dim, basis_dim);
        
        this->m_reconstruction_operators.push_back(rec_op);

        return;

    } // set_reconstruction_operator with a given cell basis

    template<size_t DIM, int... CONTINUOUS_SPACES> 
    inline void DiscontinuousPk<DIM, CONTINUOUS_SPACES...>::add_stiffness_contribution(
                    size_t var_index, double mult_cst) {

        throw " discontinuous Pk needs a stabilization which is not implemented ";

        return pe_add_stiffness_contribution<DIM>(
            this->m_reconstruction_operators[var_index],
            this->m_stiffness_matrices[var_index],
            this->m_local_bilinear_form,
            mult_cst);

    } // add_stiffness_contribution

    template<size_t DIM, int... CONTINUOUS_SPACES> 
    inline void DiscontinuousPk<DIM, CONTINUOUS_SPACES...>::add_mass_contribution(
                    size_t var_index, double mult_cst) {

        // compute mass matrix with bulk unknowns only
        // quicker than rec_op.transpose() * mass_matrix * rec_op
        auto bulk_start(this->m_bulk_dof[var_index][0]);
        auto bulk_length(this->m_n_bulk_dof(var_index));
        this->m_local_bilinear_form.block(bulk_start, bulk_start, bulk_length, bulk_length) += mult_cst *
            this->m_mass_matrices[var_index];

        return;
    } // add_mass_contribution

    // rhs is computed as    (f,v_T)_T
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    void DiscontinuousPk<DIM, CONTINUOUS_SPACES...>::set_rhs(
                double func (Eigen::ArrayXd),
                int quadra_order,
                size_t var_index) {

        // v_T indexes
        size_t start(this->m_bulk_dof[var_index][0]);
        size_t length(this->m_n_bulk_dof[var_index]);

        this->m_local_rhs.segment(start, length) += 
            this->int_f_basis(this->m_cell, func, this->m_cell_bases[var_index], quadra_order);

        return;
    } // set_rhs

    // rhs is computed as    (f,v_T)_T
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    void DiscontinuousPk<DIM, CONTINUOUS_SPACES...>::set_rhs(
                std::vector<double> func (Eigen::ArrayXd),
                int quadra_order,
                size_t var_index) {

        // v_T indexes
        size_t start(this->m_bulk_dof[var_index][0]);
        size_t length(this->m_n_bulk_dof[var_index]);

        this->m_local_rhs.segment(start, length) += 
            this->int_f_basis(this->m_cell, func, this->m_cell_bases[var_index], quadra_order);

        return;
    } // set_rhs

    // Define the reduction operator : l2proj(Bulk)
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    Eigen::VectorXd DiscontinuousPk<DIM, CONTINUOUS_SPACES...>::apply_reduction_operator(
                    double func (Eigen::ArrayXd),
                    size_t var_index,
                    bool only_bound_elemt) {

        Eigen::VectorXd reduction_vec(Eigen::VectorXd::Zero(this->m_n_cumulate_local_dof));

        if(!only_bound_elemt) {
            // l2 projection over the cell

            // compute the rhs
            size_t quadra_order(std::max(0, 2 * this->m_cell_degree));
            Eigen::VectorXd rhs(
                this->int_f_basis(this->m_cell, func, this->m_cell_bases[var_index], quadra_order));
            
            size_t start(this->m_bulk_dof[var_index][0]);
            size_t length(this->m_n_bulk_dof[var_index]);
            reduction_vec.segment(start, length) = this->m_mass_matrices[var_index].lu().solve(rhs);
        }

        return reduction_vec;
    } // apply_reduction_operator on scalar variable

    // Define the reduction operator on vectorial variable (same algo as scalar)
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    Eigen::VectorXd DiscontinuousPk<DIM, CONTINUOUS_SPACES...>::apply_reduction_operator(
                    std::vector<double> func (Eigen::ArrayXd),
                    size_t var_index,
                    bool only_bound_elemt) {

        Eigen::VectorXd reduction_vec(Eigen::VectorXd::Zero(this->m_n_cumulate_local_dof));

        if(!only_bound_elemt) {
            // l2 projection over the cell

            // compute the rhs
            size_t quadra_order(std::max(0, 2 * this->m_cell_degree));
            Eigen::VectorXd rhs(
                this->int_f_basis(this->m_cell, func, this->m_cell_bases[var_index], quadra_order));
            
            size_t start(this->m_bulk_dof[var_index][0]);
            size_t length(this->m_n_bulk_dof[var_index]);
            reduction_vec.segment(start, length) = this->m_mass_matrices[var_index].lu().solve(rhs);
        }

        return reduction_vec;
    } // apply_reduction_operator on vectorial variable



} // MyDiscreteSpace namespace
#endif /* DISCONTINUOUS_PK_CPP */
/** @} */
