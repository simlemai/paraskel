/**
  * @{
*/

#ifndef ENHANCED_CONFORMING_VEM
#define ENHANCED_CONFORMING_VEM

#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include "polytopal_element.hpp"
#include "conforming_vem.hpp"
#include "cell.hpp"

namespace MyDiscreteSpace {

    // inherits from VEMBase 
    // the 2D inherits from VEM<2, CONTINUOUS_SPACE> because 
    // there is no modification in 2D, define an enhanced 2D
    // only for more generality

    /// definition of the enhanced VEM class to be able to specify for the different values of DIM
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    class enhancedVEM : public VEMBase<DIM, CONTINUOUS_SPACES...> {
    public:
        /**
         * Default constructor prints error
         */
        enhancedVEM(MyMesh::Cell<DIM>* cell, int kb, int ks, const Eigen::ArrayXi& var_sizes);
        ~enhancedVEM() = default;   ///<  Default destructor
    };

    // partial specialization when DIM = 2 and only one variable (so one continuous space)
    // idem as conforming VEM so inherists from conforming VEM
    template <int CONTINUOUS_SPACE> 
    class enhancedVEM<2, CONTINUOUS_SPACE> : public VEM<2, CONTINUOUS_SPACE> {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param kb order of the bulk discrete unknown
         * @param ks order of the polynomial in the skeletal objects, the order of the reconstruction is ks
         * @param var_sizes list of the dimension of the variables (multiplicative factors for all dof)
         */
        enhancedVEM(MyMesh::Cell<2>* cell, int kb, int ks, const Eigen::ArrayXi& var_sizes); // Poly_base_type pbt, Quad_type qt, Data dat);
        ~enhancedVEM() = default;   ///<  Default destructor
    };

    // partial specialization when DIM = 3 and only one variable (so one continuous space)
    template <int CONTINUOUS_SPACE> 
    class enhancedVEM<3, CONTINUOUS_SPACE> : public VEMBase<3, CONTINUOUS_SPACE> {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param kb order of the bulk discrete unknown
         * @param ks order of the polynomial in the skeletal objects, the order of the reconstruction is ks
         * @param var_sizes list of the dimension of the variables (multiplicative factors for all dof)
         */
        enhancedVEM(MyMesh::Cell<3>* cell, int kb, int ks, const Eigen::ArrayXi& var_sizes); // Poly_base_type pbt, Quad_type qt, Data dat);
        ~enhancedVEM() = default;   ///<  Default destructor

        /**
         * Compute the reconstruction operator with default basis
         * @param var_sizes array with the dimension of each variable
         */
        void set_reconstruction_operator(const Eigen::ArrayXi& var_sizes);
        /**
         * Compute the reconstruction operator for 3D meshes
         * for a specific variable 
         * @param var_size dimension of variable
         * @param var_index index of the variable (0 by default)
         */
        inline void set_reconstruction_operator(size_t var_size, size_t var_index = 0);
        /**
         * Add the stabilization term of the diffusion part to the local matrix
         * @param var_index corresponding index of the variable
         * @param mult_cst multiplicative constant
         */
        inline void add_stabilization(size_t var_index, double mult_cst);

    protected:
        std::vector<std::vector<Eigen::MatrixXd>> m_edges_reconstruction_operators;   ///<  reconstruction operator along each edge, depending on the variable
        std::vector<std::vector<MyBasis::Basis<MyMesh::Face<3>*>*>> 
                m_faces_proj_dof_bases;  ///<  bases of the faces dof of degree ks - 1
    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////

    // constructor in enhancedVEM (should not be used)
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    enhancedVEM<DIM, CONTINUOUS_SPACES...>::enhancedVEM(MyMesh::Cell<DIM>* cell, int kb, int ks, const Eigen::ArrayXi& var_sizes) {
        throw " enhancedVEM not implemented for these continuous spaces ";
    }

    // constructor for enhancedVEM 2D with one variable : 
    // exactly the same as conforming VEM, defined for more generality
    template<int CONTINUOUS_SPACE> 
    enhancedVEM<2, CONTINUOUS_SPACE>::enhancedVEM(MyMesh::Cell<2>* cell, int kb, int ks, const Eigen::ArrayXi& var_sizes) : 
    VEM<2, CONTINUOUS_SPACE>(cell, kb, ks, var_sizes) {}

    // constructor for enhancedVEM 3D with one variable
    template<int CONTINUOUS_SPACE>
    enhancedVEM<3, CONTINUOUS_SPACE>::enhancedVEM(MyMesh::Cell<3>* cell, int kb, int ks, const Eigen::ArrayXi& var_sizes) : 
    // in enhanced VEM, dof are 
    //    - one at each vertex
    //    - the edges, a polynomial of degree ks - 2
    //    - in faces, a polynomial of degree ks - 2 (here is the difference with conforming VEM)
    //    - in cells, a polynomial of degree kb = ks - 1
    // All bulk dof are eliminable.
    VEMBase<3, CONTINUOUS_SPACE>(cell, 
                    kb, ks, // orders of the method
                    var_sizes, // sizes of the variables
                    (ks - 1) * ks / 2, // number of dof in each face (vertices and edges excluded) (= dim P(ks-2) )
                    ks-1,   // number of dof in each edge (vertices excluded), will be multiplied by var_size (= dim P(ks-2) )
                    1,   // number of dof in each vertex (always 1 in VEM)
                    (kb + 1) * (kb + 2) * (kb + 3) / 6) { // number of bulk dof (= dim P_kb(T) ) with kb = ks - 1

        this->m_cell_degree = ks;
        this->m_skeletal_degree = ks;  // polynomial degree of the skeletal unknowns
        this->m_bulk_degree = kb;  // polynomial degree of the bulk unknowns

        m_edges_reconstruction_operators.resize(var_sizes.size()); // give good size (nb_variables)

        for(int i = 0, size = var_sizes.size(); i < size; i++) {
            if(var_sizes[i] == 1) { // one scalar variable
                MyBasis::Basis< MyMesh::Cell<3>* >* cell_basis(
                    new MyBasis::ScaledMonomialScalarBasis<MyMesh::Cell<3>*>(cell, this->m_cell_degree));
                this->m_cell_bases.push_back(cell_basis); 

                // bulk bases : PˆBulkDegree(T)
                MyBasis::Basis< MyMesh::Cell<3>* >* bulk_basis(
                    new MyBasis::ScaledMonomialScalarBasis<MyMesh::Cell<3>*>(cell, this->m_bulk_degree));
                this->m_bulk_bases.push_back(bulk_basis);

                // faces basis of this variable PˆSkelDegree(F)
                std::vector<MyBasis::Basis<MyMesh::Face<3>*>*> faces_bases; // ks
                std::vector<MyBasis::Basis<MyMesh::Face<3>*>*> faces_dof_bases; // ks - 2
                std::vector<MyBasis::Basis<MyMesh::Face<3>*>*> faces_proj_dof_bases; // ks - 1
                for(auto& fpt : cell->get_faces()) {
                    MyBasis::Basis< MyMesh::Face<3>* >* face_basis(
                        new MyBasis::ScaledMonomialScalarBasis<MyMesh::Face<3>*>(fpt, this->m_skeletal_degree));
                    faces_bases.push_back(face_basis); 

                    MyBasis::Basis< MyMesh::Face<3>* >* face_dof_basis(
                        new MyBasis::ScaledMonomialScalarBasis<MyMesh::Face<3>*>(fpt, 
                        std::max(0, this->m_skeletal_degree - 2)));
                    faces_dof_bases.push_back(face_dof_basis);
                    
                    MyBasis::Basis< MyMesh::Face<3>* >* face_proj_dof_basis(
                        new MyBasis::ScaledMonomialScalarBasis<MyMesh::Face<3>*>(fpt, this->m_skeletal_degree - 1));
                    faces_proj_dof_bases.push_back(face_proj_dof_basis);
                }
                this->m_faces_bases.push_back(faces_bases); // all faces bases for this variable (deg ks)
                // m_faces_dof_bases must contain the bases of degree (ks - 2) for apply_reduction_operator
                // defined in VEMBase
                this->m_faces_dof_bases.push_back(faces_dof_bases); // all faces dof bases for this variable (deg ks - 2)
                m_faces_proj_dof_bases.push_back(faces_proj_dof_bases); // all faces dof bases for this variable (deg ks - 1)

                // edges bases of this variable PˆSkelDegree(E)
                std::vector<MyBasis::Basis<MyMesh::Edge<3>*>*> edges_bases; // ks
                std::vector<MyBasis::Basis<MyMesh::Edge<3>*>*> edges_dof_bases; // ks - 2
                for(auto& ept : cell->get_edges()) {
                    // define basis of the edge of degree ks
                    MyBasis::Basis< MyMesh::Edge<3>* >* edge_basis(
                        new MyBasis::ScaledMonomialScalarBasis<MyMesh::Edge<3>*>(ept, this->m_skeletal_degree));
                    edges_bases.push_back(edge_basis);

                    // define basis of the edge of degree ks - 2
                    MyBasis::Basis< MyMesh::Edge<3>* >* edge_dof_basis(
                        new MyBasis::ScaledMonomialScalarBasis<MyMesh::Edge<3>*>(ept, 
                            std::max(0, this->m_skeletal_degree - 2)));
                    edges_dof_bases.push_back(edge_dof_basis);
                }
                this->m_edges_bases.push_back(edges_bases); // all deges bases for this variable (deg ks)
                this->m_edges_dof_bases.push_back(edges_dof_bases); // all edges dof bases for this variable (deg ks - 2)

            } else if(var_sizes[i] == 3) { // one vectorial variable
                // vectorial full basis
                MyBasis::Basis< MyMesh::Cell<3>* >* cell_basis(
                    new MyBasis::ScaledMonomialVectorBasis<MyMesh::Cell<3>*, MyBasis::FULL>(cell, this->m_cell_degree));
                this->m_cell_bases.push_back(cell_basis);
                
                // bulk bases : PˆBulkDegree(T)
                MyBasis::Basis< MyMesh::Cell<3>* >* bulk_basis(
                    new MyBasis::ScaledMonomialVectorBasis<MyMesh::Cell<3>*, MyBasis::FULL>(cell, this->m_bulk_degree));
                this->m_bulk_bases.push_back(bulk_basis);

                // faces basis of this variable PˆSkelDegree(F)
                std::vector<MyBasis::Basis<MyMesh::Face<3>*>*> faces_bases; // ks
                std::vector<MyBasis::Basis<MyMesh::Face<3>*>*> faces_dof_bases; // ks - 2
                std::vector<MyBasis::Basis<MyMesh::Face<3>*>*> faces_proj_dof_bases; // ks - 1
                for(auto& fpt : cell->get_faces()) {
                    MyBasis::Basis< MyMesh::Face<3>* >* face_basis(
                        new MyBasis::ScaledMonomialVectorBasis<MyMesh::Face<3>*, MyBasis::FULL>(
                            fpt, this->m_skeletal_degree));
                    faces_bases.push_back(face_basis); 

                    MyBasis::Basis< MyMesh::Face<3>* >* face_dof_basis(
                        new MyBasis::ScaledMonomialVectorBasis<MyMesh::Face<3>*, MyBasis::FULL>(
                        fpt, std::max(0, this->m_skeletal_degree - 2)));
                    faces_dof_bases.push_back(face_dof_basis);

                    MyBasis::Basis< MyMesh::Face<3>* >* face_proj_dof_basis(
                        new MyBasis::ScaledMonomialVectorBasis<MyMesh::Face<3>*, MyBasis::FULL>(
                            fpt, this->m_skeletal_degree - 1));
                    faces_proj_dof_bases.push_back(face_proj_dof_basis);
                }
                this->m_faces_bases.push_back(faces_bases); // all faces bases for this variable (deg ks)
                // m_faces_dof_bases must contain the bases of degree (ks - 2) for apply_reduction_operator
                // defined in VEMBase
                this->m_faces_dof_bases.push_back(faces_dof_bases); // all faces dof bases for this variable (deg ks - 2)
                m_faces_proj_dof_bases.push_back(faces_proj_dof_bases); // all faces dof bases for this variable (deg ks - 1)

                // edges bases of this variable PˆSkelDegree(E)
                std::vector<MyBasis::Basis<MyMesh::Edge<3>*>*> edges_bases; // ks
                std::vector<MyBasis::Basis<MyMesh::Edge<3>*>*> edges_dof_bases; // ks - 2
                for(auto& ept : cell->get_edges()) {
                    // define basis of the edge of degree ks
                    MyBasis::Basis< MyMesh::Edge<3>* >* edge_basis(
                        new MyBasis::ScaledMonomialVectorBasis<MyMesh::Edge<3>*, MyBasis::FULL>(
                            ept, this->m_skeletal_degree));
                    edges_bases.push_back(edge_basis);

                    // define basis of the edge of degree ks - 2
                    MyBasis::Basis< MyMesh::Edge<3>* >* edge_dof_basis(
                        new MyBasis::ScaledMonomialVectorBasis<MyMesh::Edge<3>*, MyBasis::FULL>(ept, 
                            std::max(0, this->m_skeletal_degree - 2)));
                    edges_dof_bases.push_back(edge_dof_basis);
                }
                this->m_edges_bases.push_back(edges_bases); // all deges bases for this variable (deg ks)
                this->m_edges_dof_bases.push_back(edges_dof_bases); // all edges dof bases for this variable (deg ks - 2)
            }
        }
    }

    // call set_reconstruction_operator in 3D with default basis
    template<int CONTINUOUS_SPACE> 
    void enhancedVEM<3, CONTINUOUS_SPACE>::set_reconstruction_operator(const Eigen::ArrayXi& var_sizes) {

        // loop over all the variables
        for(size_t var_index = 0, nb_var = var_sizes.size(); var_index < nb_var; var_index++) {
            // set the reconstruction operator for each variable
            set_reconstruction_operator(var_sizes[var_index], var_index);
        }

        return;
    } // set_reconstruction_operator 3D

    // definition of set_reconstruction_operator in 3D
    template<int CONTINUOUS_SPACE> 
    void enhancedVEM<3, CONTINUOUS_SPACE>::set_reconstruction_operator(size_t var_size, size_t var_index) {

        // ------------------------------------------------------------------------------------
        // first, construct along each edge r_E v_E in Pk(E) 
        //     with E = [x0, x1]
        // for the stabilization computation
        // with   \int_E (r_E v_E)' z' = - \int_E v_E^o z'' + v1 z'(x1) - v0 z'(x0)    for all z in Pk(E) 
        // and    r_E v_E (x0) = v0
        // where v_E^o = \sum_{j in interior_dof_E} v_E^{o,j} \Psi_{E,j}^{k-2} \in P(k-2)(E)
        // 
        // then, construct r_F v_F in Pk(F) for each face :
        // with   \int_F grad(r_F v_F) grad(z) = 
        //              - \int_F v_F^o laplacien(z) + sum_E \int_E r_E v_E grad(z).n_F,E    for all z in Pk(F)
        // and    \int_F r_F v_F = \int_F v_F^o
        // where v_F^o = \sum_{j <= N_d^{k-2}} v_F^{o,j} \Psi_{F,j}^{k-2} \in P(k-2)(F)
        //
        // then, construct the reconstruction operator
        // with   \int_T grad(r_T v_T) grad(z) = - \int_T v_T^o laplacien(z) 
        //                      + sum_F \int_F t_F grad(z).n_T,F    for all z in Pk(T)
        // and    \int_T r_T v_T = \int_T v_T^o
        // where t_F = v_F + (proj_F^{k-1} - proj_F^{k-2}) r_F v_F
        // ------------------------------------------------------------------------------------

        std::vector<MyMesh::Edge<3>*> edges(this->m_cell->get_edges());
        std::vector<MyMesh::Face<3>*> faces(this->m_cell->get_faces());
        size_t cell_basis_dim(this->m_cell_bases[var_index]->dimension());
        size_t rec_quadra_order(2 * this->m_cell_degree - 1);

        // init RHS for r_T v_T
        Eigen::MatrixXd rhs(Eigen::MatrixXd::Zero(cell_basis_dim, this->m_n_cumulate_local_dof));

        // ------------------------------------------------------------------------------------
        // construct r_E v_E in Pk(E) and store it in m_edges_reconstruction_operators
        // with   \int_E (r_E v_E)' z' = - \int_E v_E^o z'' + v1 z'(x1) - v0 z'(x0)    for all z in Pk(E) 
        // and    r_E v_E (x0) = v0
        // where v_E^o = \sum_{j in interior_dof_E} v_E^{o,j} \Psi_{E,j}^{k-2} \in P(k-2)(E)
        size_t e_li(0);
        for(auto& ept : edges) {
            this->set_reconstruction_operator_edges(var_size, var_index,
                    ept, this->m_edges_dof[var_index][e_li], 
                    this->m_edges_dof_bases[var_index][e_li], 
                    this->m_edges_bases[var_index][e_li], 
                    m_edges_reconstruction_operators[var_index]);

            e_li++; // next edge
        }

        // ------------------------------------------------------------------------------------
        // Construct r_F v_F in Pk(F) for each face :
        // with   \int_F grad(r_F v_F) grad(z) = 
        //              - \int_F v_F^o laplacien(z) + sum_E \int_E r_E v_E grad(z).n_F,E    for all z in Pk(F)
        // and    \int_F r_F v_F = \int_F v_F^o
        // where v_F^o = \sum_{j <= N_d^{k-2}} v_F^{o,j} \Psi_{F,j}^{k-2} \in P(k-2)(F)
        //
        // and add to the rhs
        // sum_F \int_F proj_F^{k-1}(r_F v_F) grad(z).n_T,F    for all z in Pk(T), and v_F in Pk(F)
        size_t f_li(0);
        for(auto& fpt : faces) {            
            size_t face_basis_dim(this->m_faces_bases[var_index][f_li]->dimension());
            size_t face_dof_length(this->m_n_faces_dof(f_li, var_index));

            // init r_F_rhs
            Eigen::MatrixXd r_F_rhs(Eigen::MatrixXd::Zero(face_basis_dim, this->m_n_cumulate_local_dof));

            // add   sum_E \int_E r_E v_E grad(z).n_F,E    for all z in Pk(F)
            std::vector<MyMesh::Edge<3>*> face_edges(fpt->get_edges());
            for(auto& ept : face_edges) {
                // compute the outward normal to the edge
                // Store the vertices coordinates into vertices_coords
                std::vector<MyMesh::Vertex<3>*> vertices(ept->get_vertices());
                std::vector<Eigen::Array<double,3,1>> vertices_coords{vertices[0]->coords(), vertices[1]->coords()};
                Eigen::VectorXd normal(outward_normal(vertices_coords, fpt->mass_center()));

                // find the local indexes of the edge ept in the list of edges of the cell
                // to find the good edge basis
                typename std::vector<MyMesh::Edge<3>*>::iterator it(
                    std::find(edges.begin(), edges.end(), ept));
                size_t e_li(std::distance(edges.begin(), it));

                // for each edge, quadrature to compute the integral 
                // \int_E r_E v_E grad(z).n_F,E    for all z in Pk(F)
                MyQuadra::QuadraturePoints<3> e_qps(MyQuadra::integrate(ept, rec_quadra_order));
                for(auto& qp : e_qps.list()) { 
                    // evaluation of the gradients of the face basis functions at point qp (quadra of the edge)
                    std::vector<Eigen::MatrixXd> grads( // grad(z) with z in Pk(F)
                        this->m_faces_bases[var_index][f_li]->eval_gradients(qp->point()));
                    Eigen::MatrixXd eval_e_reconstruction( // row vector if scalar variable
                        this->m_edges_bases[var_index][e_li]->eval_functions(qp->point()) *
                        m_edges_reconstruction_operators[var_index][e_li]);

                    // add to r_F_rhs
                    // \int_E r_E v_E grad(z).n_F,E    for all z in Pk(F)
                    for(size_t i = 0; i < face_basis_dim; i++) { // loop over z
                        r_F_rhs.row(i) += qp->weight() * (grads[i] * normal).transpose() 
                            * eval_e_reconstruction;
                    }
                }
            } // edges

            // add to r_F_rhs, if ks > 1
            // - \int_F v_F^o laplacien(z)      for all z in Pk(F)
            // where v_F^o = \sum_{j <= N_d^{k-2}} v_F^{o,j} \Psi_{F,j}^{k-2} \in P(k-2)(F) = m_faces_dof_bases
            // Quadrature order is m_cell_degree - 2 + m_skeletal_degree - 2
            size_t face_quadra_order(std::max(0, this->m_cell_degree + this->m_skeletal_degree - 4));
            if(this->m_skeletal_degree > 1) { // no face dof if ks = 1
                MyQuadra::QuadraturePoints<3> f_qps(MyQuadra::integrate(fpt, face_quadra_order));
                // fill r_F_rhs with   - \int_F v_F^o laplacien(z) 
                for(auto& qp : f_qps.list()) {
                    Eigen::MatrixXd qp_laplacians(
                        this->m_faces_bases[var_index][f_li]->eval_laplacians(qp->point()).transpose()); // laplacien(z)
                    Eigen::MatrixXd psi(this->m_faces_dof_bases[var_index][f_li]->eval_functions(qp->point())); // each column is a basis function

                    r_F_rhs.block(0, this->m_faces_dof[var_index][f_li][0], face_basis_dim, face_dof_length) -=
                            qp->weight() * qp_laplacians * psi;
                }
            } // if ks > 1

            // ------------------------------------------------------------------------------------
            // init r_F_lhs with the stiffness matrix which contains \int_F grad(r_F v_F) grad(z)
            Eigen::MatrixXd r_F_lhs(this->compute_stiffness_matrix(
                fpt, this->m_faces_bases[var_index][f_li], 2 * this->m_cell_degree - 2));

            // ------------------------------------------------------------------------------------
            // To fix the constant, if ks > 1, add at both sides the mean product :
            // r_F_rhs \int_F v_F^o dx * \int z dx         for all z in Pk(F)
            //      where v_F^o = \sum_{j <= N_d^{k-2}} v_F^{o,j} \Psi_{F,j}^{k-2} \in P(k-2)(F)
            // r_F_lhs \int_F r_F v_F dx * \int z dx         for all z in Pk(F)
            // if ks = 1, fix the value of one node of the face
            Eigen::MatrixXd int_face_bases; // int z dx
            Eigen::MatrixXd int_interior_basis; // int v_F^o dx

            if(this->m_skeletal_degree == 1) {
                // if ks = 1 : add at both sides, 
                // lhs  1/|F| \mu(x0) z(x0)
                // rhs  1/|F| v0 z(x0)
                MyMesh::Vertex<3>* v_0(fpt->vertex(0));
                // find the local indexes of the element vertices in the list of vertices 
                // of the cell (to find the local index of the dof v0)
                std::vector<MyMesh::Vertex<3>*> cell_vertices(this->m_cell->get_vertices());
                typename std::vector<MyMesh::Vertex<3>*>::iterator it0(
                    std::find (cell_vertices.begin(), cell_vertices.end(), v_0));
                size_t v0_li(std::distance(cell_vertices.begin(), it0));
                
                double scale(1. / fpt->measure());
                // basis functions at x0 : z(x0)
                Eigen::MatrixXd eval_functions_0(
                    this->m_faces_bases[var_index][f_li]->eval_functions(v_0->coords()));
                // rhs  1/|F| * v0 z(x0)
                r_F_rhs.block(0, this->m_vertices_dof[var_index][v0_li][0], face_basis_dim, var_size) += 
                    scale * eval_functions_0.transpose(); 
                // lhs  1/|F| * \mu(x0) z(x0)
                r_F_lhs += scale * eval_functions_0.transpose() * eval_functions_0;

            } else if(var_size == 1) {
                // bases functions are scalar, so int is one value by basis function
                int_face_bases = this->int_f_basis(fpt, 
                    [](Eigen::ArrayXd pt) { return 1.; }, 
                    this->m_faces_bases[var_index][f_li], this->m_cell_degree);
                int_interior_basis = this->int_f_basis(fpt, 
                    [](Eigen::ArrayXd pt) { return 1.; }, 
                    this->m_faces_dof_bases[var_index][f_li], 
                    std::max(0, this->m_skeletal_degree - 2)).transpose();

                double scale(1. / std::pow(fpt->diam(), 2.) / fpt->measure());
                // r_F_rhs scale * \int_F v_F^o dx * \int z dx
                r_F_rhs.block(0, this->m_faces_dof[var_index][f_li][0], face_basis_dim, face_dof_length) +=
                        scale * int_face_bases * int_interior_basis;
                
                // r_F_lhs scale * \int_F r_F v_F dx * \int z dx
                r_F_lhs += scale * int_face_bases * int_face_bases.transpose();

            } else { // var_size = 3
                // bases functions are vectorial, so var_size values by basis function
                // \int_F z dx component by component
                int_face_bases = Eigen::MatrixXd::Zero(var_size, face_basis_dim);
                // \int_F v_F^o dx component by component
                int_interior_basis = Eigen::MatrixXd::Zero(var_size, 
                    this->m_faces_dof_bases[var_index][f_li]->dimension());
                
                // quadrature to compute the integrals
                MyQuadra::QuadraturePoints<3> qps(MyQuadra::integrate(fpt, this->m_cell_degree));

                for(auto& qp : qps.list()) { 
                    // \int_F z dx component by component
                    int_face_bases += qp->weight() * 
                        this->m_faces_bases[var_index][f_li]->eval_functions(qp->point()); // z
                    // \int_F v_F^o dx component by component
                    int_interior_basis += qp->weight() * 
                        this->m_faces_dof_bases[var_index][f_li]->eval_functions(qp->point()); // v_F^o
                } // quadra

                double scale(1. / std::pow(fpt->diam(), 2.) / fpt->measure());
                // r_F_rhs scale * \int_F v_F^o dx * \int z dx
                r_F_rhs.block(0, this->m_faces_dof[var_index][f_li][0], face_basis_dim, face_dof_length) +=
                        scale * int_face_bases.transpose() * int_interior_basis;
                
                // r_F_lhs scale * \int_F r_F v_F dx * \int z dx
                r_F_lhs += scale * int_face_bases.transpose() * int_face_bases;
            }


            // ------------------------------------------------------------------------------------
            // solve r_F v_F = r_F_lhs^{-1} * r_F_rhs (should be quicker than solving
            // the linear system because r_F_rhs has many columns)
            Eigen::MatrixXd f_reconstruction_operator(r_F_lhs.inverse() * r_F_rhs);

            // ------------------------------------------------------------------------------------
            // add to the rhs     \int_F t_F grad(z).n_T,F    for all z in Pk(T)
            // where t_F = proj_F^{k-1}(r_F v_F) + proj_F^{k-1}( v_F - proj_F^{k-2}(r_F v_F) )    in P{k-1}(F)

            // compute all the projections :
            // proj_F^{k-1} (r_F v_F),   - proj_F^{k-2} (r_F v_F)  and  proj_F^{k-1}( v_F - proj_F^{k-2}(r_F v_F) )

            // r_F v_F is written in m_faces_bases whose dimension is face_basis_dim
            Eigen::MatrixXd trace_face_1(Eigen::MatrixXd::Zero(  // for proj_F^{k-1} (r_F v_F)
                m_faces_proj_dof_bases[var_index][f_li]->dimension(), face_basis_dim));
            Eigen::MatrixXd trace_face_2(Eigen::MatrixXd::Zero(  // for proj_F^{k-2} (r_F v_F)
                face_dof_length, face_basis_dim));
            Eigen::MatrixXd trace_face_3(Eigen::MatrixXd::Zero(  // for proj_F^{k-1}( v_F - proj_F^{k-2}(r_F v_F) )
                m_faces_proj_dof_bases[var_index][f_li]->dimension(), face_dof_length));

            // int over the face F to compute the projections
            face_quadra_order = 2 * this->m_skeletal_degree - 1; // max of the orders
            MyQuadra::QuadraturePoints<3> t_qps(MyQuadra::integrate(fpt, face_quadra_order));
            for(auto& qp : t_qps.list()) { // int over T
                // Vectors with the values of face basis functions
                // of degree ks (m_faces_bases), ks - 1 (m_faces_proj_dof_bases) and ks - 2 (m_faces_dof_bases)
                Eigen::MatrixXd func_basis( // ks
                    this->m_faces_bases[var_index][f_li]->eval_functions(qp->point()));
                Eigen::MatrixXd func_basis_1( // ks - 1
                    m_faces_proj_dof_bases[var_index][f_li]->eval_functions(qp->point()));

                trace_face_1 += qp->weight() * func_basis_1.transpose() * func_basis; // from ks to ks - 1
#ifdef DEBUG
    if(this->m_skeletal_degree > 1) { // costly so only in debug mode
#endif
                Eigen::MatrixXd func_basis_2( // ks - 2
                    this->m_faces_dof_bases[var_index][f_li]->eval_functions(qp->point()));

                trace_face_2 += qp->weight() * func_basis_2.transpose() * func_basis; // from ks to ks - 2
                trace_face_3 += qp->weight() * func_basis_1.transpose() * func_basis_2; // from ks - 2 to ks - 1
#ifdef DEBUG
    }
#endif
            } // quadra

            // compute the inverse of the mass matrix of m_faces_proj_dof_bases
            Eigen::MatrixXd inv_mass_matrix_1(this->compute_mass_matrix(fpt, 
                m_faces_proj_dof_bases[var_index][f_li], 2 * this->m_skeletal_degree - 2).inverse());
            // init t_f with proj_F^{k-1} (r_F v_F)
            Eigen::MatrixXd t_f(inv_mass_matrix_1 * trace_face_1 * f_reconstruction_operator);

            if(this->m_skeletal_degree > 1) { // else there is no face dof, nothing to do
                // compute the inverse of the mass matrix of m_faces_dof_bases
                Eigen::MatrixXd inv_mass_matrix_2(this->compute_mass_matrix(fpt, this->m_faces_dof_bases[var_index][f_li], 
                    2 * this->m_skeletal_degree - 4).inverse());
                // - proj_F^{k-2} (r_F v_F)
                Eigen::MatrixXd proj(- inv_mass_matrix_2 * trace_face_2 * f_reconstruction_operator);

                // v_F - proj_F^{k-2} (r_F v_F)
                proj.block(0, this->m_faces_dof[var_index][f_li][0], 
                    face_dof_length, face_dof_length) +=
                    Eigen::MatrixXd::Identity(face_dof_length, face_dof_length);
            
                // add to t_f : proj_F^{k-1}( v_F - proj_F^{k-2}(r_F v_F) )
                t_f += inv_mass_matrix_1 * trace_face_3 * proj;
            } // ks > 1

            // compute the outward normal to the cell
            // Store some vertices coordinates into vertices_coords
            std::vector<MyMesh::Vertex<3>*> vertices(fpt->get_vertices());
            std::vector<Eigen::Array<double,3,1>> vertices_coords{vertices[0]->coords(), 
                vertices[1]->coords(), vertices[2]->coords()}; // face is a 2D object so at least 3 vertices
            Eigen::VectorXd normal(outward_normal(vertices_coords, this->m_cell->mass_center()));

            // quadrature to compute the integral  \int_F t_F grad(z).n_T,F    for all z in Pk(T), 
            //                                                                 and t_F in P{k-1}(F)
            rec_quadra_order = 2 * this->m_cell_degree - 2;
            MyQuadra::QuadraturePoints<3> qps(MyQuadra::integrate(fpt, rec_quadra_order));

            for(auto& qp : qps.list()) { 
                // evaluate gradients of the basis functions at point qp (quadra of the face)
                std::vector<Eigen::MatrixXd> grads(
                    this->m_cell_bases[var_index]->eval_gradients(qp->point()));
                // evaluate t_F at point qp
                Eigen::MatrixXd eval_t_f( // row vector if scalar variable
                    m_faces_proj_dof_bases[var_index][f_li]->eval_functions(qp->point()) * t_f);

                // add to the rhs
                // \int_F t_F grad(z).n_T,F    for all z in Pk(T)
                for(size_t i = 0; i < cell_basis_dim; i++) {
                    rhs.row(i) += qp->weight() * (grads[i] * normal).transpose()
                        * eval_t_f;
                }
            }

            f_li++; // next face
        }

        // ------------------------------------------------------------------------------------
        // add to the rhs
        // - \int_T v_T^o laplacien(z)     for all z in Pk(T)
        // where v_T^o = \sum_{j <= N_d^{k-1}} v_T^{o,j} \Psi_{T,j}^{k-1} \in P(k-1)(T)

        // Quadrature order is m_cell_degree - 2 + m_bulk_degree
        size_t quadra_order(std::max(0, this->m_cell_degree - 2 + this->m_bulk_degree));
        MyQuadra::QuadraturePoints<3> c_qps(MyQuadra::integrate(this->m_cell, quadra_order));
        // cell basis of degree m_cell_degree - 1 (\Psi_{T,j}^{k-1} \in P(k-1)(T))
        //         is this->m_bulk_bases[var_index]
        for(auto& qp : c_qps.list()) {
            Eigen::MatrixXd qp_laplacians(
                this->m_cell_bases[var_index]->eval_laplacians(qp->point()).transpose()); // laplacien(z)
            Eigen::MatrixXd psi(this->m_bulk_bases[var_index]->eval_functions(qp->point()));

            rhs.block(0, this->m_bulk_dof[var_index][0], cell_basis_dim, this->m_n_bulk_dof[var_index]) -= 
                qp->weight() * qp_laplacians * psi;
        }


        // ------------------------------------------------------------------------------------
        // init LHS with the stiffness matrix which contains \int_T grad(r_T v_T) grad(z)
        Eigen::MatrixXd lhs(this->m_stiffness_matrices[var_index]);
        
        // ------------------------------------------------------------------------------------
        // To fix the constant, add at both sides the mean product :
        // rhs \int_T v_T^o dx * \int z dx         for all z in Pk(T)
        //      where v_T^o = \sum_{j <= N_d^{k-1}} v_T^{o,j} \Psi_{T,j}^{k-1} \in P(k-1)(T)
        // lhs \int_T r_T v_T dx * \int z dx         for all z in Pk(T)
        Eigen::MatrixXd int_cell_bases; // int z dx
        Eigen::MatrixXd int_interior_basis; // int v_T^o dx
        if(var_size == 1) {
            // bases functions are scalar, so int is one value by basis function
            int_cell_bases = this->int_f_basis(this->m_cell, 
                [](Eigen::ArrayXd pt) { return 1.; }, 
                this->m_cell_bases[var_index], this->m_cell_degree).transpose();
            int_interior_basis = this->int_f_basis(this->m_cell, 
                [](Eigen::ArrayXd pt) { return 1.; }, 
                this->m_bulk_bases[var_index], this->m_bulk_degree).transpose();
        } else { // vectorial variable
            // bases functions are vectorial, so var_size values by basis function
            // \int_T z dx component by component
            int_cell_bases = Eigen::MatrixXd::Zero(var_size, cell_basis_dim);
            // \int_T v_T^o dx component by component
            int_interior_basis = Eigen::MatrixXd::Zero(var_size, this->m_bulk_bases[var_index]->dimension());
            
            // quadrature to compute the integrals
            MyQuadra::QuadraturePoints<3> qps(
                MyQuadra::integrate(this->m_cell, std::max(this->m_cell_degree, this->m_bulk_degree)));

            for(auto& qp : qps.list()) { 
                // \int_T z dx component by component
                int_cell_bases += qp->weight() * 
                    this->m_cell_bases[var_index]->eval_functions(qp->point()); // z
                // \int_T v_T^o dx component by component
                int_interior_basis += qp->weight() * 
                    this->m_bulk_bases[var_index]->eval_functions(qp->point()); // v_T^o
            } // quadra
        }

        double scale(1. / std::pow(this->m_cell->diam(), 2.) / this->m_cell->measure());
        // rhs scale * \int_T v_T^o dx * \int z dx
        rhs.block(0, this->m_bulk_dof[var_index][0], cell_basis_dim, this->m_n_bulk_dof[var_index]) +=
                scale * int_cell_bases.transpose() * int_interior_basis;
        // lhs scale * \int_T r_T v_T dx * \int z dx
        lhs += scale * int_cell_bases.transpose() * int_cell_bases;

        // ------------------------------------------------------------------------------------
        // solve mu = lhs^{-1} * rhs (should be quicker than solving
        // the linear system because rhs has many columns)
        this->m_reconstruction_operators.push_back(lhs.inverse() * rhs);

        return;
    } // set_reconstruction_operator in 3D

    template<int CONTINUOUS_SPACE> 
    inline void enhancedVEM<3, CONTINUOUS_SPACE>::add_stabilization(size_t var_index, double mult_cst) {

        Eigen::MatrixXd stab(Eigen::MatrixXd::Zero(
            this->m_n_cumulate_local_dof, this->m_n_cumulate_local_dof));

        // local indexes of the bulk dof
        size_t bulk_start(this->m_bulk_dof[var_index][0]);
        size_t bulk_length(this->m_n_bulk_dof[var_index]);
        size_t cell_dim(this->m_cell_bases[var_index]->dimension());

        // ------------------------------------------------------------------------------------
        // integration over the faces

        Eigen::MatrixXd trace_bulk(Eigen::MatrixXd::Zero(bulk_length, cell_dim));
        // int over the cell T to compute l2_proj_Bulk(r_T v_T)
        size_t quadra_order(this->m_bulk_degree + this->m_cell_degree);
        MyQuadra::QuadraturePoints<3> t_qps(MyQuadra::integrate(this->m_cell, quadra_order));
        for(auto& qp : t_qps.list()) { // int over T
            // Vectors with the values of m_bulk_bases[var_index] basis functions
            // and of m_cell_bases[var_index] basis functions
            // evaluated at quadrature point
            Eigen::MatrixXd func_bulk_basis(
                this->m_bulk_bases[var_index]->eval_functions(qp->point()));
            Eigen::MatrixXd func_cell_basis(
                this->m_cell_bases[var_index]->eval_functions(qp->point()));

            trace_bulk += qp->weight() * func_bulk_basis.transpose() * func_cell_basis;
        } // quadra

        // compute the inverse of the mass matrix of the bulk basis functions
        Eigen::MatrixXd inv_mass_matrix_t(this->compute_mass_matrix(this->m_cell, 
            this->m_bulk_bases[var_index], 2 * this->m_bulk_degree).inverse());

        // - l2_proj_Bulk(r_T v_T)
        Eigen::MatrixXd proj_t(
            - inv_mass_matrix_t * trace_bulk * this->m_reconstruction_operators[var_index]);
        // v_bulk - l2_proj_Bulk(r_T v_T)
        proj_t.block(0, bulk_start, bulk_length, bulk_length) +=
            Eigen::MatrixXd::Identity(bulk_length, bulk_length);


        std::vector<MyMesh::Face<3>*> faces(this->m_cell->get_faces());
        size_t f_li(0); // local index of face

        // compute the contribution of
        // l2proj_F{ks-2}(r_T v_T + l2_proj_Bulk{kb}(v_bulk - r_T v_T))
        // nothing to do if ks = 1
        if(this->m_skeletal_degree > 1) { // if smaller ks, no face dof

            for(auto& fpt : faces) {

                // local indexes of the dof of this face
                size_t face_start(this->m_faces_dof[var_index][f_li][0]);
                size_t face_length(this->m_n_faces_dof(f_li, var_index));

                Eigen::MatrixXd stab_f(Eigen::MatrixXd::Zero(face_length, this->m_n_cumulate_local_dof));
                // - v_F : identity at the corresponding dof indexes
                stab_f.block(0, face_start, face_length, face_length) -= 
                    Eigen::MatrixXd::Identity(face_length, face_length);

                // will need the projection over the face of degree k-2,
                // use m_faces_dof_bases (already deg k-2)
                // trace matrix : int over the face of m_faces_dof_bases * m_cell_bases
                Eigen::MatrixXd trace_f_cell(Eigen::MatrixXd::Zero(face_length, cell_dim));
                // trace matrix : int over the face of m_faces_dof_bases * m_bulk_bases
                Eigen::MatrixXd trace_f_bulk(Eigen::MatrixXd::Zero(face_length, bulk_length));

                quadra_order = 2 * this->m_cell_degree - 2;
                MyQuadra::QuadraturePoints<3> f_qps(MyQuadra::integrate(fpt, quadra_order));
                for(auto& qp : f_qps.list()) { // int over F
                    Eigen::MatrixXd func_face_basis(
                        this->m_faces_dof_bases[var_index][f_li]->eval_functions(qp->point()));

                    trace_f_cell += qp->weight() * func_face_basis.transpose() * 
                        this->m_cell_bases[var_index]->eval_functions(qp->point());
                    trace_f_bulk += qp->weight() * func_face_basis.transpose() *
                        this->m_bulk_bases[var_index]->eval_functions(qp->point());
                } // quadra

                // compute the mass matrix of the face basis functions of deg k - 2
                quadra_order = 2 * this->m_cell_degree - 4;
                Eigen::MatrixXd mass_matrix_f(
                    this->compute_mass_matrix(fpt, this->m_faces_dof_bases[var_index][f_li], quadra_order));
                Eigen::MatrixXd inv_mass_matrix_f(mass_matrix_f.inverse());

                // add to stab_f the term l2proj_F(r_T v_T + l2_proj_Bulk(v_bulk - r_T v_T))
                Eigen::MatrixXd proj1( // r_T v_T
                    trace_f_cell * this->m_reconstruction_operators[var_index]);
                Eigen::MatrixXd proj2(trace_f_bulk * proj_t); // l2_proj_Bulk(v_bulk - r_T v_T)
                stab_f += inv_mass_matrix_f * (proj1 + proj2);

                // add the contribution to the global stabilization
                stab += 1./fpt->diam() * stab_f.transpose() * mass_matrix_f * stab_f;

                f_li++; // next face
            }
        } // ks > 1


        // ------------------------------------------------------------------------------------
        // contribution of the edges
        std::vector<MyMesh::Edge<3>*> edges_in_cell(this->m_cell->get_edges());
        quadra_order = 2 * this->m_cell_degree;
        f_li = 0; // local index of face
        for(auto& fpt : faces) {
            std::vector<MyMesh::Edge<3>*> edges_in_face(fpt->get_edges());
            for(auto& ept : edges_in_face) {

                // find the local index of the edge ept in the list of edges of the cell
                typename std::vector<MyMesh::Edge<3>*>::iterator it(
                    std::find(edges_in_cell.begin(), edges_in_cell.end(), ept));
                size_t e_li(std::distance(edges_in_cell.begin(), it));

                // edge objects
                size_t edge_dim(this->m_edges_bases[var_index][e_li]->dimension());
                MyQuadra::QuadraturePoints<3> e_qps(MyQuadra::integrate(ept, quadra_order));
                Eigen::MatrixXd stab_e(Eigen::MatrixXd::Zero(edge_dim, this->m_n_cumulate_local_dof));

                // all the terms of the stabilization are not decomposed in the basis m_edges_bases,
                // so need to project these objects into the basis
                // add int_e r_T v_T + l2_proj_Bulk(v_bulk - r_T v_T), project first
                // trace matrix : int over the edge of edge_bases * m_cell_bases
                Eigen::MatrixXd trace_e_cell(Eigen::MatrixXd::Zero(edge_dim, cell_dim));
                // trace matrix : int over the edge of edge_bases * m_bulk_bases
                Eigen::MatrixXd trace_e_bulk(Eigen::MatrixXd::Zero(edge_dim, bulk_length));
                for(auto& qp : e_qps.list()) {

                    Eigen::MatrixXd func_edge_basis(
                        this->m_edges_bases[var_index][e_li]->eval_functions(qp->point()).transpose());

                    trace_e_cell += qp->weight() * func_edge_basis * 
                        this->m_cell_bases[var_index]->eval_functions(qp->point());
                    trace_e_bulk += qp->weight() * func_edge_basis * 
                        this->m_bulk_bases[var_index]->eval_functions(qp->point());
                }

                // compute the mass matrix of the edge basis functions
                Eigen::MatrixXd mass_matrix_e(
                    this->compute_mass_matrix(ept, this->m_edges_bases[var_index][e_li], quadra_order));
                Eigen::MatrixXd inv_mass_matrix_e(mass_matrix_e.inverse());

                // l2_proj_e(r_T v_T + l2_proj_Bulk(v_bulk - r_T v_T))
                stab_e += inv_mass_matrix_e * (
                    // l2proj_e (r_T v_T)
                    trace_e_cell * this->m_reconstruction_operators[var_index]
                    // proj_t contains   l2_proj_Bulk(v_bulk - r_T v_T)
                    // compute l2proj_e(l2_proj_Bulk(v_bulk - r_T v_T))
                    + trace_e_bulk * proj_t);

                // add   - r_e v_e (already in m_edges_bases decomposition)
                stab_e -= m_edges_reconstruction_operators[var_index][e_li];

                // add the contribution to the global stabilization
                stab += stab_e.transpose() * mass_matrix_e * stab_e;
            } // edges in f_li

            f_li++;
        } // faces

        this->m_local_bilinear_form += mult_cst * stab;

        return;
    } // add_stabilization 3D

} // MyDiscreteSpace namespace
#endif /* CONFORMING_VEM */
/** @} */
