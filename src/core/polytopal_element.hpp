/**
  * @{
*/

#ifndef POLYTOPAL_ELEMENT_HPP
#define POLYTOPAL_ELEMENT_HPP

#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include "bases.hpp"
#include "quadratures.hpp"
#include "cell.hpp"
#include "common_geometry.hpp"
#include "polytopal_element_common.hpp"


namespace MyDiscreteSpace {

    /**
     * The abstract class PolytopalElement contains 
     * all the common informations about
     * the Polytopal Element type used in one cell. 
    */
    template<size_t DIM> 
    class PolytopalElement
    {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param n_dof_face face number of dof for each variable
         * @param n_dof_edge edge number of dof for each variable
         * @param n_dof_vertex vertex number of dof for each variable
         * @param n_dof_bulk bulk number of dof for each variable
         * @param n_bulk_dof_eliminable bulk eliminable number of dof for each variable
         */
        PolytopalElement(MyMesh::Cell<DIM>* cell, 
                       const Eigen::ArrayXi& n_dof_face, 
                       const Eigen::ArrayXi& n_dof_edge, 
                       const Eigen::ArrayXi& n_dof_vertex,
                       const Eigen::ArrayXi& n_dof_bulk, 
                       const Eigen::ArrayXi& n_bulk_dof_eliminable = Eigen::ArrayXi::Constant(1, INT_MAX));
        // /**
        //  * Advanced Constructor
        //  * @param cell pointer to the cell
        //  * @param n_dof_face number of dof for each face and for each variable
        //  * @param n_dof_edge number of dof for each edge and for each variable
        //  * @param n_dof_vertex number of dof for each vertex and for each variable
        //  * @param n_dof_bulk bulk number of dof for each variable
        //  * @param n_bulk_dof_eliminable bulk eliminable number of dof for each variable
        //  */
        // PolytopalElement(MyMesh::Cell<DIM>* cell, 
        //                const std::vector<Eigen::ArrayXi>& n_dof_face, 
        //                const std::vector<Eigen::ArrayXi>& n_dof_edge, 
        //                const std::vector<Eigen::ArrayXi>& n_dof_vertex,
                            // const std::vector<Eigen::ArrayXi>& n_dof_bulk, 
                            // const std::vector<Eigen::ArrayXi>& n_bulk_dof_eliminable = Eigen::ArrayXi::Constant(1, INT_MAX));
        virtual ~PolytopalElement();      ///<  Destructor (delete pointers)

        /**
         * Default algo, takes all skeletal and bulk elements.
         * Set local_index_first_dof_in_element.
         * The local indexes of each element can be computed using
         * local_index_first_dof_in_element and n_dof_total_in_element
         */
        void init_dof_local_index();

        /**
         * Map objects which are initialized with 
         * a method applied on 1 variable
         * to the bigger objects containing the indexes
         * of the n variables. For example
         * is applied on the reconstruction operators 
         * of a pb with 2 variables :
         * - input : the reconstruction operators of 
         * each variable as if the pb contained only one var
         * [rec1]  and  [rec2]
         * - output : add null columns to 
         * the reconstruction operators of each variable to
         * map to the real local indexes taking 
         * into account that there are 2 variables
         * [rec1  0]  and  [0  rec2]
         */
        template<typename T, typename Tn>
        void map_each_to_n_variables(
            const Eigen::ArrayXi& var_sizes,
            const std::vector<PolytopalElement<DIM>*>& list_pe_one_var,
            const std::vector<T>& list_objects_one_var,
            std::vector<Tn>& list_objects_n_var);

        /**
         * mapping for one type of element (verices, faces, edges)
         * when the objects are Eigen::MatrixXd (reconstruction operators for example)
         * one column of the matrix corresponds to one dof index
         */
        void map_each_to_n_variables_by_elemt(
            size_t n_var,
            const Eigen::ArrayXi& var_sizes,
            const std::vector<size_t>& bases_dimensions,
            size_t n_elements, 
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_by_elements,
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_by_elements_ind_var,
            const std::vector<Eigen::MatrixXd>& list_objects_one_var,
            std::vector<Eigen::MatrixXd>& list_objects_n_var);

        /**
         * mapping for bulk dof
         * special algo because not the same object dimensions in m_bulk_dof
         * when the objects are Eigen::MatrixXd
         * one column of the matrix corresponds to one dof index
         */
        void map_each_to_n_variables_bulk(
            size_t n_var,
            const Eigen::ArrayXi& var_sizes,
            const std::vector<size_t>& bases_dimensions,
            const std::vector<std::vector<size_t>>& dof_local_index_ind_var,
            const std::vector<Eigen::MatrixXd>& list_objects_one_var,
            std::vector<Eigen::MatrixXd>& list_objects_n_var);

        /**
         * mapping for one type of element (verices, faces, edges)
         * when the objects are std::vector<T>
         * one object T corresponds to one dof index.
         * (lattice coordinates in Pk Lagrange for example)
         */
        template<typename T> 
        void map_each_to_n_variables_by_elemt(
            size_t n_var,
            const Eigen::ArrayXi& var_sizes,
            const std::vector<size_t>& bases_dimensions,
            size_t n_elements, 
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_by_elements,
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_by_elements_ind_var,
            const std::vector<std::vector<T>>& list_objects_one_var,
            std::vector<T>& list_objects_n_var);

        /**
         * mapping for bulk dof
         * special algo because not the same object dimensions in m_bulk_dof
         * when the objects are std::vector<T>
         * one object T corresponds to one dof index.
         * (lattice coordinates in Pk Lagrange for example)
         */
        template<typename T> 
        void map_each_to_n_variables_bulk(
            size_t n_var,
            const Eigen::ArrayXi& var_sizes,
            const std::vector<size_t>& bases_dimensions,
            const std::vector<std::vector<size_t>>& dof_local_index_ind_var,
            const std::vector<std::vector<T>>& list_objects_one_var,
            std::vector<T>& list_objects_n_var);

        /**
         * get the number of dof in each bulk element of the cell 
         * and the sum of all local dof
         */
        const Eigen::ArrayXi& n_bulk_dof() const;
        const Eigen::ArrayXi& n_bulk_dof_eliminable() const;
        size_t n_remaining_bulk_dof_sc() const;
        size_t n_cumulate_local_dof() const;

        /**
         * get the local index of the first and last dof
         * of each element of the cell, depending on the variable
         */
        const std::vector<std::vector<size_t>>& get_bulk_local_index() const;
        const std::vector<std::vector<std::vector<size_t>>>& get_faces_local_index() const;
        const std::vector<std::vector<std::vector<size_t>>>& get_edges_local_index() const;
        const std::vector<std::vector<std::vector<size_t>>>& get_vertices_local_index() const;

        /**
         * does this method require a lagrange multiplier ?
         */
        bool lagrange_multiplier() const;
        /**
         * over which variable(s) is/are applyed the Lagrange multiplier ?
         * only the variable depending on the scheme are stored here,
         * the user may add some variables from main, in this case only the
         * discrete_space list is updated.
         */
        const std::vector<size_t>& lagrange_multiplier_var_index() const;

        /**
         * Compute the local contributions to have
         * (var, 1)  which is the mean computation of a variable.
         * Is used for the Lagrange multiplier
         * also if the static condensation is used
         */
        Eigen::MatrixXd get_local_mean(
            size_t var_index, size_t var_size, double mesh_measure);
        void local_lagrange_multiplier_contributions_static_cond(
            size_t var_index, size_t var_size, double mesh_measure, 
            Eigen::MatrixXd& local_lagrange_static_cond_M,
            Eigen::VectorXd& local_lagrange_static_cond_rhs);

        /**
         * Compute the stiffness matrices (one by variable)
         * @param var_sizes array with the dimension of each variable
         */
        void init_stiffness_matrices(const Eigen::ArrayXi& var_sizes);

        /**
         * Compute the mass matrices (one by variable)
         * @param var_sizes array with the dimension of each variable
         */
        void init_mass_matrices(const Eigen::ArrayXi& var_sizes);

        /**
         * Compute the reconstruction operator
         * @param var_sizes array with the dimension of each variable
         */
        virtual void set_reconstruction_operator(const Eigen::ArrayXi& var_sizes) = 0;

        /**
         * Modify the stabilization type 
         * @param stabilization_type "ls_stabilization" or the equal order stabilization is used
         */
        void set_stabilization_type(std::string stabilization_type);

        /**
         * Compute the reduction operator (scalar variable)
         * @param func continuous function to which extract reduction
         * @param var_index corresponding index of the variable
         * @param only_bound_elemt apply the reduction operator only at the boundary elements
         */
        virtual Eigen::VectorXd apply_reduction_operator(double func (Eigen::ArrayXd),
                size_t var_index = 0, bool only_bound_elemt = false) = 0;
        /**
         * Compute the reduction operator (vectorial variable)
         * @param func continuous function to which extract reduction
         * @param var_index corresponding index of the variable
         * @param only_bound_elemt apply the reduction operator only at the boundary elements
         */
        virtual Eigen::VectorXd apply_reduction_operator(std::vector<double> func (Eigen::ArrayXd),
                size_t var_index = 0, bool only_bound_elemt = false) = 0;

        void add_to_local_bilinear_form(const Eigen::MatrixXd& local_contribution);
        void add_to_local_rhs(const Eigen::VectorXd& local_contribution);

        /**
         * Add the stiffness contribution multiplied
         * by a constant
         * to the local matrix
         * @param var_index index of the variable to add the mass contribution
         * @param mult_cst multiplicative constant
         */
        virtual void add_stiffness_contribution(size_t var_index = 0, double mult_cst = 1.) = 0; 

        /**
         * Add the mass contribution multiplied
         * by a constant
         * to the local matrix
         * @param var_index index of the variable to add the mass contribution
         * @param mult_cst multiplicative constant
         */
        virtual void add_mass_contribution(size_t var_index = 0, double mult_cst = 1.) = 0; 

        /**
         * Add the divergence term :
         * k * \int_T r_T div(u_T) * r_T div(v_T) dx
         * to each local matrix, knowing the index of the variable u_T
         * @param var_index index of the variable to add the divergence contribution
         * @param mult_cst multiplicative constant
         */
        void add_divergence_contribution(size_t var_index = 0, double mult_cst = 1.);

        /**
         * Add the divergence of a crossed term
         * k * \int_T ru_T div(u_T) * rq_T q_T dx
         * to each local matrix, knowing the indexes of the variables u_T and q_T
         * @param var_index1 index of the variable to multiply on the left
         * @param var_index2 index of the variable to multiply on the right
         * @param mesh_measure measure of the global domain
         * @param mult_cst multiplicative constant
         */
        virtual void add_divergence_contribution(
                size_t var_index1, 
                size_t var_index2, 
                double mesh_measure,
                double mult_cst = 1.) = 0;

        /**
         * Set the local right-hand-side
         * from a continuous function
         * with specific quadrature order 
         * (6 by default)
         * For a scalar variable or vectorial one
         * @param func continuous function with the rhs
         * @param quadra_order specific quadrature order 
         */
        virtual void set_rhs(double func (Eigen::ArrayXd), 
            int quadra_order, size_t var_index) = 0; 
        virtual void set_rhs(std::vector<double> func (Eigen::ArrayXd), 
            int quadra_order, size_t var_index) = 0; 

        /**
         * Compute the L2 othogonal projection of 
         * a continuous (scalar or vector valued) function
         * over an element (face or cell)
         * @param element face or cell over which to project
         * @param element_basis basis of the element
         * @param func scalar or vector valued function
         */
        template<typename RET, typename ELEMT, typename ELEMT_BASIS>
        Eigen::VectorXd orth_l2projection(
                const ELEMT element,
                const ELEMT_BASIS element_basis,
                RET func (Eigen::ArrayXd));

        /**
         * Compute the L2 othogonal projection of 
         * a continuous (scalar or vector valued) function
         * over an element (face or cell)
         * given a mass matrix
         * @param element face or cell over which to project
         * @param element_basis basis of the element
         * @param mass_matrix mass matrix of the projection
         * @param func scalar or vector valued function
         */
        template<typename RET, typename ELEMT, typename ELEMT_BASIS>
        Eigen::VectorXd orth_l2projection(
                const ELEMT element,
                const ELEMT_BASIS element_basis,
                const Eigen::MatrixXd& mass_matrix,
                RET func (Eigen::ArrayXd));

        /**
         * int_f_basis computes the vector containing 
         * the integrale over the element : int f * basis_i
         * when f is vectorial function, it
         * intregrates component by component
         * @param element mesh element over which to compute the integrale
         * @param func continuous scalar or vectorial function to integrate * basis functions
         * @param basis basis function to integrate * f
         * @param quadra_order specific quadrature order (not optional)
         */
        template<typename ELEMT, typename ELEMT_BASIS>
        inline Eigen::VectorXd int_f_basis(
            const ELEMT element,
            double func (Eigen::ArrayXd),
            const ELEMT_BASIS basis,
            size_t quadra_order);
        template<typename ELEMT, typename ELEMT_BASIS>
        inline Eigen::VectorXd int_f_basis(
            const ELEMT element,
            std::vector<double> func (Eigen::ArrayXd),
            const ELEMT_BASIS basis,
            size_t quadra_order);

        /**
         * Compute the lifting to transform into homogeneous 
         * Dirichlet the variable var_index 
         * @param func function to extract the Dirichlet value 
         * @param var_index index of the variable whose Dirichlet value is func
         */
        template<typename RET>
        inline void set_Dirichlet_lifting(RET func (Eigen::ArrayXd), size_t var_index);

        /**
         * Modify the rhs with the Dirichlet lifting
         */
        inline void add_DirichletBC_rhs();

        /**
         * Add the Neumann contribution
         * @param func function to extract the Neumann value 
         * @param var_index index of the variable where Neumann boundary is applied
         */
        template<typename RET>
        inline void set_NeumannBC(RET func (Eigen::ArrayXd), size_t var_index);

        /**
         * Eliminate the bulk unknown(s) applying the static condensation (Shur complement) 
         * on the local matrice this polytopal element
         * Implementation is written in polytopal_element_static_cond.hpp
         */
        inline void static_condensation();
        /**
         * return the permutation matrix which allows to group
         * the eliminable dof at the end of the local index list
         * Matrix of size n_bulk_dof as the skeletal dof 
         * are not concerned by the elimination
         * Implementation is written in polytopal_element_static_cond.hpp
         */
        inline Eigen::MatrixXd permutation_matrix_sc();

        /**
         * reconstruction of the numerical solution
         * starting from
         * @param n_var the number of variables in the problem
         * @param loc2glo the local to global mapping of the dof indexes
         * @param sol_values the global vector with the solution 
         * @param static_cond boolean to give the info if static cond is used 
         */
        void reconstruction(size_t n_var,
            const std::vector<size_t>& loc2glo,
            const Eigen::VectorXd& sol_values,
            bool static_cond);

        /**
         * compute the value of the reconstruction polynomial
         * at a given point defined by its coordinates
         * @param coords coordinates of the point where to compute the reconstruction
         * @param var_index index to determine which (scalar or vectorial) variable is approximated 
         */
        Eigen::VectorXd reconstruction_value_at_pt(const Eigen::VectorXd& coords, size_t var_index);

        /**
         * compute the value of the BULK reconstruction polynomial
         * at a given point defined by its coordinates
         * @param coords coordinates of the point where to compute the bulk reconstruction
         * @param var_index index to determine which (scalar or vectorial) variable is approximated 
         */
        Eigen::VectorXd bulk_reconstruction_value_at_pt(const Eigen::VectorXd& coords, size_t var_index);
        
        MyMesh::Cell<DIM>* get_cell(); ///< pointer to the cell in which this polytopal element is built
        const MyBasis::Basis<MyMesh::Cell<DIM>*>* get_cell_bases(size_t var_index = 0) const; ///< pointer to the basis of the cell (one basis by variable)
        const MyBasis::Basis<MyMesh::Cell<DIM>*>* get_bulk_bases(size_t var_index = 0) const; ///< pointer to the basis of the bulk dof (used to compute the norm of the bulk dof error)
        Eigen::VectorXd get_bulk_sol_values(size_t var_index = 0) const;
        const Eigen::VectorXd& get_reconstructed_solution(size_t var_index = 0) const;
        const Eigen::MatrixXd& get_reconstruction_operator(size_t var_index = 0) const;
        const std::vector<Eigen::MatrixXd>& get_stiffness_matrices() const;
        const std::vector<Eigen::MatrixXd>& get_mass_matrices() const;
        const Eigen::MatrixXd& get_local_bilinear_form() const;
        const Eigen::VectorXd& get_local_rhs() const;

    protected:
        MyMesh::Cell<DIM>* m_cell;  ///<  pointer to the cell
        int m_cell_degree;  ///<  polynomial degree of the numerical solution
        int m_bulk_degree;  ///<  degree of the bulk unknowns (polynomial degree) (mostly used in HHO)
        int m_skeletal_degree;  ///<  degree of the skeletal unknowns (polynomial degree) (mostly used in HHO)
        std::vector<MyBasis::Basis<MyMesh::Cell<DIM>*>*> m_cell_bases;  ///<  bases of the cell (one for each variable)
        std::vector<MyBasis::Basis<MyMesh::Cell<DIM>*>*> m_bulk_bases;  ///<  bases of the bulk dof (one for each variable)  (mostly used in HHO)
        std::vector<std::vector<MyBasis::Basis<MyMesh::Face<DIM>*>*>> 
                m_faces_bases;  ///<  bases of the faces dof (one for each variable and for each face)  (mostly used in HHO)
        
        std::string m_stabilization_type; ///< define which stabilization method is used (mostly used in HHO)

        std::vector<Eigen::MatrixXd> m_stiffness_matrices;   ///<  one stiffness matrices by variable (cell basis functions)
        std::vector<Eigen::MatrixXd> m_mass_matrices;        ///<  one mass matrix by variable (cell basis functions)
        std::vector<Eigen::MatrixXd> m_reconstruction_operators;   ///<  reconstruction operator of the FE method (for each component, variable)
        Eigen::MatrixXd m_local_bilinear_form;       ///<  matrix with the bilinear form local to the cell
        Eigen::VectorXd m_local_rhs;   ///<  vector with the right hand side local to the cell
        Eigen::VectorXd m_local_sol_values;   ///<  vectors with the numerical solution of each local dof
        std::vector<Eigen::VectorXd> m_reconstructed_solution;   ///<  vectors with the reconstruction of the numerical solution (one by variable)
        Eigen::VectorXd m_Dirichlet_lifting; ///< lifting to transform Dirichlet to Homogeneous Dirichlet

        /**
         * The local indexes of each element can be accessed using
         * m_bulk_dof, m_faces_dof, m_edges_dof and m_vertices_dof
         * The eliminable dof (static condensation) must be at the end
         * of the dof list of all the cells' elements.
        */
        size_t m_n_cumulate_local_dof;    ///<  total number of degrees of freedom in the Polytopal Element (contains also the eliminable dof)
        Eigen::ArrayXi m_n_bulk_dof;    ///<  bulk number of dof for each variable (takes into account the dimension of the variables)
        Eigen::ArrayXi m_n_bulk_eliminable_dof;    ///<  number of bulk dof that can be eliminate with static condensation for each variable (takes into account the dimension of the variables)
        size_t m_n_bulk_eliminable_dof_sum;   ///<  sum over all variables of the eliminated dof (static cond)
        Eigen::ArrayXXi m_n_faces_dof;    ///<  number of dof in each face and for each variable (takes into account the dimension of the variables), accessed with local indexes of faces. 1 line = 1 face
        Eigen::ArrayXXi m_n_edges_dof;    ///<  number of dof in each edge and for each variable (takes into account the dimension of the variables), accessed with local indexes of edges
        Eigen::ArrayXXi m_n_vertices_dof;    ///<  number of dof in each vertex and for each variable (takes into account the dimension of the variables), accessed with local indexes of vertices
        size_t m_n_remaining_dof_sc;   ///< local index of the first eliminable cell dof

        std::vector<std::vector<size_t>> m_bulk_dof;   ///< list of the local indexes of the dof of the bulk and variables
        std::vector<std::vector<std::vector<size_t>>> m_faces_dof;   ///< list of the local indexes of the dof of the faces and variables
        std::vector<std::vector<std::vector<size_t>>> m_edges_dof;   ///< list of the local indexes of the dof of the edges and variables
        std::vector<std::vector<std::vector<size_t>>> m_vertices_dof;   ///< list of the local indexes of the dof of the vertices and variables
        std::vector<size_t> m_remaining_bulk_dof_sc;   ///< index of the bulk dof which are not eliminable when static cond is performed. index is distinct than local index (= local index among the bulk dof only)

        // for static condensation
        Eigen::MatrixXd m_shur_M; ///< store matrix to reconstruct the value of the bulk unknowns and the shur elimination if Lagrange multiplier
        Eigen::VectorXd m_shur_V; ///< store vector to reconstruct the value of the bulk unknowns and the shur elimination if Lagrange multiplier

        // does this method require Lagrange multiplier ? 
        // if yes, store the index(es) of the variable(s) (pressure in taylor hood for exemple)
        std::vector<size_t> m_lagrange_multiplier_var_index; // empty if no Lagrange multiplier

        /**
         * Fill local_sol_values with the bulk values 
         * knowing the skelettal unknowns
         * because the bulk unknowns have been eliminated 
         * using the static condensation.
         * Called, if necessary, in reconstruction.
         * Implementation is written in polytopal_element_static_cond.hpp
         * @param local_sol_values local values of the unknowns (modify the bulk values)
         */
        void bulk_shur_reconstruction(Eigen::VectorXd& local_sol_values);

        /**
         * Compute the contraction product between the matrices A and B
         *  = Sum_i,j A_i,j B_i,j
         * @param A matrix
         * @param B matrix
         */
        inline double contraction_product(const Eigen::MatrixXd& A, const Eigen::MatrixXd& B);
        inline double contraction_product(double a, double b);

        template<typename ELEMT, typename ELEMT_BASIS>
        inline Eigen::MatrixXd compute_stiffness_matrix(
                const ELEMT element,
                const ELEMT_BASIS basis, 
                size_t quadra_order);
        template<typename ELEMT, typename ELEMT_BASIS>
        inline Eigen::MatrixXd compute_mass_matrix(
                const ELEMT element,
                const ELEMT_BASIS basis, 
                size_t quadra_order);

    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////
    
    template<size_t DIM>
    PolytopalElement<DIM>::PolytopalElement(MyMesh::Cell<DIM>* cell, 
                       const Eigen::ArrayXi& n_dof_face, 
                       const Eigen::ArrayXi& n_dof_edge, 
                       const Eigen::ArrayXi& n_dof_vertex,
                       const Eigen::ArrayXi& n_dof_bulk, 
                       const Eigen::ArrayXi& n_bulk_dof_eliminable) :
    m_cell(cell),
    m_n_faces_dof(cell->n_faces(), n_dof_face.size()),
    m_n_edges_dof(cell->n_edges(), n_dof_edge.size()),
    m_n_vertices_dof(cell->n_vertices(), n_dof_vertex.size()),
    m_n_bulk_dof(n_dof_bulk.size()),
    m_n_bulk_eliminable_dof(n_dof_bulk.size()) {
        for(size_t i=0, n_faces=cell->n_faces(); i<n_faces; i++) {
            m_n_faces_dof.row(i) = n_dof_face;
        }
        for(size_t i=0, n_edges=cell->n_edges(); i<n_edges; i++) {
            m_n_edges_dof.row(i) = n_dof_edge;
        }
        for(size_t i=0, n_vertices=cell->n_vertices(); i<n_vertices; i++) {
            m_n_vertices_dof.row(i) = n_dof_vertex;
        }
        m_n_bulk_dof = n_dof_bulk;
        if(n_bulk_dof_eliminable[0] < INT_MAX) {
            // if a vector is given, use it
            // store the number of ddl which can be eliminated (one value by variable)
            m_n_bulk_eliminable_dof = n_bulk_dof_eliminable;

            // prepare for static condensation
            // fill the vector m_remaining_bulk_dof_sc
            // with the index in the bulk dof list of the remaining bulk dof
            size_t index_in_bulk_list(0);
            for(size_t var = 0, n_var = m_n_bulk_dof.size(); var < n_var; var++) {
                for(size_t i = 0; i < m_n_bulk_dof[var] - m_n_bulk_eliminable_dof[var]; i++) {
                    // the first dof of the variable where some dof
                    // cannot be eliminated are stored (no elimination)
                    m_remaining_bulk_dof_sc.push_back(index_in_bulk_list + i);
                }
                index_in_bulk_list += m_n_bulk_dof[var];
            }

        } else {
            // otherwise, all the bulk dof are eliminable, and :
            m_n_bulk_eliminable_dof = m_n_bulk_dof;
            // let the vector m_remaining_bulk_dof_sc empty
        }
        m_n_bulk_eliminable_dof_sum = m_n_bulk_eliminable_dof.sum();
    }

    template<size_t DIM>
    PolytopalElement<DIM>::~PolytopalElement() {
        for(auto& cell_basis : m_cell_bases) {
            if(cell_basis != 0) { delete cell_basis; cell_basis = 0; }
        }
        for(auto& bulk_basis : m_bulk_bases) {
            if(bulk_basis != 0) { delete bulk_basis; bulk_basis = 0; }
        }
        for(auto& faces_basis : m_faces_bases) {
            for(auto& face_basis : faces_basis) {
                if(face_basis != 0) { delete face_basis; face_basis = 0; }
            }
        }
    }


    template<size_t DIM>
    void PolytopalElement<DIM>::init_dof_local_index() {
        // give local index of the discrete unknown 
        // Don't forget to set m_n_cumulate_local_dof for later allocation of lists
        size_t n_vertices(m_cell->n_vertices()); // nb of vertices
        size_t n_edges(m_cell->n_edges()); // nb of edges
        size_t n_faces(m_cell->n_faces()); // nb of faces

        // resize vectors which will define the beginning and end of dof local indexes
        m_vertices_dof.resize(m_n_vertices_dof.cols());
        if(DIM == 3) { m_edges_dof.resize(m_n_edges_dof.cols()); }
        m_faces_dof.resize(m_n_faces_dof.cols());
        m_bulk_dof.resize(m_n_bulk_dof.size());

        // ----------------------- set local indexes of all the dof -----------------------
        // ***************************************************************************** //
        //                                                                               //
        //                  If some modifications are applied on the                     //
        //                  local indexes of the dof, you may have                       //
        //                  to modify m_eliminable_bulk_dof !                            //
        //                                                                               //
        // ***************************************************************************** //
        size_t local_index(0);
        // vertices
        for(size_t var = 0, n_var = m_n_vertices_dof.cols(); var < n_var; var++) {
            m_vertices_dof[var].resize(n_vertices);
            for (size_t i = 0; i < n_vertices; i++) {
                // the dof of this vertex and this variable can be accessed via
                // m_vertices_dof[var][i][0] <= dof < m_vertices_dof[var][i][1]
                m_vertices_dof[var][i].push_back(local_index); // first dof for this vertex and this variable
                local_index += m_n_vertices_dof(i, var); // nb of dof
                m_vertices_dof[var][i].push_back(local_index); // last dof
            }
        }
        // (the following 'if' loop is not mandatory as n_edges=0, but 
        // it allows the compiler to remove the useless lines when DIM=2 )
        if(DIM == 3) { // edges
            // edges do not exist in 2D, vectors remain empty.
            for(size_t var = 0, n_var = m_n_edges_dof.cols(); var < n_var; var++) {
                m_edges_dof[var].resize(n_edges);
                for (size_t i = 0; i < n_edges; i++) {
                    // the dof of this edge and this variable can be accessed via
                    // m_edges_dof[var][i][0] <= dof < m_edges_dof[var][i][1]
                    m_edges_dof[var][i].push_back(local_index); // first dof for this edge and this variable
                    local_index += m_n_edges_dof(i, var); // nb of dof
                    m_edges_dof[var][i].push_back(local_index); // last dof
                }
            }
        }
        // faces
        for(size_t var = 0, n_var = m_n_faces_dof.cols(); var < n_var; var++) {
            m_faces_dof[var].resize(n_faces);
            for (size_t i = 0; i < n_faces; i++) {
                // the dof of this face and this variable can be accessed via
                // m_faces_dof[var][i][0] <= dof < m_faces_dof[var][i][1]
                m_faces_dof[var][i].push_back(local_index); // first dof for this face and this variable
                local_index += m_n_faces_dof(i, var); // nb of dof
                m_faces_dof[var][i].push_back(local_index); // last dof
            }
        }

        // bulk = cell
        for(size_t var = 0, n_var = m_n_bulk_dof.size(); var < n_var; var++) {
            // the bulk dof of this variable can be accessed via
            // m_bulk_dof[var][0] <= dof < m_bulk_dof[var][1]
            m_bulk_dof[var].push_back(local_index);  // first dof for this variable
            local_index += m_n_bulk_dof(var); // nb of dof
            m_bulk_dof[var].push_back(local_index); // last dof
        }
        
        // total number of dof in the Polytopal Element (number of dof when nothing is eliminated)
        m_n_cumulate_local_dof = local_index;
        // number of dof which cannot be eliminated using static condensation
        // = skeletal dof + bulk dof which cannot be eliminated
        // = cumulate local dof - eliminable dof
        m_n_remaining_dof_sc = m_n_cumulate_local_dof - m_n_bulk_eliminable_dof_sum;

        // Init m_local_bilinear_form and m_local_rhs with good sizes
        m_local_bilinear_form = Eigen::MatrixXd::Zero(m_n_cumulate_local_dof, m_n_cumulate_local_dof);
        m_local_rhs = Eigen::VectorXd::Zero(m_n_cumulate_local_dof);
        // Init m_Dirichlet_lifting with good size
        m_Dirichlet_lifting = Eigen::VectorXd::Zero(m_n_cumulate_local_dof);
        
        return;
    } // init_dof_local_index


    template<size_t DIM>
    template<typename T, typename Tn>
    void PolytopalElement<DIM>::map_each_to_n_variables(
            const Eigen::ArrayXi& var_sizes,
            const std::vector<PolytopalElement<DIM>*>& list_pe_one_var,
            const std::vector<T>& list_objects_one_var,
            std::vector<Tn>& list_objects_n_var) {

        size_t n_var(var_sizes.size());

        std::vector<size_t> bases_dimensions;
        for(size_t var = 0; var < n_var; var++) {
            bases_dimensions.push_back(m_cell_bases[var]->dimension());
        }

        // vertices
        std::vector<std::vector<std::vector<size_t>>> vertices_dof_local_index_ind_var;
        for(size_t var = 0; var < n_var; var++) {
            vertices_dof_local_index_ind_var.push_back(
                list_pe_one_var[var]->get_vertices_local_index()[0]);
        }
        map_each_to_n_variables_by_elemt(n_var, var_sizes, bases_dimensions,
            m_cell->n_vertices(), m_vertices_dof,
            vertices_dof_local_index_ind_var,
            list_objects_one_var, list_objects_n_var);

        // (the following 'if' loop is not mandatory as n_edges=0, but 
        // it allows the compiler to remove the useless lines when DIM=2 )
        if(DIM == 3) { // edges
            std::vector<std::vector<std::vector<size_t>>> edges_dof_local_index_ind_var;
            for(size_t var = 0; var < n_var; var++) {
                edges_dof_local_index_ind_var.push_back(
                    list_pe_one_var[var]->get_edges_local_index()[0]);
            }
            map_each_to_n_variables_by_elemt(n_var, var_sizes, bases_dimensions,
                m_cell->n_edges(), m_edges_dof,
                edges_dof_local_index_ind_var,
                list_objects_one_var, list_objects_n_var);
        }

        // faces
        std::vector<std::vector<std::vector<size_t>>> faces_dof_local_index_ind_var;
        for(size_t var = 0; var < n_var; var++) {
            faces_dof_local_index_ind_var.push_back(
                list_pe_one_var[var]->get_faces_local_index()[0]);
        }
        map_each_to_n_variables_by_elemt(n_var, var_sizes, bases_dimensions,
            m_cell->n_faces(), m_faces_dof,
            faces_dof_local_index_ind_var, 
            list_objects_one_var, list_objects_n_var);

        // bulk = cell, different loop as the size of the dof_local_index
        // objects are not the same
        std::vector<std::vector<size_t>> bulk_dof_local_index_ind_var;
        for(size_t var = 0; var < n_var; var++) {
            bulk_dof_local_index_ind_var.push_back(
                list_pe_one_var[var]->get_bulk_local_index()[0]);
        }
        map_each_to_n_variables_bulk(n_var, var_sizes, bases_dimensions,
            bulk_dof_local_index_ind_var, 
            list_objects_one_var, list_objects_n_var);

        return;
    } // map_each_to_n_variables

    // loop over one type of element
    // when the objects are Eigen::MatrixXd
    // map n individuals objects to n coupled objects
    template<size_t DIM>
    void PolytopalElement<DIM>::map_each_to_n_variables_by_elemt(
            size_t n_var,
            const Eigen::ArrayXi& var_sizes,
            const std::vector<size_t>& bases_dimensions,
            size_t n_elements, 
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_by_elements,
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_by_elements_ind_var,
            const std::vector<Eigen::MatrixXd>& list_objects_one_var,
            std::vector<Eigen::MatrixXd>& list_objects_n_var) {

        // loop over the variables
        for(size_t var = 0; var < n_var; var++) {
            // loop on the local index of element
            for(size_t e_l = 0; e_l < n_elements; e_l++) { 
                size_t local_index_ind_var(dof_local_index_by_elements_ind_var[var][e_l][0]);
                // loop over all dof of this object
                for(size_t e_dof = dof_local_index_by_elements[var][e_l][0], 
                            end = dof_local_index_by_elements[var][e_l][1]; e_dof < end; 
                            e_dof += var_sizes[var], local_index_ind_var += var_sizes[var]) {
                    // store the block of the object corresponding
                    list_objects_n_var[var].block(0, e_dof, bases_dimensions[var], var_sizes[var]) = 
                        list_objects_one_var[var].block(0, local_index_ind_var, 
                            bases_dimensions[var], var_sizes[var]);
                }
            }
        }
        return;
    } // map_each_to_n_variables_by_elemt

    // loop over bulk dof
    // special algo because not the same object dimensions in m_bulk_dof
    // when the objects are Eigen::MatrixXd
    // map n individuals objects to n coupled objects
    template<size_t DIM>
    void PolytopalElement<DIM>::map_each_to_n_variables_bulk(
            size_t n_var,
            const Eigen::ArrayXi& var_sizes,
            const std::vector<size_t>& bases_dimensions,
            const std::vector<std::vector<size_t>>& dof_local_index_ind_var,
            const std::vector<Eigen::MatrixXd>& list_objects_one_var,
            std::vector<Eigen::MatrixXd>& list_objects_n_var) {

        // loop over the variables
        for(size_t var = 0; var < n_var; var++) {
            size_t local_index_ind_var(dof_local_index_ind_var[var][0]);
            // loop over all bulk dof
            for(size_t b_dof = m_bulk_dof[var][0], end = m_bulk_dof[var][1]; b_dof < end; 
                        b_dof += var_sizes[var], local_index_ind_var += var_sizes[var]) {
                // store the block of the object corresponding
                list_objects_n_var[var].block(0, b_dof, bases_dimensions[var], var_sizes[var]) = 
                    list_objects_one_var[var].block(0, local_index_ind_var, 
                        bases_dimensions[var], var_sizes[var]);
            }
        }

        return;
    } // map_each_to_n_variables_bulk

    // loop over one type of element
    // when the objects are std::vector<T>
    // map n individuals objects to 1 object accessed with the local indexes
    template<size_t DIM>
    template<typename T>
    void PolytopalElement<DIM>::map_each_to_n_variables_by_elemt(
            size_t n_var,
            const Eigen::ArrayXi& var_sizes,
            const std::vector<size_t>& bases_dimensions,
            size_t n_elements, 
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_by_elements,
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_by_elements_ind_var,
            const std::vector<std::vector<T>>& list_objects_one_var,
            std::vector<T>& list_objects_n_var) {

        // loop over the variables
        for(size_t var = 0; var < n_var; var++) {
            // loop on the local index of element
            for(size_t e_l = 0; e_l < n_elements; e_l++) { 
                size_t local_index_ind_var(dof_local_index_by_elements_ind_var[var][e_l][0]);
                // loop over all dof of this variable
                for(size_t e_dof = dof_local_index_by_elements[var][e_l][0], 
                            end = dof_local_index_by_elements[var][e_l][1]; e_dof < end; 
                            e_dof += var_sizes[var], local_index_ind_var += var_sizes[var]) {
                    // store the corresponding object
                    list_objects_n_var[e_dof] = list_objects_one_var[var][local_index_ind_var];
                }
            }
        }
        return;
    } // map_each_to_n_variables_by_elemt

    // loop over bulk dof
    // special algo because not the same object dimensions in m_bulk_dof
    // when the objects are std::vector<T>
    // map n individuals objects to 1 object accessed with the local indexes
    template<size_t DIM>
    template<typename T>
    void PolytopalElement<DIM>::map_each_to_n_variables_bulk(
            size_t n_var,
            const Eigen::ArrayXi& var_sizes,
            const std::vector<size_t>& bases_dimensions,
            const std::vector<std::vector<size_t>>& dof_local_index_ind_var,
            const std::vector<std::vector<T>>& list_objects_one_var,
            std::vector<T>& list_objects_n_var) {

        // loop over the variables
        for(size_t var = 0; var < n_var; var++) {
            size_t local_index_ind_var(dof_local_index_ind_var[var][0]);
            // loop over all bulk dof
            for(size_t b_dof = m_bulk_dof[var][0], end = m_bulk_dof[var][1]; b_dof < end; 
                        b_dof += var_sizes[var], local_index_ind_var += var_sizes[var]) {
                // store the corresponding object
                list_objects_n_var[b_dof] = list_objects_one_var[var][local_index_ind_var];
            }
        }

        return;
    } // map_each_to_n_variables_bulk


    // everything concerning the Degrees of Freedom number and local numbering
    template<size_t DIM>
    size_t PolytopalElement<DIM>::n_cumulate_local_dof() const { return m_n_cumulate_local_dof; }

    template<size_t DIM>
    const Eigen::ArrayXi& PolytopalElement<DIM>::n_bulk_dof() const { return m_n_bulk_dof; }

    template<size_t DIM>
    const Eigen::ArrayXi& PolytopalElement<DIM>::n_bulk_dof_eliminable() const { return m_n_bulk_eliminable_dof; }

    template<size_t DIM>
    size_t PolytopalElement<DIM>::n_remaining_bulk_dof_sc() const { return m_remaining_bulk_dof_sc.size(); }

    template<size_t DIM>
    bool PolytopalElement<DIM>::lagrange_multiplier() const { 
        return (!m_lagrange_multiplier_var_index.empty()); 
    }

    template<size_t DIM>
    const std::vector<size_t>& PolytopalElement<DIM>::lagrange_multiplier_var_index() const { 
        return m_lagrange_multiplier_var_index; 
    }

    template<size_t DIM>
    void PolytopalElement<DIM>::set_stabilization_type(std::string stabilization_type) {
        m_stabilization_type = stabilization_type;
    }
    
    template<size_t DIM>
    const std::vector<std::vector<size_t>>&
    PolytopalElement<DIM>::get_bulk_local_index() const { return m_bulk_dof; }

    template<size_t DIM>
    const std::vector<std::vector<std::vector<size_t>>>&
    PolytopalElement<DIM>::get_faces_local_index() const { return m_faces_dof; }

    template<size_t DIM>
    const std::vector<std::vector<std::vector<size_t>>>&
    PolytopalElement<DIM>::get_edges_local_index() const { return m_edges_dof; }
    
    template<size_t DIM>
    const std::vector<std::vector<std::vector<size_t>>>&
    PolytopalElement<DIM>::get_vertices_local_index() const { return m_vertices_dof; }

    // gives the pointer to the cell
    template<size_t DIM>
    MyMesh::Cell<DIM>* PolytopalElement<DIM>::get_cell() { return m_cell; }

    // gives the pointer to the basis of the cell (polynomial of the numerical reconstruction)
    template<size_t DIM>
    const MyBasis::Basis<MyMesh::Cell<DIM>*>*
    PolytopalElement<DIM>::get_cell_bases(size_t var_index) const { return m_cell_bases[var_index]; }

    // gives the pointer to the basis of the bulk dof (used to compute the norm of the bulk dof error)
    template<size_t DIM>
    const MyBasis::Basis<MyMesh::Cell<DIM>*>*
    PolytopalElement<DIM>::get_bulk_bases(size_t var_index) const { 
#ifdef DEBUG
        if(m_bulk_bases.empty()) {
            throw " get_bulk_bases called but m_bulk_bases is empty. ";
        }
        return m_bulk_bases.at(var_index); // check if exists (m_bulk_bases is not often initialized)
#endif
        return m_bulk_bases[var_index]; 
    }

    template<size_t DIM>
    Eigen::VectorXd PolytopalElement<DIM>::get_bulk_sol_values(size_t var_index) const { 
        return m_local_sol_values.segment(m_bulk_dof[var_index][0], m_n_bulk_dof[var_index]); 
    }

    template<size_t DIM>
    const Eigen::VectorXd& PolytopalElement<DIM>::get_reconstructed_solution(size_t var_index) const { 
        return m_reconstructed_solution[var_index]; 
    }

    template<size_t DIM>
    const Eigen::MatrixXd& PolytopalElement<DIM>::get_reconstruction_operator(size_t var_index) const { 
        return m_reconstruction_operators[var_index]; 
    }

    template<size_t DIM>
    const std::vector<Eigen::MatrixXd>& PolytopalElement<DIM>::get_stiffness_matrices() const { return m_stiffness_matrices; }
    
    template<size_t DIM>
    const std::vector<Eigen::MatrixXd>& PolytopalElement<DIM>::get_mass_matrices() const { return m_mass_matrices; }

    // get the entire local bilinear form (square matrix of size m_n_cumulate_local_dof)
    // if dof are eliminated, it is done in matrix_pattern
    template<size_t DIM>
    const Eigen::MatrixXd& PolytopalElement<DIM>::get_local_bilinear_form() const { return m_local_bilinear_form; }

    // get the entire local right hand side (vector of size m_n_cumulate_local_dof)
    // if dof are eliminated, it is done in matrix_pattern
    template<size_t DIM>
    const Eigen::VectorXd& PolytopalElement<DIM>::get_local_rhs() const { return m_local_rhs; }

    template<size_t DIM>
    void PolytopalElement<DIM>::add_to_local_bilinear_form(const Eigen::MatrixXd& local_contribution) {
        m_local_bilinear_form += local_contribution;
        return;
    }

    // contraction_product = Sum_i,j A_i,j B_i,j
    template<size_t DIM>
    inline double PolytopalElement<DIM>::contraction_product(const Eigen::MatrixXd& A, const Eigen::MatrixXd& B) {
        double sum(0);
        for (size_t i = 0, size = A.size(); i < size; i++) {
            sum += (*(A.data() + i)) * (*(B.data() + i));
        }
        return sum;
    }

    // contraction_product = a * b (define to write general algo)
    template<size_t DIM>
    inline double PolytopalElement<DIM>::contraction_product(double a, double b) {
        return a * b;
    }

    // Compute stiffness matrix given a basis and a quadra order
    //  \int_T \grad u . \grad w dx 
    template<size_t DIM> 
    template<typename ELEMT, typename ELEMT_BASIS>
    inline Eigen::MatrixXd PolytopalElement<DIM>::compute_stiffness_matrix(
                const ELEMT element,
                const ELEMT_BASIS basis, 
                size_t quadra_order) {

        // dimension of the basis
        size_t basis_dim(basis->dimension());
        // quadrature
        MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(element, quadra_order));

        // with scalar variable M(i,j) = sum_qp w(qp) * grads[i].dot(grads[j])
        // with vectorial variable M(i,j) = sum_qp w(qp) * contraction_product(grads[i], grads[j])
        Eigen::MatrixXd stiffness_matrix(Eigen::MatrixXd::Zero(basis_dim, basis_dim));
        
        // compute the integral
        for(auto& qp : qps.list()) { 
            // List of matrices with the gradients of all basis functions wrt all directions
            // evaluated at quadrature point
            std::vector<Eigen::MatrixXd> grads(basis->eval_gradients(qp->point()));
            
            for(size_t j = 0; j < basis_dim; j++) {
                // stiffness matrix is symmetric
                for(size_t i = j; i < basis_dim; i++) {
                    stiffness_matrix(i,j) += qp->weight() * contraction_product(grads[i], grads[j]);
                } 
            }
        } // quadra
        // stiffness matrix is symmetric
        for(size_t j = 0; j < basis_dim; j++) {
            for(size_t i = 0; i < j; i++ ) { stiffness_matrix(i,j) = stiffness_matrix(j,i); }
        }

        return stiffness_matrix;

    } // compute_stiffness_matrix

    // Compute mass matrix given a basis and a quadra order
    //  \int_T  u .  w dx 
    template<size_t DIM> 
    template<typename ELEMT, typename ELEMT_BASIS>
    inline Eigen::MatrixXd PolytopalElement<DIM>::compute_mass_matrix(
                const ELEMT element,
                const ELEMT_BASIS basis, 
                size_t quadra_order) {

        // dimension of the basis
        size_t basis_dim(basis->dimension());
        // quadrature
        MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(element, quadra_order));

        // with scalar variable M(i,j) = sum_qp w(qp) * basis_f(i) * basis_f(j)
        // with vectorial variable M(i,j) = sum_qp w(qp) * contraction_product(basis_f(i), basis_f(j))
        Eigen::MatrixXd mass_matrix(Eigen::MatrixXd::Zero(basis_dim, basis_dim));

        // compute the integral
        for(auto& qp : qps.list()) { 
            // evaluate all basis functions at the quadrature point of the cell
            Eigen::MatrixXd eval_basis(basis->eval_functions(qp->point()));

            for(size_t j = 0; j < basis_dim; j++) {
                // mass matrix is symmetric
                for(size_t i = j; i < basis_dim; i++) {
                    mass_matrix(i,j) += qp->weight() * 
                            eval_basis.col(i).dot(eval_basis.col(j));
                }
            }
        } // quadrature
        
        // mass matrix is symmetric
        for(size_t j = 0; j < basis_dim; j++) {
            for(size_t i = 0; i < j; i++ ) { mass_matrix(i,j) = mass_matrix(j,i); }
        }

        return mass_matrix;

    } // compute_mass_matrix

    // Stiffness Matrix with    \int_T \grad u . \grad w dx 
    template<size_t DIM>
    void PolytopalElement<DIM>::init_stiffness_matrices(const Eigen::ArrayXi& var_sizes) {
        
        // Compute one stiffness matrix by variable
        for(size_t var = 0, size = var_sizes.size(); var < size; var++) {
            // it should also be possible to loop on the m_cell_bases vector,
            // there is one stiffness matrix by cell basis. But we must be able to 
            // map the good stiffness matrix to the good variale. Here I suppose
            // that the bases are different for the variables.

            m_stiffness_matrices.push_back(
                compute_stiffness_matrix(m_cell, m_cell_bases[var], std::max(0, 2 * m_cell_degree - 2)));

        } // end loop on variables

        return;
    } // init_stiffness_matrices

    template<size_t DIM>
    void PolytopalElement<DIM>::init_mass_matrices(const Eigen::ArrayXi& var_sizes) {

        // Compute one mass matrix by variable
        for(size_t var = 0, size = var_sizes.size(); var < size; var++) {
            m_mass_matrices.push_back(
                compute_mass_matrix(m_cell, m_cell_bases[var], std::max(0, 2 * m_cell_degree))
            );

        } // end loop on variables

        return;
    } // init_mass_matrices


    template<size_t DIM>
    void PolytopalElement<DIM>::add_to_local_rhs(const Eigen::VectorXd& local_contribution) {
        m_local_rhs += local_contribution;
        return;
    }

    // Add the divergence term :
    // k * \int_T r_T div(u_T) * r_T div(v_T) dx
    // need to compute the matrix div(Phi_i) * div(Phi_j)
    template<size_t DIM> 
    void PolytopalElement<DIM>::add_divergence_contribution(size_t var_index, double mult_cst) {
        
        // compute M(i,j) = sum_qp w(qp) * div(phi[i]) * div(phi[j]) 
        // polynomial basis of the cell
        size_t basis_dim(m_cell_bases[var_index]->dimension());
        // quadrature
        size_t quadra_order(std::max(0, 2 * m_cell_degree - 2));
        MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(m_cell, quadra_order));

        // always with vectorial variable :
        // M(i,j) = sum_qp w(qp) * div[i] * div[j])
        Eigen::MatrixXd divergence_matrix(Eigen::MatrixXd::Zero(basis_dim, basis_dim));
        
        // compute the integral
        for(auto& qp : qps.list()) { 
            // RowVectorXd with the divergences of all basis functions
            // evaluated at quadrature point
            Eigen::RowVectorXd div(m_cell_bases[var_index]->eval_divergences(qp->point()));
            
            for(size_t j = 0; j < basis_dim; j++) {
                // divergence matrix is symmetric
                for(size_t i = j; i < basis_dim; i++) {
                    divergence_matrix(i,j) += qp->weight() * div[i] * div[j];
                } 
            }
        } // quadra
        // divergence matrix is symmetric
        for(size_t j = 0; j < basis_dim; j++) {
            for(size_t i = 0; i < j; i++ ) { divergence_matrix(i,j) = divergence_matrix(j,i); }
        }

        // add k * \int_T r_T div(u_T) * r_T div(v_T) dx
        this->m_local_bilinear_form += mult_cst * 
            m_reconstruction_operators[var_index].transpose() 
            * divergence_matrix * m_reconstruction_operators[var_index];

        throw " add_divergence_contribution not tested. ";
        return;
    } // add_divergence_contribution

    // compute (var, 1)
    template<size_t DIM>
    Eigen::MatrixXd PolytopalElement<DIM>::get_local_mean(
                size_t var_index, size_t var_size, double mesh_measure) {
        // TODO: I cheat, I know that the constant basis functions are the first ones !

        // compute the basis functions for any point
        // (use mass center so the functions are defined)
        // to extract the constant values (first columns)
        Eigen::MatrixXd scale(m_cell_bases[var_index]->eval_functions(
                        m_cell->mass_center()).leftCols(var_size));

        return scale.inverse().transpose() * m_mass_matrices[var_index].topRows(var_size)
                    * m_reconstruction_operators[var_index] 
                    * 1./mesh_measure;
    } // get_local_mean

    // L2 othogonal projection of a continuous function over an element (face or cell)
    template<size_t DIM> 
    template<typename RET, typename ELEMT, typename ELEMT_BASIS>
    Eigen::VectorXd PolytopalElement<DIM>::orth_l2projection(
            const ELEMT element,
            const ELEMT_BASIS element_basis,
            RET func (Eigen::ArrayXd)) {

        // use quadrature of given order
        size_t quadra_order(2 * element_basis->degree());
        size_t f_quadra_order(9); // the right hand side must be exactly integrated, could be higher

        // first compute the mass matrix
        Eigen::MatrixXd mass_matrix(
            compute_mass_matrix(element, element_basis, quadra_order)
        );

        // then compute the rhs
        Eigen::VectorXd rhs(
            int_f_basis(element, func, element_basis, f_quadra_order)
        );

        // Vector of coefficients (on element basis functions) of the L2 projection of func
        return mass_matrix.lu().solve(rhs);
    } // orth_l2projection of func over an element (face or cell)

    // L2 othogonal projection of a continuous function over an element (face or cell)
    // given a mass matrix
    template<size_t DIM> 
    template<typename RET, typename ELEMT, typename ELEMT_BASIS>
    Eigen::VectorXd PolytopalElement<DIM>::orth_l2projection(
            const ELEMT element,
            const ELEMT_BASIS element_basis,
            const Eigen::MatrixXd& mass_matrix,
            RET func (Eigen::ArrayXd)) {

        // use quadrature of given order
        size_t f_quadra_order(9); // the right hand side must be exactly integrated, could be higher

        // compute the rhs
        Eigen::VectorXd rhs(
            int_f_basis(element, func, element_basis, f_quadra_order)
        );

        // Vector of coefficients (on element basis functions) of the L2 projection of func
        return mass_matrix.lu().solve(rhs);
    } // orth_l2projection of func over an element (face or cell)


    // compute the vector containing 
    // the integrale over the element : int_elmt f * basis_i
    // with f scalar function
    template<size_t DIM> 
    template<typename ELEMT, typename ELEMT_BASIS>
    inline Eigen::VectorXd PolytopalElement<DIM>::int_f_basis(
            const ELEMT element,
            double func (Eigen::ArrayXd),
            const ELEMT_BASIS basis,
            size_t quadra_order) {

        Eigen::VectorXd int_f_basis(Eigen::VectorXd::Zero(basis->dimension()));

        // sum over the quadrature to compute the integrals
        MyQuadra::QuadraturePoints<DIM> elemt_qps(MyQuadra::integrate(element, quadra_order));
        for(auto& qp : elemt_qps.list()) {
            // evaluate all basis functions at point qp
            // (each column is a basis function, thus transpose)
            Eigen::VectorXd eval_basis(basis->eval_functions(qp->point()).transpose()); 
            // value of func at quadrature point * weight
            double f_weighted(func(qp->point()) * qp->weight()); 

            int_f_basis += f_weighted * eval_basis;
        }

        return int_f_basis;
    } // int_f_basis on scalar valued function

    // compute the vector containing  (int f * basis_i)_i
    // with f vectorial function
    // (one scalar by basis function)
    template<size_t DIM> 
    template<typename ELEMT, typename ELEMT_BASIS>
    inline Eigen::VectorXd PolytopalElement<DIM>::int_f_basis(
            const ELEMT element,
            std::vector<double> func (Eigen::ArrayXd),
            const ELEMT_BASIS basis,
            size_t quadra_order) {
            
        Eigen::VectorXd int_f_basis(Eigen::VectorXd::Zero(basis->dimension()));

        // sum over the quadrature to compute the integrals
        MyQuadra::QuadraturePoints<DIM> elemt_qps(MyQuadra::integrate(element, quadra_order));
        for(auto& qp : elemt_qps.list()) {
            // evaluate all basis functions at point qp (each column is a basis function)
            Eigen::MatrixXd eval_basis(basis->eval_functions(qp->point()));
            double wgh(qp->weight());
            // value of func at quadrature point
            std::vector<double> f_qp(func(qp->point()));
            // cast into eigen vector
            Eigen::Map<Eigen::VectorXd> f(f_qp.data(), f_qp.size());

            int_f_basis += wgh * eval_basis.transpose() * f;
        }

        return int_f_basis;
    } // int_f_basis on vectorial valued function


// contains the implementations of static_condensation and bulk_shur_reconstruction
#include "polytopal_element_static_cond.hpp"

    template<size_t DIM>
    void PolytopalElement<DIM>::reconstruction(size_t n_var,
                                            const std::vector<size_t>& loc2glo,
                                            const Eigen::VectorXd& sol_values,
                                            bool static_cond) {

        // fill the vector with the values of the solution 
        // accessed with local indexes
        m_local_sol_values = Eigen::VectorXd::Zero(m_n_cumulate_local_dof);
        for(size_t iL = 0; iL < m_n_cumulate_local_dof; iL++) {
            size_t iG(loc2glo[iL]);
             // if this dof has not been eliminated
            if(iG < SIZE_MAX) { m_local_sol_values[iL] = sol_values[iG]; }
            // if iL has been eliminated by static condensation, 
            // the value is reconstructed in bulk_shur_reconstruction;
            // if iL has been eliminated because it is an Essential dof, 
            // the Dirichlet lifting will reconstructed the value
        }

        if(static_cond) bulk_shur_reconstruction(m_local_sol_values);

        // apply the Dirichlet lifting to get the solution with 
        // the initial Dirichlet BC (non-homogeneous)
        m_local_sol_values += m_Dirichlet_lifting;

        // reconstruct the solution into the bases
        for(size_t i_var = 0; i_var < n_var; i_var++) {
            m_reconstructed_solution.push_back(m_reconstruction_operators[i_var] * m_local_sol_values);
        }
            
        return;
    } // reconstruction


    template<size_t DIM> 
    template<typename RET>
    inline void PolytopalElement<DIM>::set_Dirichlet_lifting(RET func (Eigen::ArrayXd), size_t var_index) {

        // apply reduction operator ONLY on boundary elements
        // because func is set only at the boundary 
        // but all bound elements, not only ESSENTIAL one
        bool only_bound_elemt = true;
        // can be called several times (with distinct variables)
        m_Dirichlet_lifting += this->apply_reduction_operator(func, var_index, only_bound_elemt);

        return;
    } // set_Dirichlet_lifting


    template<size_t DIM> 
    inline void PolytopalElement<DIM>::add_DirichletBC_rhs() {
        // subtract the local bilinear form applied to the Dirichlet lifting : 
        //     interpolate  a(interp(u_sol), v)
        // must be done only once and after that the bilinear form is totally filled, 
        // thus outside of set_Dirichlet_lifting 
        // (called from assemble_local_contributions)
        m_local_rhs -= m_local_bilinear_form * m_Dirichlet_lifting;

        return;
    } // add_DirichletBC_rhs

    template<size_t DIM> 
    template<typename RET>
    inline void PolytopalElement<DIM>::set_NeumannBC(RET func (Eigen::ArrayXd), size_t var_index) {

throw " non-homogeneous Neumann not implemented yet ";

        return;
    } // set_NeumannBC

    template<size_t DIM>
    Eigen::VectorXd PolytopalElement<DIM>::reconstruction_value_at_pt(const Eigen::VectorXd& coords, size_t var_index) {

        // evaluate all basis functions at given point
        Eigen::MatrixXd eval_basis(m_cell_bases[var_index]->eval_functions(coords));
        return eval_basis * m_reconstructed_solution[var_index];
    } // reconstruction_value_at_pt

    template<size_t DIM>
    Eigen::VectorXd PolytopalElement<DIM>::bulk_reconstruction_value_at_pt(const Eigen::VectorXd& coords, size_t var_index) {

        // evaluate bulk basis functions at given point
        Eigen::MatrixXd eval_bulk_basis(m_bulk_bases[var_index]->eval_functions(coords));
        
        return eval_bulk_basis * 
            m_local_sol_values.segment(m_bulk_dof[var_index][0], m_n_bulk_dof[var_index]);
    } // bulk_reconstruction_value_at_pt


} // MyDiscreteSpace namespace
#endif /* POLYTOPAL_ELEMENT_HPP */
/** @} */
