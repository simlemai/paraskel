/**
  * @{
*/

#ifndef POLYTOPAL_ELEMENT_COMMON
#define POLYTOPAL_ELEMENT_COMMON

// does not belong to any class
// common implementations of the stiffness 
// and mass contribution to the local matrix
namespace MyDiscreteSpace {

    /**
     * Using the stiffness matrix, add
     * k * \int_T grad(r_T u_T) * grad(r_T v_T) dx
     * to the local matrix
     * @param reconstruction_op reconstruction operator of the concerned variable
     * @param stiffness_mat stiffness matrix of the concerned variable
     * @param local_bilinear_form local bilinear form 
     * @param mult_cst multiplicative constant
     */
    template<size_t DIM> 
    inline void pe_add_stiffness_contribution(
        Eigen::MatrixXd& reconstruction_op, Eigen::MatrixXd& stiffness_mat, 
        Eigen::MatrixXd& local_bilinear_form, double mult_cst) {

        // add k * \int_T grad(r_T u_T) * grad(r_T v_T) dx
        local_bilinear_form += mult_cst * 
            reconstruction_op.transpose() * stiffness_mat * reconstruction_op;

        return;
    } // pe_add_stiffness_contribution

    /**
     * Using the mass matrix, add
     * k * \int_T r_T u_T * r_T v_T dx
     * to the local matrix
     * @param reconstruction_op reconstruction operator of the concerned variable
     * @param mass_mat mass matrix of the concerned variable
     * @param local_bilinear_form local bilinear form 
     * @param mult_cst multiplicative constant
     */
    template<size_t DIM> 
    inline void pe_add_mass_contribution(
        Eigen::MatrixXd& reconstruction_op, Eigen::MatrixXd& mass_mat, 
        Eigen::MatrixXd& local_bilinear_form, double mult_cst) {

        // add k * \int_T grad(r_T u_T) * grad(r_T v_T) dx
        local_bilinear_form += mult_cst * 
            reconstruction_op.transpose() * mass_mat * reconstruction_op;

        return;
    } // pe_add_mass_contribution


} // MyDiscreteSpace namespace
#endif /* POLYTOPAL_ELEMENT_COMMON */
/** @} */
