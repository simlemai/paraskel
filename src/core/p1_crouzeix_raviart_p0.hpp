/**
  * @{
*/

#ifndef P1_CROUZEIX_RAVIART_P0
#define P1_CROUZEIX_RAVIART_P0

#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include "common_geometry.hpp"
#include "polytopal_element.hpp"
#include "p1_crouzeix_raviart.hpp"
#include "discontinuous_pk.hpp"
#include "cell.hpp"

namespace MyDiscreteSpace {
    /**
     * This class contains the
     * (P1^d Crouzeix-Raviart, P_0) method for Stokes. 
     * It inherits of the PolytopalElement class.
     * It must be applied on simplicial meshes only.
    */

    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    class P1CrouzeixRaviartP0Base : public PolytopalElement<DIM>
    {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param k order of the method
         * @param n_dof_face face number of dof for each variable
         * @param n_dof_edge edge number of dof for each variable
         * @param n_dof_vertex vertex number of dof for each variable
         * @param n_dof_bulk bulk number of dof for each variable
         * @param n_bulk_dof_eliminable bulk eliminable number of dof for each variable 
         */
        P1CrouzeixRaviartP0Base(MyMesh::Cell<DIM>* cell, 
                int k,
                const Eigen::ArrayXi& n_dof_face, 
                const Eigen::ArrayXi& n_dof_edge, 
                const Eigen::ArrayXi& n_dof_vertex, 
                const Eigen::ArrayXi& n_dof_bulk, 
                // by default 0 dof are eliminable using static condensation
                const Eigen::ArrayXi& n_bulk_dof_eliminable = 
                    Eigen::ArrayXi::Constant(2, 0)); // Poly_base_type pbt, Quad_type qt, int k, Data dat);
        ~P1CrouzeixRaviartP0Base() = default;   ///<  Default destructor

        /**
         * Compute the reconstruction operator
         * @param var_sizes array with the dimension of each variable
         */
        void set_reconstruction_operator(const Eigen::ArrayXi& var_sizes);

        /**
         * Using the stiffness matrix, add
         * k * \int_T grad(r_T u_T) * grad(r_T v_T) dx
         * to the local matrix
         * @param var_index index of the variable to add the stiffness contribution
         * @param mult_cst multiplicative constant
         */
        inline void add_stiffness_contribution(size_t var_index = 0, double mult_cst = 1.);

        /**
         * Using the mass matrix, add
         * k * \int_T r_T u_T * r_T v_T dx
         * to the local matrix
         * @param var_index index of the variable to add the stiffness contribution
         * @param mult_cst multiplicative constant
         */
        inline void add_mass_contribution(size_t var_index = 0, double mult_cst = 1.);

        /**
         * Compute the reduction operator(scalar variable)
         * @param func continuous function from which extract the reduction
         * @param var_index corresponding index of the variable
         * @param only_bound_elemt boolean, if true apply reduction operator only on boundary elements
         */
        Eigen::VectorXd apply_reduction_operator(double func (Eigen::ArrayXd),
                size_t var_index = 0,
                bool only_bound_elemt = false);
        /**
         * Compute the reduction operator (vectorial variable)
         * @param func continuous function to which extract reduction
         * @param var_index corresponding index of the variable
         * @param only_bound_elemt boolean, if true apply reduction operator only on boundary elements
         */
        Eigen::VectorXd apply_reduction_operator(std::vector<double> func (Eigen::ArrayXd),
                size_t var_index = 0,
                bool only_bound_elemt = false);

        /**
         * Add the divergence of the crossed terms
         * k * \int div(u) * q dx
         * and its transpose k * \int p * div(v) dx 
         * to the local matrix, knowing the indexes of the variables u and p
         * @param var_index1 index of u, v
         * @param var_index2 index of p, q
         * @param mesh_measure measure of the global domain
         * @param mult_cst multiplicative constant
         */
        void add_divergence_contribution(
                size_t var_index1, 
                size_t var_index2, 
                double mesh_measure,
                double mult_cst = 1.);

        /**
         * Set the local right-hand-side
         * from a continuous function
         * with specific quadrature order 
         * (6 by default)
         * For a scalar variable or vectorial one
         * @param func continuous function with the rhs
         * @param quadra_order specific quadrature order
         * @param var_index variable index coresponding
         */
        void set_rhs(double func (Eigen::ArrayXd), int quadra_order, size_t var_index);
        void set_rhs(std::vector<double> func (Eigen::ArrayXd), int quadra_order, size_t var_index);
    };

    /// definition of the P1CrouzeixRaviartP0 class
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    class P1CrouzeixRaviartP0 : public P1CrouzeixRaviartP0Base<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P> {
    public:
        /**
         * Default constructor prints error
         */
        P1CrouzeixRaviartP0(MyMesh::Cell<DIM>* cell, int k, int ks, const Eigen::ArrayXi& var_sizes);
        ~P1CrouzeixRaviartP0() = default;   ///<  Default destructor
    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////

    // constructor for P1CrouzeixRaviartP0Base (has access to the constructor of PolytopalElement).
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> // CONTINUOUS_SPACES are ordered : u, p
    P1CrouzeixRaviartP0Base<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::P1CrouzeixRaviartP0Base(
                       MyMesh::Cell<DIM>* cell, 
                       int k,
                       const Eigen::ArrayXi& n_dof_face, 
                       const Eigen::ArrayXi& n_dof_edge, 
                       const Eigen::ArrayXi& n_dof_vertex,
                       const Eigen::ArrayXi& n_dof_bulk, 
                       const Eigen::ArrayXi& n_bulk_dof_eliminable) :
    PolytopalElement<DIM>(cell, n_dof_face, n_dof_edge, n_dof_vertex, 
                       n_dof_bulk, n_bulk_dof_eliminable) {
        this->m_cell_degree = 1; // u is in P_1^d, p in P_0
        // vectorial full basis for the velocity
        MyBasis::Basis< MyMesh::Cell<DIM>* >* cell_basis_u(
            new MyBasis::ScaledMonomialVectorBasis<MyMesh::Cell<DIM>*, MyBasis::FULL>(cell, this->m_cell_degree));
        this->m_cell_bases.push_back(cell_basis_u);
        // scalar basis for the pressure (only constant because order is 0)
        MyBasis::Basis< MyMesh::Cell<DIM>* >* cell_basis_p(
            new MyBasis::ScaledMonomialScalarBasis<MyMesh::Cell<DIM>*>(cell, this->m_cell_degree - 1));
        this->m_cell_bases.push_back(cell_basis_p);

        // this FE requires a Lagrange multiplier applied on the pressure (var index is 1)
        this->m_lagrange_multiplier_var_index.push_back(1);
    }

    // constructor for P1CrouzeixRaviartP0 with two variables
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    P1CrouzeixRaviartP0<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::P1CrouzeixRaviartP0(
        MyMesh::Cell<DIM>* cell, int k, int ks, const Eigen::ArrayXi& var_sizes) :
    P1CrouzeixRaviartP0Base<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>(cell, 
                                        k, // order of the method : Lagrange with u in Pk and p in P(k-1)
                                        var_sizes * Eigen::Map<Eigen::Array2i> (((std::vector<int>) {1, 0}).data(), 2), // number of dof in each face  (1 for u, 0 for p)
                                        var_sizes * 0,  // number of dof in each edge (vertices excluded, always 0)
                                        var_sizes * 0,  // number of dof in each vertex (always 0 in CR)
                                        var_sizes * Eigen::Map<Eigen::Array2i> (((std::vector<int>) {0, 1}).data(), 2) ) { // number of bulk dof (0 for u, 1 for p)
        if(ks != k) throw " ks is not equal to k in P1CrouzeixRaviartP0 ";
        if(k != 1) throw " the order is not good in P1CrouzeixRaviartP0 ";
    }

    // definition of set_reconstruction_operator :
    // the size of the reconstruction operator is basis_dim * cumulate_local_dof.
    // Even with more than one variable, in this case it has nul columns 
    // coresponding to the local indexes of the other variables.
    // It allows to have generic algo in other parts of the code.
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    void P1CrouzeixRaviartP0Base<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::set_reconstruction_operator(
        const Eigen::ArrayXi& var_sizes) {

#ifdef DEBUG
        if(var_sizes.size() != 2) {
            throw " (P1CR, P0) with other than 2 continuous spaces not implemented ";
        }
#endif
        size_t u_var(0); // u is the first variable in var_sizes
        size_t p_var(1); // p is the second variable in var_sizes
        size_t var_size_u(var_sizes[u_var]); // dimension of the velocity variable (should be DIM because u is vectorial)
        size_t var_size_p(var_sizes[p_var]); // dimension of the pressure variable (should be 1 because p is scalar)
        Eigen::ArrayXi array_var_size_u(Eigen::ArrayXi::Constant(1, var_size_u));
        Eigen::ArrayXi array_var_size_p(Eigen::ArrayXi::Constant(1, var_size_p));
        size_t basis_dim_u(this->m_cell_bases[u_var]->dimension()); // dimension of the velocity basis
        size_t basis_dim_p(this->m_cell_bases[p_var]->dimension()); // dimension of the pressure basis

#ifdef DEBUG
        if(var_size_u != DIM || var_sizes[p_var] != 1) {
            throw " this (P1CR, P0) reconstruction is implemented with u in Rd, p in R; not good array var_sizes ";
        }
#endif

        // u in P1 CR
        P1CrouzeixRaviart<DIM, CONTINUOUS_SPACE_U> 
            cr_u(P1CrouzeixRaviart<DIM, CONTINUOUS_SPACE_U>(this->m_cell, 1, 1, array_var_size_u));
        // p in discontinuous P0
        DiscontinuousPk<DIM, CONTINUOUS_SPACE_P> 
            p0_p(DiscontinuousPk<DIM, CONTINUOUS_SPACE_P>(this->m_cell, 0, 0, array_var_size_p));

        // The reconstruction operator of this method is
        // based on each individual reconstruction operator of u and p
        // First set the local indexes of all the dof and the stiffness matrix of CR
        cr_u.init_dof_local_index();
        p0_p.init_dof_local_index();
        cr_u.init_stiffness_matrices(array_var_size_u);
        // then set the reconstruction operator using the specified bases
        // 0 is optional but careful, there is only one variable in each class (cr_u and p0_p)
        cr_u.set_reconstruction_operator(var_size_u, this->m_cell_bases[u_var], 0);
        p0_p.set_reconstruction_operator(var_size_p, this->m_cell_bases[p_var], 0);

        // --------------------------------------------------------------
        // map the individual methods
        // to a problem with 2 variables

        // vector containing the list of the FE with one individual variable
        std::vector<PolytopalElement<DIM>*> list_individual_fe({&cr_u, &p0_p});

        std::vector<Eigen::MatrixXd> reconstruction_operators_individual_var;
        reconstruction_operators_individual_var.push_back(cr_u.get_reconstruction_operator());
        reconstruction_operators_individual_var.push_back(p0_p.get_reconstruction_operator());

        // map_each_to_n_variables fills the object but it must already have the good size
        this->m_reconstruction_operators.push_back( // fix size for the u variable
                Eigen::MatrixXd::Zero(basis_dim_u, this->m_n_cumulate_local_dof));
        this->m_reconstruction_operators.push_back( // fix size for the p variable
                Eigen::MatrixXd::Zero(basis_dim_p, this->m_n_cumulate_local_dof));

        // So map the square reconstruction operators to the rectangular one
        // to map velocity or pressure contiguous local indexes
        // into there local indexes in the PE class
        // (add null columns)
        this->map_each_to_n_variables(var_sizes, list_individual_fe,
            reconstruction_operators_individual_var,
            this->m_reconstruction_operators);
        
        return;

    } // set_reconstruction_operator (same algo for 2D or 3D)

    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    inline void P1CrouzeixRaviartP0Base<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::add_stiffness_contribution(
                    size_t var_index, double mult_cst) {

        return pe_add_stiffness_contribution<DIM>(
            this->m_reconstruction_operators[var_index],
            this->m_stiffness_matrices[var_index],
            this->m_local_bilinear_form,
            mult_cst);

    } // add_stiffness_contribution

    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    inline void P1CrouzeixRaviartP0Base<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::add_mass_contribution(
                    size_t var_index, double mult_cst) {

        return pe_add_mass_contribution<DIM>(
            this->m_reconstruction_operators[var_index],
            this->m_mass_matrices[var_index],
            this->m_local_bilinear_form,
            mult_cst);
    } // add_mass_contribution

    // Define the reduction operator : 
    // scalar function = pressure so P0 and mean over the cell
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    Eigen::VectorXd P1CrouzeixRaviartP0Base<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::apply_reduction_operator(
                    double func (Eigen::ArrayXd),
                    size_t var_index,
                    bool only_bound_elemt) {

        // it is a scalar valued function, the reduction might be applied on the pressure
#ifdef DEBUG
        if(var_index != 1) {
            throw " apply_reduction_operator on scalar valued functions are only implemented for pressure yet. ";
        }
        if(only_bound_elemt) {
            throw " apply_reduction_operator only at boundary elements for P0 has no meaning. ";
        }
#endif

        Eigen::VectorXd reduction_vec(Eigen::VectorXd::Zero(this->m_n_cumulate_local_dof));

        // compute the mean of function over the cell
        double mean(0.);
        MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(this->m_cell, 6));
        for(auto& qp : qps.list()) { mean += qp->weight() * func(qp->point()); }
        mean /= this->m_cell->measure();

        // m_bulk_dof[var_index=1][0] contains the local index of the first (only) dof of the pressure
        reduction_vec[this->m_bulk_dof[var_index][0]] = mean;

        return reduction_vec;
    } // apply_reduction_operator on scalar variable

    // Define the reduction operator :
    // vectorial function = velocity so P1 Crouzeix Raviart
    // mean values of the func over the faces
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    Eigen::VectorXd P1CrouzeixRaviartP0Base<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::apply_reduction_operator(
                    std::vector<double> func (Eigen::ArrayXd),
                    size_t var_index,
                    bool only_bound_elemt) {

#ifdef DEBUG
        if(var_index != 0) {
            throw " apply_reduction_operator on vectorial valued functions are only implemented for velocity yet. ";
        }
#endif

        Eigen::VectorXd reduction_vec(Eigen::VectorXd::Zero(this->m_n_cumulate_local_dof));

        std::vector<MyMesh::Face<DIM>*> faces(this->m_cell->get_faces());
        size_t f_li(0);
        for(auto& fpt : faces) {
            if(!only_bound_elemt || fpt->is_boundary()) {
                // compute the mean value of the func over the face
                Eigen::VectorXd mean(Eigen::VectorXd::Zero(DIM));
                // integrate the function over the face
                // quadrature of fpt with order quadra_order (unuseful because k = 1)
                // size_t quadra_order(std::min(6, this->m_cell_degree + 1));

                MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(fpt, 6));
                for(auto& qp : qps.list()) { 
                    std::vector<double> f_qp(func(qp->point()));
                    mean += qp->weight() * Eigen::Map<Eigen::VectorXd> (f_qp.data(), f_qp.size()); 
                }
                mean /= fpt->measure();
        
                // m_faces_dof[var_index=0][f_li][0] contains the local index of the first dof 
                // of the face of the first (only) variable
                reduction_vec.segment(this->m_faces_dof[var_index][f_li][0], DIM) = mean;
            }
            f_li++;
        }

        return reduction_vec;
    } // apply_reduction_operator on vectorial variable

    // Add the divergence of the crossed terms
    // k * \int div(u) * q dx
    // and its transpose k * \int p * div(v) dx 
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    void P1CrouzeixRaviartP0Base<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::add_divergence_contribution(
                size_t var_index1, 
                size_t var_index2, 
                double mesh_measure,
                double mult_cst) {

        // cell polynomial bases of the two considered variables
        size_t basis_dim1(this->m_cell_bases[var_index1]->dimension()); // phi u
        size_t basis_dim2(this->m_cell_bases[var_index2]->dimension()); // psi p
        // use a quadrature of order 2 * m_cell_degree - 2 = 0
        // because div(u) is of order k-1 and p also
        size_t quadra_order(0);
        MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(this->m_cell, quadra_order));

        // M(i,j) = sum_qp w(qp) * div(phi[j]) * psi[i]
        Eigen::MatrixXd bases_matrix(Eigen::MatrixXd::Zero(basis_dim2, basis_dim1));

        // compute the integral
        for(auto& qp : qps.list()) { 
            // evaluate all basis functions at the quadrature point of the cell
            Eigen::RowVectorXd eval_basis1(
                    this->m_cell_bases[var_index1]->eval_divergences(qp->point())); // div(phi)
            Eigen::RowVectorXd eval_basis2(
                    this->m_cell_bases[var_index2]->eval_functions(qp->point())); // psi

            bases_matrix += qp->weight() * eval_basis2.transpose() * eval_basis1;
        } // quadra

        Eigen::MatrixXd specific_mat(
                this->m_reconstruction_operators[var_index2].transpose() 
                * bases_matrix
                * this->m_reconstruction_operators[var_index1]);

        this->m_local_bilinear_form += mult_cst * (specific_mat + specific_mat.transpose());

        return;
    } // add_divergence_contribution

    // fill the rhs with \int f r_T v_T (FE method)
    // with a specific quadrature order
    // with scalar variable
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    void P1CrouzeixRaviartP0Base<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::set_rhs(
                double func (Eigen::ArrayXd),
                int quadra_order,
                size_t var_index) {

        throw " entered in set_rhs with scalar function, should not append in (P1CR, P0) ";

        return;
    } // set_rhs scalar

    // fill the rhs with \int f r_T v_T (FE method)
    // with a specific quadrature order
    // with a vectorial variable
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    void P1CrouzeixRaviartP0Base<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::set_rhs(
                std::vector<double> func (Eigen::ArrayXd),
                int quadra_order,
                size_t var_index) {

        // var index should correspond to u
        // int_f_basis computes the vector containing  (int f * basis_i)_i 
        // with f vectorial function (one scalar by basis function)
        this->m_local_rhs += this->m_reconstruction_operators[var_index].transpose() 
                * this->int_f_basis(this->m_cell, func, this->m_cell_bases[var_index], quadra_order);

        return;
    } // set_rhs vectorial

} // MyDiscreteSpace namespace
#endif /* P1_CROUZEIX_RAVIART_P0 */
/** @} */
