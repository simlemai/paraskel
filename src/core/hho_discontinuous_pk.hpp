/**
  * @{
*/

#ifndef HHO_DISCONTINUOUS_PK
#define HHO_DISCONTINUOUS_PK

#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include "common_geometry.hpp"
#include "hho.hpp"
#include "discontinuous_pk.hpp"
#include "cell.hpp"

namespace MyDiscreteSpace {
    /**
     * return the list of eliminable dof 
     * in this HHO(kb, ks) discontinuous Pk method
     * @param kb order of the HHO bulk unknowns v_T
     * @param ks polynomial degree of the skeletal unknowns and order of the Pks discontinuous unknowns
     * @return list of 2 values : the number of u eliminable dof, the number of p eliminable dof
    */
    template<size_t DIM> 
    static inline Eigen::Array2i n_eliminable_dof_pk(size_t kb, size_t ks) {
        Eigen::Array2i n_eliminable_bulk_dof;
        // nb of u eliminable dof (all)
        n_eliminable_bulk_dof(0) = n_dof_bulk_pk<DIM>(kb);
        // nb of p eliminable dof, all except one of the bulk pressure dof can be eliminated
        n_eliminable_bulk_dof(1) = std::max(0, n_dof_bulk_pk<DIM>(ks) - 1);
        
        return n_eliminable_bulk_dof;
    } // n_eliminable_dof_pk


    /**
     * This class contains the
     * (HHO(kb, ks), discontinuous Pks) method for Stokes. 
     * It inherits of the HHOBase class to call 
     * common HHO methods as the stabilization
     * or the orthogonal l2 projections.
     * It can be applied on general meshes.
    */
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    class HHOdiscontinuousPk : public HHOBase<DIM>
    {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param kb order of the bulk unknowns v_T
         * @param ks order of the skeletal unknowns
         * @param var_sizes array with the dimensions of all the variables
         */
        HHOdiscontinuousPk(MyMesh::Cell<DIM>* cell, int kb, int ks, const Eigen::ArrayXi& var_sizes);
        ~HHOdiscontinuousPk() = default;   ///<  Default destructor

        /**
         * Compute the reconstruction operator
         * @param var_sizes array with the dimension of each variable
         */
        void set_reconstruction_operator(const Eigen::ArrayXi& var_sizes);

        /**
         * Using the stiffness matrix, add
         * k * \int_T grad(r_T u_T) * grad(r_T v_T) dx
         * and the stabilization term
         * to the local matrix
         * @param var_index index of the variable to add the stiffness contribution
         * @param mult_cst multiplicative constant
         */
        inline void add_stiffness_contribution(size_t var_index = 0, double mult_cst = 1.);

        /**
         * Add the product of the bulk unknowns
         * to the local matrix
         * @param var_index index of the variable to add the stiffness contribution
         * @param mult_cst multiplicative constant
         */
        inline void add_mass_contribution(size_t var_index = 0, double mult_cst = 1.);

        /**
         * Add the divergence of the crossed terms
         * k * \int div(u) * q dx
         * and its transpose k * \int p * div(v) dx 
         * to the local matrix, knowing the indexes of the variables u and p
         * @param var_index1 index of u, v
         * @param var_index2 index of p, q
         * @param mesh_measure measure of the global domain
         * @param mult_cst multiplicative constant
         */
        void add_divergence_contribution(
                size_t var_index1, 
                size_t var_index2, 
                double mesh_measure,
                double mult_cst = 1.);

        /**
         * Compute the reduction operator(scalar variable)
         * @param func continuous function from which extract the reduction
         * @param var_index corresponding index of the variable
         * @param only_bound_elemt boolean, if true apply reduction operator only on boundary elements
         */
        Eigen::VectorXd apply_reduction_operator(double func (Eigen::ArrayXd),
                size_t var_index = 0,
                bool only_bound_elemt = false);
        /**
         * Compute the reduction operator (vectorial variable)
         * @param func continuous function to which extract reduction
         * @param var_index corresponding index of the variable
         * @param only_bound_elemt boolean, if true apply reduction operator only on boundary elements
         */
        Eigen::VectorXd apply_reduction_operator(std::vector<double> func (Eigen::ArrayXd),
                size_t var_index = 0,
                bool only_bound_elemt = false);

        /**
         * Set the local right-hand-side
         * from a continuous function
         * with specific quadrature order 
         * (6 by default)
         * For a scalar variable or vectorial one
         * @param func continuous function with the rhs
         * @param quadra_order specific quadrature order
         * @param var_index variable index coresponding
         */
        void set_rhs(double func (Eigen::ArrayXd), int quadra_order, size_t var_index);
        void set_rhs(std::vector<double> func (Eigen::ArrayXd), int quadra_order, size_t var_index);
    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////

    // constructor for HHOdiscontinuousPk with two variables
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    HHOdiscontinuousPk<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::HHOdiscontinuousPk(
        MyMesh::Cell<DIM>* cell, int kb, int ks, const Eigen::ArrayXi& var_sizes) :
    HHOBase<DIM>(cell, 
                var_sizes * Eigen::Map<Eigen::Array2i> (((std::vector<int>) 
                    {n_dof_faces_pk<DIM>(ks), 0}).data(), 2), // number of dof in each face  (HHO(ks) for u, 0 for p)
                var_sizes * 0,  // number of dof in each edge (vertices excluded, always 0)
                var_sizes * 0,  // number of dof in each vertex (always 0 in CR)
                var_sizes * Eigen::Map<Eigen::Array2i> (((std::vector<int>) 
                    {n_dof_bulk_pk<DIM>(kb), n_dof_bulk_pk<DIM>(ks)}).data(), 2), // number of bulk dof (HHO(kb) for u, Pks for p)
                var_sizes * n_eliminable_dof_pk<DIM>(kb, ks) // number of eliminable bulk dof (all for u, all-1 for p)
                ) { 

        this->m_cell_degree = ks + 1; // polynomial degree of the numerical solution of u; p is m_cell_degree - 1
        this->m_skeletal_degree = ks;  // polynomial degree of the skeletal unknowns
        this->m_bulk_degree = kb;  // polynomial degree of the bulk unknowns (u and p)

        // chose which stabilization will be used. Can be modified using set_stabilization_type
        this->m_stabilization_type = { 
            this->m_bulk_degree - this->m_skeletal_degree == 1 ? "ls_stabilization" : "other" };

        // this PE requires a Lagrange multiplier applied on the pressure (var index is 1)
        this->m_lagrange_multiplier_var_index.push_back(1);

        // -------------  vectorial full basis for the velocity  -------------
        // velocity must be filled first !
        // vectorial full basis PˆSkelDegree(T)
        MyBasis::Basis< MyMesh::Cell<DIM>* >* cell_basis_v(
            new MyBasis::ScaledMonomialVectorBasis<MyMesh::Cell<DIM>*, MyBasis::FULL>(cell, this->m_cell_degree));
        this->m_cell_bases.push_back(cell_basis_v);

        // bulk bases : PˆBulkDegree(T)
        MyBasis::Basis< MyMesh::Cell<DIM>* >* bulk_basis_v(
            new MyBasis::ScaledMonomialVectorBasis<MyMesh::Cell<DIM>*, MyBasis::FULL>(cell, this->m_bulk_degree));
        this->m_bulk_bases.push_back(bulk_basis_v);

        // faces basis of this variable PˆSkelDegree(F)
        std::vector<MyBasis::Basis<MyMesh::Face<DIM>*>*> faces_basis_v;
        for(auto& fpt : cell->get_faces()) {
            MyBasis::Basis< MyMesh::Face<DIM>* >* face_basis_v(
                new MyBasis::ScaledMonomialVectorBasis<MyMesh::Face<DIM>*, MyBasis::FULL>(fpt, this->m_skeletal_degree));
            faces_basis_v.push_back(face_basis_v); 
        }
        this->m_faces_bases.push_back(faces_basis_v); // all faces bases for this variable

        // -------------  scalar basis for the pressure  -------------
        MyBasis::Basis< MyMesh::Cell<DIM>* >* cell_basis_p(
            new MyBasis::ScaledMonomialScalarBasis<MyMesh::Cell<DIM>*>(cell, this->m_cell_degree - 1));
        this->m_cell_bases.push_back(cell_basis_p); 
  
    }

    // definition of set_reconstruction_operator :
    // the size of the reconstruction operator is basis_dim * cumulate_local_dof.
    // Even with more than one variable, in this case it has nul columns 
    // coresponding to the local indexes of the other variables.
    // It allows to have generic algo in other parts of the code.
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    void HHOdiscontinuousPk<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::set_reconstruction_operator(
        const Eigen::ArrayXi& var_sizes) {

#ifdef DEBUG
        if(var_sizes.size() != 2) {
            throw " (HHO, discontinuous Pk) with other than 2 continuous spaces not implemented ";
        }
#endif
        size_t u_var(0); // u is the first variable in var_sizes
        size_t p_var(1); // p is the second variable in var_sizes
        size_t var_size_u(var_sizes[u_var]); // dimension of the velocity variable (should be DIM because u is vectorial)
        size_t var_size_p(var_sizes[p_var]); // dimension of the pressure variable (should be 1 because p is scalar)
        Eigen::ArrayXi array_var_size_u(Eigen::ArrayXi::Constant(1, var_size_u));
        Eigen::ArrayXi array_var_size_p(Eigen::ArrayXi::Constant(1, var_size_p));
        size_t basis_dim_u(this->m_cell_bases[u_var]->dimension()); // dimension of the velocity basis
        size_t basis_dim_p(this->m_cell_bases[p_var]->dimension()); // dimension of the pressure basis

#ifdef DEBUG
        if(var_size_u != DIM || var_sizes[p_var] != 1) {
            throw " this (HHO, discontinuous Pk) reconstruction is implemented with u in Rd, p in R; not good array var_sizes ";
        }
#endif

        // u in HHO(kb, ks)
        HHO<DIM, CONTINUOUS_SPACE_U> 
            hho_u(HHO<DIM, CONTINUOUS_SPACE_U>(
                this->m_cell, this->m_bulk_degree, this->m_skeletal_degree, array_var_size_u));
        // p in discontinuous Pk of order this->m_cell_degree - 1
        DiscontinuousPk<DIM, CONTINUOUS_SPACE_P> 
            pk_p(DiscontinuousPk<DIM, CONTINUOUS_SPACE_P>(
                this->m_cell, this->m_cell_degree - 1, this->m_cell_degree - 1, array_var_size_p));

        // The reconstruction operator of this method is
        // based on each individual reconstruction operator of u and p
        // First set the local indexes of all the dof and the stiffness matrix of CR
        hho_u.init_dof_local_index();
        pk_p.init_dof_local_index();
        hho_u.init_stiffness_matrices(array_var_size_u); // necessary to set the reconstruction op
        // then set the reconstruction operator using the specified bases
        // 0 is optional but careful that these classes are defined with one variable each
        hho_u.set_reconstruction_operator(var_size_u, this->m_cell_bases[u_var],
            this->m_bulk_bases[u_var], this->m_faces_bases[u_var], 0);
        pk_p.set_reconstruction_operator(var_size_p, this->m_cell_bases[p_var], 0);

        // --------------------------------------------------------------
        // map the individual methods
        // to a problem with 2 variables

        // vector containing the list of the PE with one individual variable
        std::vector<PolytopalElement<DIM>*> list_individual_pe({&hho_u, &pk_p});

        std::vector<Eigen::MatrixXd> reconstruction_operators_individual_var;
        reconstruction_operators_individual_var.push_back(hho_u.get_reconstruction_operator());
        reconstruction_operators_individual_var.push_back(pk_p.get_reconstruction_operator());

        // map_each_to_n_variables fills the object but it must already have the good size
        this->m_reconstruction_operators.push_back( // fix size for the u variable
                Eigen::MatrixXd::Zero(basis_dim_u, this->m_n_cumulate_local_dof));
        this->m_reconstruction_operators.push_back( // fix size for the p variable
                Eigen::MatrixXd::Zero(basis_dim_p, this->m_n_cumulate_local_dof));

        // So map the square reconstruction operators to the rectangular one
        // to map velocity or pressure contiguous local indexes
        // into there local indexes in the PE class
        // (add null columns)
        this->map_each_to_n_variables(var_sizes, list_individual_pe,
            reconstruction_operators_individual_var,
            this->m_reconstruction_operators);
        
        return;

    } // set_reconstruction_operator (same algo for 2D or 3D)

    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    inline void HHOdiscontinuousPk<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::add_stiffness_contribution(
                    size_t var_index, double mult_cst) {

        pe_add_stiffness_contribution<DIM>(
            this->m_reconstruction_operators[var_index],
            this->m_stiffness_matrices[var_index],
            this->m_local_bilinear_form,
            mult_cst);

        // for the velocity (var_index = 0), necessary to add the stabilization term
        if(var_index == 0) { this->add_stabilization(var_index, mult_cst); }

        return;

    } // add_stiffness_contribution

    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    inline void HHOdiscontinuousPk<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::add_mass_contribution(
                    size_t var_index, double mult_cst) {

        throw " add_mass_contribution in HHOdiscontinuousPk not implemented yet ";

        return pe_add_mass_contribution<DIM>(
            this->m_reconstruction_operators[var_index],
            this->m_mass_matrices[var_index],
            this->m_local_bilinear_form,
            mult_cst);
    } // add_mass_contribution

    // Add the divergence of the crossed terms
    // k * \int div(u) * q dx
    // and its transpose k * \int p * div(v) dx 
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    void HHOdiscontinuousPk<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::add_divergence_contribution(
                size_t var_index1, // u
                size_t var_index2, // p
                double mesh_measure,
                double mult_cst) {

        // the divergence operator is obtained with
        // \int div(u) * q dx = - \int_T v_T grad(q) dx + sum(i=1->n_F) \int_Fi v_Fi q normal(Fi,T) ds
        Eigen::MatrixXd div_matrix(Eigen::MatrixXd::Zero(
            this->m_n_cumulate_local_dof, this->m_n_cumulate_local_dof));
        
        // compute - \int_T v_T grad(q) dx
        size_t start_bulk_u(this->m_bulk_dof[var_index1][0]);
        size_t end_bulk_u(this->m_bulk_dof[var_index1][1]);
        size_t start_bulk_p(this->m_bulk_dof[var_index2][0]);
        size_t end_bulk_p(this->m_bulk_dof[var_index2][1]);

        // use a quadrature of order this->m_bulk_degree + this->m_cell_degree - 2
        // because v_T is of order m_bulk_degree and grad(p) of order m_cell_degree - 2
        size_t quadra_order(std::max(0, this->m_bulk_degree + this->m_cell_degree - 2));
        MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(this->m_cell, quadra_order));
        // compute the integral
        for(auto& qp : qps.list()) { 
            // evaluate all basis functions at the quadrature point of the cell
            Eigen::MatrixXd eval_basis_u( // each column is a basis function
                    this->m_bulk_bases[var_index1]->eval_functions(qp->point())); // v_T
            std::vector<Eigen::MatrixXd> eval_grad_p( // each grad = one row matrix
                    this->m_cell_bases[var_index2]->eval_gradients(qp->point())); // grad(q)

            for(size_t j = start_bulk_u, j_b = 0; j < end_bulk_u; j++, j_b++) {
                for(size_t i = start_bulk_p, i_b = 0; i < end_bulk_p; i++, i_b++) {
                    div_matrix(i,j) -= qp->weight() * eval_grad_p[i_b].row(0) * eval_basis_u.col(j_b);
                } 
            }
        }

        // compute sum(i=1->n_F) \int_Fi v_Fi q normal(Fi,T) ds
        std::vector<MyMesh::Face<DIM>*> faces(this->m_cell->get_faces());
        size_t f_li(0); // local index of face
        for(auto& fpt : faces) {

            // compute the outward normal to the face
            Eigen::VectorXd normal;
            if(DIM == 2) {
                std::vector<MyMesh::Vertex<DIM>*> vertices(fpt->get_vertices());
                // Store 2 (thus all in 2D) vertices coordinates into vertices_coords
                std::vector<Eigen::Array2d> vertices_coords{vertices[0]->coords(), vertices[1]->coords()};
                normal = outward_normal(vertices_coords, this->m_cell->mass_center());

            } else {
                std::vector<MyMesh::Vertex<DIM>*> vertices(fpt->get_vertices());
                // Store 3 vertices coordinates into vertices_coords
                std::vector<Eigen::Array3d> vertices_coords{vertices[0]->coords(), 
                                                            vertices[1]->coords(), 
                                                            vertices[2]->coords()};
                normal = outward_normal(vertices_coords, this->m_cell->mass_center());
            }

            // compute \int_Fi v_Fi q normal(Fi,T) ds
            quadra_order = std::max(0, this->m_skeletal_degree + this->m_cell_degree - 1);
            MyQuadra::QuadraturePoints<DIM> f_qps(MyQuadra::integrate(fpt, quadra_order));

            size_t start_face_u(this->m_faces_dof[var_index1][f_li][0]);
            size_t end_face_u(this->m_faces_dof[var_index1][f_li][1]);
            // compute the integral
            for(auto& qp : f_qps.list()) { 
                Eigen::MatrixXd func_face_basis_u(
                    this->m_faces_bases[var_index1][f_li]->eval_functions(qp->point())); // v_Fi
                Eigen::RowVectorXd func_eval_basis_p(
                    this->m_cell_bases[var_index2]->eval_functions(qp->point())); // v_T
                
                // \int_Fi v_Fi q normal(Fi,T) ds
                for(size_t j = start_face_u, j_b = 0; j < end_face_u; j++, j_b++) {
                    for(size_t i = start_bulk_p, i_b = 0; i < end_bulk_p; i++, i_b++) {
                        div_matrix(i,j) += qp->weight() * func_eval_basis_p[i_b] * 
                            func_face_basis_u.col(j_b).dot(normal);
                    } 
                }
            } // quadra

            f_li++;
        } // next face

        this->m_local_bilinear_form += mult_cst * (div_matrix + div_matrix.transpose());

        return;
    } // add_divergence_contribution

    // Define the reduction operator : 
    // scalar function = pressure so discontinuous Pk and orthogonal L2 proj over the cell
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    Eigen::VectorXd HHOdiscontinuousPk<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::apply_reduction_operator(
                    double func (Eigen::ArrayXd),
                    size_t var_index,
                    bool only_bound_elemt) {

        // it is a scalar valued function, the reduction might be applied on the pressure
#ifdef DEBUG
        if(var_index != 1) {
            throw " in HHOdiscontinuousPk, apply_reduction_operator on scalar valued functions are only implemented for pressure yet. ";
        }
        if(only_bound_elemt) {
            throw " apply_reduction_operator only at boundary elements for HHOdiscontinuousPk has no meaning. ";
        }
#endif
        Eigen::VectorXd reduction_vec(Eigen::VectorXd::Zero(this->m_n_cumulate_local_dof));

        // l2 orthogonal projection over the cell        
        size_t start(this->m_bulk_dof[var_index][0]);
        size_t length(this->m_n_bulk_dof[var_index]);

        reduction_vec.segment(start, length) = // the mass matrix is already computed, so give it (should be faster)
                this->orth_l2projection(this->m_cell, this->m_cell_bases[var_index], this->m_mass_matrices[var_index], func);

        return reduction_vec;
    } // apply_reduction_operator on scalar variable

    // Define the reduction operator :
    // vectorial function = velocity so HHO(kb, ks)
    // mean values of the func over the faces
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    Eigen::VectorXd HHOdiscontinuousPk<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::apply_reduction_operator(
                    std::vector<double> func (Eigen::ArrayXd),
                    size_t var_index,
                    bool only_bound_elemt) {

#ifdef DEBUG
        if(var_index != 0) {
            throw " apply_reduction_operator on vectorial valued functions are only implemented for velocity yet. ";
        }
#endif

        Eigen::VectorXd reduction_vec(Eigen::VectorXd::Zero(this->m_n_cumulate_local_dof));

        // faces of the cell
        std::vector<MyMesh::Face<DIM>*> faces(this->m_cell->get_faces());

        // l2 projection over the faces
        size_t f_li(0); // local index of face
        for(auto& fpt : faces) {
            if(!only_bound_elemt || fpt->is_boundary()) {
                size_t start(this->m_faces_dof[var_index][f_li][0]);
                size_t length(this->m_n_faces_dof(f_li, var_index));

                reduction_vec.segment(start, length) = 
                    this->orth_l2projection(fpt, this->m_faces_bases[var_index][f_li], func);
            }
            f_li++; // next face
        }

        if(!only_bound_elemt) {
            // l2 projection over the cell
            size_t start(this->m_bulk_dof[var_index][0]);
            size_t length(this->m_n_bulk_dof[var_index]);

            reduction_vec.segment(start, length) = 
                this->orth_l2projection(this->m_cell, this->m_bulk_bases[var_index], func);
        }

        return reduction_vec;
    } // apply_reduction_operator on vectorial variable

    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    void HHOdiscontinuousPk<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::set_rhs(
                double func (Eigen::ArrayXd),
                int quadra_order,
                size_t var_index) {

        throw " entered in set_rhs with scalar function, should not append in (HHO, discontinuous Pk) ";

        return;
    } // set_rhs

    // in HHO, rhs is computed as    (f,v_T)_T
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    void HHOdiscontinuousPk<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::set_rhs(
                std::vector<double> func (Eigen::ArrayXd),
                int quadra_order,
                size_t var_index) {

        // var index should correspond to u
#ifdef DEBUG
        if(var_index != 0) {
            throw " set_rhs on vectorial valued functions are only implemented for velocity yet. ";
        }
#endif
        // v_T indexes
        size_t start(this->m_bulk_dof[var_index][0]);
        size_t length(this->m_n_bulk_dof[var_index]);

        this->m_local_rhs.segment(start, length) += 
            this->int_f_basis(this->m_cell, func, this->m_bulk_bases[var_index], quadra_order);

        return;
    } // set_rhs


} // MyDiscreteSpace namespace
#endif /* HHO_DISCONTINUOUS_PK */
/** @} */
