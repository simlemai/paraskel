/**
  * @{
*/

#ifndef PK_LAGRANGE
#define PK_LAGRANGE

#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include "common_geometry.hpp"
#include "polytopal_element.hpp"
#include "cell.hpp"
// contains the implementations of part of the reconstruction op, the rhs, reduction operator computations, ...
#include "pk_lagrange_common.hpp"

namespace MyDiscreteSpace {
    /**
     * The P_k Lagrange class contains 
     * the specific informations for conforming P_k Lagrange. 
     * It inherits of the PolytopalElement class
     * It must be applied on simplicial meshes only.
    */

    template<size_t DIM, int... CONTINUOUS_SPACES> 
    class PkLagrangeBase : public PolytopalElement<DIM>
    {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param k order of the method
         * @param var_sizes array with the dimensions of all the variables
         * @param n_dof_face face number of dof
         * @param n_dof_edge edge number of dof
         * @param n_dof_vertex vertex number of dof
         * @param n_dof_bulk bulk number of dof
         */
        PkLagrangeBase(MyMesh::Cell<DIM>* cell, 
                int k,
                const Eigen::ArrayXi& var_sizes,
                int n_dof_face, 
                int n_dof_edge, 
                int n_dof_vertex, 
                int n_dof_bulk); // Poly_base_type pbt, Quad_type qt, int k, Data dat);
        ~PkLagrangeBase() = default;   ///<  Default destructor

        /**
         * Compute the reconstruction operator
         * @param var_sizes array with the dimension of each variable
         */
        virtual void set_reconstruction_operator(const Eigen::ArrayXi& var_sizes) = 0;

        /**
         * Using the stiffness matrix, add
         * k * \int_T grad(r_T u_T) * grad(r_T v_T) dx
         * to the local matrix
         * @param var_index index of the variable to add the stiffness contribution
         * @param mult_cst multiplicative constant
         */
        inline void add_stiffness_contribution(size_t var_index = 0, double mult_cst = 1.);

        /**
         * Using the mass matrix, add
         * k * \int_T r_T u_T * r_T v_T dx
         * to the local matrix
         * @param var_index index of the variable to add the stiffness contribution
         * @param mult_cst multiplicative constant
         */
        inline void add_mass_contribution(size_t var_index = 0, double mult_cst = 1.);

        /**
         * Compute the reduction operator (scalar variable)
         * @param func continuous function to which extract reduction
         * @param var_index corresponding index of the variable
         * @param only_bound_elemt boolean, if true apply reduction operator only on boundary elements
         */
        Eigen::VectorXd apply_reduction_operator(double func (Eigen::ArrayXd),
                size_t var_index = 0,
                bool only_bound_elemt = false);
        /**
         * Compute the reduction operator (vectorial variable)
         * @param func continuous function to which extract reduction
         * @param var_index corresponding index of the variable
         * @param only_bound_elemt boolean, if true apply reduction operator only on boundary elements
         */
        Eigen::VectorXd apply_reduction_operator(std::vector<double> func (Eigen::ArrayXd),
                size_t var_index = 0,
                bool only_bound_elemt = false);

        /**
         * Add a divergence term knowing two variable indexes
         * @param var_index1 index of the first variable
         * @param var_index2 index of the second variable
         * @param mesh_measure measure of the global domain
         * @param mult_cst multiplicative constant
         */
        void add_divergence_contribution(
                size_t var_index1, 
                size_t var_index2, 
                double mesh_measure,
                double mult_cst = 1.) {
            throw " add_divergence_contribution not defined in pk_lagrange ";
        }

        /**
         * Set the local right-hand-side
         * from a continuous function
         * with specific quadrature order 
         * (6 by default)
         * For a scalar variable or vectorial one
         * @param func
         * @param quadra_order
         * @param var_index
         */
        void set_rhs(double func (Eigen::ArrayXd), int quadra_order, size_t var_index);
        void set_rhs(std::vector<double> func (Eigen::ArrayXd), int quadra_order, size_t var_index);

        std::vector<Eigen::VectorXd> get_lattice_coords();

    protected:
        std::vector<Eigen::VectorXd> m_lattice_coords; ///< store the coordinates of the lattice points
    };

    /// definition of the PkLagrange class to be able to specify for the different values of DIM
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    class PkLagrange : public PkLagrangeBase<DIM, CONTINUOUS_SPACES...> {
    public:
        /**
         * Default constructor prints error
         */
        PkLagrange(MyMesh::Cell<DIM>* cell, int k, int ks, const Eigen::ArrayXi& var_sizes);
        ~PkLagrange() = default;   ///<  Default destructor
    };

    // partial specialization when DIM = 2 and only one variable (so one continuous space)
    template <int CONTINUOUS_SPACE> 
    class PkLagrange<2, CONTINUOUS_SPACE> : public PkLagrangeBase<2, CONTINUOUS_SPACE> {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param var_sizes list of the dimension of the variables (multiplicative factors for all dof)
         */
        PkLagrange(MyMesh::Cell<2>* cell, int k, int ks, const Eigen::ArrayXi& var_sizes); // Poly_base_type pbt, Quad_type qt, int k, Data dat);
        ~PkLagrange() = default;   ///<  Default destructor

        /**
         * Compute the reconstruction operator
         * @param var_sizes array with the dimension of each variable
         */
        void set_reconstruction_operator(const Eigen::ArrayXi& var_sizes);

        /**
         * Compute the reconstruction operator
         * with a specific basis (is called in Taylor Hood for example)
         * @param var_size dimension of the considered variable
         * @param cell_basis specific cell_basis
         * @param var_size index of the variable (0 by default)
         */
        inline void set_reconstruction_operator(size_t var_size,
                MyBasis::Basis<MyMesh::Cell<2>*>* cell_basis, size_t var_index = 0);
    };

    // partial specialization when DIM = 3 and only one variable (so one continuous space)
    template <int CONTINUOUS_SPACE> 
    class PkLagrange<3, CONTINUOUS_SPACE> : public PkLagrangeBase<3, CONTINUOUS_SPACE> {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param var_sizes list of the dimension of the variables (multiplicative factors for all dof)
         */
        PkLagrange(MyMesh::Cell<3>* cell, int k, int ks, const Eigen::ArrayXi& var_sizes); // Poly_base_type pbt, Quad_type qt, int k, Data dat);
        ~PkLagrange() = default;   ///<  Default destructor

        /**
         * Compute the reconstruction operator
         * @param var_sizes array with the dimension of each variable
         */
        void set_reconstruction_operator(const Eigen::ArrayXi& var_sizes);

        /**
         * Compute the reconstruction operator
         * with a specific basis  (is called in Taylor Hood for example)
         * @param var_size dimension of the considered variable
         * @param cell_basis specific cell_basis
         * @param var_size index of the variable (0 by default)
         */
        inline void set_reconstruction_operator(size_t var_size,
                MyBasis::Basis<MyMesh::Cell<3>*>* cell_basis,
                size_t var_index = 0);
    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////

    // constructor for PkLagrangeBase (has access to the constructor of PolytopalElement).
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    PkLagrangeBase<DIM, CONTINUOUS_SPACES...>::PkLagrangeBase(
                       MyMesh::Cell<DIM>* cell, 
                       int k,
                       const Eigen::ArrayXi& var_sizes,
                       int n_dof_face, 
                       int n_dof_edge, 
                       int n_dof_vertex,
                       int n_dof_bulk) :
    PolytopalElement<DIM>(cell, var_sizes * n_dof_face, var_sizes * n_dof_edge, var_sizes * n_dof_vertex, 
                       var_sizes * n_dof_bulk) {
        this->m_cell_degree = k;
        for(int i = 0, size = var_sizes.size(); i < size; i++) {
            if(var_sizes[i] == 1) { // one scalar variable
                MyBasis::Basis< MyMesh::Cell<DIM>* >* cell_basis(
                    new MyBasis::ScaledMonomialScalarBasis<MyMesh::Cell<DIM>*>(cell, this->m_cell_degree));
                this->m_cell_bases.push_back(cell_basis); 
            } else if(var_sizes[i] == DIM) { // one vectorial variable
                // vectorial full basis
                MyBasis::Basis< MyMesh::Cell<DIM>* >* cell_basis(
                    new MyBasis::ScaledMonomialVectorBasis<MyMesh::Cell<DIM>*, MyBasis::FULL>(cell, this->m_cell_degree));
                this->m_cell_bases.push_back(cell_basis);
            }
        }
    }

    // constructor 
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    PkLagrange<DIM, CONTINUOUS_SPACES...>::PkLagrange(MyMesh::Cell<DIM>* cell, int k, int ks, const Eigen::ArrayXi& var_sizes) {
        throw " PkLagrange not implemented for these continuous spaces ";
    }

    // constructor for PkLagrange 2D with one variable
    template<int CONTINUOUS_SPACE> 
    PkLagrange<2, CONTINUOUS_SPACE>::PkLagrange(MyMesh::Cell<2>* cell, int k, int ks, const Eigen::ArrayXi& var_sizes) : 
    PkLagrangeBase<2, CONTINUOUS_SPACE>(cell, 
                                        k, // order of the method (max order of the polynomials)
                                        var_sizes, // sizes of the variables
                                        n_dof_faces_pk_lagrange<2>(k), // number of dof in each face (vertices excluded)
                                        0,  // number of dof in each edge (always 0 in 2D)
                                        1,  // number of dof in each vertex (always 1 in Lagrange)
                                        n_dof_bulk_pk_lagrange<2>(k) ) { // number of bulk dof
#ifdef DEBUG
        if(ks != k) throw " ks is not equal to k in Pk Lagrange ";
#endif
    }

    // constructor for PkLagrange 3D with one variable
    template<int CONTINUOUS_SPACE> 
    PkLagrange<3, CONTINUOUS_SPACE>::PkLagrange(MyMesh::Cell<3>* cell, int k, int ks, const Eigen::ArrayXi& var_sizes) : 
    PkLagrangeBase<3, CONTINUOUS_SPACE>(cell, 
                                        k, // order of the method
                                        var_sizes, // sizes of the variables
                                        n_dof_faces_pk_lagrange<3>(k),  // number of dof in each face (vertices and edges excluded)
                                        n_dof_edges_pk_lagrange<3>(k),  // number of dof in each edge (vertices excluded)
                                        1,   // number of dof in each vertex (always 1 in Lagrange)
                                        n_dof_bulk_pk_lagrange<3>(k) ) { // number of bulk dof
#ifdef DEBUG
        if(ks != k) throw " ks is not equal to k in Pk Lagrange ";
#endif
    }


    // definition of set_reconstruction_operator :
    // knowing a base of the dof at each vertex, 
    // construct the P_dim^k(T) polynomials
    template<int CONTINUOUS_SPACE> 
    void PkLagrange<2, CONTINUOUS_SPACE>::set_reconstruction_operator(const Eigen::ArrayXi& var_sizes) {

        // only one variable
        // set the reconstruction operator
        return set_reconstruction_operator(var_sizes[0], this->m_cell_bases[0]);

    } // set_reconstruction_operator


    // definition of set_reconstruction_operator with a specific cell basis :
    // knowing a base of the dof at each vertex, 
    // construct the P_dim^k(T) polynomials in 2D
    template<int CONTINUOUS_SPACE> 
    inline void PkLagrange<2, CONTINUOUS_SPACE>::set_reconstruction_operator(
            size_t var_size, MyBasis::Basis<MyMesh::Cell<2>*>* cell_basis, size_t var_index) {

        size_t lagrange_deg(this->m_cell_degree);

        // ------- Fill the vector with the coords of the lattice points -------------
        // resize it
        this->m_lattice_coords.resize(this->m_n_cumulate_local_dof);

        // vertices dof
        size_t v_l(0); // local index of vertex
        std::vector<MyMesh::Vertex<2> *> cell_vertices(this->m_cell->get_vertices());
        for(auto& v : cell_vertices) { // Lagrange
            size_t dof(this->m_vertices_dof[var_index][v_l++][0]); // local index of the first dof carried by the vertex (can have several when vectorial variable)
            this->m_lattice_coords[dof] = v->coords();
        }

        // faces dof
        size_t f_l(0); // local index of face
        std::vector<MyMesh::Face<2>*> cell_faces(this->m_cell->get_faces());
        for(auto& f : cell_faces) {

            std::vector<Eigen::Vector2d> vertices_coords{ // coords of the 2 vertices of the 1D face
                    f->get_vertices()[0]->coords(), f->get_vertices()[1]->coords()};

            size_t dof(this->m_faces_dof[var_index][f_l][0]); // local index of the first face dof
            // loop on all the lattice points belonging to the face f
            for(size_t i = 1; i < lagrange_deg; i++) {
                // define coords of the coresponding point in the lattice
                this->m_lattice_coords[dof] = (double)i / lagrange_deg * vertices_coords[0] 
                    + (1. - (double)i / lagrange_deg) * vertices_coords[1];
                dof += var_size; // the local index of dof are fixed 
                // (the components of the vectorial variable are contigous), 
                // but the global one can be different
            }
#ifdef DEBUG
            if(dof != this->m_faces_dof[var_index][f_l][1]) {
                throw " not good computation of face dof coords in 2D reconstruction operator ";
            }
#endif
            f_l++;
        }

        // bulk dof
        std::vector<Eigen::Vector2d> vertices_coords{ // coords of the 3 vertices of the 2D cell
                cell_vertices[0]->coords(), 
                cell_vertices[1]->coords(), 
                cell_vertices[2]->coords()};

        size_t dof(this->m_bulk_dof[var_index][0]); // local index of the bulk dof
        for(size_t i = 1; i < lagrange_deg - 1; i++) {
            // store the constant wrt to i
            Eigen::Vector2d i_contribution((double)i / lagrange_deg * vertices_coords[0]);

            for(size_t j = 1; j < lagrange_deg - i; j++) {
                // compute the dof coords
                // using the following barycentric coordinates but separates what does not depend on j
                // this->m_lattice_coords[dof] = i / lagrange_deg * vertices_coords[0]
                //                         + j / lagrange_deg * vertices_coords[1]
                //                         + (lagrange_deg - i - j) / lagrange_deg * vertices_coords[2];
                this->m_lattice_coords[dof] = i_contribution
                            + (double)j / lagrange_deg * vertices_coords[1] 
                            + (double)(lagrange_deg - i - j) / lagrange_deg * vertices_coords[2];
                dof += var_size;
            }
        }

#ifdef DEBUG
        // CONTINUOUS_SPACE : only one variable
        if(dof != this->m_bulk_dof[var_index][1]) {
            throw " not good computation of bulk dof coords in 2D reconstruction operator ";
        }
#endif

        // Find the mapping between the basis functions cell_basis
        // and the Pk Lagrange form functions given by Phi_i(x_j) = \delta_ij * Id(var_size)
        // where x_j are all the lattice points (m_lattice_coords)
        size_t basis_dim(cell_basis->dimension()); // dimension of the basis
        Eigen::MatrixXd lagrange_basis(this->m_n_cumulate_local_dof, basis_dim);

        for(size_t dof = 0; dof < this->m_n_cumulate_local_dof; dof += var_size) {
            // evaluate the basis at the lattice coord
            // Block of size (p,q), starting at (i,j) : matrix.block(i,j,p,q);

            lagrange_basis.block(dof, 0, var_size, basis_dim) = 
                            cell_basis->eval_functions(this->m_lattice_coords[dof]);
        }

        this->m_reconstruction_operators.push_back(lagrange_basis.inverse());

        return;

    } // set_reconstruction_operator in 2D meshes with a specific cell basis

    // definition of set_reconstruction_operator :
    // knowing a base of the dof at each vertex, 
    // construct the P_dim^k(T) polynomials
    template<int CONTINUOUS_SPACE> 
    void PkLagrange<3, CONTINUOUS_SPACE>::set_reconstruction_operator(const Eigen::ArrayXi& var_sizes) {

        // only one variable
        // set the reconstruction operator
        return set_reconstruction_operator(var_sizes[0], this->m_cell_bases[0]);

    } // set_reconstruction_operator

    // definition of set_reconstruction_operator with a specific cell basis :
    // knowing a base of the dof at each vertex, 
    // construct the P_dim^1(T) polynomials in 3D
    template<int CONTINUOUS_SPACE> 
    inline void PkLagrange<3, CONTINUOUS_SPACE>::set_reconstruction_operator(size_t var_size,
            MyBasis::Basis<MyMesh::Cell<3>*>* cell_basis, size_t var_index) {

        size_t lagrange_deg(this->m_cell_degree);

        // ------- Fill the vector with the coords of the dof (lattice) -------------
        // resize it
        this->m_lattice_coords.resize(this->m_n_cumulate_local_dof);

        // vertices
        size_t v_l(0); // local index of vertex
        std::vector<MyMesh::Vertex<3> *> cell_vertices(this->m_cell->get_vertices());
        for(auto& v : cell_vertices) { // Lagrange
            size_t dof(this->m_vertices_dof[var_index][v_l++][0]); // local index of the first dof carried by the vertex (can have several when vectorial variable)
            this->m_lattice_coords[dof] = v->coords();
        }

        // edge
        size_t e_l(0); // local index of edge
        std::vector<MyMesh::Edge<3>*> cell_edges(this->m_cell->get_edges());
        for(auto& e : cell_edges) {

            std::vector<Eigen::Vector3d> vertices_coords{ // coords of the 2 vertices of the edge
                    e->get_vertices()[0]->coords(), e->get_vertices()[1]->coords()};

            size_t dof(this->m_edges_dof[var_index][e_l][0]); // local index of the first edge dof
            // loop on all the lattice points belonging to the edge e
            for(size_t i = 1; i < lagrange_deg; i++) {
                // define coords of the point in the lattice
                this->m_lattice_coords[dof] = (double)i / lagrange_deg * vertices_coords[0] 
                                          + (1. - (double)i / lagrange_deg) * vertices_coords[1];
                dof += var_size; // the local index of dof are fixed 
                // (the components of the vectorial variable are contigous), 
                // but the global one can be different

            }
#ifdef DEBUG
            if(dof != this->m_edges_dof[var_index][e_l][1]) {
                throw " not good computation of edge dof coords in 3D reconstruction operator ";
            }
#endif
            e_l++;
        }

        // face
        size_t f_l(0); // local index of face
        std::vector<MyMesh::Face<3>*> cell_faces(this->m_cell->get_faces());
        for(auto& f : cell_faces) {

            std::vector<Eigen::Vector3d> vertices_coords{ // coords of the 3 vertices of the 2D face
                    f->get_vertices()[0]->coords(), 
                    f->get_vertices()[1]->coords(),
                    f->get_vertices()[2]->coords()};

            // define the lattice points belonging to the face f
            size_t dof(this->m_faces_dof[var_index][f_l][0]); // local index of the first face dof
            for(size_t i = 1; i < lagrange_deg - 1; i++) {
                // store the constant wrt to i
                Eigen::Vector3d i_contribution((double)i / lagrange_deg * vertices_coords[0]);

                for(size_t j = 1; j < lagrange_deg - i; j++) {
                    // compute the lattice point coords
                    // using the following barycentric coordinates but separates what does not depend on j
                    // this->m_lattice_coords[dof] = i / lagrange_deg * vertices_coords[0]
                    //                         + j / lagrange_deg * vertices_coords[1]
                    //                         + (lagrange_deg - i - j) / lagrange_deg * vertices_coords[2];
                    this->m_lattice_coords[dof] = i_contribution
                                + (double)j / lagrange_deg * vertices_coords[1] 
                                + (double)(lagrange_deg - i - j) / lagrange_deg * vertices_coords[2];
                    dof += var_size; // the local index of dof are fixed 
                    // (the components of the vectorial variable are contigous), 
                    // but the global one can be different
                }
            }

#ifdef DEBUG
            if(dof != this->m_faces_dof[var_index][f_l][1]) {
                throw " not good computation of face dof coords in 3D reconstruction operator ";
            }
#endif
            f_l++; // next face local index
        }

        // bulk lattice points
        if(this->m_n_bulk_dof[0] > 0) { // CONTINUOUS_SPACE : only one variable
            std::vector<Eigen::Vector3d> vertices_coords{ // coords of the 4 vertices of the 3D cell
                    cell_vertices[0]->coords(), 
                    cell_vertices[1]->coords(), 
                    cell_vertices[2]->coords(),
                    cell_vertices[3]->coords()};

            size_t dof(this->m_bulk_dof[var_index][0]); // local index of the first bulk dof of this variable
            for(size_t i = 1; i < lagrange_deg - 2; i++) {
                // store the constant wrt to i
                Eigen::Vector3d i_contribution((double)i / lagrange_deg * vertices_coords[0]);

                for(size_t j = 1; j < lagrange_deg - i - 1; j++) {
                    Eigen::Vector3d j_contribution((double)j / lagrange_deg * vertices_coords[1]);

                    for(size_t k = 1; k < lagrange_deg - i - j; k++) {

                        // compute the lattice point coords
                        // using the following barycentric coordinates but separates what does not depend on k
                        // this->m_lattice_coords[dof] = i / lagrange_deg * vertices_coords[0]
                        //            + j / lagrange_deg * vertices_coords[1]
                        //            + k / lagrange_deg * vertices_coords[2]
                        //            + (lagrange_deg - i - j - k) / lagrange_deg * vertices_coords[3];
                        this->m_lattice_coords[dof] = i_contribution + j_contribution
                                    + (double)k / lagrange_deg * vertices_coords[2] 
                                    + (double)(lagrange_deg - i - j - k) / lagrange_deg * vertices_coords[3];
                        dof += var_size; // the local index of dof are fixed 
                        // (the components of the vectorial variable are contigous), 
                        // but the global one can be different
                    }
                }
            }

#ifdef DEBUG
            // CONTINUOUS_SPACE : only one variable
            if(dof != this->m_bulk_dof[var_index][1]) {
                throw " not good computation of bulk dof coords in 3D reconstruction operator ";
            }
#endif
        }

        // Find the mapping between the basis functions cell_basis
        // and the Pk Lagrange form functions given by Phi_i(x_j) = \delta_ij  * Id(var_size)
        // where x_j are all the lattice points (m_lattice_coords)
        size_t basis_dim(cell_basis->dimension()); // dimension of the basis
        Eigen::MatrixXd lagrange_basis(this->m_n_cumulate_local_dof, basis_dim);
        for(size_t dof = 0; dof < this->m_n_cumulate_local_dof; dof += var_size) {
            // evaluate the basis at the lattice coord
            // Block of size (p,q), starting at (i,j) : matrix.block(i,j,p,q);

            lagrange_basis.block(dof, 0, var_size, basis_dim) = 
                            cell_basis->eval_functions(this->m_lattice_coords[dof]);
        }

        this->m_reconstruction_operators.push_back(lagrange_basis.inverse());

        return;

    } // set_reconstruction_operator in 3D meshes with a specific cell basis

    template<size_t DIM, int... CONTINUOUS_SPACES> 
    inline void PkLagrangeBase<DIM, CONTINUOUS_SPACES...>::add_stiffness_contribution(
                    size_t var_index, double mult_cst) {

        return pe_add_stiffness_contribution<DIM>(
            this->m_reconstruction_operators[var_index],
            this->m_stiffness_matrices[var_index],
            this->m_local_bilinear_form,
            mult_cst);

    } // add_stiffness_contribution

    template<size_t DIM, int... CONTINUOUS_SPACES> 
    inline void PkLagrangeBase<DIM, CONTINUOUS_SPACES...>::add_mass_contribution(
                    size_t var_index, double mult_cst) {

        return pe_add_mass_contribution<DIM>(
            this->m_reconstruction_operators[var_index],
            this->m_mass_matrices[var_index],
            this->m_local_bilinear_form,
            mult_cst);
    } // add_mass_contribution

    // Define the reduction operator : scalar values at lattice points
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    Eigen::VectorXd PkLagrangeBase<DIM, CONTINUOUS_SPACES...>::apply_reduction_operator(
                    double func (Eigen::ArrayXd),
                    size_t var_index,
                    bool only_bound_elemt) {

        // continuous spaces informations (variables), only the number of variables is necessary here
        size_t n_var(sizeof...(CONTINUOUS_SPACES));
        size_t func_size(1); // scalar valued function

        Eigen::VectorXd reduction_vec(Eigen::VectorXd::Zero(this->m_n_cumulate_local_dof));

        // all dof are computed
        if(!only_bound_elemt) {
            // only one variable so contiguous dof indexes
            if(n_var == 1) {
                for(size_t dof = 0; dof < this->m_n_cumulate_local_dof; dof++) {
                    // evaluate func at the lattice coord
                    reduction_vec[dof] = func(this->m_lattice_coords[dof]);
                }
            } else { // more than one variable, generic algo
                if(DIM == 2) { // no edges
                    lagrange_apply_reduction_operator(
                        this->m_cell,
                        this->m_vertices_dof, this->m_faces_dof, this->m_bulk_dof,
                        var_index, func_size, func, this->m_lattice_coords,
                        reduction_vec);
                } else { // with edges
                    lagrange_apply_reduction_operator(
                        this->m_cell,
                        this->m_vertices_dof, this->m_edges_dof, this->m_faces_dof, this->m_bulk_dof,
                        var_index, func_size, func, this->m_lattice_coords,
                        reduction_vec);
                }
            } // n_var

        } else if(this->m_cell->is_boundary()) { // need to test if the object is bound, bulk cannot be
            if(DIM == 2) { // no edges
                lagrange_apply_reduction_operator_boundary(
                    this->m_cell,
                    this->m_vertices_dof, this->m_faces_dof,
                    var_index, func_size, func, this->m_lattice_coords,
                    reduction_vec);
            } else { // with edges
                lagrange_apply_reduction_operator_boundary(
                    this->m_cell,
                    this->m_vertices_dof, this->m_edges_dof, this->m_faces_dof,
                    var_index, func_size, func, this->m_lattice_coords,
                    reduction_vec);
            }

        }

        return reduction_vec;
    } // apply_reduction_operator on scalar variable

    // Define the reduction operator : vectorial values at lattice points
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    Eigen::VectorXd PkLagrangeBase<DIM, CONTINUOUS_SPACES...>::apply_reduction_operator(
                    std::vector<double> func (Eigen::ArrayXd),
                    size_t var_index,
                    bool only_bound_elemt) {

        // continuous spaces informations (variables), only the number of variables is necessary here
        size_t n_var(sizeof...(CONTINUOUS_SPACES));

        Eigen::VectorXd reduction_vec(Eigen::VectorXd::Zero(this->m_n_cumulate_local_dof));

#ifdef DEBUG
if(func(this->m_lattice_coords[0]).size() != DIM) {
    throw " apply_reduction_operator implemented only over vect valued function of dimension DIM ";
}
#endif

        if(!only_bound_elemt) { // all dof are computed
            // only one variable so contiguous dof indexes
            if(n_var == 1) {
                for(size_t dof = 0; dof < this->m_n_cumulate_local_dof; dof += DIM) {
                    // evaluate func at the lattice coord
                    std::vector<double> f_lattice(func(this->m_lattice_coords[dof]));
                    // Block containing var_size elements, starting at position dof	
                    reduction_vec.segment(dof, DIM) = Eigen::Map<Eigen::VectorXd> (f_lattice.data(), f_lattice.size());
                }
            } else { // more than one variable, generic algo
                if(DIM == 2) { // no edges
                    lagrange_apply_reduction_operator(
                        this->m_cell,
                        this->m_vertices_dof, this->m_faces_dof, this->m_bulk_dof,
                        var_index, DIM, func, this->m_lattice_coords,
                        reduction_vec);
                } else { // with edges
                    lagrange_apply_reduction_operator(
                        this->m_cell,
                        this->m_vertices_dof, this->m_edges_dof, this->m_faces_dof, this->m_bulk_dof,
                        var_index, DIM, func, this->m_lattice_coords,
                        reduction_vec);
                }
            } // n_var

        } else if(this->m_cell->is_boundary()) { // need to test if the object is bound, bulk cannot be
            if(DIM == 2) { // no edges
                lagrange_apply_reduction_operator_boundary(
                    this->m_cell,
                    this->m_vertices_dof, this->m_faces_dof,
                    var_index, DIM, func, this->m_lattice_coords,
                    reduction_vec);
            } else { // with edges
                lagrange_apply_reduction_operator_boundary(
                    this->m_cell,
                    this->m_vertices_dof, this->m_edges_dof, this->m_faces_dof,
                    var_index, DIM, func, this->m_lattice_coords,
                    reduction_vec);
            }
        }

        return reduction_vec;
    } // apply_reduction_operator on vectorial variable

    // fill the rhs with \int f r_T v_T (FE method)
    // with a specific quadrature order
    // with scalar variable
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    void PkLagrangeBase<DIM, CONTINUOUS_SPACES...>::set_rhs(
                double func (Eigen::ArrayXd),
                int quadra_order,
                size_t var_index) {

        // int_f_basis computes the vector containing  int f * basis_i
        this->m_local_rhs += this->m_reconstruction_operators[var_index].transpose() 
            * this->int_f_basis(this->m_cell, func, this->m_cell_bases[var_index], quadra_order);

        return;
    } // set_rhs scalar

    // fill the rhs with \int f r_T v_T (FE method)
    // with a specific quadrature order
    // with a vectorial variable
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    void PkLagrangeBase<DIM, CONTINUOUS_SPACES...>::set_rhs(
                std::vector<double> func (Eigen::ArrayXd),
                int quadra_order,
                size_t var_index) {

        // int_f_basis computes the vector containing  (int f * basis_i)_i
        // with f vectorial function
        // (one scalar by basis function)
        this->m_local_rhs += this->m_reconstruction_operators[var_index].transpose() 
            * this->int_f_basis(this->m_cell, func, this->m_cell_bases[var_index], quadra_order);

        return;
    } // set_rhs vectorial

    template<size_t DIM, int... CONTINUOUS_SPACES> 
    std::vector<Eigen::VectorXd> PkLagrangeBase<DIM, CONTINUOUS_SPACES...>::get_lattice_coords() { 
        return m_lattice_coords; 
    }

} // MyDiscreteSpace namespace
#endif /* PK_LAGRANGE */
/** @} */
