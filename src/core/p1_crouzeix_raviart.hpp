/**
  * @{
*/

#ifndef P1_CROUZEIX_RAVIART
#define P1_CROUZEIX_RAVIART

#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include "common_geometry.hpp"
#include "polytopal_element.hpp"
#include "cell.hpp"

namespace MyDiscreteSpace {
    /**
     * The P1 Crouzeix-Raviart class contains 
     * the specific informations of the non-conforming
     * P1 Crouzeix-Raviart. 
     * It inherits of the PolytopalElement class.
     * It must be applied on simplicial meshes only.
    */

    template<size_t DIM, int... CONTINUOUS_SPACES> 
    class P1CrouzeixRaviartBase : public PolytopalElement<DIM>
    {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param var_sizes array with the dimensions of all the variables
         * @param n_dof_face face number of dof
         * @param n_dof_edge edge number of dof
         * @param n_dof_vertex vertex number of dof
         * @param n_dof_bulk bulk number of dof
         */
        P1CrouzeixRaviartBase(MyMesh::Cell<DIM>* cell, 
                const Eigen::ArrayXi& var_sizes,
                int n_dof_face, 
                int n_dof_edge, 
                int n_dof_vertex, 
                int n_dof_bulk); // Poly_base_type pbt, Quad_type qt, int k, Data dat);
        ~P1CrouzeixRaviartBase() = default;   ///<  Default destructor

        /**
         * Compute the reconstruction operator
         * @param var_sizes array with the dimension of each variable
         */
        void set_reconstruction_operator(const Eigen::ArrayXi& var_sizes);
        /**
         * Compute the reconstruction operator
         * with a specific basis  (is called in Stokes (P1CR, P0) for example)
         * @param var_size dimension of the variable
         * @param cell_basis specific cell_basis
         * @param var_size index of the variable (0 by default)
         */
        inline void set_reconstruction_operator(size_t var_size,
                MyBasis::Basis<MyMesh::Cell<DIM>*>* cell_basis,
                size_t var_index = 0);

        /**
         * Using the stiffness matrix, add
         * k * \int_T grad(r_T u_T) * grad(r_T v_T) dx
         * to the local matrix
         * @param var_index index of the variable to add the stiffness contribution
         * @param mult_cst multiplicative constant
         */
        inline void add_stiffness_contribution(size_t var_index = 0, double mult_cst = 1.);

        /**
         * Using the mass matrix, add
         * k * \int_T r_T u_T * r_T v_T dx
         * to the local matrix
         * @param var_index index of the variable to add the stiffness contribution
         * @param mult_cst multiplicative constant
         */
        inline void add_mass_contribution(size_t var_index = 0, double mult_cst = 1.);

        /**
         * Compute the reduction operator (scalar variable)
         * @param func continuous function to which extract reduction
         * @param only_bound_elemt boolean, if true apply reduction operator only on boundary elements
         */
        Eigen::VectorXd apply_reduction_operator(double func (Eigen::ArrayXd),
                size_t var_index = 0,
                bool only_bound_elemt = false);
        /**
         * Compute the reduction operator (vectorial variable)
         * @param func continuous function to which extract reduction
         * @param only_bound_elemt boolean, if true apply reduction operator only on boundary elements
         */
        Eigen::VectorXd apply_reduction_operator(std::vector<double> func (Eigen::ArrayXd),
                size_t var_index = 0,
                bool only_bound_elemt = false);

        /**
         * Add a divergence term knowing two variable indexes
         * @param var_index1 index of the first variable
         * @param var_index2 index of the second variable
         * @param mesh_measure measure of the global domain
         * @param mult_cst multiplicative constant
         */
        void add_divergence_contribution(
                size_t var_index1, 
                size_t var_index2, 
                double mesh_measure,
                double mult_cst = 1.) {
            throw " add_divergence_contribution not defined in P1 CR ";
        }

        /**
         * Set the local right-hand-side
         * from a continuous function
         * with specific quadrature order 
         * (6 by default)
         * For a scalar variable or vectorial one
         * @param func
         * @param quadra_order
         * @param var_index
         */
        void set_rhs(double func (Eigen::ArrayXd), int quadra_order, size_t var_index);
        void set_rhs(std::vector<double> func (Eigen::ArrayXd), int quadra_order, size_t var_index);

    };

    /// definition of the P1CrouzeixRaviart class to be able to specify for the different values of DIM
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    class P1CrouzeixRaviart : public P1CrouzeixRaviartBase<DIM, CONTINUOUS_SPACES...> {
    public:
        /**
         * Default constructor prints error
         */
        P1CrouzeixRaviart(MyMesh::Cell<DIM>* cell, int k, int ks, const Eigen::ArrayXi& var_sizes);
        ~P1CrouzeixRaviart() = default;   ///<  Default destructor

    };

    // partial specialization when DIM = 2 and only one variable (so one continuous space)
    template <int CONTINUOUS_SPACE> 
    class P1CrouzeixRaviart<2, CONTINUOUS_SPACE> : public P1CrouzeixRaviartBase<2, CONTINUOUS_SPACE> {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param var_sizes list of the dimension of the variables (multiplicative factors for all dof)
         */
        P1CrouzeixRaviart(MyMesh::Cell<2>* cell, int k, int ks, const Eigen::ArrayXi& var_sizes); // Poly_base_type pbt, Quad_type qt, int k, Data dat);
        ~P1CrouzeixRaviart() = default;   ///<  Default destructor
    };

    // partial specialization when DIM = 3 and only one variable (so one continuous space)
    template <int CONTINUOUS_SPACE> 
    class P1CrouzeixRaviart<3, CONTINUOUS_SPACE> : public P1CrouzeixRaviartBase<3, CONTINUOUS_SPACE> {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param var_sizes list of the dimension of the variables (multiplicative factors for all dof)
         */
        P1CrouzeixRaviart(MyMesh::Cell<3>* cell, int k, int ks, const Eigen::ArrayXi& var_sizes); // Poly_base_type pbt, Quad_type qt, int k, Data dat);
        ~P1CrouzeixRaviart() = default;   ///<  Default destructor
    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////

    // constructor for P1CrouzeixRaviartBase (has access to the constructor of PolytopalElement).
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    P1CrouzeixRaviartBase<DIM, CONTINUOUS_SPACES...>::P1CrouzeixRaviartBase(
                       MyMesh::Cell<DIM>* cell, 
                       const Eigen::ArrayXi& var_sizes,
                       int n_dof_face, 
                       int n_dof_edge, 
                       int n_dof_vertex,
                       int n_dof_bulk) :
    PolytopalElement<DIM>(cell, var_sizes * n_dof_face, var_sizes * n_dof_edge, var_sizes * n_dof_vertex, 
                       var_sizes * n_dof_bulk) {
        this->m_cell_degree = 1;
        for(int i = 0, size = var_sizes.size(); i < size; i++) {
            if(var_sizes[i] == 1) { // one scalar variable
                MyBasis::Basis< MyMesh::Cell<DIM>* >* cell_basis(
                    new MyBasis::ScaledMonomialScalarBasis<MyMesh::Cell<DIM>*>(cell, this->m_cell_degree));
                this->m_cell_bases.push_back(cell_basis); 
            } else if(var_sizes[i] == DIM) { // one vectorial variable
                // vectorial full basis
                MyBasis::Basis< MyMesh::Cell<DIM>* >* cell_basis(
                    new MyBasis::ScaledMonomialVectorBasis<MyMesh::Cell<DIM>*, MyBasis::FULL>(cell, this->m_cell_degree));
                this->m_cell_bases.push_back(cell_basis);
            }
        }
    }

    // constructor 
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    P1CrouzeixRaviart<DIM, CONTINUOUS_SPACES...>::P1CrouzeixRaviart(MyMesh::Cell<DIM>* cell, int k, int ks, const Eigen::ArrayXi& var_sizes) {
        throw " P1CrouzeixRaviart not implemented for these continuous spaces ";
    }

    // constructor for P1CrouzeixRaviart 2D with one variable
    template<int CONTINUOUS_SPACE> 
    P1CrouzeixRaviart<2, CONTINUOUS_SPACE>::P1CrouzeixRaviart(MyMesh::Cell<2>* cell, int k, int ks, const Eigen::ArrayXi& var_sizes) : 
    P1CrouzeixRaviartBase<2, CONTINUOUS_SPACE>(cell,
                                                var_sizes,
                                                1, // number of dof in each face (vertices excluded)
                                                0,  // number of dof in each edge (always 0 in 2D)
                                                0,  // number of dof in each vertex
                                                0) {} // number of bulk dof

    // constructor for P1CrouzeixRaviart 3D with one variable
    template<int CONTINUOUS_SPACE> 
    P1CrouzeixRaviart<3, CONTINUOUS_SPACE>::P1CrouzeixRaviart(MyMesh::Cell<3>* cell, int k, int ks, const Eigen::ArrayXi& var_sizes) : 
    P1CrouzeixRaviartBase<3, CONTINUOUS_SPACE>(cell,
                                                var_sizes,
                                                1, // number of dof in each face (vertices and edges excluded)
                                                0,  // number of dof in each edge (vertices excluded)
                                                0,  // number of dof in each vertex
                                                0) {} // number of bulk dof

    // definition of set_reconstruction_operator with a specific cell basis
    // knowing a basis of the dof at each face, 
    // construct the P_DIM^1(T) polynomials
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    void P1CrouzeixRaviartBase<DIM, CONTINUOUS_SPACES...>::set_reconstruction_operator(
        size_t var_size, // this variable can be scalar or vectorial
        MyBasis::Basis<MyMesh::Cell<DIM>*>* cell_basis,
        size_t var_index) {

        // ------------------------------------------------------------------------------------
        // computation of the reconstruction over the cell T
        // \int_T \grad r_T v_T . \grad w dx   =    sum(i=1->n_F) \int_Fi v_Fi \grad w \dot normal(Fi,T) ds
        //      where v_Fi is the unknown related to the face Fi
        // To fix the constant, first look for \mu living in the basis minus the var_size constant basis function(s)
        // Then   r_T v_T = \mu - 1/|F1| \int_F1 \mu ds  + v_F1
        // because v_F1 = 1/|F1| \int_F1 r_T v_T ds
        // ------------------------------------------------------------------------------------
        size_t basis_dim(cell_basis->dimension());
        size_t n_non_cst_basis(basis_dim - var_size);
        std::vector<MyMesh::Face<DIM>*> faces(this->m_cell->get_faces());
        size_t quadra_order(0); // = std::max(0, this->m_cell_degree - 1)

        // evaluate RHS
        // will contain \int_Fi v_Fi \grad w \dot normal(Fi,T) ds 
        // and w in the base minus the constant functions
        Eigen::MatrixXd rhs(Eigen::MatrixXd::Zero(n_non_cst_basis, this->m_n_cumulate_local_dof));

        size_t f_li(0);
        for(auto& fpt : faces) {

            // compute the outward normal to the face
            Eigen::VectorXd normal;
            if(DIM == 2) {
                std::vector<MyMesh::Vertex<DIM>*> vertices(fpt->get_vertices());
                // Store 2 (thus all in 2D) vertices coordinates into vertices_coords
                std::vector<Eigen::Array2d> vertices_coords{vertices[0]->coords(), vertices[1]->coords()};
                normal = outward_normal(vertices_coords, this->m_cell->mass_center());

            } else {
                std::vector<MyMesh::Vertex<DIM>*> vertices(fpt->get_vertices());
                // Store 3 vertices coordinates into vertices_coords
                std::vector<Eigen::Array3d> vertices_coords{vertices[0]->coords(), 
                                                            vertices[1]->coords(), 
                                                            vertices[2]->coords()};
                normal = outward_normal(vertices_coords, this->m_cell->mass_center());
            }

            // for each face, quadrature of the face to compute the integral : 
            //  \int_Fi v_Fi \grad w \dot normal(Fi,T) ds
            //      where v_Fi is the unknown related to the face Fi
            MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(fpt, quadra_order));

            // remove the basis "phi = cste", 
            // TODO: I cheat, I know it is the first basis functions
            // there is var_size constant basis function(s) (1 if the variable is scalar, DIM if vectorial)
            const size_t col_j(this->m_faces_dof[var_index][f_li][0]); // v_Fi (1 at good place, 0 otherwise)
            for(auto& qp : qps.list()) { 
                // evaluate all gradients of all basis functions (of the cell) at point qp (quadra of the face)
                std::vector<Eigen::MatrixXd> grads(cell_basis->eval_gradients(qp->point()));

                // evaluate RHS    sum(i=1->n_F) \int_Fi v_Fi \grad w \dot normal(Fi,T) ds
                // and fill it using the local index of the dof in the PE
                // but remove the constant basis functions
                for(size_t i = var_size; i < basis_dim; i++) {  // grad w, for w in basis minus constante basis
                    rhs.block(i - var_size, col_j, 1, var_size) += qp->weight() * (grads[i] * normal).transpose();
                }
            }

            f_li++; // next face
        }

        // lhs is   \int_T \grad mu . \grad w dx   for w in basis minus all constant basis functions
        // this is the m_stiffness_matrix minus the first lines and first cols
        Eigen::MatrixXd lhs(
            this->m_stiffness_matrices[var_index].bottomRightCorner(n_non_cst_basis, n_non_cst_basis)
        );

        // solve lhs * \mu = rhs
        Eigen::MatrixXd mu(lhs.inverse() * rhs);

        // r_T v_T =  \mu - 1/|F1| \int_F1 \mu ds  + v_F1
        // because v_F1 = 1/|F1| \int_F1 r_T v_T ds
        // quadrature of face F1 to compute    - 1/|F1| \int_F1 \mu ds
        MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(faces[0], this->m_cell_degree));
        Eigen::MatrixXd mean_mu(Eigen::MatrixXd::Zero(var_size, this->m_n_cumulate_local_dof));
        for(auto& qp : qps.list()) { 
            // all basis functions (of the cell) at point qp (quadra of the face)
            // minus constant functions
            Eigen::MatrixXd eval_basis(
                cell_basis->eval_functions(qp->point()).rightCols(n_non_cst_basis));
            mean_mu -= qp->weight() * eval_basis * mu;
        }
        mean_mu /= faces[0]->measure();

        // then   r_T v_T =  \mu - 1/|F1| \int_F1 \mu ds  + v_F1
        // init with good size
        this->m_reconstruction_operators.push_back(Eigen::MatrixXd::Zero(basis_dim, this->m_n_cumulate_local_dof));
        // mean_mu must be written in the basis decomposition
        // find the constant basis functions (should be square)
        // TODO: I cheat because I know that the first basis functions are the constant ones
        Eigen::MatrixXd scale(
            cell_basis->eval_functions(this->m_cell->mass_center()).leftCols(var_size).inverse());

        // fill it
        this->m_reconstruction_operators[var_index].topRows(var_size) = scale * mean_mu;
        this->m_reconstruction_operators[var_index].block(0, this->m_faces_dof[var_index][0][0], var_size, var_size) += 
            scale; // v_F1
        this->m_reconstruction_operators[var_index].bottomRows(n_non_cst_basis) = mu;

        return;

    } // set_reconstruction_operator with a given cell basis

    template<size_t DIM, int... CONTINUOUS_SPACES> 
    void P1CrouzeixRaviartBase<DIM, CONTINUOUS_SPACES...>::set_reconstruction_operator(
            const Eigen::ArrayXi& var_sizes) {

        // only implemented with one variable, so only one basis in m_cell_bases
        // call set_reconstruction_operator with this basis
        return set_reconstruction_operator(var_sizes[0], this->m_cell_bases[0]);
    } // set_reconstruction_operator


    template<size_t DIM, int... CONTINUOUS_SPACES> 
    inline void P1CrouzeixRaviartBase<DIM, CONTINUOUS_SPACES...>::add_stiffness_contribution(
                    size_t var_index, double mult_cst) {

        return pe_add_stiffness_contribution<DIM>(
            this->m_reconstruction_operators[var_index],
            this->m_stiffness_matrices[var_index],
            this->m_local_bilinear_form,
            mult_cst);

    } // add_stiffness_contribution

    template<size_t DIM, int... CONTINUOUS_SPACES> 
    inline void P1CrouzeixRaviartBase<DIM, CONTINUOUS_SPACES...>::add_mass_contribution(
                    size_t var_index, double mult_cst) {

        return pe_add_mass_contribution<DIM>(
            this->m_reconstruction_operators[var_index],
            this->m_mass_matrices[var_index],
            this->m_local_bilinear_form,
            mult_cst);
    } // add_mass_contribution

    // Define the reduction operator : mean values 1/|F| \int_F f ds
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    Eigen::VectorXd P1CrouzeixRaviartBase<DIM, CONTINUOUS_SPACES...>::apply_reduction_operator(
                    double func (Eigen::ArrayXd),
                    size_t var_index,
                    bool only_bound_elemt) {

        Eigen::VectorXd reduction_vec(Eigen::VectorXd::Zero(this->m_n_cumulate_local_dof));

        std::vector<MyMesh::Face<DIM>*> faces(this->m_cell->get_faces());
        size_t f_li(0);
        for(auto& fpt : faces) {
            if(!only_bound_elemt || fpt->is_boundary()) {
                // compute the mean value of the func over the face
                double mean(0.);
                // call function
                MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(fpt, 6)); // 6 is enough because k = 1
                for(auto& qp : qps.list()) { mean += qp->weight() * func(qp->point()); }
        
                // m_faces_dof[var_index][f_li][0] contains the local index of the only dof 
                // of the face
                reduction_vec[this->m_faces_dof[var_index][f_li][0]] = mean/fpt->measure();
            }
            f_li++;
        }

        return reduction_vec;
    } // apply_reduction_operator scalar function

    // Define the reduction operator : mean values on the faces
    // one by component because vect variable
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    Eigen::VectorXd P1CrouzeixRaviartBase<DIM, CONTINUOUS_SPACES...>::apply_reduction_operator(
                    std::vector<double> func (Eigen::ArrayXd),
                    size_t var_index,
                    bool only_bound_elemt) {

        Eigen::VectorXd reduction_vec(Eigen::VectorXd::Zero(this->m_n_cumulate_local_dof));

        std::vector<MyMesh::Face<DIM>*> faces(this->m_cell->get_faces());
        size_t f_li(0);
        for(auto& fpt : faces) {
            if(!only_bound_elemt || fpt->is_boundary()) {
                // compute the mean value of the func over the face
                Eigen::VectorXd mean(Eigen::VectorXd::Zero(DIM));
                // integrate the function over the face
                // quadrature of fpt with order quadra_order (unuseful because k = 1)
                // size_t quadra_order(std::max(6, this->m_cell_degree + 1));

                MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(fpt, 6));
                for(auto& qp : qps.list()) { 
                    std::vector<double> f_qp(func(qp->point()));
                    mean += qp->weight() * Eigen::Map<Eigen::VectorXd> (f_qp.data(), f_qp.size()); 
                }
                mean /= fpt->measure();
        
                // m_faces_dof[var_index][f_li][0] contains the local index of the first dof 
                // of the face
                reduction_vec.segment(this->m_faces_dof[var_index][f_li][0], DIM) = mean;
            }
            f_li++;
        }

        return reduction_vec;

    } // apply_reduction_operator vectorial function

    // fill the rhs with \int f r_t v_t (FE method)
    // if the quadrature order is not given, use 6
    // by default
    // with a scalar variable
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    void P1CrouzeixRaviartBase<DIM, CONTINUOUS_SPACES...>::set_rhs(
                double func (Eigen::ArrayXd),
                int quadra_order,
                size_t var_index) {

        // int_f_basis computes the vector containing  (int f * basis_i)_i
        // with f scalar function
        this->m_local_rhs += this->m_reconstruction_operators[var_index].transpose() 
            * this->int_f_basis(this->m_cell, func, this->m_cell_bases[var_index], quadra_order);

        return;
    } // set_rhs scalar

    // fill the rhs with \int f r_T v_T (FE method)
    // with a specific quadrature order
    // with a vectorial variable
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    void P1CrouzeixRaviartBase<DIM, CONTINUOUS_SPACES...>::set_rhs(
                std::vector<double> func (Eigen::ArrayXd),
                int quadra_order,
                size_t var_index) {

        // int_f_basis computes the vector containing  (int f * basis_i)_i
        // with f vectorial function
        // (one scalar by basis function)
        this->m_local_rhs += this->m_reconstruction_operators[var_index].transpose() 
            * this->int_f_basis(this->m_cell, func, this->m_cell_bases[var_index], quadra_order);

        return;
    } // set_rhs vectorial

} // MyDiscreteSpace namespace
#endif /* P1_CROUZEIX_RAVIART */
/** @} */
