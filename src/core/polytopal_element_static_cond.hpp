#ifndef POLYTOPAL_ELEMENT_STATIC_COND_HPP
#define POLYTOPAL_ELEMENT_STATIC_COND_HPP

#ifndef POLYTOPAL_ELEMENT_HPP
    throw "You must NOT include this file. Include polytopal_element.hpp";
#endif

// this file is included in polytopal_element.hpp, it contains the implementation 
// of the methods concerning the static condensation

/////////////////////////////////////////////////////////////////////////
//                   Implementation of the static condensation
/////////////////////////////////////////////////////////////////////////
// Modification of the local bilinear form and the local rhs to
// eliminate the bulk eliminable unknowns from the global linear system.
// It also stores values necessary to reconstruct the eliminated dof.
template<size_t DIM>
inline void PolytopalElement<DIM>::static_condensation() {

    // if there is no eliminable dof, no modification, exit 
    if(m_n_bulk_eliminable_dof_sum == 0) return;

    // if there is no remaining bulk dof 
    // the matrix is already defined by block
    // with remaining dof (the skeletal ones)
    // and the eliminated dof (the bulk ones)
    // Permutation is necessary only if it remains bulk dof 
    if(!m_remaining_bulk_dof_sc.empty()) {
        // get the permutation matrix
        Eigen::MatrixXd perm(permutation_matrix_sc());

        size_t n_bulk_dof(m_n_bulk_dof.sum());
        // apply the permutation over the local matrix and the rhs
        m_local_bilinear_form.rightCols(n_bulk_dof) = m_local_bilinear_form.rightCols(n_bulk_dof) * perm;
        m_local_bilinear_form.bottomRows(n_bulk_dof) = 
                    perm.transpose() * m_local_bilinear_form.bottomRows(n_bulk_dof);
        m_local_rhs.tail(n_bulk_dof) = perm.transpose() * m_local_rhs.tail(n_bulk_dof);
    }
    
    // number of non eliminable dof
    size_t remaining_dof(m_n_remaining_dof_sc);
    // number of eliminable dof
    size_t eliminable_dof(m_n_bulk_eliminable_dof_sum);

    // find the eliminable bloc of the local bilinear form and inverse it
    Eigen::MatrixXd inv_TT(m_local_bilinear_form.bottomRightCorner(
        eliminable_dof, eliminable_dof).inverse());

    Eigen::MatrixXd B(m_local_bilinear_form.topRightCorner(
            remaining_dof, eliminable_dof) * inv_TT);

    // build the new local bilinear form and the new RHS
    Eigen::MatrixXd shur_local_bilinear_form(
        m_local_bilinear_form.topLeftCorner(remaining_dof, remaining_dof)
        -  B * m_local_bilinear_form.bottomLeftCorner(eliminable_dof, remaining_dof)
    );

    Eigen::VectorXd shur_local_rhs(
        m_local_rhs.head(remaining_dof)
        - B * m_local_rhs.tail(eliminable_dof)
    );

    // store values for later reconstruction of the bulk unknowns
    m_shur_M = inv_TT * m_local_bilinear_form.bottomLeftCorner(eliminable_dof, remaining_dof);
    m_shur_V = inv_TT * m_local_rhs.tail(eliminable_dof);

    // asign the new objects to the polytopal element objects
    // (it resizes the local matrices)
    m_local_bilinear_form = shur_local_bilinear_form;
    m_local_rhs = shur_local_rhs;

    return;
} // static_condensation

template<size_t DIM>
inline Eigen::MatrixXd PolytopalElement<DIM>::permutation_matrix_sc() {
    // first create a vector of size m_n_bulk_dof.sum()
    // with true if the dof is eliminated 
    size_t n_bulk_dof(m_n_bulk_dof.sum());
    std::vector<bool> eliminable_bulk_dof(n_bulk_dof, true);
    for(auto i : m_remaining_bulk_dof_sc) {
        // the remaining bulk dof are not eliminable
        eliminable_bulk_dof[i] = false;
    }

    // Create the permutation matrix
    // over the bulk dof, so size m_n_bulk_dof.sum()
    Eigen::MatrixXd perm(Eigen::MatrixXd::Zero(n_bulk_dof, n_bulk_dof));
    // permutation matrix
    // such that the remaining dof are contiguous to
    // the skeletal block (so at the beginning of the bulk dof)
    size_t compt_not_elim(0); // index of the remaining dof (must be at the beginning of the bulk dof)
    size_t compt_elim(m_remaining_bulk_dof_sc.size()); // index of the eliminable dof (compt start after the remaining dof)
    size_t row(0); // index of bulk dof, corresponds to the row of the perm matrix
    for(auto is_elim : eliminable_bulk_dof) {
        if(is_elim) {
            perm(row++, compt_elim++) = 1.;
        } else {
            perm(row++, compt_not_elim++) = 1.;
        }
    }

    return perm;
} // permutation_matrix_sc


// compute (Lambda, q) with static condensation
template<size_t DIM>
void PolytopalElement<DIM>::local_lagrange_multiplier_contributions_static_cond(
        size_t var_index, size_t var_size, double mesh_measure,
        Eigen::MatrixXd& local_lagrange_static_cond_M,
        Eigen::VectorXd& local_lagrange_static_cond_rhs) {

    // computes the Lagrange multiplier contributions
    // and eliminates the eliminated dof using static condensation
    // TODO: I cheat, I know that the constant basis functions are the first ones !

    // get all the local contributions
    // number of row(s) = size of the variable
    Eigen::MatrixXd local_lagrange(
        get_local_mean(var_index, var_size, mesh_measure));

    // if there is no eliminable dof, no modifications
    if(m_n_bulk_eliminable_dof_sum == 0) {
        local_lagrange_static_cond_M = local_lagrange;
        local_lagrange_static_cond_rhs = Eigen::VectorXd::Zero(var_size); // TODO: implement non-nul mean
        return;
    }

    // if there is no remaining bulk dof 
    // the matrix is already defined by block
    // with remaining dof (the skeletal ones)
    // and the eliminated dof (the bulk ones)
    // Permutation is necessary only if it remains bulk dof 
    if(!m_remaining_bulk_dof_sc.empty()) {
        // get the permutation matrix
        Eigen::MatrixXd perm(permutation_matrix_sc());

        size_t n_bulk_dof(m_n_bulk_dof.sum());
        // apply the permutation over the local matrix and the rhs
        local_lagrange.rightCols(n_bulk_dof) = local_lagrange.rightCols(n_bulk_dof) * perm;
        // no permutation over the rhs because is null 
    }

    // eliminate using shur complement
    local_lagrange_static_cond_M = local_lagrange.leftCols(m_n_remaining_dof_sc)
        - local_lagrange.rightCols(m_n_bulk_eliminable_dof_sum) * m_shur_M;

    local_lagrange_static_cond_rhs = - local_lagrange.rightCols(m_n_bulk_eliminable_dof_sum) * m_shur_V;

    return;
} // local_lagrange_multiplier_contributions_static_cond


// Modify the vector containing the local values of the dof 
// (the skeletal values are given) to reconstruct the eliminated dof.
template<size_t DIM>
void PolytopalElement<DIM>::bulk_shur_reconstruction(Eigen::VectorXd& local_sol_values) {

    // if there is no eliminable dof, no modification, exit 
    if(m_n_bulk_eliminable_dof_sum == 0) return;

    // reconstruct the eliminated values
    local_sol_values.tail(m_n_bulk_eliminable_dof_sum) = m_shur_V 
                        - m_shur_M * local_sol_values.head(m_n_remaining_dof_sc);

    // if necessary (there are some bulk dof 
    // which were not eliminated)
    // apply the permutation to reorder
    // and respect the bulk local dof index
    if(!m_remaining_bulk_dof_sc.empty()) {
        // get the permutation matrix
        Eigen::MatrixXd perm(permutation_matrix_sc());

        size_t n_bulk_dof(m_n_bulk_dof.sum());
        // apply the permutation to local_sol_values
        local_sol_values.tail(n_bulk_dof) = perm * local_sol_values.tail(n_bulk_dof);
    }

    return;
} // bulk_shur_reconstruction

#endif /* POLYTOPAL_ELEMENT_STATIC_COND_HPP */
    
