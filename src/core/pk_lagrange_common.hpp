#ifndef PK_LAGRANGE_COMMON
#define PK_LAGRANGE_COMMON

// does not belong to any class
// common implementations of the Pk Lagrange, 
// also used in Taylor-Hood and other classes
namespace MyDiscreteSpace {

    // number of dof in each face (vertices excluded)
    // in the Pk Lagrange method when the variable is scalar 
    // (will be multiplied by the size of each variable)
    template<size_t DIM> 
    inline int n_dof_faces_pk_lagrange(int lagrange_order) {

        if(DIM == 2) {
            return lagrange_order-1;
        } else if(DIM == 3) {
            return lagrange_order*(lagrange_order-3)/2+1;
        }

    } // n_dof_faces_pk_lagrange with one variable

    // number of dof in each face (vertices excluded)
    // in the Pk Lagrange method when the variable is scalar 
    // (will be multiplied by the size of each variable)
    // with several variables !
    template<size_t DIM> 
    inline Eigen::ArrayXi n_dof_faces_pk_lagrange(
            const std::vector<int>& lagrange_orders) {

        std::vector<int> lagrange_dimensions;
        for(int k : lagrange_orders) {
            lagrange_dimensions.push_back(n_dof_faces_pk_lagrange<DIM>(k));
        }

        // map into eigen array to allow coefwise multiplication
        return Eigen::Map<Eigen::ArrayXi> (lagrange_dimensions.data(), lagrange_dimensions.size());
    } // n_dof_faces_pk_lagrange

    // number of dof in each edge (vertices excluded)
    // in the Pk Lagrange method when the variable is scalar 
    // (will be multiplied by the size of each variable)
    template<size_t DIM> 
    inline int n_dof_edges_pk_lagrange(int lagrange_order) {

        if(DIM == 2) {
            return 0; // no edge in 2D
        } else if(DIM == 3) {
            return lagrange_order-1;
        }

    } // n_dof_edges_pk_lagrange

    // number of dof in each edge (vertices excluded)
    // in the Pk Lagrange method when the variable is scalar 
    // (will be multiplied by the size of each variable)
    // with several variables !
    template<size_t DIM> 
    inline Eigen::ArrayXi n_dof_edges_pk_lagrange(
            const std::vector<int>& lagrange_orders) {

        std::vector<int> lagrange_dimensions;
        for(int k : lagrange_orders) {
            lagrange_dimensions.push_back(n_dof_edges_pk_lagrange<DIM>(k));
        }

        // map into eigen array to allow coefwise multiplication
        return Eigen::Map<Eigen::ArrayXi> (lagrange_dimensions.data(), lagrange_dimensions.size());
    } // n_dof_edges_pk_lagrange

    // number of bulk dof
    // in the Pk Lagrange method when the variable is scalar 
    // (will be multiplied by the size of each variable)
    template<size_t DIM> 
    inline int n_dof_bulk_pk_lagrange(int lagrange_order) {

        if(DIM == 2) {
            return lagrange_order*(lagrange_order-3)/2+1;
        } else if(DIM == 3) {
            return (lagrange_order+1)*(lagrange_order+2)*(lagrange_order-9)/6 + 6*lagrange_order+2;
        }

    } // n_dof_bulk_pk_lagrange

    // number of bulk dof
    // in the Pk Lagrange method when the variable is scalar 
    // (will be multiplied by the size of each variable)
    // with several variables !
    template<size_t DIM> 
    inline Eigen::ArrayXi n_dof_bulk_pk_lagrange(
            const std::vector<int>& lagrange_orders) {

        std::vector<int> lagrange_dimensions;
        for(int k : lagrange_orders) {
            lagrange_dimensions.push_back(n_dof_bulk_pk_lagrange<DIM>(k));
        }

        // map into eigen array to allow coefwise multiplication
        return Eigen::Map<Eigen::ArrayXi> (lagrange_dimensions.data(), lagrange_dimensions.size());
    } // n_dof_bulk_pk_lagrange


    // compute the reduction operator of all the skeletal elements 
    // a list of skeletal objects
    // func is scalar
    // fill reduction_vector
    inline void apply_reduction_operator_by_elemts(
            size_t n_elements, 
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_by_elements,
            size_t var_index,
            size_t func_size,
            double func (Eigen::ArrayXd),
            std::vector<Eigen::VectorXd>& lattice_coords,
            Eigen::VectorXd& reduction_vector) {

        for(size_t e_li = 0; e_li < n_elements; e_li++) {
            // loop over all dof belonging to the element e
            for(size_t dof = dof_local_index_by_elements[var_index][e_li][0], 
                    end = dof_local_index_by_elements[var_index][e_li][1]; 
                    dof < end; dof++) {

                reduction_vector[dof] = func(lattice_coords[dof]);
            }
        }

        return;
    } // apply_reduction_operator_by_elemts on scalar variable

    // compute the reduction operator of the boundary elements 
    // a list of skeletal objects
    // func is scalar
    // fill reduction_vector
    template<typename ELEMT>
    inline void apply_reduction_operator_boundary_by_elemts(
            const std::vector<ELEMT>& elements, 
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_by_elements,
            size_t var_index,
            size_t func_size,
            double func (Eigen::ArrayXd),
            std::vector<Eigen::VectorXd>& lattice_coords,
            Eigen::VectorXd& reduction_vector) {

        size_t e_li(0); // local index of element
        for(auto& e : elements) {
            if(e->is_boundary()) {
                // loop over all dof belonging to the element e
                for(size_t dof = dof_local_index_by_elements[var_index][e_li][0], 
                        end = dof_local_index_by_elements[var_index][e_li][1]; 
                        dof < end; dof++) {

                    reduction_vector[dof] = func(lattice_coords[dof]);
                }
            }
            e_li++; // next element
        }

        return;
    } // apply_reduction_operator_boundary_by_elemts on scalar variable

    // compute the reduction operator of the bulk dof 
    // func is scalar
    // fill reduction_vector
    inline void apply_reduction_operator_bulk(
            const std::vector<std::vector<size_t>>& dof_local_index_bulk,
            size_t var_index,
            size_t func_size,
            double func (Eigen::ArrayXd),
            std::vector<Eigen::VectorXd>& lattice_coords,
            Eigen::VectorXd& reduction_vector) {

        // loop over all dof belonging to the bulk
        for(size_t dof = dof_local_index_bulk[var_index][0], 
                end = dof_local_index_bulk[var_index][1]; 
                dof < end; dof++) {

            reduction_vector[dof] = func(lattice_coords[dof]);
        }

        return;
    } // apply_reduction_operator_bulk on scalar variable



    // compute the reduction operator of all the elements 
    // a list of skeletal objects
    // func is vectorial
    // fill reduction_vector
    inline void apply_reduction_operator_by_elemts(
            size_t n_elements, 
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_by_elements,
            size_t var_index,
            size_t func_size,
            std::vector<double> func (Eigen::ArrayXd),
            std::vector<Eigen::VectorXd>& lattice_coords,
            Eigen::VectorXd& reduction_vector) {

        for(size_t e_li = 0; e_li < n_elements; e_li++) {
            // loop over all dof belonging to the element e
            for(size_t dof = dof_local_index_by_elements[var_index][e_li][0], 
                    end = dof_local_index_by_elements[var_index][e_li][1]; 
                    dof < end; dof += func_size) {

                // evaluate the function at this point
                std::vector<double> f_lattice(func(lattice_coords[dof]));
                // cast into eigen vector and fill reduction_vector
                reduction_vector.segment(dof, func_size) = Eigen::Map<Eigen::VectorXd> (f_lattice.data(), f_lattice.size()); 
            }
        }

        return;
    } // apply_reduction_operator_by_elemts on vectorial variable

    // compute the reduction operator of the boundary elements 
    // a list of skeletal objects
    // func is vectorial
    // fill reduction_vector
    template<typename ELEMT>
    inline void apply_reduction_operator_boundary_by_elemts(
            const std::vector<ELEMT>& elements, 
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_by_elements,
            size_t var_index,
            size_t func_size,
            std::vector<double> func (Eigen::ArrayXd),
            std::vector<Eigen::VectorXd>& lattice_coords,
            Eigen::VectorXd& reduction_vector) {

        size_t e_li(0); // local index of element
        for(auto& e : elements) {
            if(e->is_boundary()) {
                // loop over all dof belonging to the element e
                for(size_t dof = dof_local_index_by_elements[var_index][e_li][0], 
                        end = dof_local_index_by_elements[var_index][e_li][1]; 
                        dof < end; dof += func_size) {

                    // evaluate the function at this point
                    std::vector<double> f_lattice(func(lattice_coords[dof]));
                    // cast into eigen vector and fill reduction_vector
                    reduction_vector.segment(dof, func_size) = Eigen::Map<Eigen::VectorXd> (f_lattice.data(), f_lattice.size()); 
                }
            }
            e_li++; // next element
        }

        return;

    } // apply_reduction_operator_boundary_by_elemts on vectorial variable

    // compute the reduction operator of the bulk dof 
    // func is vectorial
    // fill reduction_vector
    inline void apply_reduction_operator_bulk(
            const std::vector<std::vector<size_t>>& dof_local_index_bulk,
            size_t var_index,
            size_t func_size,
            std::vector<double> func (Eigen::ArrayXd),
            std::vector<Eigen::VectorXd>& lattice_coords,
            Eigen::VectorXd& reduction_vector) {

        // loop over all dof belonging to the bulk
        for(size_t dof = dof_local_index_bulk[var_index][0], 
                end = dof_local_index_bulk[var_index][1]; 
                dof < end; dof += func_size) {

            // evaluate the function at this point
            std::vector<double> f_lattice(func(lattice_coords[dof]));
            // cast into eigen vector and fill reduction_vector
            reduction_vector.segment(dof, func_size) = Eigen::Map<Eigen::VectorXd> (f_lattice.data(), f_lattice.size()); 
        }

        return;
    } // apply_reduction_operator_bulk on vectorial variable


    // compute the reduction operator of a 
    // scalar or vectorial function
    // 2D (no edges)
    // fill reduction_vector
    template<size_t DIM, typename RET>
    inline void lagrange_apply_reduction_operator(
            const MyMesh::Cell<DIM>* cell_pt, 
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_vertices,
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_faces,
            const std::vector<std::vector<size_t>>& dof_local_index_bulk,
            size_t var_index,
            size_t func_size,
            RET func (Eigen::ArrayXd),
            std::vector<Eigen::VectorXd>& lattice_coords,
            Eigen::VectorXd& reduction_vector) {

        // vertices
        apply_reduction_operator_by_elemts(
            cell_pt->n_vertices(), 
            dof_local_index_vertices,
            var_index, func_size, func, lattice_coords,
            reduction_vector);

        // faces
        apply_reduction_operator_by_elemts(
            cell_pt->n_faces(), 
            dof_local_index_faces,
            var_index, func_size, func, lattice_coords,
            reduction_vector);

        // bulk
        apply_reduction_operator_bulk(
            dof_local_index_bulk,
            var_index, func_size, func, lattice_coords,
            reduction_vector);

        return;
    } // lagrange_apply_reduction_operator on 2D meshes


    // compute the reduction operator of a 
    // scalar or vectorial function
    // 3D (with edges)
    // fill reduction_vector
    template<size_t DIM, typename RET>
    inline void lagrange_apply_reduction_operator(
            const MyMesh::Cell<DIM>* cell_pt, 
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_vertices,
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_edges,
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_faces,
            const std::vector<std::vector<size_t>>& dof_local_index_bulk,
            size_t var_index,
            size_t func_size,
            RET func (Eigen::ArrayXd),
            std::vector<Eigen::VectorXd>& lattice_coords,
            Eigen::VectorXd& reduction_vector) {

        // vertices
        apply_reduction_operator_by_elemts(
            cell_pt->n_vertices(), 
            dof_local_index_vertices,
            var_index, func_size, func, lattice_coords,
            reduction_vector);

        // edges
        apply_reduction_operator_by_elemts(
            cell_pt->n_edges(), 
            dof_local_index_edges,
            var_index, func_size, func, lattice_coords,
            reduction_vector);

        // faces
        apply_reduction_operator_by_elemts(
            cell_pt->n_faces(), 
            dof_local_index_faces,
            var_index, func_size, func, lattice_coords,
            reduction_vector);

        // bulk
        apply_reduction_operator_bulk(
            dof_local_index_bulk,
            var_index, func_size, func, lattice_coords,
            reduction_vector);

        return;
    } // lagrange_apply_reduction_operator on 3D meshes


    // compute the reduction operator only of 
    // the boundary elements of a 
    // scalar or vectorial function
    // 2D (no edges)
    // fill reduction_vector
    template<size_t DIM, typename RET>
    inline void lagrange_apply_reduction_operator_boundary(
            const MyMesh::Cell<DIM>* cell_pt, 
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_vertices,
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_faces,
            size_t var_index,
            size_t func_size,
            RET func (Eigen::ArrayXd),
            std::vector<Eigen::VectorXd>& lattice_coords,
            Eigen::VectorXd& reduction_vector) {

        // vertices
        apply_reduction_operator_boundary_by_elemts(
            cell_pt->get_vertices(), 
            dof_local_index_vertices,
            var_index, func_size, func, lattice_coords,
            reduction_vector);

        // faces
        apply_reduction_operator_boundary_by_elemts(
            cell_pt->get_faces(), 
            dof_local_index_faces,
            var_index, func_size, func, lattice_coords,
            reduction_vector);

        return;
    } // lagrange_apply_reduction_operator_boundary on 2D meshes


    // compute the reduction operator only of 
    // the boundary elements of a 
    // scalar or vectorial function
    // 3D (with edges)
    // fill reduction_vector
    template<size_t DIM, typename RET>
    inline void lagrange_apply_reduction_operator_boundary(
            const MyMesh::Cell<DIM>* cell_pt, 
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_vertices,
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_edges,
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_faces,
            size_t var_index,
            size_t func_size,
            RET func (Eigen::ArrayXd),
            std::vector<Eigen::VectorXd>& lattice_coords,
            Eigen::VectorXd& reduction_vector) {

        // vertices
        apply_reduction_operator_boundary_by_elemts(
            cell_pt->get_vertices(), 
            dof_local_index_vertices,
            var_index, func_size, func, lattice_coords,
            reduction_vector);

        // edges
        apply_reduction_operator_boundary_by_elemts(
            cell_pt->get_edges(), 
            dof_local_index_edges,
            var_index, func_size, func, lattice_coords,
            reduction_vector);

        // faces
        apply_reduction_operator_boundary_by_elemts(
            cell_pt->get_faces(), 
            dof_local_index_faces,
            var_index, func_size, func, lattice_coords,
            reduction_vector);

        return;
    } // lagrange_apply_reduction_operator_boundary on 3D meshes


} // MyDiscreteSpace namespace
#endif /* PK_LAGRANGE_COMMON */
