/**
  * @{
*/

#ifndef HHO_HPP
#define HHO_HPP

#include <iostream>
#include <cstring>
#include <vector>
#include <Eigen/Dense>
#include "common_geometry.hpp"
#include "hho_common.hpp"
#include "cell.hpp"

namespace MyDiscreteSpace {

    /**
     * The HHO class contains 
     * the specific informations of HHO. 
     * It inherits of the HHOBase class
     * which contains some common informations
     * like the orthogonal l2 projection or the stabilization
    */
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    class HHO : public HHOBase<DIM>
    {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param kb order of the bulk discrete unknown
         * @param ks order of the polynomial in the skeletal objects, the order of the reconstruction is ks + 1
         * @param var_sizes array with the dimensions of all the variables
         */
        HHO(MyMesh::Cell<DIM>* cell, int kb, int ks, const Eigen::ArrayXi& var_sizes);
        ~HHO() = default;   ///<  Default destructor

        /**
         * Compute the reconstruction operator
         * @param var_sizes array with the dimension of each variable
         */
        void set_reconstruction_operator(const Eigen::ArrayXi& var_sizes);
        /**
         * Compute the reconstruction operator
         * with a specific basis (is called for Stokes for example)
         * @param var_size dimension of the variable
         * @param cell_basis specific cellbasis corresponding to the variable
         * @param bulk_basis specific bulk basis corresponding to the variable
         * @param faces_basis specific faces bases corresponding to the variable
         * @param var_index index of the variable (0 by default)
         */
        inline void set_reconstruction_operator(size_t var_size,
                MyBasis::Basis<MyMesh::Cell<DIM>*>* cell_basis, 
                MyBasis::Basis<MyMesh::Cell<DIM>*>* bulk_basis, 
                std::vector<MyBasis::Basis<MyMesh::Face<DIM>*>*> faces_basis,
                size_t var_index = 0);

        /**
         * Using the stiffness matrix, add
         * k * \int_T grad(r_T u_T) * grad(r_T v_T) dx
         * and the stabilization term
         * to the local matrix
         * @param var_index index of the variable to add the stiffness contribution
         * @param mult_cst multiplicative constant
         */
        inline void add_stiffness_contribution(size_t var_index = 0, double mult_cst = 1.);

        /**
         * Add the product of the bulk unknowns
         * to the local matrix
         * @param var_index index of the variable to add the stiffness contribution
         * @param mult_cst multiplicative constant
         */
        inline void add_mass_contribution(size_t var_index = 0, double mult_cst = 1.);

        /**
         * Add a divergence term knowing two variable indexes
         * Not implemented in HHO yet
         * @param var_index1 index of the first variable
         * @param var_index2 index of the second variable
         * @param mesh_measure measure of the global domain
         * @param mult_cst multiplicative constant
         */
        void add_divergence_contribution(
                size_t var_index1, 
                size_t var_index2, 
                double mesh_measure,
                double mult_cst = 1.) {
            throw " add_divergence_contribution not defined in hho ";
        }

        /**
         * Compute the reduction operator (scalar variable)
         * @param func scalar valued continuous function to which extract reduction
         * @param only_bound_elemt boolean, if true apply reduction operator only on boundary elements
         * @return vector containing the reduction of the scalar valued function
         */
        Eigen::VectorXd apply_reduction_operator(double func (Eigen::ArrayXd),
                size_t var_index = 0,
                bool only_bound_elemt = false);
        /**
         * Compute the reduction operator (vectorial variable)
         * @param func vectorial valued continuous function to which extract reduction
         * @param only_bound_elemt boolean, if true apply reduction operator only on boundary elements
         * @return vector containing the reduction of the vectorial valued function
         */
        Eigen::VectorXd apply_reduction_operator(std::vector<double> func (Eigen::ArrayXd),
                size_t var_index = 0,
                bool only_bound_elemt = false);

        /**
         * Set the local right-hand-side
         * from a continuous function
         * with specific quadrature order 
         * (6 by default)
         * For a scalar variable or vectorial one
         * @param func
         * @param quadra_order
         * @param var_index
         */
        void set_rhs(double func (Eigen::ArrayXd), int quadra_order, size_t var_index);
        void set_rhs(std::vector<double> func (Eigen::ArrayXd), int quadra_order, size_t var_index);

    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////

    // constructor for HHO (has access to the constructor of HHOBase).
    // kb is the order of the polynomial in the bulk
    // ks is the order of the polynomial in the skeletal objects
    // The order of convergence of the method is thus ks+1
    template<size_t DIM, int... CONTINUOUS_SPACES>  
    HHO<DIM, CONTINUOUS_SPACES...>::HHO(
                       MyMesh::Cell<DIM>* cell, 
                       int kb, int ks,
                       const Eigen::ArrayXi& var_sizes) :
    HHOBase<DIM>(cell, 
                var_sizes * n_dof_faces_pk<DIM>(ks), // nb of face dof
                var_sizes * 0, var_sizes * 0, // no edge or vertex dof
                var_sizes * n_dof_bulk_pk<DIM>(kb) // nb of bulk dof, 
                //all bulk dof can be eliminated so not necessary to precise their number
                ) {

#ifdef DEBUG
        if(kb != ks && kb != ks + 1) {
            throw " ERROR: the bulk and skeletal degrees are not coherent for HHO. ";
        }
#endif
        this->m_cell_degree = ks + 1; // polynomial degree of the numerical solution
        this->m_skeletal_degree = ks;  // polynomial degree of the skeletal unknowns
        this->m_bulk_degree = kb;  // polynomial degree of the bulk unknowns

        // chose which stabilization will be used. Can be modified using set_stabilization_type
        this->m_stabilization_type = { 
            this->m_bulk_degree - this->m_skeletal_degree == 1 ? "ls_stabilization" : "other" };

        for(int i = 0, nb_var = var_sizes.size(); i < nb_var; i++) {
            // Warning : m_cell_bases contains the bases in which is decomposed the numerical solution
            // in HHO, it is PˆSkelDegree(T)
            // m_bulk_bases contains the bases of the cell with polynomial degree m_bulk_degree
            if(var_sizes[i] == 1) { // one scalar variable
                MyBasis::Basis< MyMesh::Cell<DIM>* >* cell_basis(
                    new MyBasis::ScaledMonomialScalarBasis<MyMesh::Cell<DIM>*>(cell, this->m_cell_degree));
                this->m_cell_bases.push_back(cell_basis); 

                // bulk bases : PˆBulkDegree(T)
                MyBasis::Basis< MyMesh::Cell<DIM>* >* bulk_basis(
                    new MyBasis::ScaledMonomialScalarBasis<MyMesh::Cell<DIM>*>(cell, this->m_bulk_degree));
                this->m_bulk_bases.push_back(bulk_basis);

                // faces basis of this variable PˆSkelDegree(F)
                std::vector<MyBasis::Basis<MyMesh::Face<DIM>*>*> faces_basis;
                for(auto& fpt : cell->get_faces()) {
                    MyBasis::Basis< MyMesh::Face<DIM>* >* face_basis(
                        new MyBasis::ScaledMonomialScalarBasis<MyMesh::Face<DIM>*>(fpt, this->m_skeletal_degree));
                    faces_basis.push_back(face_basis); 
                }
                this->m_faces_bases.push_back(faces_basis); // all faces bases for this variable

            } else if(var_sizes[i] == DIM) { // one vectorial variable
                // vectorial full basis PˆSkelDegree(T)
                MyBasis::Basis< MyMesh::Cell<DIM>* >* cell_basis(
                    new MyBasis::ScaledMonomialVectorBasis<MyMesh::Cell<DIM>*, MyBasis::FULL>(cell, this->m_cell_degree));
                this->m_cell_bases.push_back(cell_basis);

                // bulk bases : PˆBulkDegree(T)
                MyBasis::Basis< MyMesh::Cell<DIM>* >* bulk_basis(
                    new MyBasis::ScaledMonomialVectorBasis<MyMesh::Cell<DIM>*, MyBasis::FULL>(cell, this->m_bulk_degree));
                this->m_bulk_bases.push_back(bulk_basis);

                // faces basis of this variable PˆSkelDegree(F)
                std::vector<MyBasis::Basis<MyMesh::Face<DIM>*>*> faces_basis;
                for(auto& fpt : cell->get_faces()) {
                    MyBasis::Basis< MyMesh::Face<DIM>* >* face_basis(
                        new MyBasis::ScaledMonomialVectorBasis<MyMesh::Face<DIM>*, MyBasis::FULL>(fpt, this->m_skeletal_degree));
                    faces_basis.push_back(face_basis); 
                }
                this->m_faces_bases.push_back(faces_basis); // all faces bases for this variable
            }
        }
    }

    // definition of set_reconstruction_operator with default cell basis
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    void HHO<DIM, CONTINUOUS_SPACES...>::set_reconstruction_operator(const Eigen::ArrayXi& var_sizes) {

        // loop over all the variables
        for(size_t var_index = 0, nb_var = var_sizes.size(); var_index < nb_var; var_index++) {
            // set the reconstruction operator for each variable
            set_reconstruction_operator(var_sizes[var_index], 
                this->m_cell_bases[var_index], 
                this->m_bulk_bases[var_index], 
                this->m_faces_bases[var_index],
                var_index);
        }

        return;
    } // set_reconstruction_operator with default cell basis

    // definition of set_reconstruction_operator of one variable knowing the bases
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    inline void HHO<DIM, CONTINUOUS_SPACES...>::set_reconstruction_operator(size_t var_size,
            MyBasis::Basis<MyMesh::Cell<DIM>*>* cell_basis, 
            MyBasis::Basis<MyMesh::Cell<DIM>*>* bulk_basis, 
            std::vector<MyBasis::Basis<MyMesh::Face<DIM>*>*> faces_basis,
            size_t var_index) {

        // ------------------------------------------------------------------------------------
        // computation of the reconstruction operator over the cell T
        // \int_T \grad r_T vˆtilde_T . \grad w dx   =  \int_T \grad v_T . \grad w dx
        //     - sum(i=1->n_F) \int_Fi (v_T - v_Fi) \grad w \dot normal(Fi,T) ds
        // with w in in the cell basis (grad of constant basis functions is null)
        //
        // To fix the constant, product of the mean over the cell on the left and on the right
        // 1/|T| \int_T r_T vˆtilde_T dx * 1/|T| \int_T w dx = 1/|T| \int_T v_T dx * 1/|T| \int_T w dx
        // ------------------------------------------------------------------------------------

        size_t cell_basis_dim(cell_basis->dimension());
        std::vector<MyMesh::Face<DIM>*> faces(this->m_cell->get_faces());

        // matrix with the right hand side
        Eigen::MatrixXd rhs(Eigen::MatrixXd::Zero(cell_basis_dim, this->m_n_cumulate_local_dof));

        // compute \int_T \grad v_T . \grad w dx with v_T in bulk_basis
        // and w in m_cell_bases minus all constant basis functions
        size_t quadra_order(std::max(0, this->m_cell_degree + this->m_bulk_degree - 2));
        MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(this->m_cell, quadra_order));

        size_t start_bulk_basis_index(this->m_bulk_dof[var_index][0]);
        size_t end_bulk_basis_index(this->m_bulk_dof[var_index][1]);
        // compute the integral
        for(auto& qp : qps.list()) { 
            // List of matrices with the gradients of all basis functions wrt all directions
            // evaluated at quadrature point
            std::vector<Eigen::MatrixXd> grads_cell_basis(
                cell_basis->eval_gradients(qp->point())); // grad w
            std::vector<Eigen::MatrixXd> grads_bulk_basis(
                bulk_basis->eval_gradients(qp->point())); // grad v_T
            
            for(size_t col_j = start_bulk_basis_index, j = 0; 
                    col_j < end_bulk_basis_index; col_j++, j++) {
                for(size_t i = var_size; i < cell_basis_dim; i++) {
                    rhs(i, col_j) += 
                        qp->weight() * this->contraction_product(grads_cell_basis[i], grads_bulk_basis[j]);
                } 
            }
        } // quadra

        // add the term - sum(i=1->n_F) \int_Fi (v_T - v_Fi) \grad w \dot normal(Fi,T) ds
        // with v_T in bulk_basis, w in cell_basis minus all constant basis functions
        // and v_Fi in faces_basis[Fi]
        size_t f_li(0); // local index of face
        for(auto& fpt : faces) {

            // compute the outward normal to the face
            Eigen::VectorXd normal;
            // TODO : use if constexpr (DIM == 2) of C++17
            if(DIM == 2) {
                std::vector<MyMesh::Vertex<DIM>*> vertices(fpt->get_vertices());
                // Store 2 (thus all in 2D) vertices coordinates into vertices_coords
                std::vector<Eigen::Array2d> vertices_coords{vertices[0]->coords(), vertices[1]->coords()};
                normal = outward_normal(vertices_coords, this->m_cell->mass_center());

            } else {
                std::vector<MyMesh::Vertex<DIM>*> vertices(fpt->get_vertices());
                // Store 3 vertices coordinates into vertices_coords
                std::vector<Eigen::Array3d> vertices_coords{vertices[0]->coords(), 
                                                            vertices[1]->coords(), 
                                                            vertices[2]->coords()};
                normal = outward_normal(vertices_coords, this->m_cell->mass_center());
            }

            // compute - \int_Fi (v_T - v_Fi) \grad w \dot normal(Fi,T) ds
            // with v_T in bulk_basis, v_Fi in faces_basis[Fi],
            // and w in cell_basis minus all constant basis functions
            quadra_order = std::max(0, this->m_cell_degree - 1 + 
                        std::max(this->m_bulk_degree, this->m_skeletal_degree) );
            MyQuadra::QuadraturePoints<DIM> f_qps(MyQuadra::integrate(fpt, quadra_order));

            size_t start_face_basis_index(this->m_faces_dof[var_index][f_li][0]);
            size_t end_face_basis_index(this->m_faces_dof[var_index][f_li][1]);
            // compute the integral
            for(auto& qp : f_qps.list()) { 
                // List of matrices with the gradients of cell_basis basis functions wrt all directions
                // evaluated at quadrature point
                std::vector<Eigen::MatrixXd> grads_cell_basis(
                    cell_basis->eval_gradients(qp->point())); // \grad w
                Eigen::MatrixXd func_bulk_basis(
                    bulk_basis->eval_functions(qp->point()).transpose()); // v_T
                Eigen::MatrixXd func_face_basis(
                    faces_basis[f_li]->eval_functions(qp->point()).transpose()); // v_Fi
                
                // - \int_Fi v_T \grad w \dot normal(Fi,T) ds
                for(size_t col_j = start_bulk_basis_index, j = 0; 
                        col_j < end_bulk_basis_index; col_j++, j++) {
                    for(size_t i = var_size; i < cell_basis_dim; i++) { // remove the constant basis function
                        rhs(i, col_j) -= 
                                qp->weight() * func_bulk_basis.row(j) * (grads_cell_basis[i] * normal);
                    } 
                }
                //  \int_Fi v_Fi \grad w \dot normal(Fi,T) ds
                for(size_t col_j = start_face_basis_index, j = 0; 
                        col_j < end_face_basis_index; col_j++, j++) {
                    for(size_t i = var_size; i < cell_basis_dim; i++) {
                        rhs(i, col_j) += 
                                qp->weight() * func_face_basis.row(j) * (grads_cell_basis[i] * normal);
                    } 
                }
            } // quadra

            f_li++;
        } // next face


        // matrix with the left hand side
        // \int_T \grad r_T vˆtilde_T . \grad w dx    for w in cell basis
        Eigen::MatrixXd lhs(this->m_stiffness_matrices[var_index]);

        // At both sides, 
        // add the mean product :
        // rhs \int v_T dx * \int w dx
        // lhs \int r_T vˆtilde_T dx * \int w dx
        // with v_T in bulk_basis and w in cell_basis
        Eigen::MatrixXd int_bulk_bases(
            Eigen::MatrixXd::Zero(var_size, this->m_n_bulk_dof[var_index])); // int v_T
        Eigen::MatrixXd int_cell_bases(Eigen::MatrixXd::Zero(var_size, cell_basis_dim)); // int w

        if(var_size == 1) {
            // bases functions are scalar, so int is one value by basis function
            int_bulk_bases = this->int_f_basis(this->m_cell, 
                [](Eigen::ArrayXd pt) { return 1.; },
                bulk_basis, this->m_bulk_degree).transpose();
            int_cell_bases = this->int_f_basis(this->m_cell, 
                [](Eigen::ArrayXd pt) { return 1.; }, 
                cell_basis, this->m_cell_degree).transpose();

        } else if(var_size == DIM) { // vectorial variable
            // bases functions are vectorial, so DIM values by basis function
            // \int_T w dx component by component
            // \int_T v_T dx component by component
            // quadrature to compute the integrals
            MyQuadra::QuadraturePoints<DIM> qps(
                MyQuadra::integrate(this->m_cell, std::max(this->m_cell_degree, this->m_bulk_degree)));

            for(auto& qp : qps.list()) { 
                // evaluate bulk basis functions at quadrature point
                Eigen::MatrixXd bulk_basis_qp(
                    bulk_basis->eval_functions(qp->point())); // v_T
                // evaluate cell basis functions at quadrature point
                Eigen::MatrixXd cell_basis_qp(
                    cell_basis->eval_functions(qp->point())); // w

                // \int_T v_T dx component by component
                int_bulk_bases += qp->weight() * bulk_basis_qp;
                // \int_T w dx component by component
                int_cell_bases += qp->weight() * cell_basis_qp;
            } // quadra
        }

        double scale(1. / std::pow(this->m_cell->diam(), 2.) / this->m_cell->measure());
        // rhs scale * \int v_T dx * \int w dx
        rhs.rightCols(this->m_n_bulk_dof[var_index]) +=
                scale * int_cell_bases.transpose() * int_bulk_bases;
        // lhs scale * \int r_T vˆtilde_T dx * \int w dx
        lhs += scale * int_cell_bases.transpose() * int_cell_bases;

        // solve lhs * r_T vˆtilde_T = rhs
        // and store it into the reconstruction op vector
        this->m_reconstruction_operators.push_back(lhs.inverse() * rhs);
        
        return;
    } // set_reconstruction_operator

    template<size_t DIM, int... CONTINUOUS_SPACES> 
    inline void HHO<DIM, CONTINUOUS_SPACES...>::add_stiffness_contribution(
                    size_t var_index, double mult_cst) {

        pe_add_stiffness_contribution<DIM>(
            this->m_reconstruction_operators[var_index],
            this->m_stiffness_matrices[var_index],
            this->m_local_bilinear_form,
            mult_cst);

        this->add_stabilization(var_index, mult_cst);

        return;
    } // add_stiffness_contribution

    template<size_t DIM, int... CONTINUOUS_SPACES> 
    inline void HHO<DIM, CONTINUOUS_SPACES...>::add_mass_contribution(
                    size_t var_index, double mult_cst) {

        // compute mass matrix with bulk unknowns only
        auto bulk_start(this->m_bulk_dof[var_index][0]);
        auto bulk_length(this->m_n_bulk_dof(var_index));
        this->m_local_bilinear_form.block(bulk_start, bulk_start, bulk_length, bulk_length) += mult_cst *
            this->compute_mass_matrix(this->m_cell, this->m_bulk_bases[var_index], 2 * this->m_bulk_degree);
        
        return;
    } // add_mass_contribution

    // in HHO, rhs is computed as    (f,v_T)_T
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    void HHO<DIM, CONTINUOUS_SPACES...>::set_rhs(
                double func (Eigen::ArrayXd),
                int quadra_order,
                size_t var_index) {

        // v_T indexes
        size_t start(this->m_bulk_dof[var_index][0]);
        size_t length(this->m_n_bulk_dof[var_index]);

        this->m_local_rhs.segment(start, length) += 
            this->int_f_basis(this->m_cell, func, this->m_bulk_bases[var_index], quadra_order);

        return;
    } // set_rhs

    // in HHO, rhs is computed as    (f,v_T)_T
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    void HHO<DIM, CONTINUOUS_SPACES...>::set_rhs(
                std::vector<double> func (Eigen::ArrayXd),
                int quadra_order,
                size_t var_index) {

        // v_T indexes
        size_t start(this->m_bulk_dof[var_index][0]);
        size_t length(this->m_n_bulk_dof[var_index]);

        this->m_local_rhs.segment(start, length) += 
            this->int_f_basis(this->m_cell, func, this->m_bulk_bases[var_index], quadra_order);

        return;
    } // set_rhs

    // Define the reduction operator
    // (l2proj(F0), l2proj(F1), ..., l2proj(Fn), l2proj(Bulk))
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    Eigen::VectorXd HHO<DIM, CONTINUOUS_SPACES...>::apply_reduction_operator(
                    double func (Eigen::ArrayXd),
                    size_t var_index,
                    bool only_bound_elemt) {

        Eigen::VectorXd reduction_vec(Eigen::VectorXd::Zero(this->m_n_cumulate_local_dof));

        // faces of the cell
        std::vector<MyMesh::Face<DIM>*> faces(this->m_cell->get_faces());

        // l2 projection over the faces
        size_t f_li(0); // local index of face
        for(auto& fpt : faces) {
            if(!only_bound_elemt || fpt->is_boundary()) {
                size_t start(this->m_faces_dof[var_index][f_li][0]);
                size_t length(this->m_n_faces_dof(f_li, var_index));

                reduction_vec.segment(start, length) = 
                    this->orth_l2projection(fpt, this->m_faces_bases[var_index][f_li], func);
            }
            f_li++; // next face
        }

        if(!only_bound_elemt) {
            // l2 projection over the bulk
            size_t start(this->m_bulk_dof[var_index][0]);
            size_t length(this->m_n_bulk_dof[var_index]);

            reduction_vec.segment(start, length) = 
                this->orth_l2projection(this->m_cell, this->m_bulk_bases[var_index], func);
        }

        return reduction_vec;
    } // apply_reduction_operator on scalar variable

    // Define the reduction operator on vectorial variable (same algo as scalar)
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    Eigen::VectorXd HHO<DIM, CONTINUOUS_SPACES...>::apply_reduction_operator(
                    std::vector<double> func (Eigen::ArrayXd),
                    size_t var_index,
                    bool only_bound_elemt) {

        Eigen::VectorXd reduction_vec(Eigen::VectorXd::Zero(this->m_n_cumulate_local_dof));

        // faces of the cell
        std::vector<MyMesh::Face<DIM>*> faces(this->m_cell->get_faces());

        // l2 projection over the faces
        size_t f_li(0); // local index of face
        for(auto& fpt : faces) {
            if(!only_bound_elemt || fpt->is_boundary()) {
                size_t start(this->m_faces_dof[var_index][f_li][0]);
                size_t length(this->m_n_faces_dof(f_li, var_index));

                reduction_vec.segment(start, length) = 
                    this->orth_l2projection(fpt, this->m_faces_bases[var_index][f_li], func);
            }
            f_li++; // next face
        }

        if(!only_bound_elemt) {
            // l2 projection over the cell
            size_t start(this->m_bulk_dof[var_index][0]);
            size_t length(this->m_n_bulk_dof[var_index]);

            reduction_vec.segment(start, length) = 
                this->orth_l2projection(this->m_cell, this->m_bulk_bases[var_index], func);
        }

        return reduction_vec;
    } // apply_reduction_operator on vectorial variable

} // MyDiscreteSpace namespace
#endif /* HHO_HPP */
/** @} */
