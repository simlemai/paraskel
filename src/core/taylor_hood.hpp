/**
  * @{
*/

#ifndef TAYLOR_HOOD
#define TAYLOR_HOOD

#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include "common_geometry.hpp"
#include "polytopal_element.hpp"
#include "pk_lagrange.hpp"
#include "cell.hpp"

namespace MyDiscreteSpace {

    /**
     * return the list of eliminable dof 
     * in this Taylor-Hood method of order k
     * u is in P_k^d, p in P_{k-1}
     * @param k order of the method 
     * @return list of 2 values : the number of u eliminable dof, the number of p eliminable dof
    */
    template<size_t DIM> 
    static inline Eigen::Array2i n_eliminable_dof_th(int k) {
        Eigen::Array2i n_eliminable_bulk_dof(n_dof_bulk_pk_lagrange<DIM>(std::vector<int> {k, k-1}));

        // all except one of the bulk pressure dof can be eliminated, all velocity bulk dof can be eliminated
        // p must be the second variable
        if(n_eliminable_bulk_dof(1) > 0) { n_eliminable_bulk_dof(1) -= 1; }

        return n_eliminable_bulk_dof;
    } // n_eliminable_dof_th

    /**
     * This class contains the
     * (P_k^d, P_{k-1}) Taylor-Hood method for Stokes. 
     * It inherits of the PolytopalElement class.
     * It must be applied on simplicial meshes only.
     * It is based on the Pk Lagrange method.
    */
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    class TaylorHoodBase : public PolytopalElement<DIM>
    {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param k order of the method
         * @param n_dof_face face number of dof for each variable
         * @param n_dof_edge edge number of dof for each variable
         * @param n_dof_vertex vertex number of dof for each variable
         * @param n_dof_bulk bulk number of dof for each variable
         * @param n_bulk_dof_eliminable bulk eliminable number of dof for each variable 
         */
        TaylorHoodBase(MyMesh::Cell<DIM>* cell, 
                int k,
                const Eigen::ArrayXi& n_dof_face, 
                const Eigen::ArrayXi& n_dof_edge, 
                const Eigen::ArrayXi& n_dof_vertex, 
                const Eigen::ArrayXi& n_dof_bulk, 
                // by default 0 dof are eliminable using static condensation
                const Eigen::ArrayXi& n_bulk_dof_eliminable = 
                    Eigen::ArrayXi::Constant(2, 0)); // Poly_base_type pbt, Quad_type qt, int k, Data dat);
        ~TaylorHoodBase() = default;   ///<  Default destructor

        /**
         * Compute the reconstruction operator
         * @param var_sizes array with the dimension of each variable
         */
        void set_reconstruction_operator(const Eigen::ArrayXi& var_sizes);

        /**
         * Using the stiffness matrix, add
         * k * \int_T grad(r_T u_T) * grad(r_T v_T) dx
         * to the local matrix
         * @param var_index index of the variable to add the stiffness contribution
         * @param mult_cst multiplicative constant
         */
        inline void add_stiffness_contribution(size_t var_index = 0, double mult_cst = 1.);

        /**
         * Using the mass matrix, add
         * k * \int_T r_T u_T * r_T v_T dx
         * to the local matrix
         * @param var_index index of the variable to add the stiffness contribution
         * @param mult_cst multiplicative constant
         */
        inline void add_mass_contribution(size_t var_index = 0, double mult_cst = 1.);

        /**
         * Compute the reduction operator(scalar variable)
         * @param func continuous function from which extract the reduction
         * @param var_index corresponding index of the variable
         * @param only_bound_elemt boolean, if true apply reduction operator only on boundary elements
         */
        Eigen::VectorXd apply_reduction_operator(double func (Eigen::ArrayXd),
                size_t var_index = 0,
                bool only_bound_elemt = false);
        /**
         * Compute the reduction operator (vectorial variable)
         * @param func continuous function to which extract reduction
         * @param var_index corresponding index of the variable
         * @param only_bound_elemt boolean, if true apply reduction operator only on boundary elements
         */
        Eigen::VectorXd apply_reduction_operator(std::vector<double> func (Eigen::ArrayXd),
                size_t var_index = 0,
                bool only_bound_elemt = false);

        /**
         * Add the divergence of the crossed terms
         * k * \int div(u) * q dx
         * and its transpose k * \int p * div(v) dx 
         * to the local matrix, knowing the indexes of the variables u and p
         * @param var_index1 index of u, v
         * @param var_index2 index of p, q
         * @param mesh_measure measure of the global domain
         * @param mult_cst multiplicative constant
         */
        void add_divergence_contribution(
                size_t var_index1, 
                size_t var_index2, 
                double mesh_measure,
                double mult_cst = 1.);

        /**
         * Set the local right-hand-side
         * from a continuous function
         * with specific quadrature order 
         * (6 by default)
         * For a scalar variable or vectorial one
         * @param func continuous function with the rhs
         * @param quadra_order specific quadrature order
         * @param var_index variable index coresponding
         */
        void set_rhs(double func (Eigen::ArrayXd), int quadra_order, size_t var_index);
        void set_rhs(std::vector<double> func (Eigen::ArrayXd), int quadra_order, size_t var_index);

    protected:
        std::vector<Eigen::VectorXd> m_lattice_coords; ///< store the coordinates of the dof (lattice)
    };

    /// definition of the TaylorHood class
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    class TaylorHood : public TaylorHoodBase<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P> {
    public:
        /**
         * Default constructor prints error
         */
        TaylorHood(MyMesh::Cell<DIM>* cell, int k, int ks, const Eigen::ArrayXi& var_sizes);
        ~TaylorHood() = default;   ///<  Default destructor
    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////

    // constructor for TaylorHoodBase (has access to the constructor of PolytopalElement).
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> // CONTINUOUS_SPACES are ordered : u, p
    TaylorHoodBase<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::TaylorHoodBase(
                       MyMesh::Cell<DIM>* cell, 
                       int k,
                       const Eigen::ArrayXi& n_dof_face, 
                       const Eigen::ArrayXi& n_dof_edge, 
                       const Eigen::ArrayXi& n_dof_vertex,
                       const Eigen::ArrayXi& n_dof_bulk, 
                       const Eigen::ArrayXi& n_bulk_dof_eliminable) :
    PolytopalElement<DIM>(cell, n_dof_face, n_dof_edge, n_dof_vertex, 
                       n_dof_bulk, n_bulk_dof_eliminable) {
        this->m_cell_degree = k; // u is in P_k^d, p in P_{k-1}
        // vectorial full basis for the velocity
        MyBasis::Basis< MyMesh::Cell<DIM>* >* cell_basis_u(
            new MyBasis::ScaledMonomialVectorBasis<MyMesh::Cell<DIM>*, MyBasis::FULL>(cell, this->m_cell_degree));
        this->m_cell_bases.push_back(cell_basis_u);
        // scalar basis for the pressure
        MyBasis::Basis< MyMesh::Cell<DIM>* >* cell_basis_p(
            new MyBasis::ScaledMonomialScalarBasis<MyMesh::Cell<DIM>*>(cell, this->m_cell_degree - 1));
        this->m_cell_bases.push_back(cell_basis_p);

        // this FE requires a Lagrange multiplier applied on the pressure (var index is 1)
        this->m_lagrange_multiplier_var_index.push_back(1);
    }

    // constructor for Taylor-Hood with two variables
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    TaylorHood<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::TaylorHood(MyMesh::Cell<DIM>* cell, int k, int ks, const Eigen::ArrayXi& var_sizes) :
    TaylorHoodBase<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>(cell, 
                                        k, // order of the method : Lagrange with u in Pk and p in P(k-1)
                                        var_sizes * n_dof_faces_pk_lagrange<DIM>(std::vector<int> {k, k-1}), // number of dof in each face (vertices and edges excluded)
                                        var_sizes * n_dof_edges_pk_lagrange<DIM>(std::vector<int> {k, k-1}),  // number of dof in each edge (vertices excluded, always 0 in 2D)
                                        var_sizes * 1,  // number of dof in each vertex (always 1 in Lagrange)
                                        var_sizes * n_dof_bulk_pk_lagrange<DIM>(std::vector<int> {k, k-1}),   // number of bulk dof
                                        // all except one of the bulk pressure dof can be eliminated, 
                                        // all velocity bulk dof can be eliminated
                                        var_sizes * n_eliminable_dof_th<DIM>(k)) {    // number of eliminable bulk dof u is first var, p second
        if(ks != k) throw " ks is not equal to k in Taylor Hood ";
        if(k <= 1) throw " the order is not big enough to have stability of the Taylor Hood method ";
    }

    // definition of set_reconstruction_operator :
    // the size of the reconstruction operator is basis_dim * cumulate_local_dof.
    // Even with more than one variable, in this case it has nul columns 
    // coresponding to the local indexes of the other variables.
    // It allows to have generic algo in other parts of the code.
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    void TaylorHoodBase<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::set_reconstruction_operator(
        const Eigen::ArrayXi& var_sizes) {

#ifdef DEBUG
        if(var_sizes.size() != 2) {
            throw " Taylor Hood with other than 2 continuous spaces not implemented ";
        }
#endif
        size_t u_var(0); // u is the first variable in var_sizes
        size_t p_var(1); // p is the second variable in var_sizes
        size_t lagrange_deg_u(this->m_cell_degree);
        size_t lagrange_deg_p(this->m_cell_degree - 1);
        size_t var_size_u(var_sizes[u_var]); // dimension of the velocity variable (should be DIM because u is vectorial)
        size_t var_size_p(var_sizes[p_var]); // dimension of the pressure variable (should be 1 because p is scalar)
        Eigen::ArrayXi array_var_size_u(Eigen::ArrayXi::Constant(1, var_size_u));
        Eigen::ArrayXi array_var_size_p(Eigen::ArrayXi::Constant(1, var_size_p));
        size_t basis_dim_u(this->m_cell_bases[u_var]->dimension()); // dimension of the velocity basis
        size_t basis_dim_p(this->m_cell_bases[p_var]->dimension()); // dimension of the pressure basis

#ifdef DEBUG
        if(var_size_u != DIM || var_sizes[p_var] != 1) {
            throw " this Taylor Hood reconstruction is implemented with u in Rd, p in R; not good array var_sizes ";
        }
#endif

        // Lagrange with u in Pk and p in P(k-1)
        PkLagrange<DIM, CONTINUOUS_SPACE_U> 
            lagrange_u(PkLagrange<DIM, CONTINUOUS_SPACE_U>(this->m_cell, lagrange_deg_u, lagrange_deg_u, array_var_size_u));
        PkLagrange<DIM, CONTINUOUS_SPACE_P> 
            lagrange_p(PkLagrange<DIM, CONTINUOUS_SPACE_P>(this->m_cell, lagrange_deg_p, lagrange_deg_p, array_var_size_p));

        // The reconstruction operator of the Taylor Hood method is
        // based on each Lagrange reconstruction operator of u and p
        // First set the local indexes of all the dof
        lagrange_u.init_dof_local_index();
        lagrange_p.init_dof_local_index();
        // then set the reconstruction operator in each fe using the specified bases 
        // 0 is optional but careful, there is only one variable in each class (cr_u and p0_p)
        lagrange_u.set_reconstruction_operator(var_size_u, this->m_cell_bases[u_var], 0);
        lagrange_p.set_reconstruction_operator(var_size_p, this->m_cell_bases[p_var], 0);


        // --------------------------------------------------------------
        // map the Lagrange method of one variable (lagrange_u or lagrange_p)
        // to a probleme with 2 variables

        // vector containing the list of the FE with one individual variable
        std::vector<PolytopalElement<DIM>*> lagrange_fe({&lagrange_u, &lagrange_p});

        std::vector<Eigen::MatrixXd> reconstruction_operators_individual_var;
        reconstruction_operators_individual_var.push_back(lagrange_u.get_reconstruction_operator());
        reconstruction_operators_individual_var.push_back(lagrange_p.get_reconstruction_operator());

        // map_each_to_n_variables fills the object but it must already have the good size
        this->m_reconstruction_operators.push_back( // fix size for the u variable
                Eigen::MatrixXd::Zero(basis_dim_u, this->m_n_cumulate_local_dof));
        this->m_reconstruction_operators.push_back( // fix size for the p variable
                Eigen::MatrixXd::Zero(basis_dim_p, this->m_n_cumulate_local_dof));

        // Map the square reconstruction operators to the rectangular one
        // to map velocity or pressure contiguous local indexes
        // into there local indexes in the PE class
        // (add null columns)
        this->map_each_to_n_variables(var_sizes, lagrange_fe,
            reconstruction_operators_individual_var,
            this->m_reconstruction_operators);
        
        // idem for the lattice coordinates,
        // store them respectivly with the good dof indexes
        std::vector<std::vector<Eigen::VectorXd>> lattice_coords_indivdual_var;
        lattice_coords_indivdual_var.push_back(lagrange_u.get_lattice_coords());
        lattice_coords_indivdual_var.push_back(lagrange_p.get_lattice_coords());
        // map_each_to_n_variables fill the object but it must already have the good size
        this->m_lattice_coords.resize(this->m_n_cumulate_local_dof);
        // store the lattice coordinates with the local indexes
        // of the Taylor Hood (Pk, P(k-1)) scheme
        this->map_each_to_n_variables(var_sizes, lagrange_fe,
            lattice_coords_indivdual_var, this->m_lattice_coords);

        return;

    } // set_reconstruction_operator (same algo for 2D or 3D)

    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    inline void TaylorHoodBase<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::add_stiffness_contribution(
                    size_t var_index, double mult_cst) {

        return pe_add_stiffness_contribution<DIM>(
            this->m_reconstruction_operators[var_index],
            this->m_stiffness_matrices[var_index],
            this->m_local_bilinear_form,
            mult_cst);

    } // add_stiffness_contribution

    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    inline void TaylorHoodBase<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::add_mass_contribution(
                    size_t var_index, double mult_cst) {

        return pe_add_mass_contribution<DIM>(
            this->m_reconstruction_operators[var_index],
            this->m_mass_matrices[var_index],
            this->m_local_bilinear_form,
            mult_cst);
    } // add_mass_contribution

    // Define the reduction operator : 
    // call the Lagrange one (value at lattice points)
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    Eigen::VectorXd TaylorHoodBase<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::apply_reduction_operator(
                    double func (Eigen::ArrayXd),
                    size_t var_index,
                    bool only_bound_elemt) {

        Eigen::VectorXd reduction_vec(Eigen::VectorXd::Zero(this->m_n_cumulate_local_dof));
        size_t func_size(1); // scalar valued function

        if(!only_bound_elemt) { // all dof are computed
            if(DIM == 2) { // no edges
                lagrange_apply_reduction_operator(
                    this->m_cell,
                    this->m_vertices_dof, this->m_faces_dof, this->m_bulk_dof,
                    var_index, func_size, func, this->m_lattice_coords,
                    reduction_vec);
            } else { // with edges
                lagrange_apply_reduction_operator(
                    this->m_cell,
                    this->m_vertices_dof, this->m_edges_dof, this->m_faces_dof, this->m_bulk_dof,
                    var_index, func_size, func, this->m_lattice_coords,
                    reduction_vec);
            }

        } else if(this->m_cell->is_boundary()) { // need to test if the object is bound, bulk cannot be
            if(DIM == 2) { // no edges
                lagrange_apply_reduction_operator_boundary(
                    this->m_cell,
                    this->m_vertices_dof, this->m_faces_dof,
                    var_index, func_size, func, this->m_lattice_coords,
                    reduction_vec);
            } else { // with edges
                lagrange_apply_reduction_operator_boundary(
                    this->m_cell,
                    this->m_vertices_dof, this->m_edges_dof, this->m_faces_dof,
                    var_index, func_size, func, this->m_lattice_coords,
                    reduction_vec);
            }
        }
        return reduction_vec;
    } // apply_reduction_operator on scalar variable

    // Define the reduction operator : vectorial values at lattice points
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    Eigen::VectorXd TaylorHoodBase<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::apply_reduction_operator(
                    std::vector<double> func (Eigen::ArrayXd),
                    size_t var_index,
                    bool only_bound_elemt) {

        Eigen::VectorXd reduction_vec(Eigen::VectorXd::Zero(this->m_n_cumulate_local_dof));
        // vectorial values so the size is DIM
        size_t func_size(DIM);

        if(!only_bound_elemt) { // all dof are computed
            if(DIM == 2) { // no edges
                lagrange_apply_reduction_operator(
                    this->m_cell,
                    this->m_vertices_dof, this->m_faces_dof, this->m_bulk_dof,
                    var_index, func_size, func, this->m_lattice_coords,
                    reduction_vec);
            } else { // with edges
                lagrange_apply_reduction_operator(
                    this->m_cell,
                    this->m_vertices_dof, this->m_edges_dof, this->m_faces_dof, this->m_bulk_dof,
                    var_index, func_size, func, this->m_lattice_coords,
                    reduction_vec);
            }

        } else if(this->m_cell->is_boundary()) { // need to test if the object is bound, bulk cannot be
            if(DIM == 2) { // no edges
                lagrange_apply_reduction_operator_boundary(
                    this->m_cell,
                    this->m_vertices_dof, this->m_faces_dof,
                    var_index, func_size, func, this->m_lattice_coords,
                    reduction_vec);
            } else { // with edges
                lagrange_apply_reduction_operator_boundary(
                    this->m_cell,
                    this->m_vertices_dof, this->m_edges_dof, this->m_faces_dof,
                    var_index, func_size, func, this->m_lattice_coords,
                    reduction_vec);
            }
        }

        return reduction_vec;
    } // apply_reduction_operator on vectorial variable

    // Add the divergence of the crossed terms
    // k * \int div(u) * q dx
    // and its transpose k * \int p * div(v) dx 
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    void TaylorHoodBase<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::add_divergence_contribution(
                size_t var_index1, 
                size_t var_index2, 
                double mesh_measure,
                double mult_cst) {

        // polynomial cell bases of the two considered variables
        size_t basis_dim1(this->m_cell_bases[var_index1]->dimension()); // phi
        size_t basis_dim2(this->m_cell_bases[var_index2]->dimension()); // psi
        // use a quadrature of order 2 * m_cell_degree - 2
        // because div(u) is of order k-1 and p also
        size_t quadra_order(std::max(0, 2 * this->m_cell_degree - 2));
        MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(this->m_cell, quadra_order));

        // M(i,j) = sum_qp w(qp) * div(phi[j]) * psi[i]
        Eigen::MatrixXd bases_matrix(Eigen::MatrixXd::Zero(basis_dim2, basis_dim1));

        // compute the integral
        for(auto& qp : qps.list()) { 
            // evaluate all basis functions at the quadrature point of the cell
            Eigen::RowVectorXd eval_basis1(
                    this->m_cell_bases[var_index1]->eval_divergences(qp->point())); // div(phi)
            Eigen::RowVectorXd eval_basis2(
                    this->m_cell_bases[var_index2]->eval_functions(qp->point())); // psi

            bases_matrix += qp->weight() * eval_basis2.transpose() * eval_basis1;
        } // quadra

        Eigen::MatrixXd specific_mat(
                this->m_reconstruction_operators[var_index2].transpose() 
                * bases_matrix
                * this->m_reconstruction_operators[var_index1]);

        this->m_local_bilinear_form += mult_cst * (specific_mat + specific_mat.transpose());

        return;
    } // add_divergence_contribution

    // fill the rhs with \int f r_T v_T (FE method)
    // with a specific quadrature order
    // with scalar variable
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    void TaylorHoodBase<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::set_rhs(
                double func (Eigen::ArrayXd),
                int quadra_order,
                size_t var_index) {

        throw " entered in set_rhs with scalar function, should not append in taylor_hood ";

        return;
    } // set_rhs scalar

    // fill the rhs with \int f r_T v_T (FE method)
    // with a specific quadrature order
    // with a vectorial variable
    template<size_t DIM, int CONTINUOUS_SPACE_U, int CONTINUOUS_SPACE_P> 
    void TaylorHoodBase<DIM, CONTINUOUS_SPACE_U, CONTINUOUS_SPACE_P>::set_rhs(
                std::vector<double> func (Eigen::ArrayXd),
                int quadra_order,
                size_t var_index) {

        // In Taylor Hood, var index should correspond to u
        // int_f_basis computes the vector containing  (int f * basis_i)_i 
        // with f vectorial function (one scalar by basis function)
        this->m_local_rhs += this->m_reconstruction_operators[var_index].transpose() 
            * this->int_f_basis(this->m_cell, func, this->m_cell_bases[var_index], quadra_order);

        return;
    } // set_rhs vectorial

} // MyDiscreteSpace namespace
#endif /* TAYLOR_HOOD */
/** @} */
