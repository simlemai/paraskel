#ifndef NORM_HPP
#define NORM_HPP

#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include "discrete_space.hpp"
#include "polytopal_element.hpp"
#include "quadratures.hpp"

namespace MyDiscreteSpace {

    /**
     * convert into Eigen::Vector
     * the data which can be double or already eigen vector
     */
    template<typename RET>
    inline static Eigen::VectorXd to_eigen_vector(RET data);

    /**
     * Use the mass matrix to compute the L2 norm
     * of a function defined by  coordinates.dot(cell_basis)
     * @param pe Polytopal Element (cell) in which the norm is computed
     * @param coordinates coordinates of the function in the cell basis
     * @param var_index index of the variable considered
     */
    template<size_t DIM>
    inline double discrete_l2norm(PolytopalElement<DIM>* pe, Eigen::VectorXd& coordinates, size_t var_index); 

    /**
     * Use the stiffness matrix to compute the H1 semi norm
     * of a function defined by  coordinates.dot(cell_basis)
     * @param pe Polytopal Element (cell) in which the norm is computed
     * @param coordinates coordinates of the function in the cell basis
     * @param var_index index of the variable considered
     */
    template<size_t DIM>
    inline double discrete_h1norm(PolytopalElement<DIM>* pe, Eigen::VectorXd& coordinates, size_t var_index); 

    /**
     * Compute the discrete relative l2 error 
     * over the whole discrete space
     * between the numerical result and the exact
     * solution given by the continuous function func
     * using the canonical interpolation
     * @param ds discrete space
     * @param func continuous exact solution
     * @param var_index index of the variable considered
     */
    template<size_t DIM>
    inline double discrete_relative_l2error(DiscreteSpace<DIM>* ds, double func (Eigen::ArrayXd), size_t var_index = 0);

    /**
     * Compute the discrete relative l2 error over a list of pe
     * between the numerical result and the exact
     * solution given by the continuous function func
     * using the canonical interpolation
     * @param pe_list list of the Polytopal Element instantiations
     * @param func continuous exact solution
     * @param var_index index of the variable considered
     */
    template<size_t DIM>
    inline double discrete_relative_l2error(const std::vector<PolytopalElement<DIM>*>& pe_list, 
                                   double func (Eigen::ArrayXd), size_t var_index = 0);

    /**
     * Compute the discrete relative error (H1 semi norm) 
     * over the whole discrete space
     * between the numerical result and the exact
     * solution given by the continuous function func
     * using the canonical interpolation
     * @param ds discrete space
     * @param func continuous exact solution
     * @param var_index index of the variable considered
     */
    template<size_t DIM>
    inline double discrete_relative_h1error(DiscreteSpace<DIM>* ds, double func (Eigen::ArrayXd), size_t var_index = 0);

    /**
     * Compute the discrete relative error (H1 semi norm) over a list of pe
     * between the numerical result and the exact
     * solution given by the continuous function func
     * using the canonical interpolation
     * @param pe_list list of the Polytopal Element instantiations
     * @param func continuous exact solution
     * @param var_index index of the variable considered
     */
    template<size_t DIM>
    inline double discrete_relative_h1error(const std::vector<PolytopalElement<DIM>*>& pe_list, 
                                   double func (Eigen::ArrayXd), size_t var_index = 0);

    /**
     * Compute the squared l2 norm given a 
     * continuous function (scalar or vectoriel)
     * and a quadrature order
     * @param pe Polytopal Element (cell) in which the norm is computed
     * @param func continuous exact function
     * @param quadra_order order of quadrature
     */
    template<size_t DIM, typename T>
    inline double l2norm2(PolytopalElement<DIM>* pe, 
                         T func (Eigen::ArrayXd),
                         size_t quadra_order);
    /**
     * Compute the l2 norm given a 
     * continuous function (scalar or vectoriel)
     * and a quadrature order
     * @param pe Polytopal Element (cell) in which the norm is computed
     * @param func continuous exact function
     * @param quadra_order order of quadrature
     */
    template<size_t DIM, typename T>
    inline double l2norm(PolytopalElement<DIM>* pe, 
                         T func (Eigen::ArrayXd),
                         size_t quadra_order);

    /**
     * Compute the squared l2 norm given the weights 
     * and the values at quadrature points 
     * which could be a scalar or a vector
     * @param weights weights of the quadrature
     * @param u_qps value at quadrature points
     */
    template<typename T>
    inline double l2norm2(const std::vector<double>& weights,
                         const std::vector<T>& u_qps);
    /**
     * Compute the l2 norm given the weights 
     * and the values at quadrature points 
     * which could be a scalar or a vector
     * @param weights weights of the quadrature
     * @param u_qps value at quadrature points
     */
    template<typename T>
    inline double l2norm(const std::vector<double>& weights,
                         const std::vector<T>& u_qps);

    /**
     * Compute the relative l2 error over the whole discrete space
     * between the numerical result and the exact
     * solution given by the continuous function func
     * with a given order of quadrature
     * @param ds discrete space
     * @param func continuous exact solution of the variable considered (can return a scalar or std::vector)
     * @param quadra_order order of quadrature
     * @param var_index index of the variable considered
     */
    template<size_t DIM, typename RET>
    inline double relative_l2error(DiscreteSpace<DIM>* ds, 
                RET func (Eigen::ArrayXd),
                size_t quadra_order = 10,
                size_t var_index = 0);

    /**
     * Compute the relative l2 error over a list of pe
     * between the numerical result and the exact
     * solution given by the continuous function func
     * with a given order of quadrature
     * @param pe_list list of the Polytopal Element instantiations
     * @param func continuous exact solution of the variable considered (can return a scalar or std::vector)
     * @param quadra_order order of quadrature
     * @param var_index index of the variable considered
     */
    template<size_t DIM, typename RET>
    inline double relative_l2error(const std::vector<PolytopalElement<DIM> *>& pe_list, 
                RET func (Eigen::ArrayXd),
                size_t quadra_order = 10,
                size_t var_index = 0);

    /**
     * Compute the relative l2 error using the bulk approximation
     * over the whole discrete space
     * between the numerical result and the l2 projection of
     * the exact solution computed from the continuous function func
     * with a given order of quadrature
     * @param ds discrete space
     * @param func continuous exact solution of the variable considered (can return a scalar or std::vector)
     * @param quadra_order order of quadrature
     * @param var_index index of the variable considered
     */
    template<size_t DIM, typename RET>
    inline double relative_bulk_l2error(DiscreteSpace<DIM>* ds, 
                RET func (Eigen::ArrayXd),
                size_t quadra_order = 10,
                size_t var_index = 0);

    /**
     * Compute the relative l2 error using the bulk approximation
     * over a list of pe
     * between the numerical result and the l2 projection of
     * the exact solution computed from the continuous function func
     * with a given order of quadrature
     * @param pe_list list of the Polytopal Element instantiations
     * @param func continuous exact solution of the variable considered (can return a scalar or std::vector)
     * @param quadra_order order of quadrature
     * @param var_index index of the variable considered
     */
    template<size_t DIM, typename RET>
    inline double relative_bulk_l2error(const std::vector<PolytopalElement<DIM> *>& pe_list, 
                RET func (Eigen::ArrayXd),
                size_t quadra_order = 10,
                size_t var_index = 0);

    /**
     * Compute the relative error (H1 semi norm)
     * over the whole discrete space
     * between the numerical result and the exact
     * solution given by the continuous function func
     * with a given order of quadrature
     * @param ds discrete space
     * @param grad_func gradient of the continuous exact solution of the variable considered (can be eigen vector or eigen matrix)
     * @param quadra_order order of quadrature
     * @param var_index index of the variable considered
     */
    template<size_t DIM, typename RET>
    inline double relative_h1error(DiscreteSpace<DIM>* ds, 
                RET grad_func (Eigen::ArrayXd),
                size_t quadra_order = 10,
                size_t var_index = 0);

    /**
     * Compute the relative error (H1 semi norm) over a list of pe
     * between the numerical result and the exact
     * solution given by the continuous function func
     * with a given order of quadrature
     * @param pe_list list of the Polytopal Element instantiations
     * @param grad_func gradient of the continuous exact solution of the variable considered (can be eigen vector or eigen matrix)
     * @param quadra_order order of quadrature
     * @param var_index index of the variable considered
     */
    template<size_t DIM, typename RET>
    inline double relative_h1error(const std::vector<PolytopalElement<DIM> *>& pe_list, 
                RET grad_func (Eigen::ArrayXd),
                size_t quadra_order = 10,
                size_t var_index = 0);


    /**
     * Compute the absolute l2 error over the whole discrete space
     * between the numerical result and the exact
     * solution given by the continuous function func
     * with a given order of quadrature
     * @param ds discrete space
     * @param func continuous exact solution of the variable considered (can return a scalar or std::vector)
     * @param quadra_order order of quadrature
     * @param var_index index of the variable considered
     */
    template<size_t DIM, typename RET>
    inline double absolute_l2error(DiscreteSpace<DIM>* ds, 
                RET func (Eigen::ArrayXd),
                size_t quadra_order = 10,
                size_t var_index = 0);

    /**
     * Compute the absolute l2 error over a list of pe
     * between the numerical result and the exact
     * solution given by the continuous function func
     * with a given order of quadrature
     * @param pe_list list of the Polytopal Element instantiations
     * @param func continuous exact solution of the variable considered (can return a scalar or std::vector)
     * @param quadra_order order of quadrature
     * @param var_index index of the variable considered
     */
    template<size_t DIM, typename RET>
    inline double absolute_l2error(const std::vector<PolytopalElement<DIM> *>& pe_list, 
                RET func (Eigen::ArrayXd),
                size_t quadra_order = 10,
                size_t var_index = 0);

    /**
     * Compute the absolute error (H1 semi norm)
     * over the whole discrete space
     * between the numerical result and the exact
     * solution given by the continuous function func
     * with a given order of quadrature
     * @param ds discrete space
     * @param grad_func gradient of the continuous exact solution of the variable considered (can be eigen vector or eigen matrix)
     * @param quadra_order order of quadrature
     * @param var_index index of the variable considered
     */
    template<size_t DIM, typename RET>
    inline double absolute_h1error(DiscreteSpace<DIM>* ds, 
                RET grad_func (Eigen::ArrayXd),
                size_t quadra_order = 10,
                size_t var_index = 0);

    /**
     * Compute the absolute error (H1 semi norm) over a list of pe
     * between the numerical result and the exact
     * solution given by the continuous function func
     * with a given order of quadrature
     * @param pe_list list of the Polytopal Element instantiations
     * @param grad_func gradient of the continuous exact solution of the variable considered (can be eigen vector or eigen matrix)
     * @param quadra_order order of quadrature
     * @param var_index index of the variable considered
     */
    template<size_t DIM, typename RET>
    inline double absolute_h1error(const std::vector<PolytopalElement<DIM> *>& pe_list, 
                RET grad_func (Eigen::ArrayXd),
                size_t quadra_order = 10,
                size_t var_index = 0);


    /////////////////////////////////////////////////////////////////////////
    //                   Implementations
    /////////////////////////////////////////////////////////////////////////
    template<typename RET>
    inline static Eigen::VectorXd to_eigen_vector(RET data) {
        throw " type not recognized ";
        return Eigen::VectorXd(0);
    }

    inline static Eigen::VectorXd to_eigen_vector(double data) {
        return Eigen::VectorXd::Constant(1, data);
    }

    inline static Eigen::VectorXd to_eigen_vector(std::vector<double> data) {
        // cast into eigen vector
        Eigen::Map<Eigen::VectorXd> ret(data.data(), data.size());
        return ret;
    }

    // compute the L2 norm using the mass matrix
    template<size_t DIM>
    inline double discrete_l2norm(PolytopalElement<DIM> * pe, Eigen::VectorXd& coordinates,
                            size_t var_index) {

        // computes the norm
        return std::sqrt( coordinates.transpose() * pe->get_mass_matrices()[var_index] * coordinates );
    } // discrete_l2norm
    
    // compute the H1 semi norm using the stiffness matrix
    template<size_t DIM>
    inline double discrete_h1norm(PolytopalElement<DIM> * pe, Eigen::VectorXd& coordinates,
                            size_t var_index) {

        // computes the norm
        return std::sqrt( coordinates.transpose() * pe->get_stiffness_matrices()[var_index] * coordinates );
    } // discrete_h1norm
    

    template<size_t DIM>
    inline double discrete_relative_l2error(const std::vector<PolytopalElement<DIM> *>& pe_list, 
                                   double func (Eigen::ArrayXd), size_t var_index) {

        double diff_norm(0);
        double exact_norm(0);
        for (auto& pe : pe_list) {

            Eigen::VectorXd reducted_exact_sol(pe->apply_reduction_operator(func, var_index));
            Eigen::VectorXd reconstructed_exact_sol(pe->get_reconstruction_operator(var_index) * reducted_exact_sol);
            Eigen::VectorXd diff(pe->get_reconstructed_solution(var_index) - reconstructed_exact_sol);

            diff_norm += std::pow(discrete_l2norm(pe, diff, var_index), 2.);
            exact_norm += std::pow(discrete_l2norm(pe, reconstructed_exact_sol, var_index), 2.);
        }

        return std::sqrt(diff_norm) / std::sqrt(exact_norm);
    } // discrete_relative_l2error
    
    template<size_t DIM>
    inline double discrete_relative_l2error(DiscreteSpace<DIM>* ds, double func (Eigen::ArrayXd),
                                    size_t var_index) {

        return discrete_relative_l2error(ds->get_polytopal_elements(), func, var_index);
    } // discrete_relative_l2error

    template<size_t DIM>
    inline double discrete_relative_h1error(const std::vector<PolytopalElement<DIM> *>& pe_list, 
                                   double func (Eigen::ArrayXd), size_t var_index) {

        double diff_norm(0);
        double exact_norm(0);
        for (auto& pe : pe_list) {

            Eigen::VectorXd reducted_exact_sol(pe->apply_reduction_operator(func, var_index));
            Eigen::VectorXd reconstructed_exact_sol(pe->get_reconstruction_operator(var_index) * reducted_exact_sol);
            Eigen::VectorXd diff(pe->get_reconstructed_solution(var_index) - reconstructed_exact_sol);

            diff_norm += std::pow(discrete_h1norm(pe, diff, var_index), 2.);
            exact_norm += std::pow(discrete_h1norm(pe, reconstructed_exact_sol, var_index),2.);
        }

        return std::sqrt(diff_norm) / std::sqrt(exact_norm);
    } // discrete_relative_h1error
    
    template<size_t DIM>
    inline double discrete_relative_h1error(DiscreteSpace<DIM>* ds, double func (Eigen::ArrayXd),
                                    size_t var_index) {

        return discrete_relative_h1error(ds->get_polytopal_elements(), func, var_index);
    } // discrete_relative_h1error


    template<size_t DIM, typename T>
    inline double l2norm2(PolytopalElement<DIM>* pe, 
            T func (Eigen::ArrayXd),
            size_t quadra_order) {

        double norm2(0);
        // construct quadrature points of given quadra order
        MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(pe->get_cell(), quadra_order));

        for(auto& qp : qps.list()) { 
            norm2 += qp->weight() * std::pow(func(qp->point()), 2.);
        }

        return norm2;
    } // l2norm2

    template<size_t DIM, typename T>
    inline double l2norm(PolytopalElement<DIM>* pe, 
                         T func (Eigen::ArrayXd),
                         size_t quadra_order) {

        return std::sqrt(l2norm2(pe, func, quadra_order));
    } // l2norm

    // specialization for eigen object l2 norm
    inline double l2norm2(const std::vector<double>& weights,
                         const std::vector<Eigen::MatrixXd>& u_qps) {

        double norm2(0);
        for(size_t i = 0, size = weights.size(); i < size; i++) {
            norm2 += weights[i] * u_qps[i].squaredNorm();
        }

        return norm2;
    } // l2norm2

    // specialization for eigen object l2 norm
    inline double l2norm(const std::vector<double>& weights,
                         const std::vector<Eigen::MatrixXd>& u_qps) {

        return std::sqrt(l2norm2(weights, u_qps));
    } // l2norm


    template<size_t DIM, typename RET>
    inline double relative_l2error(const std::vector<PolytopalElement<DIM> *>& pe_list, 
                RET func (Eigen::ArrayXd),
                size_t quadra_order,
                size_t var_index) {

        double diff_norm(0);
        double exact_norm(0);

        for (auto& pe : pe_list) {
            // construct quadrature points of given quadra order
            MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(pe->get_cell(), quadra_order));
            // get the polynomial basis of the cell
            const MyBasis::Basis<MyMesh::Cell<DIM>*>* basis(pe->get_cell_bases(var_index));

            // store the numerical solution in basis coordinates
            Eigen::VectorXd num_sol(pe->get_reconstructed_solution(var_index));
            
            // store the quadra weights and diff values at quadra points
            // to compute the L2 norm
            std::vector<double> qps_w;
            std::vector<Eigen::MatrixXd> diff_qps, exact_f_qps;
            qps_w.reserve(qps.size());
            diff_qps.reserve(qps.size());
            exact_f_qps.reserve(qps.size());

            for(auto& qp : qps.list()) { 
                qps_w.push_back(qp->weight());

                Eigen::MatrixXd eval_basis(basis->eval_functions(qp->point())); 
                // compute func(quadra_point) and cast it to eigen vector
                Eigen::VectorXd f(to_eigen_vector(func(qp->point())));
                // diff between numerical result and evaluation of f
                diff_qps.push_back(eval_basis * num_sol - f);
                exact_f_qps.push_back(f);
            }

            diff_norm += l2norm2(qps_w, diff_qps);
            exact_norm += l2norm2(qps_w, exact_f_qps);
        }

        return std::sqrt(diff_norm) / std::sqrt(exact_norm);
    } // relative_l2error, scal or vect value

    template<size_t DIM, typename RET>
    inline double relative_l2error(DiscreteSpace<DIM>* ds, 
                RET func (Eigen::ArrayXd),
                size_t quadra_order,
                size_t var_index) {

        return relative_l2error(ds->get_polytopal_elements(), func, quadra_order, var_index);
    } // relative_l2error


    template<size_t DIM, typename RET>
    inline double relative_bulk_l2error(const std::vector<PolytopalElement<DIM> *>& pe_list, 
                RET func (Eigen::ArrayXd),
                size_t quadra_order,
                size_t var_index) {

        double diff_norm(0);
        double l2proj_f_norm(0);

        for (auto& pe : pe_list) {
            // get the polynomial basis corresponding to the bulk dof
            auto basis(pe->get_bulk_bases(var_index));

            // store the numerical solution in basis coordinates
            auto num_sol_at_bulk_dof(pe->get_bulk_sol_values(var_index));
            // l2 projection over the bulk of func
            auto l2proj_f(pe->orth_l2projection(pe->get_cell(), basis, func));
            // diff between the 2
            auto diff(num_sol_at_bulk_dof - l2proj_f);

            // construct quadrature points of given quadra order
            MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(pe->get_cell(), quadra_order));
            
            // store the quadra weights and diff values at quadra points
            // to compute the L2 norm
            std::vector<double> qps_w;
            std::vector<Eigen::MatrixXd> diff_qps, l2proj_f_qps;
            qps_w.reserve(qps.size());
            diff_qps.reserve(qps.size());
            l2proj_f_qps.reserve(qps.size());

            for(auto& qp : qps.list()) { 
                qps_w.push_back(qp->weight());

                auto eval_basis(basis->eval_functions(qp->point())); 
                // diff between numerical result and l2 projection of f
                diff_qps.push_back(eval_basis * diff);
                l2proj_f_qps.push_back(eval_basis * l2proj_f);
            }

            diff_norm += l2norm2(qps_w, diff_qps);
            l2proj_f_norm += l2norm2(qps_w, l2proj_f_qps);
        }

        return std::sqrt(diff_norm) / std::sqrt(l2proj_f_norm);
    } // relative_bulk_l2error, scal or vect value

    template<size_t DIM, typename RET>
    inline double relative_bulk_l2error(DiscreteSpace<DIM>* ds, 
                RET func (Eigen::ArrayXd),
                size_t quadra_order,
                size_t var_index) {

        return relative_bulk_l2error(ds->get_polytopal_elements(), func, quadra_order, var_index);
    } // relative_bulk_l2error


    template<size_t DIM, typename RET>
    inline double relative_h1error(const std::vector<PolytopalElement<DIM> *>& pe_list, 
                RET grad_func (Eigen::ArrayXd),
                size_t quadra_order,
                size_t var_index) {

        double diff_norm(0);
        double exact_norm(0);

        for (auto& pe : pe_list) {
            // construct quadrature points of given quadra order
            MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(pe->get_cell(), quadra_order));
            // get the polynomial basis of the cell
            const MyBasis::Basis<MyMesh::Cell<DIM>*>* basis(pe->get_cell_bases(var_index));

            // store the numerical solution in basis coordinates
            Eigen::VectorXd num_sol(pe->get_reconstructed_solution(var_index));

            // store the quadra weights and diff values at quadra points
            // to compute the L2 vectorial norm
            std::vector<double> qps_w;
            std::vector<Eigen::MatrixXd> diff_qps, exact_f_qps;
            qps_w.reserve(qps.size());
            diff_qps.reserve(qps.size());
            exact_f_qps.reserve(qps.size());

            for(auto& qp : qps.list()) { 
                std::vector<Eigen::MatrixXd> grads_functions(basis->eval_gradients(qp->point()));
                qps_w.push_back(qp->weight());
                // compute the numerical gradients (grads_functions[0].rows() gives the size of the variable : scalar or vectorial)
                Eigen::MatrixXd num_grads(Eigen::MatrixXd::Zero(grads_functions[0].rows(), DIM));
                size_t i(0);
                for(auto& grad : grads_functions) {
                    num_grads += num_sol[i++] * grad;
                }

                diff_qps.push_back(num_grads - grad_func(qp->point()));
                exact_f_qps.push_back(grad_func(qp->point()));
            }

            diff_norm += l2norm2(qps_w, diff_qps);
            exact_norm += l2norm2(qps_w, exact_f_qps);
        }

        return std::sqrt(diff_norm) / std::sqrt(exact_norm);
    } // relative_h1error

    template<size_t DIM, typename RET>
    inline double relative_h1error(DiscreteSpace<DIM>* ds, 
                RET grad_func (Eigen::ArrayXd),
                size_t quadra_order,
                size_t var_index) {

        return relative_h1error(ds->get_polytopal_elements(), grad_func, quadra_order, var_index);
    } // relative_h1error


    template<size_t DIM, typename RET>
    inline double absolute_l2error(const std::vector<PolytopalElement<DIM> *>& pe_list, 
                RET func (Eigen::ArrayXd),
                size_t quadra_order,
                size_t var_index) {

        double diff_norm(0);

        for (auto& pe : pe_list) {
            // construct quadrature points of given quadra order
            MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(pe->get_cell(), quadra_order));
            // get the polynomial basis of the cell
            const MyBasis::Basis<MyMesh::Cell<DIM>*>* basis(pe->get_cell_bases(var_index));

            // store the numerical solution in basis coordinates
            Eigen::VectorXd num_sol(pe->get_reconstructed_solution(var_index));
            
            // store the quadra weights and diff values at quadra points
            // to compute the L2 norm
            std::vector<double> qps_w;
            std::vector<Eigen::MatrixXd> diff_qps;
            qps_w.reserve(qps.size());
            diff_qps.reserve(qps.size());

            for(auto& qp : qps.list()) { 
                qps_w.push_back(qp->weight());

                Eigen::MatrixXd eval_basis(basis->eval_functions(qp->point())); 
                // compute func(quadra_point) and cast it to eigen vector
                Eigen::VectorXd f(to_eigen_vector(func(qp->point())));
                // diff between numerical result and evaluation of f
                diff_qps.push_back(eval_basis * num_sol - f);
            }

            diff_norm += l2norm2(qps_w, diff_qps);
        }

        return std::sqrt(diff_norm);
    } // absolute_l2error, scal or vect value

    template<size_t DIM, typename RET>
    inline double absolute_l2error(DiscreteSpace<DIM>* ds, 
                RET func (Eigen::ArrayXd),
                size_t quadra_order,
                size_t var_index) {

        return absolute_l2error(ds->get_polytopal_elements(), func, quadra_order, var_index);
    } // absolute_l2error


    template<size_t DIM, typename RET>
    inline double absolute_h1error(const std::vector<PolytopalElement<DIM> *>& pe_list, 
                RET grad_func (Eigen::ArrayXd),
                size_t quadra_order,
                size_t var_index) {

        double diff_norm(0);

        for (auto& pe : pe_list) {
            // construct quadrature points of given quadra order
            MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(pe->get_cell(), quadra_order));
            // get the polynomial basis of the cell
            const MyBasis::Basis<MyMesh::Cell<DIM>*>* basis(pe->get_cell_bases(var_index));

            // store the numerical solution in basis coordinates
            Eigen::VectorXd num_sol(pe->get_reconstructed_solution(var_index));

            // store the quadra weights and diff values at quadra points
            // to compute the L2 vectorial norm
            std::vector<double> qps_w;
            std::vector<Eigen::MatrixXd> diff_qps;
            qps_w.reserve(qps.size());
            diff_qps.reserve(qps.size());

            for(auto& qp : qps.list()) { 
                std::vector<Eigen::MatrixXd> grads_functions(basis->eval_gradients(qp->point()));
                qps_w.push_back(qp->weight());
                // compute the numerical gradients (grads_functions[0].rows() gives the size of the variable : scalar or vectorial)
                Eigen::MatrixXd num_grads(Eigen::MatrixXd::Zero(grads_functions[0].rows(), DIM));
                size_t i(0);
                for(auto& grad : grads_functions) {
                    num_grads += num_sol[i++] * grad;
                }

                diff_qps.push_back(num_grads - grad_func(qp->point()));
            }

            diff_norm += l2norm2(qps_w, diff_qps);
        }

        return std::sqrt(diff_norm);
    } // absolute_h1error

    template<size_t DIM, typename RET>
    inline double absolute_h1error(DiscreteSpace<DIM>* ds, 
                RET grad_func (Eigen::ArrayXd),
                size_t quadra_order,
                size_t var_index) {

        return absolute_h1error(ds->get_polytopal_elements(), grad_func, quadra_order, var_index);
    } // absolute_h1error

} // MyDiscreteSpace namespace
#endif /* NORM_HPP */
