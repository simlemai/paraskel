#ifndef MATRIX_PATTERN_HPP
#define MATRIX_PATTERN_HPP

#include <iostream>
#include <vector>
#include <string>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <time.h>
#include "discrete_space.hpp"
#include "polytopal_element.hpp"
#include "mesh.hpp"

namespace MySolver {
    /**
     * The class MatrixPattern determines the global
     * index of all the degrees of freedom.
     * It also creates and stores 
     * the local to global operator.
    */
    template<size_t DIM> 
    class MatrixPattern
    {
    public:
        /**
         * Constructor
         * @param mesh pointer to the mesh
         * @param ds discrete space containing all the polytopal element instantiations
         * @param essentialBC_elimination boolean which defines if the essential BC are eliminated or not
         * @param naturalBC_elimination   boolean which defines if the natural BC are eliminated or not
         */
        MatrixPattern(MyMesh::Mesh<DIM>* mesh, const MyDiscreteSpace::DiscreteSpace<DIM>* ds,
            bool static_condensation = true,
            bool essentialBC_elimination = true, bool naturalBC_elimination = false);
        ~MatrixPattern() = default;   ///<  Default destructor

        /**
         * Assemble local contributions of all the polytopal elements of ds
         * into global matrix and global RHS
         * @param mesh pointer to the mesh
         * @param ds discrete space containing all the polytopal element instantiations
         */
        void assemble_local_contributions(const MyMesh::Mesh<DIM>* mesh,
                        MyDiscreteSpace::DiscreteSpace<DIM>* ds);
                        
        inline bool static_cond() const;      ///< static cond is used or not ?
        Eigen::SparseMatrix<double, Eigen::ColMajor>& global_matrix();  ///<  gives pointer to m_global_matrix
        Eigen::VectorXd& global_rhs();    ///<  gives pointer to m_global_rhs

        std::vector<std::vector<size_t>>& get_local_to_global_operator();  ///<  returns the local to global matrix
        
        inline double get_init_time() const; // with the global dof numbering and local_to_global mapping
        inline double get_assemble_time() const;

    private:
        /// do we use static condensation to reduce the size of the system ?
        bool m_static_cond;
        /// If true, there is no bulk dof (or because of the method, 
        // or because they are eliminated by static condensation
        bool m_no_bulk_dof;
        /// do we eliminate the natural BC unknowns to reduce the size of the system ?
        bool m_natural_bc_elimination;
        /// do we eliminate the essential BC unknowns to reduce the size of the system ?
        bool m_essential_bc_elimination;
        /// total number of dof in the global pb (size of matrix so miss the eliminated dof)
        size_t m_n_cumulate_dof;
        /// local (index in each polytopal element class) to global (index in the matrix) operator 
        std::vector<std::vector<size_t>> m_local_to_global_operator;
        /// Global Matrix
        Eigen::SparseMatrix<double, Eigen::ColMajor> m_global_matrix; // should be ColMajor for SparseLU
        /// Global right hand side
        Eigen::VectorXd m_global_rhs;

        /// Timers
        double m_init_time;
        double m_assemble_time;

        /**
         * For each element (vertices, faces,...), 
         * fill the bool "is_eliminated"
         * depending on the BC, ...
         * Carreful : does not depends on the static condensation !
         * @param mesh pointer to the mesh
         */
        void set_eliminated_unknowns(
            MyMesh::Mesh<DIM>* mesh, const MyDiscreteSpace::DiscreteSpace<DIM>* ds);
        template<typename ELEMT>
        inline void set_eliminated_unknowns_by_elements(
            size_t n_variables, const std::vector<ELEMT>& elements);

        /**
         * Give global index (of the dof in the global matrix) in each PE
         * @param mesh pointer to the mesh
         * @param ds discrete space containing all the polytopal element instantiations
         */
        inline void set_global_index(
            MyMesh::Mesh<DIM>* mesh, const MyDiscreteSpace::DiscreteSpace<DIM>* ds);

        /**
         * Init the global index of all geometrical elements to -1 
         * (they have no global indexes yet)
         * @param mesh mesh to extract the elements (cells, faces, ...)
         * @param n_var number of variables
         */
        inline void init_discrete_unknown_global_index(
            MyMesh::Mesh<DIM>* mesh, size_t n_var);

        /**
         * Init the global index of a list of elements to -1 (they have no global indexes yet)
         * @param elements list of elements (cells, faces, ...)
         * @param init vector of size n_variables and values -1
         */
        template<typename ELEMT>
        inline void init_discrete_unknown_global_index_elements(
            const std::vector<ELEMT>& elements, const std::vector<int>& init);

        /**
         * Shift the global index of all geometrical elements for a variable
         * @param mesh mesh to extract the elements (cells, faces, ...)
         * @param var_index index of the variable
         * @param shift shift to apply
         */
        inline void shift_discrete_unknown_global_index(
            MyMesh::Mesh<DIM>* mesh, size_t var_index, size_t shift);

        /**
         * Shift the global index of a list of elements for a variable
         * @param elements list of elements (cells, faces, ...)
         * @param var_index index of the variable
         * @param shift shift to apply
         */
        template<typename ELEMT>
        inline void shift_discrete_unknown_global_index_elements(
            const std::vector<ELEMT>& elements, size_t var_index, size_t shift);

        /**
         * For one cell, give global index of the skeletal dof in each PE
         * @param discr_unknown_gi 
         * @param cell_pt cell pointer
         * @param pe polytopal element instantiation of the cell
         */
        inline void set_global_index_one_cell(
            std::vector<size_t>& discr_unknown_gi,
            const MyMesh::Cell<DIM>* cell_pt, 
            const MyDiscreteSpace::PolytopalElement<DIM>* pe);

        /**
         * Does the loop over the skeletal elements (vertices, faces, ...)
         * Computes the global indexes of the dof in each PE
         * @param flag (void if interior cell, char if boundary cell)
         * @param discr_unknown_gi 
         * @param cell_pt cell pointer
         * @param pe polytopal element instantiation of the cell
         */
        template<typename T>
        inline void set_global_index_all_skeletal(
            const T flag,
            std::vector<size_t>& discr_unknown_gi,
            const MyMesh::Cell<DIM>* cell_pt, 
            const MyDiscreteSpace::PolytopalElement<DIM>* pe);

        /**
         * Is called for each type of skeletal elements (vertices, faces, ...)
         * Computes the global indexes of the dof in each PE
         * if the element does not belong to a boundary cell
         * @param flag 
         * @param discr_unknown_gi 
         * @param elements
         * @param elemts_local_index
         */
        template<typename ELEMT>
        inline void set_global_index_by_elements(
            bool flag,
            std::vector<size_t>& discr_unknown_gi,
            const std::vector<ELEMT>& elements, 
            const std::vector<std::vector<std::vector<size_t>>>& elemts_local_index);

        /**
         * Is called for each type of skeletal elements (vertices, faces, ...)
         * Computes the global indexes of the dof in each PE
         * if the element belongs to a boundary cell
         * @param flag 
         * @param discr_unknown_gi 
         * @param elements
         * @param elemts_local_index
         */
        template<typename ELEMT>
        inline void set_global_index_by_elements(
            const std::string flag,
            std::vector<size_t>& discr_unknown_gi,
            const std::vector<ELEMT>& elements, 
            const std::vector<std::vector<std::vector<size_t>>>& elemts_local_index);

        /**
         * Is called for each skeletal element which has no eliminated variable
         * Computes the global indexes of the dof in each PE
         * @param discr_unknown_gi 
         * @param element
         * @param l_elm
         * @param n_var number of variables in the problem
         * @param elemts_local_index
         */
        template<typename ELEMT>
        inline void set_global_index_no_eliminated_var(
            std::vector<size_t>& discr_unknown_gi,
            const ELEMT& element, 
            size_t l_elm,
            size_t n_var,
            const std::vector<std::vector<std::vector<size_t>>>& elemts_local_index);

        /**
         * Creates the mapping between the global index and the local index in each PE
         * @param mesh pointer to the mesh
         * @param ds discrete space containing all the polytopal element instantiations
         */
        inline void set_local_to_global_operator(
            MyMesh::Mesh<DIM>* mesh, const MyDiscreteSpace::DiscreteSpace<DIM>* ds);
    
        /**
         * For one cell, map the local to global index of the skeletal dof
         * @param cell_gi 
         * @param cell_pt cell pointer
         * @param pe polytopal element instantiation of the cell
         */
        inline void set_local_to_global_operator_one_cell(
            size_t cell_gi,
            const MyMesh::Cell<DIM>* cell_pt, 
            const MyDiscreteSpace::PolytopalElement<DIM>* pe);

        /**
         * Does the loop over the skeletal elements (vertices, faces, ...)
         * Map the local to the global indexes of the dof in each PE
         * @param flag (bool if interior cell, char if boundary cell)
         * @param cell_gi 
         * @param cell_pt cell pointer
         * @param pe polytopal element instantiation of the cell
         */
        template<typename T>
        inline void set_local_to_global_operator_all_skeletal(
            const T flag,
            size_t cell_gi,
            const MyMesh::Cell<DIM>* cell_pt, 
            const MyDiscreteSpace::PolytopalElement<DIM>* pe);

        /**
         * Is called for each type of skeletal elements (vertices, faces, ...)
         * Map the local to the global indexes of the dof in each PE
         * if the element does not belong to a boundary cell
         * @param flag 
         * @param cell_gi 
         * @param elements
         * @param elemts_local_index
         */
        template<typename ELEMT>
        inline void set_local_to_global_operator_by_elements(
            bool flag,
            size_t cell_gi,
            const std::vector<ELEMT>& elements, 
            const std::vector<std::vector<std::vector<size_t>>>& elemts_local_index);

        /**
         * Is called for each type of skeletal elements (vertices, faces, ...)
         * Map the local to the global indexes of the dof in each PE
         * if the element belongs to a boundary cell
         * @param flag 
         * @param cell_gi 
         * @param elements
         * @param elemts_local_index
         */
        template<typename ELEMT>
        inline void set_local_to_global_operator_by_elements(
            const std::string flag,
            size_t cell_gi,
            const std::vector<ELEMT>& elements, 
            const std::vector<std::vector<std::vector<size_t>>>& elemts_local_index);

        /**
         * Fill essential_dof and essential_dof_gi with the info
         * of the dof which are ESSENTIAL BC
         * Loops over an elements list (vertices or faces) of a boundary cell
         * @param cell_gi cell global index 
         * @param elements list of elements 
         * @param n_variables nb of variables in the model
         * @param dof_local_index_by_elements first and last local index of the dof in the elements depending on the variable
         * @param essential_dof list of size n_dof with boolean (true if the dof is ESSENTIAL)
         * @param essential_dof_gi list of global index of all the essential dof
         */
        template<typename ELEMT>
        inline void set_essential_dof_by_elements(
            const size_t cell_gi,
            const std::vector<ELEMT>& elements, 
            const size_t n_variables,
            const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_by_elements,
            std::vector<bool>& essential_dof, 
            std::vector<size_t>& essential_dof_gi);

    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////
    template<size_t DIM> 
    MatrixPattern<DIM>::MatrixPattern(
                    MyMesh::Mesh<DIM>* mesh, const MyDiscreteSpace::DiscreteSpace<DIM>* ds,
                    bool static_condensation,
                    bool essentialBC_elimination, 
                    bool naturalBC_elimination) :
    m_n_cumulate_dof(0),
    m_static_cond(static_condensation),
    m_no_bulk_dof(ds->no_bulk_dof() || ( m_static_cond && ds->are_bulk_dof_eliminable() )),
    m_natural_bc_elimination(naturalBC_elimination),
    m_essential_bc_elimination(essentialBC_elimination),
    m_init_time(0.0),
    m_assemble_time(0.0)
    {
        // timer
        clock_t t;
        t = clock();

        // initialize m_local_to_global_operator with the good size and at value SIZE_MAX
        for(auto& cpt : mesh->get_cells()) {
            MyDiscreteSpace::PolytopalElement<DIM>* pe(ds->polytopal_element(cpt->global_index()));
            // m_n_cumulate_local_dof contains the number of dof before any elimination
            size_t loc2glo_size(pe->n_cumulate_local_dof());
            // Careful : if there is no bulk dof, local_index(cell) = n_cumulate_local_dof,
            // and it is necessary to have loc2glo_size > any local_index
            if(pe->n_bulk_dof().sum()==0) loc2glo_size++;

            std::vector<size_t> cell_elemts(loc2glo_size, SIZE_MAX);
            m_local_to_global_operator.push_back(cell_elemts);
        }
        // set the boolean vector is_eliminated for each BC element and for each variable
        set_eliminated_unknowns(mesh, ds);
        // set the global index and the local_to_global operator
        set_global_index(mesh, ds);
        set_local_to_global_operator(mesh, ds);

        t = clock() - t;
        m_init_time = ((double)t)/CLOCKS_PER_SEC;

    }

    template<size_t DIM> 
    std::vector<std::vector<size_t>>& MatrixPattern<DIM>::get_local_to_global_operator() {
        return m_local_to_global_operator;
    }

    template<size_t DIM> 
    Eigen::SparseMatrix<double, Eigen::ColMajor>& MatrixPattern<DIM>::global_matrix() { return m_global_matrix; }

    template<size_t DIM> 
    Eigen::VectorXd& MatrixPattern<DIM>::global_rhs() { return m_global_rhs; }

    template<size_t DIM> 
    inline bool MatrixPattern<DIM>::static_cond() const { return m_static_cond; }

    template<size_t DIM> 
    template<typename ELEMT>
    inline void MatrixPattern<DIM>::set_essential_dof_by_elements(
        const size_t cell_gi,
        const std::vector<ELEMT>& elements, 
        const size_t n_variables,
        const std::vector<std::vector<std::vector<size_t>>>& dof_local_index_by_elements,
        std::vector<bool>& essential_dof, 
        std::vector<size_t>& essential_dof_gi) {

        size_t e_li(0); // local index in the list of elements of the cell
        for(auto& e : elements) {
            // loop over all variables
            for(size_t var = 0; var < n_variables; var++) {
                // if this variable is ESSENTIAL (Dirichlet),
                // find its dof global index
                if(e->bc_type(var) == MyDiscreteSpace::ESSENTIAL) {// i-eme variable of geometrical object e
                    // extrema of local indexes of the dof of this element
                    size_t dof_local_index_start(dof_local_index_by_elements[var][e_li][0]);
                    size_t dof_local_index_end(dof_local_index_by_elements[var][e_li][1]);
                    // loop over all dof of this variable
                    for(size_t dof = dof_local_index_start; dof < dof_local_index_end; dof++) {
                        // find dof global index
                        size_t dof_gi(m_local_to_global_operator[cell_gi][dof]);
                        essential_dof[dof_gi] = true;
                        essential_dof_gi.push_back(dof_gi);
                    }
                }
            } // loop on var
            e_li++;
        } // loop on elements

    } // essential_dof_by_elements

    template<size_t DIM> 
    void MatrixPattern<DIM>::assemble_local_contributions(
                    const MyMesh::Mesh<DIM>* mesh,
                    MyDiscreteSpace::DiscreteSpace<DIM>* ds) {

        // this method does :
        // 1 - transform all the local rhs with 
        //     the Dirichlet contributions
        // 2 - if selected, eliminate all or some of the bulk unknowns 
        //     by static condensation (change the local matrices)
        // 3 - identify the essential dof wrt there global index 
        //     (empty lists if the essential dof are eliminated 
        //     because they have no global index in this case)
        // 4 - assemble global matrix and global RHS 
        //     from the local contributions of all pe
        //     (avoid essential dof which need special treatment)
        //     Takes into account the Lagrange multiplier if necessary
        // 5 - fill essential dof rows in global matrix and RHS


        clock_t t;
        t = clock();

        // 1 - transform all the local rhs with 
        //     the Dirichlet contributions
        ds->add_DirichletBC_rhs();

        // 2 - if selected, elimination of all or some of the bulk unknowns 
        //     by static condensation (change the local matrices)
        if(m_static_cond) ds->static_condensation();

        // 3 - find the global indexes of the ESSENTIAL BC dof
        // to modify there contributions to the global system
        // if they are not already eliminated 
        
        std::vector<bool> essential_dof(m_n_cumulate_dof, false); // the global index is or not an essential dof ?
        // list of global index of the essential dof
        std::vector<size_t> essential_dof_gi;
        // TODO: essential_dof_gi.reserve()

        // if Dirichlet dof are eliminated, nothing changes
        // because Dirichlet dof have no global number 
        // thus they are already not in the global system
        if(!m_essential_bc_elimination) { 

            size_t n_variables(ds->n_var()); // nb of variables in the model

            // loop over the bound cells
            for(auto& cpt : mesh->get_bound_cells()) {
                size_t c_gi(cpt->global_index());

                // polytopal element of this cell
                MyDiscreteSpace::PolytopalElement<DIM>* pe(ds->polytopal_element(c_gi));

                // find all the dof global indexes which are ESSENTIAL BC
                // it can be vertices' dof or edges' dof or faces' dof

                //      vertices
                // fill essential_dof and essential_dof_gi with the info
                // of the vertices which are ESSENTIAL BC
                set_essential_dof_by_elements(c_gi, cpt->get_vertices(), 
                    n_variables, pe->get_vertices_local_index(),
                    essential_dof, essential_dof_gi);

                if(DIM == 3) {
                    //      edges
                    // fill essential_dof and essential_dof_gi with the info
                    // of the vertices which are ESSENTIAL BC
                    set_essential_dof_by_elements(c_gi, cpt->get_edges(), 
                        n_variables, pe->get_edges_local_index(),
                        essential_dof, essential_dof_gi);
                }

                //      faces
                set_essential_dof_by_elements(c_gi, cpt->get_faces(), 
                    n_variables, pe->get_faces_local_index(),
                    essential_dof, essential_dof_gi);

            } // loop on bound cells

        } // essential dof elimination



        // 4 - init global matrix and global RHS
        // when the Lagrange multiplier is used, there are more lines in the matrices (at least one)
        size_t global_size(m_n_cumulate_dof);
        if(ds->lagrange_multiplier()) { 
            // add the necessary space for each variable which have a Lagrange multiplier
            for(auto& var_index : ds->lagrange_multiplier_var_index()) { 
                global_size += ds->var_sizes()[var_index];
            }
        }

        m_global_rhs = Eigen::VectorXd::Zero(global_size);
        m_global_matrix.resize(global_size, global_size);
        // triplets (i, j, x_i,j) which will init m_global_matrix
        std::vector<Eigen::Triplet<double>> triplets_glob_mat;
        // TODO: triplets_glob_mat.reserve();

        // assemble global matrix and global RHS 
        // from the local contributions of all pe
        for(auto& pe : ds->get_polytopal_elements()) {
            // get global index of cell
            size_t c_gi(pe->get_cell()->global_index());
            // get local contributions
            Eigen::MatrixXd loc_mat(pe->get_local_bilinear_form());
            Eigen::VectorXd loc_rhs(pe->get_local_rhs());

            for(size_t iL = 0, nb_local_dof = loc_rhs.size(); iL < nb_local_dof; iL++) {
                size_t iG(m_local_to_global_operator[c_gi][iL]);
                // if this element is eliminated (global index = SIZE_MAX ), 
                // do nothing. It can happen with homogeneous Dirichlet or eliminated bulk dof (by static cond).
                if(iG == SIZE_MAX) { continue; } 
                // if the dof is ESSENTIAL (and not eliminated), it is an Homogeneous Dirichlet BC 
                // (the problem is always transformed into HD), done later
                if(essential_dof[iG]) { continue; } 

                // for non ESSENTIAL dof :
                // Right Hand Side
                m_global_rhs[iG] += loc_rhs[iL];
                // global matrix
                for(size_t jL = 0; jL < nb_local_dof; jL++) {
                        size_t jG(m_local_to_global_operator[c_gi][jL]);
                        if(jG == SIZE_MAX) { continue; } 

                        triplets_glob_mat.emplace_back(iG, jG, loc_mat(iL, jL));
                }
            } // dof in pe

        } // end pe loop

        // Add the Lagrange multiplier terms if necessary
        // this modifications cannot affect ESSENTIAL dof because Lagrange multiplier
        // is a global method which is applied on non ESSENTIAL variables
        if(ds->lagrange_multiplier()) {
            size_t lambda_iG(m_n_cumulate_dof); // global index of the Lagrange multiplier lambda

            double mesh_measure(mesh->measure());
            // loop over all the Lagrange multipliers
            for(auto& var_index : ds->lagrange_multiplier_var_index()) { 
                size_t var_size(ds->var_sizes()[var_index]);
                
                if(m_static_cond) { 
                    // the static condensation is done at the local level 
                    // for the lagrange multiplier contributions :
                    // assemble different objects computed in each pe.

                    // the rhs will be summed over all the pe
                    m_global_rhs.segment(lambda_iG, var_size) = Eigen::VectorXd::Zero(var_size);

                    for(auto& pe : ds->get_polytopal_elements()) {
                        // get global index of cell
                        size_t c_gi(pe->get_cell()->global_index());
                        
                        // compute the local contributions, I prefer doing it here to avoid 
                        // computing it several times if called several times from main and PE file
                        // the Shur complement has been done localy
                        // it modifies the global matrix and the global rhs
                        Eigen::MatrixXd local_lagrange_static_cond_M;
                        Eigen::VectorXd local_lagrange_static_cond_rhs;
                        // get (lambda, q) with static condensation
                        pe->local_lagrange_multiplier_contributions_static_cond(
                            var_index, var_size, mesh_measure, 
                            local_lagrange_static_cond_M, local_lagrange_static_cond_rhs);

                        // Right Hand Side
                        m_global_rhs.segment(lambda_iG, var_size) += local_lagrange_static_cond_rhs;

                        // global matrix
                        for(size_t jL = 0, nb_local_dof = local_lagrange_static_cond_M.cols(); 
                                   jL < nb_local_dof; jL++) {
                            size_t jG(m_local_to_global_operator[c_gi][jL]); 
                            // if this element is eliminated (global index = SIZE_MAX ), 
                            // do nothing. It can happen with homogeneous Dirichlet
                            if(jG == SIZE_MAX) { continue; } 

                            // loop over the size of the variable, 1 if scalar variable
                            for(size_t k = 0; k < var_size; k++) {
                                // add the contribution and its transpose
                                triplets_glob_mat.emplace_back(lambda_iG + k, jG, local_lagrange_static_cond_M(k, jL)); 
                                triplets_glob_mat.emplace_back(jG, lambda_iG + k, local_lagrange_static_cond_M(k, jL)); 
                            }
                        }

                    } // loop over pe

                } else { // no static condensation
                    for(auto& pe : ds->get_polytopal_elements()) {
                        // get global index of cell
                        size_t c_gi(pe->get_cell()->global_index());
                        // compute local contributions, I prefer doing it here to avoid 
                        // computing it several times if called several times from main and PE file
                        // get (lambda, q)
                        Eigen::MatrixXd local_lagrange(
                            pe->get_local_mean(
                                var_index, var_size, mesh_measure));
                        // // Right Hand Side
                        // m_global_rhs[lambda_iG] = 0.0; already the case
                        // global matrix
                        for(size_t jL = 0, nb_local_dof = local_lagrange.cols(); jL < nb_local_dof; jL++) {
                            size_t jG(m_local_to_global_operator[c_gi][jL]);
                            // if this element is eliminated (global index = SIZE_MAX ), 
                            // do nothing. It can happen with homogeneous Dirichlet or 
                            // eliminated bulk dof (by static cond).
                            if(jG == SIZE_MAX) { continue; } 

                            // loop over the size of the variable, 1 if scalar variable
                            for(size_t k = 0; k < var_size; k++) {
                                // add the contribution and its transpose
                                triplets_glob_mat.emplace_back(lambda_iG + k, jG, local_lagrange(k, jL)); 
                                triplets_glob_mat.emplace_back(jG, lambda_iG + k, local_lagrange(k, jL)); 
                            }
                        }
                    } // loop over pe
                } // static cond

                lambda_iG += var_size; // next Lagrange multiplier, next global indexes
            }
        }

        // 5 - Add the contribution of the ESSENTIAL dof (Homogeneous Dirichlet BC)
        // empty loop if the dof are eliminated
        for(size_t iG : essential_dof_gi) {
            // // Right Hand Side
            // m_global_rhs[iG] = 0.0; already the case
            // global matrix
            // quicker to use triplets_glob_mat than m_global_matrix.coeffRef(iG, iG).
            // triplets_glob_mat will be summed up with other values
            // but ESSENTIAL dof should not have any previous contribution, 
            // and it does not change anything if the constant is modified 
            // (except if it becomes 0 !)
            triplets_glob_mat.emplace_back(iG, iG, 100.);
        }

        // Assemble the global linear system
        // setFromTriplets creates a sorted and compressed sparse matrix 
        // where duplicates have been summed up
        m_global_matrix.setFromTriplets(triplets_glob_mat.begin(), triplets_glob_mat.end());


        t = clock() - t;
        m_assemble_time += ((double)t)/CLOCKS_PER_SEC;

        return;

    } // assemble_local_contributions


    template<size_t DIM> 
    void MatrixPattern<DIM>::set_eliminated_unknowns(
                    MyMesh::Mesh<DIM>* mesh, const MyDiscreteSpace::DiscreteSpace<DIM>* ds) {

        // Initialize the Boundary elements depending on the type of BC (essential, natural)
        size_t n_variables(ds->n_var());
        // it is necessary to initialize is_eliminated at least for every elements of the boundary cells
        for(auto& cpt : mesh->get_bound_cells()) {

            //      vertices
            set_eliminated_unknowns_by_elements(n_variables, cpt->get_vertices());
            //      edges
            if(DIM == 3) {
                set_eliminated_unknowns_by_elements(n_variables, cpt->get_edges());
            }
            //      faces
            set_eliminated_unknowns_by_elements(n_variables, cpt->get_faces());

        } // end loop bound cells
        return;
    } // set_eliminated_unknowns

    template<size_t DIM> 
    template<typename ELEMT>
    inline void MatrixPattern<DIM>::set_eliminated_unknowns_by_elements(
                size_t n_variables,
                const std::vector<ELEMT>& elements) {

        for(auto& e : elements) { // loop over the list of elements

            std::vector<bool> b;
            for(size_t i = 0; i < n_variables; i++) {
                // the unknown is eliminated if its type (natural, essential) is eliminated
                b.push_back((e->bc_type(i) == MyDiscreteSpace::ESSENTIAL && m_essential_bc_elimination) || 
                            (e->bc_type(i) == MyDiscreteSpace::NATURAL && m_natural_bc_elimination) );
            }
            e->set_is_eliminated(b);
        }
        return;
    } // set_eliminated_unknowns_by_elements


    template<size_t DIM> 
    void MatrixPattern<DIM>::set_global_index(
                    MyMesh::Mesh<DIM>* mesh, const MyDiscreteSpace::DiscreteSpace<DIM>* ds) {
        
        // *****************************************************************************
        //
        //   Warning : the eliminated elements (static cond or BC elements) must have 
        //                       discrete_unknown_global_index = -1
        //
        // *****************************************************************************

        size_t n_var(ds->n_var()); // nb of variables in the model
        // -1 everywhere
        init_discrete_unknown_global_index(mesh, n_var); 

        // compt of the global indexes
        // to obtain consecutive indexes by variables
        // for example in Stokes : u_0 ... u_n p_0 ... p_m,
        // Give number from 0 to n and from 0 to m, then apply a shift
        std::vector<size_t> discr_unknown_gi(n_var, 0); 

        if(m_no_bulk_dof) {
            // there is no bulk dof in the global numerotation
            // Numerotation of the skeletal dof only

            for (auto& cpt : mesh->get_cells()) {
                set_global_index_one_cell(
                    discr_unknown_gi, cpt, ds->polytopal_element(cpt->global_index()));
            }

        } else {
            // for at least one pe, all its bulk dof are not eliminated

            for (auto& cpt : mesh->get_cells()) {
                size_t c_gi(cpt->global_index()); // cell global index

                // Polytopal element instantiation of this cell
                MyDiscreteSpace::PolytopalElement<DIM>* pe(ds->polytopal_element(c_gi));
                size_t n_bulk_dof_total(pe->n_bulk_dof().sum()); // sum of the bulk dof over each variable
                Eigen::ArrayXi n_bulk_dof_eliminable(pe->n_bulk_dof_eliminable()); // eliminable bulk dof for each variable
                size_t n_remaining_bulk_dof_sc(pe->n_remaining_bulk_dof_sc()); // nb of bulk dof which cannot be eliminated if static cond is performed

                set_global_index_one_cell(discr_unknown_gi, cpt, pe);

                //      bulk  (special treatment because smaller algorithm)
                // if there is NO static condensation AND this cell contains bulk dof
                if(!m_static_cond && n_bulk_dof_total > 0) {

                    const std::vector<std::vector<size_t>> bulk_local_index(pe->get_bulk_local_index());
                    // give new global indexes to all bulk dof for each variable
                    for(size_t var = 0; var < n_var; var++) {
                        cpt->set_discrete_unknown_global_index(var, discr_unknown_gi[var]);
                        // number of dof thanks to the extrema of local index of the dof for this var 
                        discr_unknown_gi[var] += bulk_local_index[var][1] - bulk_local_index[var][0];
                    }

                // if there is static condensation AND bulk dof remains (if not every bulk dof are eliminable)
                } else if(m_static_cond && n_remaining_bulk_dof_sc > 0) {

                    const std::vector<std::vector<size_t>> bulk_local_index(pe->get_bulk_local_index());
                    
                    // give new global indexes to the remaining bulk dof
                    for(size_t var = 0; var < n_var; var++) {
                        cpt->set_discrete_unknown_global_index(var, discr_unknown_gi[var]);
                        // number of dof 
                        discr_unknown_gi[var] += bulk_local_index[var][1] - bulk_local_index[var][0]
                                                - n_bulk_dof_eliminable[var];
                    }
                }
            } // loop over cells
        } // end if bulk dof

        // necessary to apply a shit, up to now different variables
        // have the same global index
        size_t shift(discr_unknown_gi[0]);
        for(size_t var = 1; var < n_var; var++) { // do nothing if only one variable
            // shift everywhere
            shift_discrete_unknown_global_index(mesh, var, shift); 
            // cumulate the shift for the next variable and for m_n_cumulate_dof
            shift += discr_unknown_gi[var];
        }

        // store the number of dof after elimination (size of the global matrix)
        m_n_cumulate_dof = shift;

        return;
    } // set_global_index

    template<size_t DIM> 
    inline void MatrixPattern<DIM>::init_discrete_unknown_global_index(
        MyMesh::Mesh<DIM>* mesh, size_t n_var) {

        std::vector<int> init(n_var, -1);

        init_discrete_unknown_global_index_elements(mesh->get_cells(), init); 
        init_discrete_unknown_global_index_elements(mesh->get_faces(), init); 
        if(DIM == 3) { init_discrete_unknown_global_index_elements(
                            mesh->get_edges(), init); }
        init_discrete_unknown_global_index_elements(mesh->get_vertices(), init); 

        return;
    } // init_discrete_unknown_global_index

    template<size_t DIM> 
    template<typename ELEMT>
    inline void MatrixPattern<DIM>::init_discrete_unknown_global_index_elements(
        const std::vector<ELEMT>& elements, const std::vector<int>& init) {
        
        for (auto& pt : elements) {
            pt->set_discrete_unknown_global_index(init);
        }
        return;
    } // init_discrete_unknown_global_index_elements

    template<size_t DIM> 
    inline void MatrixPattern<DIM>::shift_discrete_unknown_global_index(
        MyMesh::Mesh<DIM>* mesh, size_t var_index, size_t shift) {
        
        // shift everywhere
        shift_discrete_unknown_global_index_elements(mesh->get_cells(), var_index, shift); 
        shift_discrete_unknown_global_index_elements(mesh->get_faces(), var_index, shift); 
        if(DIM == 3) { shift_discrete_unknown_global_index_elements(
                            mesh->get_edges(), var_index, shift); }
        shift_discrete_unknown_global_index_elements(mesh->get_vertices(), var_index, shift); 

        return;
    } // shift_discrete_unknown_global_index
    
    template<size_t DIM> 
    template<typename ELEMT>
    inline void MatrixPattern<DIM>::shift_discrete_unknown_global_index_elements(
        const std::vector<ELEMT>& elements, size_t var_index, size_t shift) {

        for (auto& pt : elements) {
            int gi(pt->discrete_unknown_global_index(var_index));
            // if this element has a global index, shift it
            if( gi >= 0) { 
                pt->set_discrete_unknown_global_index(var_index, gi + shift);
            }
        }
        return;
    } // shift_discrete_unknown_global_index_elements


    template<size_t DIM> 
    void MatrixPattern<DIM>::set_global_index_one_cell(
                std::vector<size_t>& discr_unknown_gi,
                const MyMesh::Cell<DIM>* cell_pt, 
                const MyDiscreteSpace::PolytopalElement<DIM>* pe) {
        
        // TODO: find a better way to distinguish which implementation
        // of set_global_index_all_skeletal is called.
        // Up to now, distinguish between string or bool but are not usefull

        // Give global number of the skeletal elements
        if(cell_pt->is_boundary()) { // there is at least one element of this cell which belongs to the boundary
            // In this case, test if the element belongs to the boundary and if any var is eliminated
            set_global_index_all_skeletal(
                std::string("BC"), discr_unknown_gi, cell_pt, pe);
        } else { // there is no BC element in the cell
            // In this case, NO test if the element belongs to the boundary
            set_global_index_all_skeletal(
                true, discr_unknown_gi, cell_pt, pe);
        } // end loop if cell BC
        return;
    } // set_global_index_one_cell

    template<size_t DIM> 
    template<typename T>
    void MatrixPattern<DIM>::set_global_index_all_skeletal(
                const T flag,
                std::vector<size_t>& discr_unknown_gi,
                const MyMesh::Cell<DIM>* cell_pt, 
                const MyDiscreteSpace::PolytopalElement<DIM>* pe) {

        // Loops over all skeletal elements of cell which can carry discrete unknowns
        //      vertices
        set_global_index_by_elements(
            flag,
            discr_unknown_gi, cell_pt->get_vertices(),
            pe->get_vertices_local_index());
        
        //      edges
        if(DIM == 3) {
            set_global_index_by_elements(
                flag,
                discr_unknown_gi, cell_pt->get_edges(),
                pe->get_edges_local_index());
        }

        //      faces
        set_global_index_by_elements(
            flag,
            discr_unknown_gi, cell_pt->get_faces(),
            pe->get_faces_local_index());

        return;
    } // set_global_index_all_skeletal


    template<size_t DIM> 
    template<typename ELEMT>
    inline void MatrixPattern<DIM>::set_global_index_by_elements(
                const std::string flag,
                std::vector<size_t>& discr_unknown_gi,
                const std::vector<ELEMT>& elements, 
                const std::vector<std::vector<std::vector<size_t>>>& elemts_local_index) {

        size_t n_var(elemts_local_index.size()); // nb of variables

        size_t l_e(0); // local index of this element
        for(auto& e : elements) { // loop over the list of elements

            // The element belongs to a boundary cell, so it may have one variable (at least) 
            // which is eliminated. 
            // If the element contains one variable eliminated (at least), loop over variables
            std::vector<bool> is_elim_e(e->is_eliminated()); // for this elemt, is the variable eliminated ?

            // if all variables are eliminated, skip the element
            if( std::all_of(is_elim_e.begin(), is_elim_e.end(), [](bool b){return b;}) ) {
                l_e++;
                continue;
            }

            // if at least one variable of this element is eliminated (but not all), enters the following loop
            // special treatment as test over is_elim_e is necessary
            if( std::any_of(is_elim_e.begin(), is_elim_e.end(), [](bool b){return b;}) ) {
            
                // loop over all variables
                for(size_t var = 0; var < n_var; var++) {
                    // if this element on this variable is not eliminated
                    if(!is_elim_e[var]) {
                        // if this element on this variable has no global indexes yet
                        if(e->discrete_unknown_global_index(var)<0) {
                            // save the global index of the first dof of this element and this variable
                            e->set_discrete_unknown_global_index(var, discr_unknown_gi[var]);
                            // new global indexes
                            discr_unknown_gi[var] += 
                                    elemts_local_index[var][l_e][1] - elemts_local_index[var][l_e][0];
                        }
                    }
                } // end loop over the variables                

            } else { // no dof are eliminated in this element, do the algo without  is_elim_e[var]
                set_global_index_no_eliminated_var(
                    discr_unknown_gi, e, l_e, n_var, elemts_local_index);
            }

            l_e++; // next element

        } // end loop over elements
        return;
    } // set_global_index_by_elements

    template<size_t DIM> 
    template<typename ELEMT>
    inline void MatrixPattern<DIM>::set_global_index_by_elements(
                bool flag,
                std::vector<size_t>& discr_unknown_gi,
                const std::vector<ELEMT>& elements, 
                const std::vector<std::vector<std::vector<size_t>>>& elemts_local_index) {

        size_t n_var(elemts_local_index.size()); // nb of variables

        size_t l_e(0); // local index of this element
        for(auto& e : elements) { // loop over the list of elements

            // no variable is eliminated thus no test if elim,
            set_global_index_no_eliminated_var(
                discr_unknown_gi, e, l_e, n_var, elemts_local_index);
        
            l_e++; // next element
        } // end loop over elements

        return;
    } // set_global_index_by_elements

    template<size_t DIM> 
    template<typename ELEMT>
    inline void MatrixPattern<DIM>::set_global_index_no_eliminated_var(
                std::vector<size_t>& discr_unknown_gi,
                const ELEMT& element, 
                size_t l_elm,
                size_t n_var,
                const std::vector<std::vector<std::vector<size_t>>>& elemts_local_index) {


        // loop over all variables
        for(size_t var = 0; var < n_var; var++) {
            // if this element on this variable has no global indexes yet
            if(element->discrete_unknown_global_index(var)<0) {
                // save the global index of the first dof of this element and this variable
                element->set_discrete_unknown_global_index(var, discr_unknown_gi[var]);
                // new global indexes
                discr_unknown_gi[var] += 
                        elemts_local_index[var][l_elm][1] - elemts_local_index[var][l_elm][0];
            }
        } // end loop over the variables                

        return;
    } // set_global_index_no_eliminated_var
    
    // the eliminated dof (static cond or particular BC) have discrete_unknown_global_index = -1 
    template<size_t DIM> 
    void MatrixPattern<DIM>::set_local_to_global_operator(
                    MyMesh::Mesh<DIM>* mesh, const MyDiscreteSpace::DiscreteSpace<DIM>* ds) {
        
        size_t n_var(ds->n_var()); // nb of variables in the model

        if(m_no_bulk_dof) {
            // there is no bulk dof in the global numerotation
            // Mapping of the skeletal dof only

            for (auto& cpt : mesh->get_cells()) {
                set_local_to_global_operator_one_cell(
                    cpt->global_index(), cpt, ds->polytopal_element(cpt->global_index())); // skeletal dof
            }

        } else {
            // for at least one pe, all its bulk dof are not eliminated

            for (auto& cpt : mesh->get_cells()) {
                size_t c_gi(cpt->global_index()); // cell global index

                // Polytopal element instantiation of this cell
                MyDiscreteSpace::PolytopalElement<DIM>* pe(ds->polytopal_element(c_gi));
                size_t n_bulk_dof_total(pe->n_bulk_dof().sum()); // sum of the bulk dof over each variable
                Eigen::ArrayXi n_bulk_dof_eliminable(pe->n_bulk_dof_eliminable()); // eliminable bulk dof for each variable
                size_t n_remaining_bulk_dof_sc(pe->n_remaining_bulk_dof_sc()); // nb of bulk dof which cannot be eliminated if static cond is performed

                set_local_to_global_operator_one_cell(c_gi, cpt, pe); // skeletal dof

                //      bulk  (special treatment because smaller algorithm)
                // if there is NO static condensation AND this cell contains bulk dof
                if(!m_static_cond && n_bulk_dof_total > 0) {

                    const std::vector<std::vector<size_t>> bulk_local_index(pe->get_bulk_local_index());

                    for(size_t var = 0; var < n_var; var++) {
                        // map local indexes of all bulk dof for each variable 
                        // to their global indexes
                        int g_i(cpt->discrete_unknown_global_index(var));

                        for(size_t l_dof=bulk_local_index[var][0], bulk_dof_end=bulk_local_index[var][1];
                                   l_dof<bulk_dof_end; l_dof++) {
                            m_local_to_global_operator[c_gi][l_dof] = g_i++;
                        }
                    }

                // if there is static condensation AND bulk dof remains (if not every dof are eliminable)
                } else if(m_static_cond && n_remaining_bulk_dof_sc > 0) {

                    const std::vector<std::vector<size_t>> bulk_local_index(pe->get_bulk_local_index());
                    size_t l_dof(bulk_local_index[0][0]); // first local index of a bulk dof
                    
                    for(size_t var = 0; var < n_var; var++) {
                        // map local indexes of remaining bulk dof to their global indexes
                        int g_i(cpt->discrete_unknown_global_index(var));
                        // A permutation will be done to group
                        // the remaining bulk dof at the end of the skeletal list
                        // Thus the local bulk indexes will not coincide with there
                        // index in local_matrix and local_rhs in assemble

                        size_t n_remaining_bulk_dof_sc_var(bulk_local_index[var][1] - bulk_local_index[var][0]
                                - n_bulk_dof_eliminable[var]);

                        for(size_t i = 0; i < n_remaining_bulk_dof_sc_var; i++) {
                            // map local indexes of remaining bulk dof to their global indexes
                            // after permutation, the remaining dof are always contiguous
                            // at the beginning of the bulk list
                            // this is why it is necessary to use l_dof
                            // instead of bulk_local_index[var][]
                            m_local_to_global_operator[c_gi][l_dof++] = g_i++;
                        }
                    }
                }
            } // loop over cells
        } // end if bulk dof

        return;
    } // set_local_to_global_operator

    template<size_t DIM> 
    void MatrixPattern<DIM>::set_local_to_global_operator_one_cell(
                size_t cell_gi,
                const MyMesh::Cell<DIM>* cell_pt, 
                const MyDiscreteSpace::PolytopalElement<DIM>* pe) {
        
        // TODO: find a better way to distinguish which implementation
        // of set_global_index_all_skeletal is called.
        // Up to now, distinguish between string or bool but are not usefull

        // map local to global indexes of the skeletal elements
        if(cell_pt->is_boundary()) { // there is at least one element of this cell which belongs to the boundary
            // In this case, test if the element belongs to the boundary and if any var is eliminated
            set_local_to_global_operator_all_skeletal(
                std::string("BC"), cell_gi, cell_pt, pe);
        } else { // there is no BC element in the cell
            // In this case, NO test if the element belongs to the boundary
            set_local_to_global_operator_all_skeletal(
                true, cell_gi, cell_pt, pe);
        } // end loop if cell BC
        return;
    } // set_local_to_global_operator_one_cell

    template<size_t DIM> 
    template<typename T>
    void MatrixPattern<DIM>::set_local_to_global_operator_all_skeletal(
                const T flag,
                size_t cell_gi,
                const MyMesh::Cell<DIM>* cell_pt, 
                const MyDiscreteSpace::PolytopalElement<DIM>* pe) {

        // Loops over all skeletal elements of cell which can carry discrete unknowns
        //      vertices
        set_local_to_global_operator_by_elements(
            flag,
            cell_gi, cell_pt->get_vertices(),
            pe->get_vertices_local_index());
        
        //      edges
        if(DIM == 3) {
            set_local_to_global_operator_by_elements(
                flag,
                cell_gi, cell_pt->get_edges(),
                pe->get_edges_local_index());
        }

        //      faces
        set_local_to_global_operator_by_elements(
            flag,
            cell_gi, cell_pt->get_faces(),
            pe->get_faces_local_index());

        return;
    } // set_local_to_global_operator_all_skeletal


    template<size_t DIM> 
    template<typename ELEMT>
    inline void MatrixPattern<DIM>::set_local_to_global_operator_by_elements(
                const std::string flag,
                size_t cell_gi,
                const std::vector<ELEMT>& elements, 
                const std::vector<std::vector<std::vector<size_t>>>& elemts_local_index) {

        size_t n_var(elemts_local_index.size()); // nb of variables

        size_t l_e(0); // local index of this element
        for(auto& e : elements) { // loop over the list of elements

            // The element belongs to a boundary cell, so it may have one variable (at least) 
            // which is eliminated, thus test if g_i < 0
            
            // loop over all variables
            for(size_t var = 0; var < n_var; var++) {
                int g_i(e->discrete_unknown_global_index(var));
                // if this element has no global index, it is eliminated and nothing to do
                if(g_i < 0) { continue; }

                // map local indexes of all dof for each variable 
                // to their global indexes
                for(size_t l_dof=elemts_local_index[var][l_e][0], 
                        l_dof_end=elemts_local_index[var][l_e][1];
                        l_dof<l_dof_end; l_dof++) {
                    m_local_to_global_operator[cell_gi][l_dof] = g_i++;
                }
            } // end loop over the variables                

            l_e++; // next element

        } // end loop over elements
        return;
    } // set_local_to_global_operator_by_elements

    template<size_t DIM> 
    template<typename ELEMT>
    inline void MatrixPattern<DIM>::set_local_to_global_operator_by_elements(
                bool flag,
                size_t cell_gi,
                const std::vector<ELEMT>& elements, 
                const std::vector<std::vector<std::vector<size_t>>>& elemts_local_index) {

        size_t n_var(elemts_local_index.size()); // nb of variables

        size_t l_e(0); // local index of this element
        for(auto& e : elements) { // loop over the list of elements
            // no variable is eliminated thus no test if g_i < 0
            // loop over all variables
            for(size_t var = 0; var < n_var; var++) {
                int g_i(e->discrete_unknown_global_index(var));

                // map local indexes of all dof for each variable 
                // to their global indexes
                for(size_t l_dof=elemts_local_index[var][l_e][0], 
                        l_dof_end=elemts_local_index[var][l_e][1];
                        l_dof<l_dof_end; l_dof++) {
                    m_local_to_global_operator[cell_gi][l_dof] = g_i++;
                }

            } // end loop over the variables                
        
            l_e++; // next element
        } // end loop over elements

        return;
    } // set_local_to_global_operator_by_elements


    template<size_t DIM> 
    inline double MatrixPattern<DIM>::get_assemble_time() const { return m_assemble_time; }

    template<size_t DIM> 
    inline double MatrixPattern<DIM>::get_init_time() const { return m_init_time; }


} // MySolver namespace
#endif /* MATRIX_PATTERN_HPP */
    
