#ifndef DISCRETE_SPACE_HPP
#define DISCRETE_SPACE_HPP

#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include <time.h>
#include <functional>
#include <algorithm>
#include "mesh.hpp"
#include "cell.hpp"
#include "polytopal_element.hpp"

namespace MyDiscreteSpace {
    /**
     * The enum ContinuousSpace contains
     * all the possible continuous spaces
     * to which the variables belong.
     * Warning :  when adding a new continuous space,
     * fill m_var_sizes
    */
  enum ContinuousSpace { L2, H1, H1d };

    /**
     * The enum BoundaryType contains
     * all the possible BC types
    */
    enum BoundaryType { NATURAL, ESSENTIAL, NO_BC }; // TODO: is NO_BC useful ?

    /**
     * The class DiscreteSpace creates and stores 
     * the list of local polytopal element instantiations
     * (one by cell). 
    */
    template<size_t DIM> //, Adaptivity_type ADA>
    class DiscreteSpace
    {
    public:
        DiscreteSpace(MyMesh::Mesh<DIM>* const mesh);   ///<  Constructor (init size of m_polytopal_elements)
        ~DiscreteSpace();   ///<  Destructor (delete pointers)
        /**
         * Initialisation (same polytopal element for every cells)
         * The polytopal element type PET (HHO, FE, ...) is given by a parameter,
         * the same for continuous spaces (enum).
         * @param mesh pointer to the mesh
         * @param kb order of the bulk discrete unknown
         * @param ks order of the reconstruction
         */
        template<template<size_t, int...> class PET, int... CONTINUOUS_SPACES>
        void init(MyMesh::Mesh<DIM>* const mesh, int kb, int ks = -20); // Polytopal_element_type pet, Poly_base_type pbt, Quad_type qt, Data dat);
        /**
         * Initialisation of some cells
         * The polytopal element type PET (HHO, FE, ...) is given by a parameter,
         * the same for continuous spaces (enum).
         * @param cells list of cells
         * @param kb order of the bulk discrete unknown
         * @param ks order of the reconstruction
         */
        template<template<size_t, int...> class PET, int... CONTINUOUS_SPACES>
        void init_cells(std::vector<MyMesh::Cell<DIM>*> cells, int kb, int ks = -20); // Polytopal_element_type pet, Poly_base_type pbt, Quad_type qt, Data dat);

        /**
         * Add the term k * \int grad(u) * grad(v) dx to each local matrix
         * @param var_index index of the variable (0 by default)
         * @param mult_cst multiplicative constant k : k * \int grad(u) * grad(v) dx
         */
        void add_stiffness_contribution(size_t var_index = 0, double mult_cst = 1.);
        /**
         * Add the term k * \int u * v dx to each local matrix
         * @param var_index index of the variable (0 by default)
         * @param mult_cst multiplicative constant k : k * \int u * v dx
         */
        void add_mass_contribution(size_t var_index = 0, double mult_cst = 1.);
        /**
         * Add the divergence term :
         * k * \int_T r_T div(u_T) * r_T div(v_T) dx
         * to each local matrix, knowing the index of the variable u_T
         * @param var_index index of the variable to add the divergence contribution
         * @param mult_cst multiplicative constant
         */
        void add_divergence_contribution(size_t var_index = 0, double mult_cst = 1.);
        /**
         * Add the divergence of a cross term term \int div(u) * p 
         * to each local matrix knowing the indexes of the variables
         * @param var_index1 index of the variable to multiply on the left
         * @param var_index2 index of the variable to multiply on the right
         * @param mult_cst multiplicative constant
         */
        void add_divergence_contribution(
                size_t var_index1, 
                size_t var_index2, 
                double mult_cst = 1.);

        template<typename RET>
        inline void set_rhs(RET func (Eigen::ArrayXd), int quadra_order = 6, 
            size_t var_index = 0);   ///< set all local rhs with specific quadra order (6 by default)
        
        /**
         * Eliminate the bulk unknown(s) applying the static condensation (Shur complement) 
         * on the local matrices of all the polytopal elements of this discrete space.
         */
        void static_condensation();

        /**
         * reconstruction of the solution
         * @param loc2glo local to global mapping of the dof indexes
         * @param sol_values global vector with the solution 
         * @param static_cond boolean to give the info if static cond is used 
         */
        void reconstruction(std::vector<std::vector<size_t>>& loc2glo,
            Eigen::VectorXd& sol_values,
            bool static_cond);

        std::vector<PolytopalElement<DIM> *> get_polytopal_elements() const; ///< get all the Polytopal Elements of the mesh
        PolytopalElement<DIM>* polytopal_element(size_t g_i) const; ///< get a constant pointer to a Polytopal Element in a cell using its global index
        size_t n_var() const;  ///< get the number of variables (= the number of continuous spaces)
        Eigen::ArrayXi var_sizes() const;  ///< get the sizes of all variables in a vector
        int sum_var_sizes() const;  ///< get the sum of the sizes of all variables
        bool are_bulk_dof_eliminable() const;  ///< get the bool to know if static cond is enough to eliminate all cells dof
        bool no_bulk_dof() const;  ///< get the bool to know if there are bulk dof in the method
        bool lagrange_multiplier() const;  ///< get the bool to know if a Lagrange multiplier is used
        const std::vector<size_t>& lagrange_multiplier_var_index() const; ///< over which variable(s) is/are applyed the Lagrange multiplier ?

        /**
         * Add the var index to the var index list on which applying the Lagrange multiplier
         * @param var_index index of the variable on which applying the Lagrange multiplier
         */
        void add_lagrange_multiplier(size_t var_index = 0);
    
        /**
         * Modify the stabilization type 
         * @param stabilization_type "ls_stabilization" or the equal order stabilization is used
         */
        inline void set_stabilization_type(std::string stabilization_type);

        /**
         * For all elements of the boudary cells 
         * of the mesh which verify a certain condition, 
         * set the type of BC
         * with the value bound_type
         * @param mesh mesh
         * @param bound_type type of the BC
         * @param condition function depending on the coords to set the type of BC (true by default)
         */
        inline void set_BCtype(const MyMesh::Mesh<DIM>* mesh, 
                                int bound_type,
                                std::function<bool(Eigen::ArrayXd)> condition = 
                                    [](Eigen::ArrayXd pt) { return true; } );
        /**
         * For all elements of a list of cells
         * which verify a certain condition, 
         * set the type of BC
         * with the value bound_type
         * @param cells list of cells
         * @param bound_type type of the BC
         * @param condition function depending on the coords to set the type of BC  (true by default)
         */
        inline void set_BCtype(const std::vector<MyMesh::Cell<DIM>*> cells, 
                                int bound_type,
                                std::function<bool(Eigen::ArrayXd)> condition = 
                                    [](Eigen::ArrayXd pt) { return true; } );

        /**
         * For all elements of the boudary cells 
         * of the mesh, set the type of BC
         * of a ONLY one variable
         * with the value bound_type
         * @param mesh mesh
         * @param var_index index of the variable to set
         * @param bound_type type of the BC
         * @param condition function depending on the coords to set the type of BC  (true by default)
         */
        inline void set_BCtype(const MyMesh::Mesh<DIM>* mesh,
                                size_t var_index,
                                int bound_type,
                                std::function<bool(Eigen::ArrayXd)> condition = 
                                    [](Eigen::ArrayXd pt) { return true; } );
        /**
         * For all elements of a list of cells,
         * set the type of BC
         * with the value bound_type
         * @param cells list of cells
         * @param var_index index of the variable to set
         * @param bound_type type of the BC
         * @param condition function depending on the coords to set the type of BC (true by default)
         */
        inline void set_BCtype(const std::vector<MyMesh::Cell<DIM>*> cells, 
                                size_t var_index,
                                int bound_type,
                                std::function<bool(Eigen::ArrayXd)> condition = 
                                    [](Eigen::ArrayXd pt) { return true; } );

        /**
         * For each polytopal element instantiation,
         * compute the lifting to transform into homogeneous 
         * Dirichlet the variable var_index 
         * @param func function to extract the Dirichlet value 
         * @param var_index index of the variable whose Dirichlet value is func
         */
        template<typename RET>
        inline void set_local_DirichletBC(RET func (Eigen::ArrayXd), size_t var_index = 0);
        /**
         * For each polytopal element instantiation,
         * add the Neumann contribution
         * @param func function to extract the Neumann value 
         * @param var_index index of the variable where Neumann boundary is applied
         */
        template<typename RET>
        inline void non_homogeneous_NeumannBC(RET func (Eigen::ArrayXd), size_t var_index = 0);

        /**
         * Modify the rhs with the Dirichlet lifting
         */
        inline void add_DirichletBC_rhs();

        /**
         * For the visualization, it is necessary 
         * to reconstruct the value of the
         * numerical solution at the vertices
         * @param mesh pointer to the mesh
         * @param conforming_method is the method conforming (false by default)
         * @param var_index index to determine which (scalar or vectorial) variable is approximated 
         */
        Eigen::MatrixXd reconstruction_at_vertices(const MyMesh::Mesh<DIM>* mesh, 
                bool conforming_method = false, size_t var_index = 0);

        /**
         * For the visualization, it might also be necessary 
         * to reconstruct the value of the
         * numerical solution at the vertices
         * only from the bulk dof !
         * @param mesh pointer to the mesh
         * @param conforming_method is the method conforming (false by default)
         * @param var_index index to determine which (scalar or vectorial) variable is approximated 
         */
        Eigen::MatrixXd bulk_reconstruction_at_vertices(const MyMesh::Mesh<DIM>* mesh, 
                bool conforming_method = false, size_t var_index = 0);

        /**
         * Timers
         */
        inline double get_init_time() const;
        inline double get_local_system_time() const;


    private:
        MyMesh::Mesh<DIM>* m_mesh;      ///< pointer to the mesh containing this discrete space
        std::vector<PolytopalElement<DIM> *> m_polytopal_elements;    ///<  list of local polytopal element (one PE by cell)
        std::vector<int> m_continuous_spaces;   ///<  store the continous space(s) of the unknown(s) (enum type)
        Eigen::ArrayXi m_var_sizes;     ///<  vector with the dimension of each variable
        int m_sum_var_sizes;     ///<  sum of all the dimensions of the variables (dof multiplicative factor)
        bool m_are_bulk_dof_eliminable; ///< is it possible, for all PE, to eliminate every bulk dof ?
        bool m_no_bulk_dof; ///< is there any bulk dof in the mehod ?
        std::vector<size_t> m_lagrange_multiplier_var_index; ///< on which var index is applied the Lagrange multiplier ?

        double m_init_time; // timer with the init time
        double m_local_system_time; // timer with the cumul of time to constrcut the local systeme (mass and stiffness matrices, rhs, Dirichlet BC)

        /**
         * For a list of elements (vertices, faces,...), 
         * set the type of BC
         * with the value bound_type
         * when it follows a certain condition
         * @param elements list of elements
         * @param bound_type type of the BC
         * @param condition function depending on the coords to set the type of BC
         */
        template<typename ELEMT>
        inline void set_BCtype_by_elements(const std::vector<ELEMT>& elements, 
                                int bound_type,
                                std::function<bool(Eigen::ArrayXd)> condition);
        inline void set_BCtype_by_elements(const std::vector<MyMesh::Vertex<DIM>*>& elements, 
                                int bound_type,
                                std::function<bool(Eigen::ArrayXd)> condition);

        /**
         * For a list of elements (vertices, faces,...), 
         * set the type of BC of only one variable
         * with the value bound_type
         * when it follows a certain condition
         * @param elements list of elements
         * @param var_index index of the variable to set
         * @param bound_type type of the BC
         * @param condition function depending on the coords to set the type of BC
         */
        template<typename ELEMT>
        inline void set_BCtype_by_elements(const std::vector<ELEMT>& elements, 
                                size_t var_index,
                                int bound_type,
                                std::function<bool(Eigen::ArrayXd)> condition = 
                                    [](Eigen::ArrayXd pt) { return true; } );
        inline void set_BCtype_by_elements(const std::vector<MyMesh::Vertex<DIM>*>& elements, 
                                size_t var_index,
                                int bound_type,
                                std::function<bool(Eigen::ArrayXd)> condition = 
                                    [](Eigen::ArrayXd pt) { return true; } );

    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////
    template<size_t DIM> 
    DiscreteSpace<DIM>::DiscreteSpace(MyMesh::Mesh<DIM>* const mesh) :
    m_mesh(mesh),
    m_polytopal_elements(mesh->n_cells()),
    m_sum_var_sizes(0),
    m_are_bulk_dof_eliminable(true), // let m_are_bulk_dof_eliminable init with true for init_cells
    m_no_bulk_dof(true), // let m_no_bulk_dof init with true for init_cells
    m_init_time(0.0),
    m_local_system_time(0.0) {}

    template<size_t DIM>
    DiscreteSpace<DIM>::~DiscreteSpace() {
        for(auto& pe : m_polytopal_elements) { delete pe; pe = 0; }
    }

    // Build one PolytopalElement instantiation by cell in the mesh
    // with reconstruction order ks and bulk order kb
    // Stores the pointer in m_polytopal_elements
    // TODO: define several init(), depending on the continuous space
    template<size_t DIM> 
    template<template<size_t, int...> class PET, int... CONTINUOUS_SPACES>
    void DiscreteSpace<DIM>::init(MyMesh::Mesh<DIM>* const mesh, int kb, int ks) {

        clock_t t(clock());

        if(ks == -20) { ks = kb; } // better way to define that ks is otional ?

#ifdef DEBUG
        if(m_continuous_spaces.size()!=0) {
            throw "try to call 'init' several times or after 'init_cells'";
            return;
        }
#endif

        // Initialize the continuous spaces informations
        m_continuous_spaces = std::vector<int> {CONTINUOUS_SPACES...};

        // and the size of each variable
        m_var_sizes = Eigen::ArrayXi::Zero(m_continuous_spaces.size());
        size_t var = 0;
        for(int space : m_continuous_spaces) {
            if(space == L2) {
                m_var_sizes[var++] = 1;
            } else if (space == H1) {
                m_var_sizes[var++] = 1;
            } else if (space == H1d) {
                m_var_sizes[var++] = DIM;
            } else {
                throw " Continuous space not recognized ";
                return;
            }
        }

        m_sum_var_sizes = m_var_sizes.sum();

        // creates the polytopal element instantiations
        size_t i = 0;
        for (auto& c : mesh->get_cells()) {
            PolytopalElement<DIM>* pe(new PET<DIM, CONTINUOUS_SPACES...>(c, kb, ks, m_var_sizes));
            m_polytopal_elements[i++] = pe;
            // set the local indexes of all the dof of this Polytopal Element instantiation
            pe->init_dof_local_index();

            // set the stiffness matrix in each pe
            // there is one stiffness matrix by variable
            pe->init_stiffness_matrices(m_var_sizes);
            // set the mass matrix in each pe. It is not used at every simu but almost
            // as it is used to compute the L2 error
            pe->init_mass_matrices(m_var_sizes);
            // set the reconstruction operator in each pe using sometimes the stiffness matrix 
            pe->set_reconstruction_operator(m_var_sizes);
        }

        // set m_are_bulk_dof_eliminable and m_no_bulk_dof, 
        // knowing that the same PE type is used in every cells,
        // so test only over the first PE
        m_no_bulk_dof = (m_polytopal_elements[0]->n_bulk_dof().sum() == 0);
        m_are_bulk_dof_eliminable = (m_polytopal_elements[0]->n_remaining_bulk_dof_sc() == 0);

        // set m_lagrange_multiplier_var_index knowing that the same PE type is used 
        // in every cells, so test only over the first PE
        // the user can also add Lagrange multiplier from the main 
        // if it is not linked to the PE type (Neumann Poisson pb for example)
        if(m_polytopal_elements[0]->lagrange_multiplier()) {
            // if the Lagrange multiplier is used, its var indexes have been stored
            m_lagrange_multiplier_var_index = m_polytopal_elements[0]->lagrange_multiplier_var_index();
        }

        // initialize the type of BC (ESSENTIAL, NATURAL,...) 
        // for all variables and all BC elements
        // with the value NATURAL by default
        this->set_BCtype(mesh, NATURAL);

        t = clock() - t;
        m_init_time = ((double)t)/CLOCKS_PER_SEC;

        return;
    } // init
    
    // Init with a list of cells, not necessary all cells of the mesh
    template<size_t DIM> 
    template<template<size_t, int...> class PET, int... CONTINUOUS_SPACES>
    void DiscreteSpace<DIM>::init_cells(std::vector<MyMesh::Cell<DIM>*> cells, int kb, int ks) {

        clock_t t;
        t = clock();

        if(ks == -20) { ks = kb; } // better way to define that ks is otional ?

        // init cells migth be called several times, 
        // thus init m_continuous_spaces only if not already done
        if(m_continuous_spaces.size()==0) {
            // Initialize the continuous spaces informations
            m_continuous_spaces = std::vector<int> {CONTINUOUS_SPACES...};

            // and the size of each variable
            m_var_sizes = Eigen::ArrayXi::Zero(m_continuous_spaces.size());
            int var = 0;
            for(int space : m_continuous_spaces) {
                if(space == L2) {
                    m_var_sizes[var++] = 1;
                } else if (space == H1) {
                    m_var_sizes[var++] = 1;
                } else if (space == H1d) {
                    m_var_sizes[var++] = DIM;
                } else {
                    throw " Continuous space not recognized ";
                    return;
                }
            }

            m_sum_var_sizes = m_var_sizes.sum();

            // initialize the type of BC (ESSENTIAL, NATURAL,...) 
            // for all variables and all elements
            // with the value NATURAL
            this->set_BCtype(cells, NATURAL);

#ifdef DEBUG
        } else {
            // Test if the continuous spaces are the same in every call of this function
            // std::vector<int> tmp_continuous_spaces(std::vector<int> {CONTINUOUS_SPACES...});
            if(m_continuous_spaces != std::vector<int> {CONTINUOUS_SPACES...}) {
                throw " init_cells called with different continuous spaces";
                return;
            }
#endif
        }

        // creates the polytopal element instantiations
        for (auto& c : cells) {
            PolytopalElement<DIM>* pe(new PET<DIM, CONTINUOUS_SPACES...>(c, kb, ks, m_var_sizes));
#ifdef DEBUG
            // prevent from defining more than one PE in a cell
            if(m_polytopal_elements.at(c->global_index()) != nullptr) 
                throw "In 'init_cells', trying to define a new PE for a cell which already has one";
#endif
            m_polytopal_elements[c->global_index()] = pe;

            // Init the dof local index in each cell
            pe->init_dof_local_index();
        }

        // set m_are_bulk_dof_eliminable and m_no_bulk_dof, 
        // knowing that NOT the same PE tye is used in every cells,
        // so test over the first cell of this group and with the previous value of m_are_bulk_dof_eliminable
        size_t c_gi(cells[0]->global_index());
        m_no_bulk_dof = m_no_bulk_dof && (m_polytopal_elements[c_gi]->n_bulk_dof().sum() == 0);
        m_are_bulk_dof_eliminable = m_are_bulk_dof_eliminable && 
                (m_polytopal_elements[c_gi]->n_remaining_bulk_dof_sc() == 0);

        t = clock() - t;
        m_init_time += ((double)t)/CLOCKS_PER_SEC;

        return;
    } // init_cells

    template<size_t DIM> 
    void DiscreteSpace<DIM>::add_stiffness_contribution(size_t var_index, double mult_cst) {

        clock_t t;
        t = clock();
        
        for (auto& pe : m_polytopal_elements) {
            pe->add_stiffness_contribution(var_index, mult_cst);
        }

        t = clock() - t;
        m_local_system_time += ((double)t)/CLOCKS_PER_SEC;

        return;
    } // add_stiffness_contribution

    template<size_t DIM> 
    void DiscreteSpace<DIM>::add_mass_contribution(size_t var_index, double mult_cst) {

        clock_t t;
        t = clock();

        for (auto& pe : m_polytopal_elements) {
            pe->add_mass_contribution(var_index, mult_cst);
        }

        t = clock() - t;
        m_local_system_time += ((double)t)/CLOCKS_PER_SEC;

        return;
    } // add_mass_contribution

    template<size_t DIM> 
    void DiscreteSpace<DIM>::add_divergence_contribution(
                size_t var_index1, 
                size_t var_index2, 
                double mult_cst) {
        clock_t t;
        t = clock();

        double measure(m_mesh->measure());

        for (auto& pe : m_polytopal_elements) {
            pe->add_divergence_contribution(var_index1, var_index2, measure, mult_cst);
        }

        t = clock() - t;
        m_local_system_time += ((double)t)/CLOCKS_PER_SEC;

        return; 
    } // add_divergence_contribution


    // set all local rhs with specific quadra order
    template<size_t DIM> 
    template<typename RET>
    inline void DiscreteSpace<DIM>::set_rhs(RET func (Eigen::ArrayXd), int quadra_order,
        size_t var_index) {

        clock_t t;
        t = clock();

        for (auto& pe : m_polytopal_elements) {
            pe->set_rhs(func, quadra_order, var_index);
        }

        t = clock() - t;
        m_local_system_time += ((double)t)/CLOCKS_PER_SEC;
        
        return;
    } // set_rhs
    
    template<size_t DIM> 
    void DiscreteSpace<DIM>::static_condensation() {

        clock_t t;
        t = clock();

        for (auto& pe : m_polytopal_elements) {
            pe->static_condensation();
        }

        t = clock() - t;
        m_local_system_time += ((double)t)/CLOCKS_PER_SEC;
        
        return;
    } // static_condensation

    template<size_t DIM> 
    void DiscreteSpace<DIM>::reconstruction(std::vector<std::vector<size_t>>& loc2glo,
                                            Eigen::VectorXd& sol_values,
                                            bool static_cond) {
        size_t n_variables(this->n_var()); // nb of variables in the pb
        for (auto& pe : m_polytopal_elements) {
            size_t c_gi(pe->get_cell()->global_index());
            pe->reconstruction(n_variables, loc2glo[c_gi], sol_values, static_cond);
        }

        return;
    } // reconstruction

    template<size_t DIM> 
    std::vector<PolytopalElement<DIM> *> DiscreteSpace<DIM>::get_polytopal_elements() const {
        return m_polytopal_elements;
    }

    template<size_t DIM> 
    PolytopalElement<DIM>* DiscreteSpace<DIM>::polytopal_element(size_t g_i) const {
#ifdef DEBUG
        return m_polytopal_elements.at(g_i);
#else
        return m_polytopal_elements[g_i];
#endif
    }

    template<size_t DIM> 
    size_t DiscreteSpace<DIM>::n_var() const { return m_continuous_spaces.size(); }

    template<size_t DIM> 
    Eigen::ArrayXi DiscreteSpace<DIM>::var_sizes() const { return m_var_sizes; }

    template<size_t DIM> 
    int DiscreteSpace<DIM>::sum_var_sizes() const { return m_sum_var_sizes; }

    template<size_t DIM> 
    bool DiscreteSpace<DIM>::are_bulk_dof_eliminable() const { return m_are_bulk_dof_eliminable; }

    template<size_t DIM> 
    bool DiscreteSpace<DIM>::no_bulk_dof() const { return m_no_bulk_dof; }

    template<size_t DIM> 
    bool DiscreteSpace<DIM>::lagrange_multiplier() const { 
        return (!m_lagrange_multiplier_var_index.empty()); 
    }

    template<size_t DIM> 
    const std::vector<size_t>& DiscreteSpace<DIM>::lagrange_multiplier_var_index() const { 
        return m_lagrange_multiplier_var_index; 
    }

    // the user can also add Lagrange multiplier from the main 
    // if it is not linked to the PE type (Neumann Poisson pb for example)
    template<size_t DIM> 
    void DiscreteSpace<DIM>::add_lagrange_multiplier(size_t var_index) { 
        // if var_index not already in m_lagrange_multiplier_var_index, add it
        // this check is important as the user can add the variable 
        // (which may already be stored)
        // and if a variable is present twice, the global matrix is not anymore inversible
        if(m_lagrange_multiplier_var_index.empty()) { 
            m_lagrange_multiplier_var_index.push_back(var_index); 
        } else if ( std::find(m_lagrange_multiplier_var_index.begin(), 
                m_lagrange_multiplier_var_index.end(), var_index) != m_lagrange_multiplier_var_index.end() ) { 
            m_lagrange_multiplier_var_index.push_back(var_index); 
        }
        return;
    }

    template<size_t DIM> 
    inline void DiscreteSpace<DIM>::set_stabilization_type(std::string stabilization_type) {

        for(auto& pe : m_polytopal_elements) { 
            pe->set_stabilization_type(stabilization_type);
        }
        return;
    
    } // set_stabilization_type

    template<size_t DIM> 
    inline void DiscreteSpace<DIM>::set_BCtype(const std::vector<MyMesh::Cell<DIM>*> cells, 
                    int bound_type,
                    std::function<bool(Eigen::ArrayXd)> condition) {

        clock_t t;
        t = clock();

        for(auto& cpt : cells) {
            //      vertices
            this->set_BCtype_by_elements(cpt->get_vertices(), bound_type, condition);
            //      edges
            if(DIM == 3) {
                this->set_BCtype_by_elements(cpt->get_edges(), bound_type, condition);
            }
            //      faces
            this->set_BCtype_by_elements(cpt->get_faces(), bound_type, condition);
        }

        t = clock() - t;
        m_local_system_time += ((double)t)/CLOCKS_PER_SEC;

        return;
    } // set_BCtype

    template<size_t DIM> 
    inline void DiscreteSpace<DIM>::set_BCtype(const MyMesh::Mesh<DIM>* mesh, 
                    int bound_type,
                    std::function<bool(Eigen::ArrayXd)> condition) {
        return set_BCtype(mesh->get_bound_cells(), bound_type, condition);
    }

    template<size_t DIM> 
    template<typename ELEMT>
    inline void DiscreteSpace<DIM>::set_BCtype_by_elements(
                            const std::vector<ELEMT>& elements, 
                            int bound_type,
                            std::function<bool(Eigen::ArrayXd)> condition) {

        size_t n_variables(this->n_var());
        
        for(auto& e : elements) { // loop over the list of elements
            if(e->is_boundary()) {
                if( condition(e->mass_center()) ) {
                    // this BC is set for all variables whose mass center respect a certain condition
                    std::vector<int> bc(n_variables, bound_type);
                    e->set_bc_type(bc);
                }
            } else {
                // this element is set to be non-BC for all variables
                // probably not useful, could be removed
                std::vector<int> bc(n_variables, NO_BC);
                e->set_bc_type(bc);
            }
        }
        return;
    } // set_BCtype_by_elements with condition on the mass center coord

    template<size_t DIM> 
    inline void DiscreteSpace<DIM>::set_BCtype_by_elements( // spe for vertices
                            const std::vector<MyMesh::Vertex<DIM>*>& vertices, 
                            int bound_type,
                            std::function<bool(Eigen::ArrayXd)> condition) {

        size_t n_variables(this->n_var());
        
        for(auto& v : vertices) { // loop over the list of vertices
            if(v->is_boundary()) {
                if( condition(v->coords()) ) { // condition is tested over the vertex coordinates
                    // this BC is set for all variables
                    std::vector<int> bc(n_variables, bound_type);
                    v->set_bc_type(bc);
                }
            } else {
                // this vertex is set to be non-BC for all variables
                // probably not useful, could be removed
                std::vector<int> bc(n_variables, NO_BC);
                v->set_bc_type(bc);
            }
        }
        return;
    } // set_BCtype_by_elements with condition on the vertex coord

    template<size_t DIM> 
    inline void DiscreteSpace<DIM>::set_BCtype(const std::vector<MyMesh::Cell<DIM>*> cells, 
            size_t var_index, int bound_type, std::function<bool(Eigen::ArrayXd)> condition) {

        clock_t t;
        t = clock();

        for(auto& cpt : cells) {
            //      vertices
            this->set_BCtype_by_elements(cpt->get_vertices(), var_index, bound_type, condition);
            //      edges
            if(DIM == 3) {
                this->set_BCtype_by_elements(cpt->get_edges(), var_index, bound_type, condition);
            }
            //      faces
            this->set_BCtype_by_elements(cpt->get_faces(), var_index, bound_type, condition);
        }

        t = clock() - t;
        m_local_system_time += ((double)t)/CLOCKS_PER_SEC;

        return;
    } // set_BCtype for only one variable

    template<size_t DIM> 
    inline void DiscreteSpace<DIM>::set_BCtype(const MyMesh::Mesh<DIM>* mesh, 
            size_t var_index, int bound_type, std::function<bool(Eigen::ArrayXd)> condition) {
        return set_BCtype(mesh->get_bound_cells(), var_index, bound_type, condition);
    }

    template<size_t DIM> 
    template<typename ELEMT>
    inline void DiscreteSpace<DIM>::set_BCtype_by_elements(
                            const std::vector<ELEMT>& elements, 
                            size_t var_index,
                            int bound_type,
                            std::function<bool(Eigen::ArrayXd)> condition) {
        
        for(auto& e : elements) { // loop over the list of elements
            if(e->is_boundary() && condition(e->mass_center())) {
                // this BC is set for the variable var_index
                e->set_bc_type(bound_type, var_index);
            }
        }
        return;
    } // set_BCtype_by_elements with condition on the mass center coord

    template<size_t DIM> 
    inline void DiscreteSpace<DIM>::set_BCtype_by_elements( // spe for vertices
                            const std::vector<MyMesh::Vertex<DIM>*>& vertices, 
                            size_t var_index,
                            int bound_type,
                            std::function<bool(Eigen::ArrayXd)> condition) {
        
        for(auto& v : vertices) { // loop over the list of vertices
            if(v->is_boundary() && condition(v->coords())) {
                // this BC is set for the variable var_index
                v->set_bc_type(bound_type, var_index);
            }
        }
        return;
    } // set_BCtype_by_elements with condition on the vertex coord

    template<size_t DIM> 
    template<typename RET>
    void DiscreteSpace<DIM>::set_local_DirichletBC(RET func (Eigen::ArrayXd), 
                                               size_t var_index) {
        clock_t t;
        t = clock();

        for(auto& pe : m_polytopal_elements) { 
            pe->set_Dirichlet_lifting(func, var_index);
        }

        t = clock() - t;
        m_local_system_time += ((double)t)/CLOCKS_PER_SEC;
        
        return;
    } // set_local_DirichletBC

    template<size_t DIM> 
    template<typename RET>
    void DiscreteSpace<DIM>::non_homogeneous_NeumannBC(RET func (Eigen::ArrayXd), 
                                               size_t var_index) {
        clock_t t;
        t = clock();

        for(auto& pe : m_polytopal_elements) { 
            pe->set_NeumannBC(func, var_index);
        }

        t = clock() - t;
        m_local_system_time += ((double)t)/CLOCKS_PER_SEC;
        
        return;
    } // non_homogeneous_NeumannBC

    template<size_t DIM> 
    void DiscreteSpace<DIM>::add_DirichletBC_rhs() {

        clock_t t;
        t = clock();

        for(auto& pe : m_polytopal_elements) { 
            pe->add_DirichletBC_rhs();
        }

        t = clock() - t;
        m_local_system_time += ((double)t)/CLOCKS_PER_SEC;

        return;
    } // add_DirichletBC_rhs

    template<size_t DIM> 
    Eigen::MatrixXd DiscreteSpace<DIM>::reconstruction_at_vertices(
                    const MyMesh::Mesh<DIM>* mesh, bool conforming_method, size_t var_index) {

        size_t var_size(m_var_sizes[var_index]); // 1 if scalar, >1 if vectorial
        Eigen::MatrixXd numerical_sol_vertices;
        
        if (conforming_method) {

            // get only one value by vertex because the method is conforming
            numerical_sol_vertices.resize(var_size, mesh->n_vertices());

#ifdef DEBUG
            std::cout << "-------------------------------------------------" << std::endl;
            std::cout << " Careful, this computation reconstruction of the " << std::endl;
            std::cout << " vertices values supposes the method is conforming ! " << std::endl;
            std::cout << "-------------------------------------------------" << std::endl;
#endif

            size_t visu_point_gi(0);
            for(auto& v : mesh->get_vertices()) {
                MyMesh::Cell<DIM>* c(v->get_neighbour_cells()[0]); // necessary to find the Polytopal Element class
                PolytopalElement<DIM>* pe(m_polytopal_elements[c->global_index()]);
                // get the (scalar or vectorial) value of the numerical solution at the vertex
                numerical_sol_vertices.col(visu_point_gi++) = 
                    pe->reconstruction_value_at_pt(v->coords(), var_index);
            }

        } else {

            // save one value by vertex by cell
            // first find size
            size_t n_visu_points(0);
            for(auto& c : mesh->get_cells()) {
                n_visu_points += c->n_vertices();
            }
            numerical_sol_vertices.resize(var_size, n_visu_points);

            size_t visu_point_gi(0);
            // Careful : if you change the order of the values in numerical_sol_vertices,
            // the visu file vtk_writer must be modified !
            // for each cell
            for(auto& c : mesh->get_cells()) {
                PolytopalElement<DIM>* pe(m_polytopal_elements[c->global_index()]);
                // store the values at all vertices
                for(auto& v : c->get_vertices()) {
                    // get the (scalar or vectorial) value of the numerical solution at the vertex
                    numerical_sol_vertices.col(visu_point_gi++) = 
                        pe->reconstruction_value_at_pt(v->coords(), var_index);
                }
            }

        } // distinction between conforming and non conforming method

        return numerical_sol_vertices;
    } // reconstruction_at_vertices

    template<size_t DIM> 
    Eigen::MatrixXd DiscreteSpace<DIM>::bulk_reconstruction_at_vertices(
                    const MyMesh::Mesh<DIM>* mesh, bool conforming_method, size_t var_index) {

        size_t var_size(m_var_sizes[var_index]); // 1 if scalar, >1 if vectorial
        Eigen::MatrixXd numerical_sol_vertices;
        
        if (conforming_method) {

            // get only one value by vertex because the method is conforming
            numerical_sol_vertices.resize(var_size, mesh->n_vertices());

#ifdef DEBUG
            std::cout << "-------------------------------------------------" << std::endl;
            std::cout << " Careful, this computation reconstruction of the " << std::endl;
            std::cout << " vertices values supposes the method is conforming ! " << std::endl;
            std::cout << "-------------------------------------------------" << std::endl;
#endif

            size_t visu_point_gi(0);
            for(auto& v : mesh->get_vertices()) {
                MyMesh::Cell<DIM>* c(v->get_neighbour_cells()[0]); // necessary to find the Polytopal Element class
                PolytopalElement<DIM>* pe(m_polytopal_elements[c->global_index()]);
                // get the (scalar or vectorial) value of the numerical solution at the vertex
                numerical_sol_vertices.col(visu_point_gi++) = 
                    pe->bulk_reconstruction_value_at_pt(v->coords(), var_index);
            }

        } else {

            // save one value by vertex by cell
            // first find size
            size_t n_visu_points(0);
            for(auto& c : mesh->get_cells()) {
                n_visu_points += c->n_vertices();
            }
            numerical_sol_vertices.resize(var_size, n_visu_points);

            size_t visu_point_gi(0);
            // Careful : if you change the order of the values in numerical_sol_vertices,
            // the visu file vtk_writer must be modified !
            // for each cell
            for(auto& c : mesh->get_cells()) {
                PolytopalElement<DIM>* pe(m_polytopal_elements[c->global_index()]);
                // store the values at all vertices
                for(auto& v : c->get_vertices()) {
                    // get the (scalar or vectorial) value of the numerical solution at the vertex
                    numerical_sol_vertices.col(visu_point_gi++) = 
                        pe->bulk_reconstruction_value_at_pt(v->coords(), var_index);
                }
            }

        } // distinction between conforming and non conforming method

        return numerical_sol_vertices;
    } // bulk_reconstruction_at_vertices

    template<size_t DIM> 
    inline double DiscreteSpace<DIM>::get_init_time() const { return m_init_time; }

    template<size_t DIM> 
    inline double DiscreteSpace<DIM>::get_local_system_time() const { return m_local_system_time; }

} // MyDiscreteSpace namespace
#endif /* DISCRETE_SPACE_HPP */
