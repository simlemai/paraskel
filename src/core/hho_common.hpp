/**
  * @{
*/

#ifndef HHO_BASE_HPP
#define HHO_BASE_HPP

#include <iostream>
#include <cstring>
#include <vector>
#include <Eigen/Dense>
#include "common_geometry.hpp"
#include "polytopal_element.hpp"
#include "cell.hpp"

namespace MyDiscreteSpace {

    // number of dof in each face (edges and vertices excluded)
    // in the HHO method when the variable is scalar 
    // = dim Pk on DIM-1 objects
    // (will be multiplied by the size of each variable)
    template<size_t DIM> 
    inline int n_dof_faces_pk(int skel_order) {

        if(DIM == 2) {
            return skel_order + 1;
        } else if(DIM == 3) {
            return (skel_order + 2) * (skel_order + 1) / 2;
        }

    } // n_dof_faces_pk with one variable

    // number of bulk dof
    // in the HHO method when the variable is scalar 
    // = dim Pk on DIM objects
    // (will be multiplied by the size of each variable)
    template<size_t DIM> 
    inline int n_dof_bulk_pk(int bulk_order) {

        if(DIM == 2) {
            return (bulk_order + 2) * (bulk_order + 1) / 2;
        } else if(DIM == 3) {
            return (bulk_order + 1) * (bulk_order + 2) * (bulk_order + 3) / 6;
        }

    } // n_dof_bulk_pk with one variable


    /**
     * The HHOBase class contains 
     * some common informations on HHO 
     * (orthogonal l2 projections, stabilization,...). 
     * It inherits of the PolytopalElement class
    */
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    class HHOBase : public PolytopalElement<DIM>
    {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param n_dof_face face number of dof for each variable
         * @param n_dof_edge edge number of dof for each variable
         * @param n_dof_vertex vertex number of dof for each variable
         * @param n_dof_bulk bulk number of dof for each variable
         * @param n_bulk_dof_eliminable bulk eliminable number of dof for each variable
         */
        HHOBase(MyMesh::Cell<DIM>* cell, 
                const Eigen::ArrayXi& n_dof_face, 
                const Eigen::ArrayXi& n_dof_edge, 
                const Eigen::ArrayXi& n_dof_vertex,
                const Eigen::ArrayXi& n_dof_bulk, 
                const Eigen::ArrayXi& n_bulk_dof_eliminable = Eigen::ArrayXi::Constant(1, INT_MAX));
        ~HHOBase() = default;   ///<  Default destructor

        /**
         * Add the stabilization term of the diffusion part to the local matrix
         * @param var_index corresponding index of the variable
         * @param mult_cst multiplicative constant
         */
        void add_stabilization(size_t var_index, double mult_cst);

        /**
         * Compute the Lehrenfeld-Schöberl stabilization 
         * @param var_index index of the corresponding variable
         */
        Eigen::MatrixXd ls_stabilization(
                size_t var_index = 0);

        /**
         * Compute the equal-order stabilization 
         * @param var_index index of the corresponding variable
         */
        Eigen::MatrixXd equal_order_stabilization(
                size_t var_index = 0);

        /**
         * Compute the reconstruction operator
         * @param var_sizes array with the dimension of each variable
         */
        virtual void set_reconstruction_operator(const Eigen::ArrayXi& var_sizes) = 0;

        /**
         * Compute the reduction operator (scalar variable)
         * @param func continuous function to which extract reduction
         * @param var_index corresponding index of the variable
         * @param only_bound_elemt apply the reduction operator only at the boundary elements
         */
        virtual Eigen::VectorXd apply_reduction_operator(double func (Eigen::ArrayXd),
                size_t var_index = 0, bool only_bound_elemt = false) = 0;
        /**
         * Compute the reduction operator (vectorial variable)
         * @param func continuous function to which extract reduction
         * @param var_index corresponding index of the variable
         * @param only_bound_elemt apply the reduction operator only at the boundary elements
         */
        virtual Eigen::VectorXd apply_reduction_operator(std::vector<double> func (Eigen::ArrayXd),
                size_t var_index = 0, bool only_bound_elemt = false) = 0;

        /**
         * Add the stiffness contribution multiplied
         * by a constant
         * to the local matrix
         * @param var_index index of the variable to add the mass contribution
         * @param mult_cst multiplicative constant
         */
        virtual void add_stiffness_contribution(size_t var_index = 0, double mult_cst = 1.) = 0; 

        /**
         * Add the mass contribution multiplied
         * by a constant
         * to the local matrix
         * @param var_index index of the variable to add the mass contribution
         * @param mult_cst multiplicative constant
         */
        virtual void add_mass_contribution(size_t var_index = 0, double mult_cst = 1.) = 0; 

        /**
         * Add the divergence of a crossed term
         * k * \int_T ru_T div(u_T) * rq_T q_T dx
         * to each local matrix, knowing the indexes of the variables u_T and q_T
         * @param var_index1 index of the variable to multiply on the left
         * @param var_index2 index of the variable to multiply on the right
         * @param mesh_measure measure of the global domain
         * @param mult_cst multiplicative constant
         */
        virtual void add_divergence_contribution(
                size_t var_index1, 
                size_t var_index2, 
                double mesh_measure,
                double mult_cst = 1.) = 0;
                
        /**
         * Set the local right-hand-side
         * from a continuous function
         * with specific quadrature order 
         * (6 by default)
         * For a scalar variable or vectorial one
         * @param func continuous function with the rhs
         * @param quadra_order specific quadrature order 
         */
        virtual void set_rhs(double func (Eigen::ArrayXd), 
            int quadra_order, size_t var_index) = 0; 
        virtual void set_rhs(std::vector<double> func (Eigen::ArrayXd), 
            int quadra_order, size_t var_index) = 0; 

    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////

    // constructor for HHOBase (has access to the constructor of PolytopalElement).
    template<size_t DIM, int... CONTINUOUS_SPACES>  
    HHOBase<DIM, CONTINUOUS_SPACES...>::HHOBase(
                        MyMesh::Cell<DIM>* cell, 
                        const Eigen::ArrayXi& n_dof_face, 
                        const Eigen::ArrayXi& n_dof_edge, 
                        const Eigen::ArrayXi& n_dof_vertex,
                        const Eigen::ArrayXi& n_dof_bulk, 
                        const Eigen::ArrayXi& n_bulk_dof_eliminable) :
    PolytopalElement<DIM>(cell, n_dof_face, n_dof_edge, n_dof_vertex, 
                        n_dof_bulk, n_bulk_dof_eliminable) {}


    template<size_t DIM, int... CONTINUOUS_SPACES> 
    void HHOBase<DIM, CONTINUOUS_SPACES...>::add_stabilization(
                    size_t var_index, double mult_cst) {

        if(this->m_stabilization_type ==  "ls_stabilization") {
            this->m_local_bilinear_form += mult_cst * ls_stabilization(var_index);

#ifdef DEBUG
    if(this->m_cell->global_index() == 0) {
        std::cout << "In HHO, Lehrenfeld-Schöberl stabilization is used. " << std::endl;
    }
#endif
        } else {
            this->m_local_bilinear_form += mult_cst * equal_order_stabilization(var_index);

#ifdef DEBUG
    if(this->m_cell->global_index() == 0) {
        std::cout << "In HHO, equal-order stabilization is used. " << std::endl;
    }
#endif
        }
                        
        return;
    } // add_stabilization


    // Compute the Lehrenfeld-Schöberl stabilization
    // to penalize the difference : v_F - l2proj_F(v_T)
    // when bulk_degree = skel_degree + 1
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    Eigen::MatrixXd HHOBase<DIM, CONTINUOUS_SPACES...>::ls_stabilization(
                    size_t var_index) {
        
        Eigen::MatrixXd stab(Eigen::MatrixXd::Zero(
            this->m_n_cumulate_local_dof, this->m_n_cumulate_local_dof));

        // local indexes of the bulk dof
        size_t bulk_start(this->m_bulk_dof[var_index][0]);
        size_t bulk_length(this->m_n_bulk_dof(var_index));

        std::vector<MyMesh::Face<DIM>*> faces(this->m_cell->get_faces());
        size_t f_li(0); // local index of face
        for(auto& fpt : faces) {

            // local indexes of the dof of this face
            size_t face_start(this->m_faces_dof[var_index][f_li][0]);
            size_t face_length(this->m_n_faces_dof(f_li, var_index));
            
            // trace matrix : int over the face of m_skel_bases * m_bulk_bases
            Eigen::MatrixXd trace_f(Eigen::MatrixXd::Zero(face_length, bulk_length));
            size_t quadra_order(this->m_skeletal_degree + this->m_bulk_degree);
            MyQuadra::QuadraturePoints<DIM> f_qps(MyQuadra::integrate(fpt, quadra_order));
            for(auto& qp : f_qps.list()) { // int over F
                // Vectors with the values of m_bulk_bases[var_index] basis functions
                // and of m_faces_bases[var_index] basis functions
                // evaluated at quadrature point
                Eigen::MatrixXd func_bulk_basis(
                    this->m_bulk_bases[var_index]->eval_functions(qp->point()));
                Eigen::MatrixXd func_face_basis(
                    this->m_faces_bases[var_index][f_li]->eval_functions(qp->point()));

                trace_f += qp->weight() * func_face_basis.transpose() * func_bulk_basis;
            } // quadra

            // compute the mass matrix of the face basis functions
            quadra_order = 2 * this->m_skeletal_degree;
            Eigen::MatrixXd mass_matrix_f(
                this->compute_mass_matrix(fpt, this->m_faces_bases[var_index][f_li], quadra_order));

            // v_F - l2proj_F(v_T)
            Eigen::MatrixXd stab_f(Eigen::MatrixXd::Zero(face_length, this->m_n_cumulate_local_dof));
            // v_F : identity at the corresponding dof indexes
            stab_f.block(0, face_start, face_length, face_length) = 
                Eigen::MatrixXd::Identity(face_length, face_length);
            // - l2proj_F(v_T) : mass_matrix_f.inverse() * trace_f at the bulk dof indexes
            stab_f.block(0, bulk_start, face_length, bulk_length) -= mass_matrix_f.inverse() * trace_f;

            // add the contribution to the global LS stabilization
            stab += 1./fpt->diam() * stab_f.transpose() * mass_matrix_f * stab_f;

            f_li++; // next face
        }

        return stab;
    } // ls_stabilization

    // Compute the equal-order stabilization
    // to penalize the difference : v_F - l2proj_F(v_T)
    // when bulk_degree = skel_degree
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    Eigen::MatrixXd HHOBase<DIM, CONTINUOUS_SPACES...>::equal_order_stabilization(
                    size_t var_index) {
        
        Eigen::MatrixXd stab(Eigen::MatrixXd::Zero(
            this->m_n_cumulate_local_dof, this->m_n_cumulate_local_dof));

        // local indexes of the bulk dof
        size_t bulk_start(this->m_bulk_dof[var_index][0]);
        size_t bulk_length(this->m_n_bulk_dof(var_index));
        size_t cell_dim(this->m_cell_bases[var_index]->dimension());

        Eigen::MatrixXd trace_bulk(Eigen::MatrixXd::Zero(bulk_length, cell_dim));
        // int over the cell T to compute l2_proj_Bulk(r_T v_T)
        size_t quadra_order(this->m_bulk_degree + this->m_cell_degree);
        MyQuadra::QuadraturePoints<DIM> t_qps(MyQuadra::integrate(this->m_cell, quadra_order));
        for(auto& qp : t_qps.list()) { // int over T
            // Vectors with the values of m_bulk_bases[var_index] basis functions
            // and of this->m_cell_bases[var_index] basis functions
            // evaluated at quadrature point
            Eigen::MatrixXd func_bulk_basis(
                this->m_bulk_bases[var_index]->eval_functions(qp->point()));
            Eigen::MatrixXd func_cell_basis(
                this->m_cell_bases[var_index]->eval_functions(qp->point()));

            trace_bulk += qp->weight() * func_bulk_basis.transpose() * func_cell_basis;
        } // quadra

        // compute the mass matrix of the bulk basis functions
        quadra_order = 2 * this->m_bulk_degree;
        Eigen::MatrixXd inv_mass_matrix_t(
            this->compute_mass_matrix(this->m_cell, this->m_bulk_bases[var_index], quadra_order).inverse());

        // l2_proj_Bulk(r_T v_T)
        Eigen::MatrixXd proj_t(
            inv_mass_matrix_t * trace_bulk * this->m_reconstruction_operators[var_index]);
        // l2_proj_Bulk(r_T v_T) - v_bulk
        proj_t.block(0, bulk_start, bulk_length, bulk_length) -=
            Eigen::MatrixXd::Identity(bulk_length, bulk_length);

        std::vector<MyMesh::Face<DIM>*> faces(this->m_cell->get_faces());
        size_t f_li(0); // local index of face
        for(auto& fpt : faces) {

            // local indexes of the dof of this face
            size_t face_start(this->m_faces_dof[var_index][f_li][0]);
            size_t face_length(this->m_n_faces_dof(f_li, var_index));
            
            // trace matrix : int over the face of m_skel_bases * m_bulk_bases
            // trace matrix : int over the face of m_skel_bases * m_cell_bases
            Eigen::MatrixXd trace_f_cell(Eigen::MatrixXd::Zero(face_length, cell_dim));
            Eigen::MatrixXd trace_f_bulk(Eigen::MatrixXd::Zero(face_length, bulk_length));
            // necessary to integrate polynomials of order m_skeletal_degree + this->m_cell_degree
            // and m_skeletal_degree + m_bulk_degree, use the max
            quadra_order = this->m_skeletal_degree + 
                std::max(this->m_cell_degree, this->m_bulk_degree);
            MyQuadra::QuadraturePoints<DIM> f_qps(MyQuadra::integrate(fpt, quadra_order));
            for(auto& qp : f_qps.list()) { // int over F
                // Vectors with the values of m_bulk_bases[var_index] basis functions
                // and of m_faces_bases[var_index] basis functions
                // evaluated at quadrature point
                Eigen::MatrixXd func_bulk_basis(
                    this->m_bulk_bases[var_index]->eval_functions(qp->point()));
                Eigen::MatrixXd func_face_basis(
                    this->m_faces_bases[var_index][f_li]->eval_functions(qp->point()));

                trace_f_bulk += qp->weight() * func_face_basis.transpose() * func_bulk_basis;

                // Vectors with the values of this->m_cell_bases[var_index] basis functions
                // evaluated at quadrature point
                Eigen::MatrixXd func_cell_basis(
                    this->m_cell_bases[var_index]->eval_functions(qp->point()));

                trace_f_cell += qp->weight() * func_face_basis.transpose() * func_cell_basis;
            } // quadra

            // compute the mass matrix of the face basis functions
            quadra_order = 2 * this->m_skeletal_degree;
            Eigen::MatrixXd mass_matrix_f(
                this->compute_mass_matrix(fpt, this->m_faces_bases[var_index][f_li], quadra_order));
            Eigen::MatrixXd inv_mass_matrix_f(mass_matrix_f.inverse());

            // l2proj_F(r_T v_T)
            Eigen::MatrixXd proj1(
                - trace_f_cell * this->m_reconstruction_operators[var_index]);

            // l2proj_F(l2_proj_Bulk(r_T v_T) - v_bulk)
            Eigen::MatrixXd proj2(trace_f_bulk * proj_t);

            Eigen::MatrixXd stab_f(Eigen::MatrixXd::Zero(face_length, this->m_n_cumulate_local_dof));
            // v_F : identity at the corresponding dof indexes
            stab_f.block(0, face_start, face_length, face_length) = 
                Eigen::MatrixXd::Identity(face_length, face_length);

            stab_f += inv_mass_matrix_f * (proj1 + proj2);

            // add the contribution to the global equal-order stabilization
            stab += 1./fpt->diam() * stab_f.transpose() * mass_matrix_f * stab_f;

            f_li++; // next face
        }

        return stab;
    } // equal_order_stabilization

} // MyDiscreteSpace namespace
#endif /* HHO_BASE_HPP */
/** @} */
