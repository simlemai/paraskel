/**
  * @{
*/

#ifndef CONFORMING_VEM
#define CONFORMING_VEM

#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include "polytopal_element.hpp"
#include "cell.hpp"

namespace MyDiscreteSpace {
    /**
     * The VEM class contains 
     * the specific informations for conforming VEM. 
     * It inherits of the PolytopalElement class
    */
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    class VEMBase : public PolytopalElement<DIM>
    {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param kb order of the bulk discrete unknown
         * @param ks order of the polynomial in the skeletal objects, the order of the reconstruction is ks
         * @param var_sizes array with the dimensions of all the variables
         * @param n_dof_face face number of dof
         * @param n_dof_edge edge number of dof
         * @param n_dof_vertex vertex number of dof
         * @param n_dof_bulk bulk number of dof
         */
        VEMBase(MyMesh::Cell<DIM>* cell, 
                int kb, int ks,
                const Eigen::ArrayXi& var_sizes,
                int n_dof_face, 
                int n_dof_edge, 
                int n_dof_vertex, 
                int n_dof_bulk); // all bulk dof are eliminable by default
        virtual ~VEMBase();   ///<  Destructor

        /**
         * Compute the reconstruction operator with default basis
         * @param var_sizes array with the dimension of each variable
         */
        virtual void set_reconstruction_operator(const Eigen::ArrayXi& var_sizes) = 0;

        /**
         * For the edge : 1D object (face in 2D meshes or edge in 3D meshes)
         * computes the reconstruction and evaluates it
         * at the quadrature points.
         * (This is what is necessary to compute the reconstruction 
         * of the other objects.)
         * @param var_size dimension of the variable
         * @param var_index index of the variable to add the stiffness contribution
         * @param element_dof local index of the element dof
         * @param element_dof_basis basis of the dof edge (degree ks - 2)
         * @param element_basis basis of the edge (degree ks)
         * @param reconstruction_operator_edges fill the reconstruction operator along each edges
         */
        template<typename ELEMT, typename ELEMT_BASIS>
        inline void set_reconstruction_operator_edges(
            size_t var_size,
            size_t var_index,
            const ELEMT element,
            const std::vector<size_t>& element_dof,
            const ELEMT_BASIS element_dof_basis,
            const ELEMT_BASIS element_basis,
            std::vector<Eigen::MatrixXd>& reconstruction_operator_edges);

        /**
         * Using the stiffness matrix, add
         * k * \int_T grad(r_T u_T) * grad(r_T v_T) dx
         * to the local matrix
         * @param var_index index of the variable to add the stiffness contribution
         * @param mult_cst multiplicative constant
         */
        inline void add_stiffness_contribution(size_t var_index = 0, double mult_cst = 1.);

        /**
         * Using the mass matrix, add
         * k * \int_T r_T u_T * r_T v_T dx
         * to the local matrix
         * @param var_index index of the variable to add the stiffness contribution
         * @param mult_cst multiplicative constant
         */
        inline void add_mass_contribution(size_t var_index = 0, double mult_cst = 1.);

        /**
         * Compute the reduction operator (scalar variable)
         * @param func continuous function to which extract reduction
         * @param only_bound_elemt boolean, if true apply reduction operator only on boundary elements
         */
        Eigen::VectorXd apply_reduction_operator(double func (Eigen::ArrayXd),
                size_t var_index = 0,
                bool only_bound_elemt = false);
        /**
         * Compute the reduction operator (vectorial variable)
         * @param func continuous function to which extract reduction
         * @param only_bound_elemt boolean, if true apply reduction operator only on boundary elements
         */
        Eigen::VectorXd apply_reduction_operator(std::vector<double> func (Eigen::ArrayXd),
                size_t var_index = 0,
                bool only_bound_elemt = false);
                
        /**
         * Add the stabilization term of the diffusion part to the local matrix
         * @param var_index corresponding index of the variable
         * @param mult_cst multiplicative constant
         */
        virtual inline void add_stabilization(size_t var_index, double mult_cst) = 0;

        /**
         * Add a divergence term knowing two variable indexes
         * @param var_index1 index of the first variable
         * @param var_index2 index of the second variable
         * @param mesh_measure measure of the global domain
         * @param mult_cst multiplicative constant
         */
        void add_divergence_contribution(
                size_t var_index1, 
                size_t var_index2, 
                double mesh_measure,
                double mult_cst = 1.) {
            throw " add_divergence_contribution not implemented in VEM ";
        }

        /**
         * Set the local right-hand-side
         * from a continuous function
         * with specific quadrature order 
         * (6 by default)
         * For a scalar variable or vectorial one
         * @param func
         * @param quadra_order
         * @param var_index
         */
        void set_rhs(double func (Eigen::ArrayXd), int quadra_order, size_t var_index);
        void set_rhs(std::vector<double> func (Eigen::ArrayXd), int quadra_order, size_t var_index);

    protected:
        std::vector<std::vector<MyBasis::Basis<MyMesh::Face<DIM>*>*>> 
                m_faces_dof_bases;  ///<  bases of the faces dof
        std::vector<std::vector<MyBasis::Basis<MyMesh::Edge<DIM>*>*>> 
                m_edges_bases;  ///<  bases of the edges (deg ks), used only in 3D
        std::vector<std::vector<MyBasis::Basis<MyMesh::Edge<DIM>*>*>> 
                m_edges_dof_bases;  ///<  bases of the edges dof (deg ks - 2), used only in 3D
    };

    /// definition of the VEM class to be able to specify for the different values of DIM
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    class VEM : public VEMBase<DIM, CONTINUOUS_SPACES...> {
    public:
        /**
         * Default constructor prints error
         */
        VEM(MyMesh::Cell<DIM>* cell, int kb, int ks, const Eigen::ArrayXi& var_sizes);
        ~VEM() = default;   ///<  Default destructor
    };

    // partial specialization when DIM = 2 and only one variable (so one continuous space)
    template <int CONTINUOUS_SPACE> 
    class VEM<2, CONTINUOUS_SPACE> : public VEMBase<2, CONTINUOUS_SPACE> {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param kb order of the bulk discrete unknown
         * @param ks order of the polynomial in the skeletal objects, the order of the reconstruction is ks
         * @param var_sizes list of the dimension of the variables (multiplicative factors for all dof)
         */
        VEM(MyMesh::Cell<2>* cell, int kb, int ks, const Eigen::ArrayXi& var_sizes); // Poly_base_type pbt, Quad_type qt, Data dat);
        ~VEM() = default;   ///<  Default destructor

        /**
         * Compute the reconstruction operator with default basis
         * @param var_sizes array with the dimension of each variable
         */
        void set_reconstruction_operator(const Eigen::ArrayXi& var_sizes);
        /**
         * Compute the reconstruction operator for 2D meshes
         * for a specific variable 
         * @param var_size dimension of variable
         * @param var_index index of the variable (0 by default)
         */
        inline void set_reconstruction_operator(size_t var_size, size_t var_index = 0);
        /**
         * Add the stabilization term of the diffusion part to the local matrix
         * @param var_index corresponding index of the variable
         * @param mult_cst multiplicative constant
         */
        inline void add_stabilization(size_t var_index, double mult_cst);

    protected:
        std::vector<std::vector<Eigen::MatrixXd>> m_faces_reconstruction_operators;   ///<  reconstruction operator along each face, depending on the variable
    };

    // partial specialization when DIM = 3 and only one variable (so one continuous space)
    template <int CONTINUOUS_SPACE> 
    class VEM<3, CONTINUOUS_SPACE> : public VEMBase<3, CONTINUOUS_SPACE> {
    public:
        /**
         * Constructor
         * @param cell pointer to the cell
         * @param kb order of the bulk discrete unknown
         * @param ks order of the polynomial in the skeletal objects, the order of the reconstruction is ks
         * @param var_sizes list of the dimension of the variables (multiplicative factors for all dof)
         */
        VEM(MyMesh::Cell<3>* cell, int kb, int ks, const Eigen::ArrayXi& var_sizes); // Poly_base_type pbt, Quad_type qt, Data dat);
        ~VEM() = default;   ///<  Default destructor

        /**
         * Compute the reconstruction operator with default basis
         * @param var_sizes array with the dimension of each variable
         */
        void set_reconstruction_operator(const Eigen::ArrayXi& var_sizes);
        /**
         * Compute the reconstruction operator for 3D meshes
         * for a specific variable 
         * @param var_size dimension of variable
         * @param var_index index of the variable (0 by default)
         */
        inline void set_reconstruction_operator(size_t var_size, size_t var_index = 0);
        /**
         * Add the stabilization term of the diffusion part to the local matrix
         * @param var_index corresponding index of the variable
         * @param mult_cst multiplicative constant
         */
        inline void add_stabilization(size_t var_index, double mult_cst);

    protected:
        std::vector<std::vector<Eigen::MatrixXd>> m_edges_reconstruction_operators;   ///<  reconstruction operator along each edge, depending on the variable
    };

    /////////////////////////////////////////////////////////////////////////
    //                   Class Methods implementations
    /////////////////////////////////////////////////////////////////////////

    // constructor for VEMBase (has access to the constructor of PolytopalElement).
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    VEMBase<DIM, CONTINUOUS_SPACES...>::VEMBase(MyMesh::Cell<DIM>* cell, 
                       int kb, int ks,
                       const Eigen::ArrayXi& var_sizes,
                       int n_dof_face, 
                       int n_dof_edge, 
                       int n_dof_vertex,
                       int n_dof_bulk) :
    PolytopalElement<DIM>(cell, var_sizes * n_dof_face, var_sizes * n_dof_edge, var_sizes * n_dof_vertex, 
                       var_sizes * n_dof_bulk) {
#ifdef DEBUG
        if(ks<1) { throw " k<=0 in VEM "; } // k = 1 is the smallest case
        if(kb != ks - 1) { throw " kb != ks - 1 in VEM "; }
#endif
    }

    // destructor for VEMBase
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    VEMBase<DIM, CONTINUOUS_SPACES...>::~VEMBase() {
        for(auto& faces_basis : m_faces_dof_bases) {
            for(auto& face_basis : faces_basis) {
                if(face_basis != 0) { delete face_basis; face_basis = 0; }
            }
        }
        if(DIM == 3) {
            for(auto& edges_basis : m_edges_bases) {
                for(auto& edge_basis : edges_basis) {
                    if(edge_basis != 0) { delete edge_basis; edge_basis = 0; }
                }
            }
            for(auto& edges_basis : m_edges_dof_bases) {
                for(auto& edge_basis : edges_basis) {
                    if(edge_basis != 0) { delete edge_basis; edge_basis = 0; }
                }
            }
        }
    }

    // constructor in VEM (should not be used)
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    VEM<DIM, CONTINUOUS_SPACES...>::VEM(MyMesh::Cell<DIM>* cell, int kb, int ks, const Eigen::ArrayXi& var_sizes) {
        throw " VEM not implemented for these continuous spaces ";
    }

    // constructor for VEM 2D with one variable
    template<int CONTINUOUS_SPACE> 
    VEM<2, CONTINUOUS_SPACE>::VEM(MyMesh::Cell<2>* cell, int kb, int ks, const Eigen::ArrayXi& var_sizes) : 
    // in VEM, dof are 
    //    - one at each vertex
    //    - the faces moments of degree ks - 2
    //    - in cells, a polynomial of degree kb = ks - 1
    // All bulk dof are eliminable.
    VEMBase<2, CONTINUOUS_SPACE>(cell, 
                    kb, ks, // orders of the method
                    var_sizes, // sizes of the variables
                    ks-1, // number of dof in each face (vertices excluded), will be multiplied by var_size (= dim P(ks-2) )
                    0,   // number of dof in each edge (no edge in 2D)
                    1,   // number of dof in each vertex (always 1 in VEM)
                    (kb + 2) * (kb + 1) / 2) { // number of bulk dof (= dim P_kb(T) )

        this->m_cell_degree = ks;
        this->m_skeletal_degree = ks;  // polynomial degree of the skeletal unknowns
        this->m_bulk_degree = kb;  // polynomial degree of the bulk unknowns

        m_faces_reconstruction_operators.resize(var_sizes.size()); // give good size (nb_variables)

        for(int i = 0, size = var_sizes.size(); i < size; i++) {
            if(var_sizes[i] == 1) { // scalar variable
                MyBasis::Basis< MyMesh::Cell<2>* >* cell_basis(
                    new MyBasis::ScaledMonomialScalarBasis<MyMesh::Cell<2>*>(cell, this->m_cell_degree));
                this->m_cell_bases.push_back(cell_basis); 

                // bulk bases : PˆBulkDegree(T)
                MyBasis::Basis< MyMesh::Cell<2>* >* bulk_basis(
                    new MyBasis::ScaledMonomialScalarBasis<MyMesh::Cell<2>*>(cell, this->m_bulk_degree));
                this->m_bulk_bases.push_back(bulk_basis);

                // faces basis of this variable PˆSkelDegree(F)
                std::vector<MyBasis::Basis<MyMesh::Face<2>*>*> faces_bases; // ks
                std::vector<MyBasis::Basis<MyMesh::Face<2>*>*> faces_dof_bases; // ks - 2 (faces dof minus vertices dof)
                for(auto& fpt : cell->get_faces()) {
                    MyBasis::Basis< MyMesh::Face<2>* >* face_basis(
                        new MyBasis::ScaledMonomialScalarBasis<MyMesh::Face<2>*>(fpt, this->m_skeletal_degree));
                    faces_bases.push_back(face_basis); 

                    MyBasis::Basis< MyMesh::Face<2>* >* face_dof_basis(
                        new MyBasis::ScaledMonomialScalarBasis<MyMesh::Face<2>*>(fpt, 
                        std::max(0, this->m_skeletal_degree - 2)));
                    faces_dof_bases.push_back(face_dof_basis);
                }
                this->m_faces_bases.push_back(faces_bases); // all faces bases for this variable (deg ks)
                this->m_faces_dof_bases.push_back(faces_dof_bases); // all faces dof bases for this variable (deg ks - 2)

            } else if(var_sizes[i] == 2) { // vectorial variable
                // vectorial full basis
                MyBasis::Basis< MyMesh::Cell<2>* >* cell_basis(
                    new MyBasis::ScaledMonomialVectorBasis<MyMesh::Cell<2>*, MyBasis::FULL>(cell, this->m_cell_degree));
                this->m_cell_bases.push_back(cell_basis);

                // bulk bases : PˆBulkDegree(T)
                MyBasis::Basis< MyMesh::Cell<2>* >* bulk_basis(
                    new MyBasis::ScaledMonomialVectorBasis<MyMesh::Cell<2>*, MyBasis::FULL>(cell, this->m_bulk_degree));
                this->m_bulk_bases.push_back(bulk_basis);

                // faces basis of this variable PˆSkelDegree(F)
                std::vector<MyBasis::Basis<MyMesh::Face<2>*>*> faces_bases; // ks
                std::vector<MyBasis::Basis<MyMesh::Face<2>*>*> faces_dof_bases; // ks - 2 (faces dof minus vertices dof)
                for(auto& fpt : cell->get_faces()) {
                    MyBasis::Basis< MyMesh::Face<2>* >* face_basis(
                        new MyBasis::ScaledMonomialVectorBasis<MyMesh::Face<2>*, MyBasis::FULL>(fpt, this->m_skeletal_degree));
                    faces_bases.push_back(face_basis); 

                    MyBasis::Basis< MyMesh::Face<2>* >* face_dof_basis(
                        new MyBasis::ScaledMonomialVectorBasis<MyMesh::Face<2>*, MyBasis::FULL>(fpt, 
                        std::max(0, this->m_skeletal_degree - 2)));
                    faces_dof_bases.push_back(face_dof_basis);
                }
                this->m_faces_bases.push_back(faces_bases); // all faces bases for this variable (deg ks)
                this->m_faces_dof_bases.push_back(faces_dof_bases); // all faces dof bases for this variable (deg ks - 2)
            }
        }
    }


    // constructor for VEM 3D with one variable
    template<int CONTINUOUS_SPACE>
    VEM<3, CONTINUOUS_SPACE>::VEM(MyMesh::Cell<3>* cell, int kb, int ks, const Eigen::ArrayXi& var_sizes) : 
    // in VEM, dof are 
    //    - one at each vertex
    //    - the edges, a polynomial of degree ks - 2
    //    - in faces, a polynomial of degree ks - 1
    //    - in cells, a polynomial of degree kb = ks - 1
    // All bulk dof are eliminable.
    VEMBase<3, CONTINUOUS_SPACE>(cell, 
                    kb, ks, // orders of the method
                    var_sizes, // sizes of the variables
                    (ks + 1) * ks / 2, // number of dof in each face (vertices and edges excluded) (= dim P(ks-1) )
                    ks-1,   // number of dof in each edge (vertices excluded), will be multiplied by var_size (= dim P(ks-2) )
                    1,   // number of dof in each vertex (always 1 in VEM)
                    (kb + 1) * (kb + 2) * (kb + 3) / 6) { // number of bulk dof (= dim P_kb(T) ) with kb = ks - 1

        this->m_cell_degree = ks;
        this->m_skeletal_degree = ks;  // polynomial degree of the skeletal unknowns
        this->m_bulk_degree = kb;  // polynomial degree of the bulk unknowns

        m_edges_reconstruction_operators.resize(var_sizes.size()); // give good size (nb_variables)

        for(int i = 0, size = var_sizes.size(); i < size; i++) {
            if(var_sizes[i] == 1) { // scalar variable
                MyBasis::Basis< MyMesh::Cell<3>* >* cell_basis(
                    new MyBasis::ScaledMonomialScalarBasis<MyMesh::Cell<3>*>(cell, this->m_cell_degree));
                this->m_cell_bases.push_back(cell_basis); 

                // bulk bases : PˆBulkDegree(T)
                MyBasis::Basis< MyMesh::Cell<3>* >* bulk_basis(
                    new MyBasis::ScaledMonomialScalarBasis<MyMesh::Cell<3>*>(cell, this->m_bulk_degree));
                this->m_bulk_bases.push_back(bulk_basis);

                // faces basis of this variable PˆSkelDegree(F)
                std::vector<MyBasis::Basis<MyMesh::Face<3>*>*> faces_bases; // ks
                std::vector<MyBasis::Basis<MyMesh::Face<3>*>*> faces_dof_bases; // ks - 1
                for(auto& fpt : cell->get_faces()) {
                    MyBasis::Basis< MyMesh::Face<3>* >* face_basis(
                        new MyBasis::ScaledMonomialScalarBasis<MyMesh::Face<3>*>(fpt, this->m_skeletal_degree));
                    faces_bases.push_back(face_basis); 

                    MyBasis::Basis< MyMesh::Face<3>* >* face_dof_basis(
                        new MyBasis::ScaledMonomialScalarBasis<MyMesh::Face<3>*>(fpt, this->m_skeletal_degree - 1));
                    faces_dof_bases.push_back(face_dof_basis);
                }
                this->m_faces_bases.push_back(faces_bases); // all faces bases for this variable (deg ks)
                this->m_faces_dof_bases.push_back(faces_dof_bases); // all faces dof bases for this variable (deg ks - 1)

                // edges bases of this variable PˆSkelDegree(E)
                std::vector<MyBasis::Basis<MyMesh::Edge<3>*>*> edges_bases; // ks
                std::vector<MyBasis::Basis<MyMesh::Edge<3>*>*> edges_dof_bases; // ks - 2
                for(auto& ept : cell->get_edges()) {
                    // define basis of the edge of degree ks
                    MyBasis::Basis< MyMesh::Edge<3>* >* edge_basis(
                        new MyBasis::ScaledMonomialScalarBasis<MyMesh::Edge<3>*>(ept, this->m_skeletal_degree));
                    edges_bases.push_back(edge_basis);

                    // define basis of the edge of degree ks - 2
                    MyBasis::Basis< MyMesh::Edge<3>* >* edge_dof_basis(
                        new MyBasis::ScaledMonomialScalarBasis<MyMesh::Edge<3>*>(ept, 
                            std::max(0, this->m_skeletal_degree - 2)));
                    edges_dof_bases.push_back(edge_dof_basis);
                }
                this->m_edges_bases.push_back(edges_bases); // all deges bases for this variable (deg ks)
                this->m_edges_dof_bases.push_back(edges_dof_bases); // all edges dof bases for this variable (deg ks - 2)

            } else if(var_sizes[i] == 3) { // vectorial variable
                // vectorial full basis
                MyBasis::Basis< MyMesh::Cell<3>* >* cell_basis(
                    new MyBasis::ScaledMonomialVectorBasis<MyMesh::Cell<3>*, MyBasis::FULL>(cell, this->m_cell_degree));
                this->m_cell_bases.push_back(cell_basis);

                // bulk bases : PˆBulkDegree(T)
                MyBasis::Basis< MyMesh::Cell<3>* >* bulk_basis(
                    new MyBasis::ScaledMonomialVectorBasis<MyMesh::Cell<3>*, MyBasis::FULL>(cell, this->m_bulk_degree));
                this->m_bulk_bases.push_back(bulk_basis);

                // faces basis of this variable PˆSkelDegree(F)
                std::vector<MyBasis::Basis<MyMesh::Face<3>*>*> faces_bases; // ks
                std::vector<MyBasis::Basis<MyMesh::Face<3>*>*> faces_dof_bases; // ks - 1
                for(auto& fpt : cell->get_faces()) {
                    MyBasis::Basis< MyMesh::Face<3>* >* face_basis(
                        new MyBasis::ScaledMonomialVectorBasis<MyMesh::Face<3>*, MyBasis::FULL>(
                            fpt, this->m_skeletal_degree));
                    faces_bases.push_back(face_basis); 

                    MyBasis::Basis< MyMesh::Face<3>* >* face_dof_basis(
                        new MyBasis::ScaledMonomialVectorBasis<MyMesh::Face<3>*, MyBasis::FULL>(
                            fpt, this->m_skeletal_degree - 1));
                    faces_dof_bases.push_back(face_dof_basis);
                }
                this->m_faces_bases.push_back(faces_bases); // all faces bases for this variable (deg ks)
                this->m_faces_dof_bases.push_back(faces_dof_bases); // all faces dof bases for this variable (deg ks - 1)

                // edges bases of this variable PˆSkelDegree(E)
                std::vector<MyBasis::Basis<MyMesh::Edge<3>*>*> edges_bases; // ks
                std::vector<MyBasis::Basis<MyMesh::Edge<3>*>*> edges_dof_bases; // ks - 2
                for(auto& ept : cell->get_edges()) {
                    // define basis of the edge of degree ks
                    MyBasis::Basis< MyMesh::Edge<3>* >* edge_basis(
                        new MyBasis::ScaledMonomialVectorBasis<MyMesh::Edge<3>*, MyBasis::FULL>(
                            ept, this->m_skeletal_degree));
                    edges_bases.push_back(edge_basis);

                    // define basis of the edge of degree ks - 2
                    MyBasis::Basis< MyMesh::Edge<3>* >* edge_dof_basis(
                        new MyBasis::ScaledMonomialVectorBasis<MyMesh::Edge<3>*, MyBasis::FULL>(ept, 
                            std::max(0, this->m_skeletal_degree - 2)));
                    edges_dof_bases.push_back(edge_dof_basis);
                }
                this->m_edges_bases.push_back(edges_bases); // all deges bases for this variable (deg ks)
                this->m_edges_dof_bases.push_back(edges_dof_bases); // all edges dof bases for this variable (deg ks - 2)
            }
        }
    }

    // call set_reconstruction_operator in 2D with default basis
    template<int CONTINUOUS_SPACE> 
    void VEM<2, CONTINUOUS_SPACE>::set_reconstruction_operator(const Eigen::ArrayXi& var_sizes) {

        // the loop over all the variables is strange because there is only one continuous space
        for(size_t var_index = 0, nb_var = var_sizes.size(); var_index < nb_var; var_index++) {
            // set the reconstruction operator for each variable
            set_reconstruction_operator(var_sizes[var_index], var_index);
        }

        return;
    }

    // definition of set_reconstruction_operator in 2D whatever the cell basis
    template<int CONTINUOUS_SPACE> 
    void VEM<2, CONTINUOUS_SPACE>::set_reconstruction_operator(size_t var_size, size_t var_index) {

        std::vector<MyMesh::Face<2>*> faces(this->m_cell->get_faces());
        size_t cell_basis_dim(this->m_cell_bases[var_index]->dimension());
        // init RHS
        Eigen::MatrixXd rhs(Eigen::MatrixXd::Zero(cell_basis_dim, this->m_n_cumulate_local_dof));

        // ------------------------------------------------------------------------------------
        // first, construct along each "face" r_F v_F in Pk(F) 
        //     face F is 1D objects : F = [x0, x1]
        // with   \int_F (r_F v_F)' z' = - \int_F v_F^o z'' + v1 z'(x1) - v0 z'(x0)    for all z in Pk(F) 
        // and    r_F v_F (x0) = v0
        // where v_F^o = \sum_{j in interior_dof_F} v_F^{o,j} \Psi_{F,j}^{k-2} \in P(k-2)(F)
        // 
        // then, construct the reconstruction operator
        // with   \int_T grad(r_T v_T) grad(z) = 
        //              - \int_T v_T^o laplacien(z) + sum_F \int_F r_F v_F grad(z).n_T,F    for all z in Pk(T)
        // and    \int_T r_T v_T = \int_T v_T^o
        // where v_T^o = \sum_{j <= N_d^{k-1}} v_T^{o,j} \Psi_{T,j}^{k-1} \in P(k-1)(T)
        // ------------------------------------------------------------------------------------

        // add to the rhs
        // sum_F \int_F r_F v_F grad(z).n_T,F    for all z in Pk(T), and v_F in Pk(F)
        size_t rec_quadra_order(2 * this->m_cell_degree - 1);
        size_t f_li(0);
        for(auto& fpt : faces) {

            // ------------------------------------------------------------------------------------
            // construct along the "face" r_F v_F in Pk(F) 
            //     face F is 1D objects : F = [x0, x1]
            // with   \int_F (r_F v_F)' z' = - \int_F v_F^o z'' + v1 z'(x1) - v0 z'(x0)    for all z in Pk(F) 
            // and    r_F v_F (x0) = v0
            // where v_F^o = \sum_{j in interior_dof_F} v_F^{o,j} \Psi_{F,j}^{k-2} \in P(k-2)(F)
            // ------------------------------------------------------------------------------------

            // compute the face reconstruction operator of the variable
            // and add it in m_faces_reconstruction_operators[var_index]
            this->set_reconstruction_operator_edges(var_size, var_index,
                    fpt, this->m_faces_dof[var_index][f_li], 
                    this->m_faces_dof_bases[var_index][f_li], 
                    this->m_faces_bases[var_index][f_li], 
                    m_faces_reconstruction_operators[var_index]);

            // compute the outward normal to the face
            // Store the vertices coordinates into vertices_coords
            std::vector<MyMesh::Vertex<2>*> vertices(fpt->get_vertices());
            std::vector<Eigen::Array<double,2,1>> vertices_coords{vertices[0]->coords(), vertices[1]->coords()};
            Eigen::VectorXd normal(outward_normal(vertices_coords, this->m_cell->mass_center()));

            // for each face, quadrature to compute the integral 
            // \int_F r_F v_F grad(z).n_T,F         for all z in Pk(T)
            MyQuadra::QuadraturePoints<2> f_qps(MyQuadra::integrate(fpt, rec_quadra_order));

            for(auto& qp : f_qps.list()) { 
                // evaluate gradients of the basis functions at point qp (quadra of the face)
                std::vector<Eigen::MatrixXd> grads(
                    this->m_cell_bases[var_index]->eval_gradients(qp->point()));
                Eigen::MatrixXd eval_f_reconstruction( // row vector if scalar variable
                    this->m_faces_bases[var_index][f_li]->eval_functions(qp->point()) * 
                    m_faces_reconstruction_operators[var_index][f_li]);

                // add to the rhs
                // \int_F r_F v_F grad(z).n_T,F         for all z in Pk(T)
                for(size_t i = 0; i < cell_basis_dim; i++) {
                    rhs.row(i) += qp->weight() * (grads[i] * normal).transpose() * eval_f_reconstruction;
                }
            } // qps

            f_li++; // next face
        }

        // add to the rhs
        // - \int_T v_T^o laplacien(z)      for all z in Pk(T)
        // where v_T^o = \sum_{j <= N_d^{k-1}} v_T^{o,j} \Psi_{T,j}^{k-1} \in P(k-1)(T) = m_bulk_bases
        // Quadrature order is m_cell_degree - 2 + m_bulk_degree
        size_t quadra_order(std::max(0, this->m_cell_degree - 2 + this->m_bulk_degree));
        MyQuadra::QuadraturePoints<2> c_qps(MyQuadra::integrate(this->m_cell, quadra_order));
        // cell basis of degree m_cell_degree - 1 (\Psi_{T,j}^{k-1} \in P(k-1)(T))
        //         is this->m_bulk_bases[var_index]
        // fill rhs with   - \int_T v_T^o laplacien(z)
        for(auto& qp : c_qps.list()) {
            Eigen::MatrixXd qp_laplacians(
                this->m_cell_bases[var_index]->eval_laplacians(qp->point()).transpose()); // laplacien(z)
            Eigen::MatrixXd psi(this->m_bulk_bases[var_index]->eval_functions(qp->point())); // each column is a basis function

            rhs.block(0, this->m_bulk_dof[var_index][0], cell_basis_dim, this->m_n_bulk_dof(var_index)) -=
                    qp->weight() * qp_laplacians * psi;
        }

        // ------------------------------------------------------------------------------------
        // init LHS with the stiffness matrix which contains \int_T grad(r_T v_T) grad(z)
        Eigen::MatrixXd lhs(this->m_stiffness_matrices[var_index]);
        
        // ------------------------------------------------------------------------------------
        // To fix the constant, add at both sides the mean product :
        // rhs \int_T v_T^o dx * \int z dx         for all z in Pk(T)
        //      where v_T^o = \sum_{j <= N_d^{k-1}} v_T^{o,j} \Psi_{T,j}^{k-1} \in P(k-1)(T)
        // lhs \int_T r_T v_T dx * \int z dx         for all z in Pk(T)
        Eigen::MatrixXd int_cell_bases; // int z dx
        Eigen::MatrixXd int_interior_basis; // int v_T^o dx
        if(var_size == 1) {
            // bases functions are scalar, so int is one value by basis function
            int_cell_bases = this->int_f_basis(this->m_cell, 
                [](Eigen::ArrayXd pt) { return 1.; }, 
                this->m_cell_bases[var_index], this->m_cell_degree).transpose();
            int_interior_basis = this->int_f_basis(this->m_cell, 
                [](Eigen::ArrayXd pt) { return 1.; }, 
                this->m_bulk_bases[var_index], this->m_bulk_degree).transpose();
        } else { // vectorial variable
            // bases functions are vectorial, so var_size values by basis function
            // \int_T z dx component by component
            int_cell_bases = Eigen::MatrixXd::Zero(var_size, cell_basis_dim);
            // \int_T v_T^o dx component by component
            int_interior_basis = Eigen::MatrixXd::Zero(var_size, this->m_bulk_bases[var_index]->dimension());
            
            // quadrature to compute the integrals
            MyQuadra::QuadraturePoints<2> qps(
                MyQuadra::integrate(this->m_cell, std::max(this->m_cell_degree, this->m_bulk_degree)));

            for(auto& qp : qps.list()) { 
                // \int_T z dx component by component
                int_cell_bases += qp->weight() * 
                    this->m_cell_bases[var_index]->eval_functions(qp->point()); // z
                // \int_T v_T^o dx component by component
                int_interior_basis += qp->weight() * 
                    this->m_bulk_bases[var_index]->eval_functions(qp->point()); // v_T^o
            } // quadra
        }

        double scale(1. / std::pow(this->m_cell->diam(), 2.) / this->m_cell->measure());
        // rhs scale * \int_T v_T^o dx * \int z dx
        rhs.block(0, this->m_bulk_dof[var_index][0], cell_basis_dim, this->m_n_bulk_dof[var_index]) +=
                scale * int_cell_bases.transpose() * int_interior_basis;
        // lhs scale * \int_T r_T v_T dx * \int z dx
        lhs += scale * int_cell_bases.transpose() * int_cell_bases;

        // ------------------------------------------------------------------------------------
        // solve mu = lhs^{-1} * rhs (should be quicker than solving
        // the linear system because rhs has many columns)
        this->m_reconstruction_operators.push_back(lhs.inverse() * rhs);

        return;
    } // set_reconstruction_operator 2D

    // call set_reconstruction_operator in 3D with default basis
    template<int CONTINUOUS_SPACE> 
    void VEM<3, CONTINUOUS_SPACE>::set_reconstruction_operator(const Eigen::ArrayXi& var_sizes) {

        // loop over all the variables
        for(size_t var_index = 0, nb_var = var_sizes.size(); var_index < nb_var; var_index++) {
            // set the reconstruction operator for each variable
            set_reconstruction_operator(var_sizes[var_index], var_index);
        }

        return;
    } // set_reconstruction_operator 3D

    // definition of set_reconstruction_operator in 3D
    template<int CONTINUOUS_SPACE> 
    void VEM<3, CONTINUOUS_SPACE>::set_reconstruction_operator(size_t var_size, size_t var_index) {

        // ------------------------------------------------------------------------------------
        // first, construct along each edge r_E v_E in Pk(E) 
        //     with E = [x0, x1]
        // for the stabilization computation
        // with   \int_E (r_E v_E)' z' = - \int_E v_E^o z'' + v1 z'(x1) - v0 z'(x0)    for all z in Pk(E) 
        // and    r_E v_E (x0) = v0
        // where v_E^o = \sum_{j in interior_dof_E} v_E^{o,j} \Psi_{E,j}^{k-2} \in P(k-2)(E)
        // 
        // then, construct the reconstruction operator
        // with   \int_T grad(r_T v_T) grad(z) = - \int_T v_T^o laplacien(z) 
        //                      + sum_F \int_F v_F grad(z).n_T,F    for all z in Pk(T)
        // and    \int_T r_T v_T = \int_T v_T^o
        // where v_F = \sum_{j <= N_{d-1}^{k-1}} v_F^j \Psi_{F,j}^{k-1} \in P(k-1)(F)
        // ------------------------------------------------------------------------------------

        std::vector<MyMesh::Edge<3>*> edges(this->m_cell->get_edges());
        std::vector<MyMesh::Face<3>*> faces(this->m_cell->get_faces());
        size_t cell_basis_dim(this->m_cell_bases[var_index]->dimension());
        size_t rec_quadra_order(2 * this->m_cell_degree - 1);


        // ------------------------------------------------------------------------------------
        // construct r_E v_E in Pk(E) and store it in m_edges_reconstruction_operators
        // for the stabilization computation
        // with   \int_E (r_E v_E)' z' = - \int_E v_E^o z'' + v1 z'(x1) - v0 z'(x0)    for all z in Pk(E) 
        // and    r_E v_E (x0) = v0
        // where v_E^o = \sum_{j in interior_dof_E} v_E^{o,j} \Psi_{E,j}^{k-2} \in P(k-2)(E)
        size_t e_li(0);
        for(auto& ept : edges) {
            this->set_reconstruction_operator_edges(var_size, var_index,
                    ept, this->m_edges_dof[var_index][e_li], 
                    this->m_edges_dof_bases[var_index][e_li], 
                    this->m_edges_bases[var_index][e_li], 
                    m_edges_reconstruction_operators[var_index]);

            e_li++; // next edge
        }

        // ------------------------------------------------------------------------------------
        // construct the reconstruction operator
        // with   \int_T grad(r_T v_T) grad(z) = - \int_T v_T^o laplacien(z) 
        //                      + sum_F \int_F v_F grad(z).n_T,F    for all z in Pk(T)
        // where v_F = \sum_{j <= N_{d-1}^{k-1}} v_F^j \Psi_{F,j}^{k-1} \in P(k-1)(F)
        // and    \int_T r_T v_T = \int_T v_T^o

        // init RHS for r_T v_T
        Eigen::MatrixXd rhs(Eigen::MatrixXd::Zero(cell_basis_dim, this->m_n_cumulate_local_dof));

        // quadrature order 
        rec_quadra_order = 2 * this->m_cell_degree - 2;
        size_t f_li(0);
        for(auto& fpt : faces) {
            // ------------------------------------------------------------------------------------
            // add to the rhs
            // \int_F v_F grad(z).n_T,F    for all z in Pk(T)
            // where v_F = \sum_{j <= N_{d-1}^{k-1}} v_F^j \Psi_{F,j}^{k-1} \in P(k-1)(F) = this->m_faces_dof_bases

            // compute the outward normal to the cell
            // Store some vertices coordinates into vertices_coords
            std::vector<MyMesh::Vertex<3>*> vertices(fpt->get_vertices());
            std::vector<Eigen::Array<double,3,1>> vertices_coords{vertices[0]->coords(), 
                vertices[1]->coords(), vertices[2]->coords()}; // face is a 2D object so at least 3 vertices
            Eigen::VectorXd normal(outward_normal(vertices_coords, this->m_cell->mass_center()));

            // quadrature to compute the integral 
            MyQuadra::QuadraturePoints<3> qps(MyQuadra::integrate(fpt, rec_quadra_order));

            for(auto& qp : qps.list()) { 
                // evaluation of the gradients of the basis functions at point qp (quadra of the face)
                std::vector<Eigen::MatrixXd> grads(
                    this->m_cell_bases[var_index]->eval_gradients(qp->point()));
                // evaluation of \Psi_{F,j}^{k-1}
                Eigen::MatrixXd psi(this->m_faces_dof_bases[var_index][f_li]->eval_functions(qp->point()));

                // add to the rhs
                // \int_F v_F grad(z).n_T,F         for all z in Pk(T)
                for(size_t i = 0; i < cell_basis_dim; i++) { // loop on z
                    rhs.block(i, this->m_faces_dof[var_index][f_li][0], 1, this->m_n_faces_dof(f_li, var_index)) += 
                        qp->weight() * (grads[i] * normal).transpose() * psi;
                }
            } // qps

            f_li++; // next face
        }

        // ------------------------------------------------------------------------------------
        // add to the rhs
        // - \int_T v_T^o laplacien(z)     for all z in Pk(T)
        // where v_T^o = \sum_{j <= N_d^{k-1}} v_T^{o,j} \Psi_{T,j}^{k-1} \in P(k-1)(T)
        
        // Quadrature order is m_cell_degree - 2 + m_bulk_degree
        size_t quadra_order(std::max(0, this->m_cell_degree - 2 + this->m_bulk_degree));
        MyQuadra::QuadraturePoints<3> c_qps(MyQuadra::integrate(this->m_cell, quadra_order));
        // cell basis of degree m_cell_degree - 1 (\Psi_{T,j}^{k-1} \in P(k-1)(T))
        //         is this->m_bulk_bases[var_index]
        for(auto& qp : c_qps.list()) {
            Eigen::MatrixXd qp_laplacians(
                this->m_cell_bases[var_index]->eval_laplacians(qp->point())); // laplacien(z)
            Eigen::MatrixXd psi(this->m_bulk_bases[var_index]->eval_functions(qp->point()));

            rhs.block(0, this->m_bulk_dof[var_index][0], cell_basis_dim, this->m_n_bulk_dof(var_index)) -= 
                qp->weight() * qp_laplacians.transpose() * psi;
        }


        // ------------------------------------------------------------------------------------
        // init LHS with the stiffness matrix which contains \int_T grad(r_T v_T) grad(z)
        Eigen::MatrixXd lhs(this->m_stiffness_matrices[var_index]);
        
        // ------------------------------------------------------------------------------------
        // To fix the constant, add at both sides the mean product :
        // rhs \int_T v_T^o dx * \int_T z dx         for all z in Pk(T)
        //      where v_T^o = \sum_{j <= N_d^{k-1}} v_T^{o,j} \Psi_{T,j}^{k-1} \in P(k-1)(T)
        // lhs \int_T r_T v_T dx * \int_T z dx         for all z in Pk(T)
        Eigen::MatrixXd int_cell_bases; // int z dx
        Eigen::MatrixXd int_interior_basis; // int v_T^o dx
        if(var_size == 1) {
            // bases functions are scalar, so int is one value by basis function
            int_cell_bases = this->int_f_basis(this->m_cell, 
                [](Eigen::ArrayXd pt) { return 1.; }, 
                this->m_cell_bases[var_index], this->m_cell_degree).transpose();
            int_interior_basis = this->int_f_basis(this->m_cell, 
                [](Eigen::ArrayXd pt) { return 1.; }, 
                this->m_bulk_bases[var_index], this->m_bulk_degree).transpose();
        } else { // vectorial variable
            // bases functions are vectorial, so var_size values by basis function
            // \int_T z dx component by component
            int_cell_bases = Eigen::MatrixXd::Zero(var_size, cell_basis_dim);
            // \int_T v_T^o dx component by component
            int_interior_basis = Eigen::MatrixXd::Zero(var_size, this->m_bulk_bases[var_index]->dimension());
            
            // quadrature to compute the integrals
            MyQuadra::QuadraturePoints<3> qps(
                MyQuadra::integrate(this->m_cell, std::max(this->m_cell_degree, this->m_bulk_degree)));

            for(auto& qp : qps.list()) { 
                // \int_T z dx component by component
                int_cell_bases += qp->weight() * 
                    this->m_cell_bases[var_index]->eval_functions(qp->point()); // z
                // \int_T v_T^o dx component by component
                int_interior_basis += qp->weight() * 
                    this->m_bulk_bases[var_index]->eval_functions(qp->point()); // v_T^o
            } // quadra
        }

        double scale(1. / std::pow(this->m_cell->diam(), 2.) / this->m_cell->measure());
        // rhs scale * \int_T v_T^o dx * \int z dx
        rhs.block(0, this->m_bulk_dof[var_index][0], cell_basis_dim, this->m_n_bulk_dof[var_index]) +=
                scale * int_cell_bases.transpose() * int_interior_basis;
        // lhs scale * \int_T r_T v_T dx * \int z dx
        lhs += scale * int_cell_bases.transpose() * int_cell_bases;

        // ------------------------------------------------------------------------------------
        // solve mu = lhs^{-1} * rhs (should be quicker than solving
        // the linear system because rhs has many columns)
        this->m_reconstruction_operators.push_back(lhs.inverse() * rhs);

        return;
    } // set_reconstruction_operator in 3D

    template<size_t DIM, int... CONTINUOUS_SPACES> 
    template<typename ELEMT, typename ELEMT_BASIS>
    inline void VEMBase<DIM, CONTINUOUS_SPACES...>::set_reconstruction_operator_edges(
        size_t var_size,
        size_t var_index,
        const ELEMT element,
        const std::vector<size_t>& element_dof,
        const ELEMT_BASIS element_dof_basis, // deg ks - 2
        const ELEMT_BASIS element_basis,     // deg ks
        std::vector<Eigen::MatrixXd>& reconstruction_operator_edges) {

        // will later need the vertices of the cell
        std::vector<MyMesh::Vertex<DIM>*> cell_vertices(this->m_cell->get_vertices());

        // ------------------------------------------------------------------------------------
        // I look for \mu in Pk(elm) such that    
        // \int_elm (\mu)' z' dx    +  1/|elm| \mu(x0) z(x0) 
        //     = v1 z'(x1) - v0 z'(x0) - \int_elm v_elm^o z''   +  1/|elm| v0 z(x0)
        //          for all z in Pk(elm)
        // where v_elm^o = \sum_{j in interior_dof_elm} v_elm^{o,j} \Psi_{elm,j}^{k-2} \in P(k-2)(elm)
        // ------------------------------------------------------------------------------------

        size_t elm_basis_dim(element_basis->dimension());

        // *********************************************************
        // Matrix with    \int_elm (\mu)' z' dx  
        //          with z in Pk(elm)
        Eigen::MatrixXd lhs(Eigen::MatrixXd::Zero(elm_basis_dim, elm_basis_dim));
        // quadrature on this element of order  2 * (k-1), k > 0
        size_t quadra_order(2 * this->m_skeletal_degree - 2);
        MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(element, quadra_order));

        for(auto& qp : qps.list()) {
            // one column by basis function
            Eigen::MatrixXd qp_derivatives(element_basis->eval_derivatives(qp->point()));

            // lhs += qp->weight() * qp_derivatives.transpose() * qp_derivatives;
            for(size_t j = 0; j < elm_basis_dim; j++) {
                for(size_t i = j; i < elm_basis_dim; i++) { // matrix is symmetric
                    lhs(i, j) += qp->weight() * (qp_derivatives.col(i)).dot(qp_derivatives.col(j));
                }
            }
        }
        // fill the symmetric part
        for(size_t j = 0; j < elm_basis_dim; j++) {
            for(size_t i = 0; i < j; i++ ) { lhs(i,j) = lhs(j,i); }
        }
        
        // *********************************************************
        // rhs = v1 z'(x1) - v0 z'(x0) - \int_elm v_elm^o z''
        //          with z in Pk(elm)
        Eigen::MatrixXd rhs(Eigen::MatrixXd::Zero(elm_basis_dim, this->m_n_cumulate_local_dof));
        // fill rhs with    v1 z'(x1) - v0 z'(x0)
        // vertices of the element (2 vertices because 1D object)
        std::vector<MyMesh::Vertex<DIM>*> vertices(element->get_vertices());
        // find the local indexes of the element vertices in the list of vertices 
        // of the cell (to find the local index of the dof v0 and v1)
        typename std::vector<MyMesh::Vertex<DIM>*>::iterator it0(
            std::find (cell_vertices.begin(), cell_vertices.end(), vertices[0]));
        size_t v0_li(std::distance(cell_vertices.begin(), it0));
        typename std::vector<MyMesh::Vertex<DIM>*>::iterator it1(
            std::find (cell_vertices.begin(), cell_vertices.end(), vertices[1]));
        size_t v1_li(std::distance(cell_vertices.begin(), it1));
        // fill rhs with  v1 z'(x1) - v0 z'(x0)
        // using the local index of the dof in the PE
        Eigen::MatrixXd eval_derivatives_0(
            element_basis->eval_derivatives(vertices[0]->coords()).transpose());
        Eigen::MatrixXd eval_derivatives_1(
            element_basis->eval_derivatives(vertices[1]->coords()).transpose());

        rhs.block(0, this->m_vertices_dof[var_index][v1_li][0], 
            elm_basis_dim, this->m_n_vertices_dof(v1_li, var_index)) += eval_derivatives_1;
        rhs.block(0, this->m_vertices_dof[var_index][v0_li][0],
            elm_basis_dim, this->m_n_vertices_dof(v0_li, var_index)) -= eval_derivatives_0;


        // fill rhs with   - \int_elm v_elm^o z''
        // where v_elm^o = \sum_{j in interior_dof_elm} v_elm^{o,j} \Psi_{elm,j}^{k-2} \in P(k-2)(elm) = element_dof_basis
        //          with z in Pk(elm)
        if(this->m_skeletal_degree > 1) { // otherwise v_elm^o is empty
            quadra_order = 2 * this->m_skeletal_degree - 4;
            MyQuadra::QuadraturePoints<DIM> e_qps(MyQuadra::integrate(element, quadra_order));
            int n_element_dof(element_dof[1] - element_dof[0]);

            // in v_elm^{o,j} : \Psi_{elm,j}^{k-2} are the members of element_dof_basis
            for(auto& qp : e_qps.list()) {
                Eigen::MatrixXd qp_sec_derivatives(
                    element_basis->eval_sec_derivatives(qp->point()).transpose()); // z''
                Eigen::MatrixXd psi(
                    element_dof_basis->eval_functions(qp->point())); // each column is a basis function

                rhs.block(0, element_dof[0], elm_basis_dim, n_element_dof) -=
                        qp->weight() * qp_sec_derivatives * psi;
            }
        } // if ks > 1

        // *********************************************************
        // To fix the constant, add at both sides, 
        // lhs  1/|elm| \mu(x0) z(x0)
        // rhs  1/|elm| v0 z(x0)

        // basis functions at x0 : z(x0)
        Eigen::MatrixXd eval_functions_0(element_basis->eval_functions(vertices[0]->coords()));
        // lhs  1/|elm| \mu(x0) z(x0)
        lhs += 1./element->measure() * eval_functions_0.transpose() * eval_functions_0;
        // rhs  1/|elm| v0 z(x0)
        rhs.block(0, this->m_vertices_dof[var_index][v0_li][0], elm_basis_dim, var_size) += 
            1./element->measure() * eval_functions_0.transpose(); 


        // compute edge_rec_op = lhs^{-1} * rhs (should be quicker than solving
        // the linear system because rhs has many columns)
        reconstruction_operator_edges.push_back(lhs.inverse() * rhs);
            
        return;
    } // set_reconstruction_operator_edges

    template<int CONTINUOUS_SPACE> 
    inline void VEM<2, CONTINUOUS_SPACE>::add_stabilization(size_t var_index, double mult_cst) {

        Eigen::MatrixXd stab(Eigen::MatrixXd::Zero(
            this->m_n_cumulate_local_dof, this->m_n_cumulate_local_dof));

        // local indexes of the bulk dof
        size_t bulk_start(this->m_bulk_dof[var_index][0]);
        size_t bulk_length(this->m_n_bulk_dof(var_index));
        size_t cell_dim(this->m_cell_bases[var_index]->dimension());

        Eigen::MatrixXd trace_bulk(Eigen::MatrixXd::Zero(bulk_length, cell_dim));
        // int over the cell T to compute l2_proj_Bulk(r_T v_T)
        size_t quadra_order(this->m_bulk_degree + this->m_cell_degree);
        MyQuadra::QuadraturePoints<2> t_qps(MyQuadra::integrate(this->m_cell, quadra_order));
        for(auto& qp : t_qps.list()) { // int over T
            // Vectors with the values of m_bulk_bases[var_index] basis functions
            // and of this->m_cell_bases[var_index] basis functions
            // evaluated at quadrature point
            Eigen::MatrixXd func_bulk_basis(
                this->m_bulk_bases[var_index]->eval_functions(qp->point()));
            Eigen::MatrixXd func_cell_basis(
                this->m_cell_bases[var_index]->eval_functions(qp->point()));

            trace_bulk += qp->weight() * func_bulk_basis.transpose() * func_cell_basis;
        } // quadra

        // compute the mass matrix of the bulk basis functions
        quadra_order = 2 * this->m_bulk_degree;
        Eigen::MatrixXd inv_mass_matrix_t(
            this->compute_mass_matrix(this->m_cell, this->m_bulk_bases[var_index], quadra_order).inverse());

        // - l2_proj_Bulk(r_T v_T)
        Eigen::MatrixXd proj_t(
            - inv_mass_matrix_t * trace_bulk * this->m_reconstruction_operators[var_index]);
        // l2_proj_Bulk(v_o_T - r_T v_T)   =    v_o_T - l2_proj_Bulk(r_T v_T)
        proj_t.block(0, bulk_start, bulk_length, bulk_length) +=
            Eigen::MatrixXd::Identity(bulk_length, bulk_length);

        // will be necessary to integrate polynomials of order 2 * this->m_cell_degree
        quadra_order = 2 * this->m_cell_degree;
        size_t f_li(0); // local index of face
        for(auto& fpt : this->m_cell->get_faces()) {

            // face bases dimension
            size_t face_dim(this->m_faces_bases[var_index][f_li]->dimension());
            
            // compute the projection over the face of degree k
            // of r_T v_T + l2_proj_Bulk(v_o_T - r_T v_T) 
            // the projection is not necessary on the paper but the object 
            // are not decomposed in the basis m_faces_bases so the projection
            // is necessary to change the basis.

            // trace matrix : int over the face of m_faces_bases * m_cell_bases
            Eigen::MatrixXd trace_f_cell(Eigen::MatrixXd::Zero(face_dim, cell_dim));
            // trace matrix : int over the face of m_faces_bases * m_bulk_bases
            Eigen::MatrixXd trace_f_bulk(Eigen::MatrixXd::Zero(face_dim, bulk_length));
            MyQuadra::QuadraturePoints<2> f_qps(MyQuadra::integrate(fpt, quadra_order));
            for(auto& qp : f_qps.list()) { // int over F
                // Vector with the values of m_faces_bases[var_index][f_li] basis functions
                // evaluated at quadrature point
                Eigen::MatrixXd func_face_basis(
                    this->m_faces_bases[var_index][f_li]->eval_functions(qp->point()));

                trace_f_bulk += qp->weight() * func_face_basis.transpose() * 
                    this->m_bulk_bases[var_index]->eval_functions(qp->point());
                trace_f_cell += qp->weight() * func_face_basis.transpose() * 
                    this->m_cell_bases[var_index]->eval_functions(qp->point());
            } // quadra

            // compute the mass matrix of the face basis functions
            Eigen::MatrixXd mass_matrix_f(
                this->compute_mass_matrix(fpt, this->m_faces_bases[var_index][f_li], quadra_order));
            Eigen::MatrixXd inv_mass_matrix_f(mass_matrix_f.inverse());

            // fill stab_f with :
            Eigen::MatrixXd stab_f(Eigen::MatrixXd::Zero(face_dim, this->m_n_cumulate_local_dof));
            
            //          + l2proj_F(r_T v_T + l2_proj_Bulk(v_o_T - r_T v_T))
            stab_f += inv_mass_matrix_f * (
                trace_f_cell * this->m_reconstruction_operators[var_index]  // r_T v_T
                + trace_f_bulk * proj_t  // l2_proj_Bulk(v_o_T - r_T v_T)
            );

            //          - r_F v_F  (already in good basis, no proj)
            stab_f -= m_faces_reconstruction_operators[var_index][f_li];
            

            // add the contribution to the global stabilization
            stab += 1./fpt->diam() * stab_f.transpose() * mass_matrix_f * stab_f;

            f_li++; // next face
        }

        this->m_local_bilinear_form += mult_cst * stab;

        return;
    } // add_stabilization 2D

    template<int CONTINUOUS_SPACE> 
    inline void VEM<3, CONTINUOUS_SPACE>::add_stabilization(size_t var_index, double mult_cst) {

        Eigen::MatrixXd stab(Eigen::MatrixXd::Zero(
            this->m_n_cumulate_local_dof, this->m_n_cumulate_local_dof));

        // local indexes of the bulk dof
        size_t bulk_start(this->m_bulk_dof[var_index][0]);
        size_t bulk_length(this->m_n_bulk_dof(var_index));
        size_t cell_dim(this->m_cell_bases[var_index]->dimension());

        // ------------------------------------------------------------------------------------
        // integration over the faces

        Eigen::MatrixXd trace_bulk(Eigen::MatrixXd::Zero(bulk_length, cell_dim));
        // int over the cell T to compute l2_proj_Bulk(r_T v_T)
        size_t quadra_order(this->m_bulk_degree + this->m_cell_degree);
        MyQuadra::QuadraturePoints<3> t_qps(MyQuadra::integrate(this->m_cell, quadra_order));
        for(auto& qp : t_qps.list()) { // int over T
            // Vectors with the values of m_bulk_bases[var_index] basis functions
            // and of m_cell_bases[var_index] basis functions
            // evaluated at quadrature point
            Eigen::MatrixXd func_bulk_basis(
                this->m_bulk_bases[var_index]->eval_functions(qp->point()));
            Eigen::MatrixXd func_cell_basis(
                this->m_cell_bases[var_index]->eval_functions(qp->point()));

            trace_bulk += qp->weight() * func_bulk_basis.transpose() * func_cell_basis;
        } // quadra

        // compute the inverse of the mass matrix of the bulk basis functions
        Eigen::MatrixXd inv_mass_matrix_t(this->compute_mass_matrix(this->m_cell, 
            this->m_bulk_bases[var_index], 2 * this->m_bulk_degree).inverse());

        // - l2_proj_Bulk(r_T v_T)
        Eigen::MatrixXd proj_t(
            - inv_mass_matrix_t * trace_bulk * this->m_reconstruction_operators[var_index]);
        // v_bulk - l2_proj_Bulk(r_T v_T)
        proj_t.block(0, bulk_start, bulk_length, bulk_length) +=
            Eigen::MatrixXd::Identity(bulk_length, bulk_length);


        std::vector<MyMesh::Face<3>*> faces(this->m_cell->get_faces());
        size_t f_li(0); // local index of face
        for(auto& fpt : faces) {

            // local indexes of the dof of this face
            size_t face_start(this->m_faces_dof[var_index][f_li][0]);
            size_t face_length(this->m_n_faces_dof(f_li, var_index));

            Eigen::MatrixXd stab_f(Eigen::MatrixXd::Zero(face_length, this->m_n_cumulate_local_dof));
            // - v_F : identity at the corresponding dof indexes
            stab_f.block(0, face_start, face_length, face_length) -= 
                Eigen::MatrixXd::Identity(face_length, face_length);

            // will need the projection over the face of degree k-1,
            // use m_faces_dof_bases (already deg k-1)
            // trace matrix : int over the face of m_faces_dof_bases * m_cell_bases
            Eigen::MatrixXd trace_f_cell(Eigen::MatrixXd::Zero(face_length, cell_dim));
            // trace matrix : int over the face of m_faces_dof_bases * m_bulk_bases
            Eigen::MatrixXd trace_f_bulk(Eigen::MatrixXd::Zero(face_length, bulk_length));

            quadra_order = 2 * this->m_cell_degree - 1;
            MyQuadra::QuadraturePoints<3> f_qps(MyQuadra::integrate(fpt, quadra_order));
            for(auto& qp : f_qps.list()) { // int over F
                Eigen::MatrixXd func_face_basis(
                    this->m_faces_dof_bases[var_index][f_li]->eval_functions(qp->point()));

                trace_f_cell += qp->weight() * func_face_basis.transpose() * 
                    this->m_cell_bases[var_index]->eval_functions(qp->point());
                trace_f_bulk += qp->weight() * func_face_basis.transpose() *
                    this->m_bulk_bases[var_index]->eval_functions(qp->point());
            } // quadra

            // compute the mass matrix of the face basis functions of deg k - 1
            quadra_order = 2 * this->m_cell_degree - 2;
            Eigen::MatrixXd mass_matrix_f(
                this->compute_mass_matrix(fpt, this->m_faces_dof_bases[var_index][f_li], quadra_order));
            Eigen::MatrixXd inv_mass_matrix_f(mass_matrix_f.inverse());

            // add to stab_f the term l2proj_F(r_T v_T + l2_proj_Bulk(v_bulk - r_T v_T))
            Eigen::MatrixXd proj1( // r_T v_T
                trace_f_cell * this->m_reconstruction_operators[var_index]);
            Eigen::MatrixXd proj2(trace_f_bulk * proj_t); // l2_proj_Bulk(v_bulk - r_T v_T)
            stab_f += inv_mass_matrix_f * (proj1 + proj2);

            // add the contribution to the global stabilization
            stab += 1./fpt->diam() * stab_f.transpose() * mass_matrix_f * stab_f;

            f_li++; // next face
        }


        // ------------------------------------------------------------------------------------
        // contribution of the edges
        std::vector<MyMesh::Edge<3>*> edges_in_cell(this->m_cell->get_edges());
        quadra_order = 2 * this->m_cell_degree;
        f_li = 0; // local index of face
        for(auto& fpt : faces) {
            std::vector<MyMesh::Edge<3>*> edges_in_face(fpt->get_edges());
            for(auto& ept : edges_in_face) {

                // find the local index of the edge ept in the list of edges of the cell
                typename std::vector<MyMesh::Edge<3>*>::iterator it(
                    std::find(edges_in_cell.begin(), edges_in_cell.end(), ept));
                size_t e_li(std::distance(edges_in_cell.begin(), it));

                // edge objects
                size_t edge_dim(this->m_edges_bases[var_index][e_li]->dimension());
                MyQuadra::QuadraturePoints<3> e_qps(MyQuadra::integrate(ept, quadra_order));
                Eigen::MatrixXd stab_e(Eigen::MatrixXd::Zero(edge_dim, this->m_n_cumulate_local_dof));

                // all the terms of the stabilization are not decomposed in the basis m_edges_bases,
                // so need to project these objects into the basis
                // add int_e r_T v_T + l2_proj_Bulk(v_bulk - r_T v_T), project first
                // trace matrix : int over the edge of edge_bases * m_cell_bases
                Eigen::MatrixXd trace_e_cell(Eigen::MatrixXd::Zero(edge_dim, cell_dim));
                // trace matrix : int over the edge of edge_bases * m_bulk_bases
                Eigen::MatrixXd trace_e_bulk(Eigen::MatrixXd::Zero(edge_dim, bulk_length));
                for(auto& qp : e_qps.list()) {

                    Eigen::MatrixXd func_edge_basis(
                        this->m_edges_bases[var_index][e_li]->eval_functions(qp->point()).transpose());

                    trace_e_cell += qp->weight() * func_edge_basis * 
                        this->m_cell_bases[var_index]->eval_functions(qp->point());
                    trace_e_bulk += qp->weight() * func_edge_basis * 
                        this->m_bulk_bases[var_index]->eval_functions(qp->point());
                }

                // compute the mass matrix of the edge basis functions
                Eigen::MatrixXd mass_matrix_e(
                    this->compute_mass_matrix(ept, this->m_edges_bases[var_index][e_li], quadra_order));
                Eigen::MatrixXd inv_mass_matrix_e(mass_matrix_e.inverse());

                // l2_proj_e(r_T v_T + l2_proj_Bulk(v_bulk - r_T v_T))
                stab_e += inv_mass_matrix_e * (
                    // l2proj_e (r_T v_T)
                    trace_e_cell * this->m_reconstruction_operators[var_index]
                    // proj_t contains   l2_proj_Bulk(v_bulk - r_T v_T)
                    // compute l2proj_e(l2_proj_Bulk(v_bulk - r_T v_T))
                    + trace_e_bulk * proj_t);

                // add   - r_e v_e (already in m_edges_bases decomposition)
                stab_e -= m_edges_reconstruction_operators[var_index][e_li];

                // add the contribution to the global stabilization
                stab += stab_e.transpose() * mass_matrix_e * stab_e;
            } // edges in f_li

            f_li++;
        } // faces

        this->m_local_bilinear_form += mult_cst * stab;

        return;
    } // add_stabilization 3D

    template<size_t DIM, int... CONTINUOUS_SPACES> 
    inline void VEMBase<DIM, CONTINUOUS_SPACES...>::add_stiffness_contribution(
                    size_t var_index, double mult_cst) {

        pe_add_stiffness_contribution<DIM>(
            this->m_reconstruction_operators[var_index],
            this->m_stiffness_matrices[var_index],
            this->m_local_bilinear_form,
            mult_cst);

        add_stabilization(var_index, mult_cst);

        return;
    } // add_stiffness_contribution

    template<size_t DIM, int... CONTINUOUS_SPACES> 
    inline void VEMBase<DIM, CONTINUOUS_SPACES...>::add_mass_contribution(
                    size_t var_index, double mult_cst) {

        return pe_add_mass_contribution<DIM>(
            this->m_reconstruction_operators[var_index],
            this->m_mass_matrices[var_index],
            this->m_local_bilinear_form,
            mult_cst);
    } // add_mass_contribution

    // Define the reduction operator :
    // the value of the function at the nodes,
    // the l2proj of degree ks-2 in 3D over the edges
    // the l2proj over the faces of degree ks-2 in 2D, ks-1 in 3D and ks - 2 in enhanced 3D
    // the l2proj of degree kb over the bulk
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    Eigen::VectorXd VEMBase<DIM, CONTINUOUS_SPACES...>::apply_reduction_operator(
                    double func (Eigen::ArrayXd),
                    size_t var_index,
                    bool only_bound_elemt) {

        Eigen::VectorXd reduction_vec(Eigen::VectorXd::Zero(this->m_n_cumulate_local_dof));

        // value of the function at the nodes
        std::vector<MyMesh::Vertex<DIM>*> vertices(this->m_cell->get_vertices());
        // this->m_vertices_dof[var_index][v_li][0] contains the local index of the dof of vertex v_li
        size_t v_li(0);
        for(auto& v : vertices) {
            if(!only_bound_elemt || v->is_boundary()) {
                reduction_vec[this->m_vertices_dof[var_index][v_li][0]] = func(v->coords());
            }
            v_li++;
        }

        if(DIM == 3) { // not necessary because edges is empty in 2D but should help the compiler
#ifdef DEBUG
// in 3D, if m_skeletal_degree == 1, there is no edge dof
// and the bases are empty, which creates a pb in DEBUG mode
// so add an if loop
        if(this->m_skeletal_degree > 1) {
#endif
            // l2proj of degree ks-2 over the edges
            std::vector<MyMesh::Edge<DIM>*> edges(this->m_cell->get_edges());
            size_t e_li(0); // local index of edge
            for(auto& ept : edges) {
                if(!only_bound_elemt || ept->is_boundary()) {
                    size_t start(this->m_edges_dof[var_index][e_li][0]);
                    size_t length(this->m_n_edges_dof(e_li, var_index));

                    reduction_vec.segment(start, length) = 
                        this->orth_l2projection(ept, this->m_edges_dof_bases[var_index][e_li], func);
                }
                e_li++; // next edge
            }
#ifdef DEBUG
        } // end if this->m_skeletal_degree > 1
#endif
        }

        // l2proj over the faces of degree ks-2 in 2D and ks-1 in 3D or ks-2 in enhanced 3D
        // good basis is contained in m_faces_dof_bases
        std::vector<MyMesh::Face<DIM>*> faces(this->m_cell->get_faces());
        size_t f_li(0); // local index of face
        for(auto& fpt : faces) {
            if(!only_bound_elemt || fpt->is_boundary()) {
                size_t start(this->m_faces_dof[var_index][f_li][0]);
                size_t length(this->m_n_faces_dof(f_li, var_index));
#ifdef DEBUG
// in some cases there is no face dof
// and the bases are empty, which creates a pb in debug mode
// so add an if loop
// costly, so only in debug
                if(length > 0) {
#endif
                reduction_vec.segment(start, length) = 
                    this->orth_l2projection(fpt, m_faces_dof_bases[var_index][f_li], func);
#ifdef DEBUG
                }
#endif
            }
            f_li++; // next face
        }

        // l2proj of degree m_bulk_degree over the bulk
        if(!only_bound_elemt) {
            // l2 projection over the bulk
            size_t start(this->m_bulk_dof[var_index][0]);
            size_t length(this->m_n_bulk_dof[var_index]);

            reduction_vec.segment(start, length) = 
                this->orth_l2projection(this->m_cell, this->m_bulk_bases[var_index], func);
        }

        return reduction_vec;
    } // apply_reduction_operator on scalar variable

    // Define the reduction operator
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    Eigen::VectorXd VEMBase<DIM, CONTINUOUS_SPACES...>::apply_reduction_operator(
                    std::vector<double> func (Eigen::ArrayXd),
                    size_t var_index,
                    bool only_bound_elemt) {

#ifdef DEBUG
if(func(this->m_cell->mass_center()).size() != DIM) {
    throw " apply_reduction_operator implemented only over vect valued function of dimension DIM ";
}
#endif
        Eigen::VectorXd reduction_vec(Eigen::VectorXd::Zero(this->m_n_cumulate_local_dof));

        // value of the function at the nodes
        std::vector<MyMesh::Vertex<DIM>*> vertices(this->m_cell->get_vertices());
        // this->m_vertices_dof[var_index][v_li][0] contains the first local index of the dof of vertex v_li
        // because vectorial unknown, there are DIM dof at each vertex
        size_t v_li(0);
        for(auto& v : vertices) {
            if(!only_bound_elemt || v->is_boundary()) {
                // get func value
                std::vector<double> f_v(func(v->coords()));
                // convert into eigen vector and store it
                reduction_vec.segment(this->m_vertices_dof[var_index][v_li][0], DIM) = 
                    Eigen::Map<Eigen::VectorXd> (f_v.data(), f_v.size());
            }
            v_li++;
        }

        if(DIM == 3) { // not necessary because edges is empty in 2D but should help the compiler
#ifdef DEBUG
// in 3D, if m_skeletal_degree == 1, there is no edge dof
// and the bases are empty, which creates a pb in DEBUG mode
// so add an if loop
        if(this->m_skeletal_degree > 1) {
#endif
            // l2proj of degree ks-2 over the edges
            std::vector<MyMesh::Edge<DIM>*> edges(this->m_cell->get_edges());
            size_t e_li(0); // local index of edge
            for(auto& ept : edges) {
                if(!only_bound_elemt || ept->is_boundary()) {
                    size_t start(this->m_edges_dof[var_index][e_li][0]);
                    size_t length(this->m_n_edges_dof(e_li, var_index));

                    reduction_vec.segment(start, length) = 
                        this->orth_l2projection(ept, this->m_edges_dof_bases[var_index][e_li], func);
                }
                e_li++; // next edge
            }
#ifdef DEBUG
        } // end if this->m_skeletal_degree > 1
#endif
        }

        // l2proj over the faces of degree ks-2 in 2D and ks-1 in 3D or ks-2 in enhanced 3D
        // good basis is contained in m_faces_dof_bases
        std::vector<MyMesh::Face<DIM>*> faces(this->m_cell->get_faces());
        size_t f_li(0); // local index of face
        for(auto& fpt : faces) {
            if(!only_bound_elemt || fpt->is_boundary()) {
                size_t start(this->m_faces_dof[var_index][f_li][0]);
                size_t length(this->m_n_faces_dof(f_li, var_index));
#ifdef DEBUG
// in some cases there is no face dof
// and the bases are empty, which creates a pb in debug mode
// so add an if loop
// costly, so only in debug
    if(length > 0) {
#endif
                reduction_vec.segment(start, length) = 
                    this->orth_l2projection(fpt, m_faces_dof_bases[var_index][f_li], func);
#ifdef DEBUG
    } // end if this->m_skeletal_degree > 1
#endif
            }
            f_li++; // next face
        }

        // l2proj of degree m_bulk_degree over the bulk
        if(!only_bound_elemt) {
            // l2 projection over the bulk
            size_t start(this->m_bulk_dof[var_index][0]);
            size_t length(this->m_n_bulk_dof[var_index]);

            reduction_vec.segment(start, length) = 
                this->orth_l2projection(this->m_cell, this->m_bulk_bases[var_index], func);
        }

        return reduction_vec;

    } // apply_reduction_operator on vectorial variable


    // the rhs is filled with    (f,v_T)_T
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    void VEMBase<DIM, CONTINUOUS_SPACES...>::set_rhs(
                double func (Eigen::ArrayXd),
                int quadra_order,
                size_t var_index) {

        // v_T indexes
        size_t start(this->m_bulk_dof[var_index][0]);
        size_t length(this->m_n_bulk_dof[var_index]);

        this->m_local_rhs.segment(start, length) += 
            this->int_f_basis(this->m_cell, func, this->m_bulk_bases[var_index], quadra_order);

        return;
    } // set_rhs

    // the rhs is filled with    (f,v_T)_T
    template<size_t DIM, int... CONTINUOUS_SPACES> 
    void VEMBase<DIM, CONTINUOUS_SPACES...>::set_rhs(
                std::vector<double> func (Eigen::ArrayXd),
                int quadra_order,
                size_t var_index) {

        // v_T indexes
        size_t start(this->m_bulk_dof[var_index][0]);
        size_t length(this->m_n_bulk_dof[var_index]);

        this->m_local_rhs.segment(start, length) += 
            this->int_f_basis(this->m_cell, func, this->m_bulk_bases[var_index], quadra_order);

        return;
    } // set_rhs

} // MyDiscreteSpace namespace
#endif /* CONFORMING_VEM */
/** @} */
