#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <memory>
#include "common_io.hpp"
#include "read_and_build_mesh.hpp"
#include "vtk_writer.hpp"
#include "quadratures.hpp"
#include "hho.hpp"
#include "conforming_vem.hpp"
#include "pk_lagrange.hpp"
#include "discrete_space.hpp"
#include "matrix_pattern.hpp"
using namespace std;
// using namespace MyMesh;

#define DIM 2

int main(int argc, char** argv)
{
    // set kb and ks
    // CAREFUL : they can be modified by parse_execution_options (at the execution)
    int bulk_k(1), skeletal_k(1);

    string full_mesh_file, output_vtk_dir, output_vtk_meshfile, 
           output_vtk_numsol_file, output_vtk_exactsol_file,
           output_tex_dir, output_tex_file, 
           output_error_directory, output_error_textfile, output_solver_textfile;

    // fill full_mesh_file and output options
    try {
        // CAREFUL : bulk_k and skeletal_k can be modified at the execution using -b and/or -s
        parse_execution_options(argc, argv, DIM, bulk_k, skeletal_k, full_mesh_file, 
                output_vtk_dir, output_vtk_meshfile, output_vtk_numsol_file, output_vtk_exactsol_file,
                output_tex_dir, output_tex_file, output_error_directory, 
                output_error_textfile, output_solver_textfile);
    } catch (const char* msg) {
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    unique_ptr<MyMesh::Mesh<DIM>> mesh(nullptr);
    try {
        mesh = MyMesh::read_and_build_mesh<DIM>(full_mesh_file);
    } catch (const char* msg) {
        cerr << "Could not read the mesh file " << full_mesh_file << " or build the mesh " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    if(!output_vtk_meshfile.empty()) {
        // write vtu file with the mesh
        try {
            // creates the VtkVisu class
            MyVisu::VtkVisu<DIM> vtk(mesh.get());
            vtk.write_mesh(output_vtk_dir, output_vtk_meshfile);
        } catch (const char* msg) {
            cerr << msg << endl;
            return EXIT_FAILURE; 
        }
    }

    // Creates face quadrature
    size_t deg(0);

    try {
        for(auto& fpt : mesh->get_faces()) {
            // quadrature
            MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(fpt, deg));

            cout << " --------------------------------- " << endl;
            cout << " Quadratures for degree  " << deg << endl << endl;
            for(auto& qp : qps.list()) {
                cout << "     pt    ";
                for(size_t i=0; i<DIM; i++) cout << "   " << qp->point()[i];
                cout << "    with weight " << qp->weight() << endl;
            }
        }
    } catch (const char* msg) {
        cerr << " Could not create face quadrature " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    // creates the polytopal element classes over the whole mesh with k order in cells and ks order at skeletal unknowns
    MyDiscreteSpace::DiscreteSpace<DIM> ds(MyDiscreteSpace::DiscreteSpace<DIM>(mesh.get()));
    try {
        // creates the polytopal element instantiations with local nb of dof, 
        // reconstruction operator, stiffness matrix
        ds.init<MyDiscreteSpace::VEM,MyDiscreteSpace::H1>(mesh.get(), bulk_k, skeletal_k);
    } catch (const char* msg) {
        cerr << "Could not init the DiscreteSpace " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    // Initialize the global indexes and the map between the local and global indexes
    // Must be done after init or init_cells (for all cells)
    MySolver::MatrixPattern<DIM> mp(mesh.get(), &ds);
    try {
        mp.assemble_local_contributions(mesh.get(), &ds);

        //solve the equation global_matrix * x = global_rhs
        Eigen::BiCGSTAB<Eigen::SparseMatrix<double> > solver;
        solver.compute(mp.global_matrix());
        Eigen::VectorXd res(solver.solve(mp.global_rhs()));

        cout << " -------------------------------- " << endl;
        cout << "result of equation " << endl;
        cout << res << endl;

    } catch (const char* msg) {
        cerr << "Could not assemble the local contributions " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }


    mesh.reset(nullptr); mesh = 0;

    cout << "end of code" << endl;
}
