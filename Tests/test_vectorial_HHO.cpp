/**
  * @{
*/
#include <iostream>
#include <vector>
#include <memory>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SparseLU>
#include <time.h>
#include "common_io.hpp"
#include "mesh.hpp"
#include "read_and_build_mesh.hpp"
#include "vtk_writer.hpp"
#include "tex_writer.hpp"
#include "hho.hpp"
#include "discrete_space.hpp"
#include "matrix_pattern.hpp"
#include "norm.hpp"
#include <math.h>       // cos
#define PI 3.14159265

using namespace std;

/**
 * @file test_vectorial_HHO.cpp
 * 
 * The aim of this test is to introduce a vectorial unknown.
 * Solve the Poisson problem with u in Rd :
 *         - Laplacien(u) = f,                                                 <br>
 *                      u = u0 at the boundary                                 <br>
 * 
 * Test with u(x,y) = ( cos(Pi x) * cos(Pi y);                                 <br>
 *                      sin(Pi x) * sin(Pi y) + x;                             <br>
 *                      cos(Pi x) * sin(Pi y) + z )                            <br>
 *        so f(x,y) = ( 2 * Pi^2 * cos(Pi x) * cos(Pi y);                      <br>
 *                      2 * Pi^2 * sin(Pi x) * sin(Pi y);                      <br>
 *                      2 * Pi^2 * cos(Pi x) * sin(Pi y) )
 * 
*/


#define DIM 2  ///< dimension must be known at the compilation


std::vector<double> exact_sol(const Eigen::ArrayXd pt) {  // also used to define u_0
    std::vector<double> sol(DIM);

    sol[0] = cos(PI * pt[0]) * cos(PI * pt[1]);
    sol[1] = sin(PI * pt[0]) * sin(PI * pt[1]) + pt[0];
#if DIM==3
    sol[2] = cos(PI * pt[0]) * sin(PI * pt[1]) + pt[2];
#endif
    return sol;
}

Eigen::MatrixXd grad_exact_sol(const Eigen::ArrayXd pt) {
    Eigen::MatrixXd grad(Eigen::MatrixXd::Zero(DIM, DIM));
    grad(0, 0) = - PI * sin(PI * pt[0]) * cos(PI * pt[1]); // sol[0] \x
    grad(0, 1) = - PI * cos(PI * pt[0]) * sin(PI * pt[1]); // sol[0] \y
    grad(1, 0) = PI * cos(PI * pt[0]) * sin(PI * pt[1]) + 1.; // sol[1] \x
    grad(1, 1) = PI * sin(PI * pt[0]) * cos(PI * pt[1]); // sol[1] \y
#if DIM==3
    grad(2, 0) = - PI * sin(PI * pt[0]) * sin(PI * pt[1]); // sol[2] \x
    grad(2, 1) = PI * cos(PI * pt[0]) * cos(PI * pt[1]); // sol[2] \y
    grad(2, 2) = 1.; // \z
#endif

    return grad;
}

std::vector<double> continuous_rhs(const Eigen::ArrayXd pt) { 
    std::vector<double> rhs(DIM);
    rhs[0] =  2. * std::pow(PI, 2) * cos(PI * pt[0]) * cos(PI * pt[1]);
    rhs[1] =  2. * std::pow(PI, 2) * sin(PI * pt[0]) * sin(PI * pt[1]);
#if DIM==3
    rhs[2] = 2. * std::pow(PI, 2) * cos(PI * pt[0]) * sin(PI * pt[1]);
#endif
    return rhs;
}


int main(int argc, char** argv)
{
    // set kb and ks
    // CAREFUL : they can be modified by parse_execution_options (at the execution)
    int bulk_k(2), skeletal_k(1);

    // timer
    clock_t total_time(clock());
    double mesh_read_time, solver_time, reconstruction_time, visu_time, norm_time, total_time_s;

    string full_mesh_file, output_vtk_dir, output_vtk_meshfile, 
           output_vtk_numsol_file, output_vtk_exactsol_file,
           output_tex_dir, output_tex_file, 
           output_error_directory, output_error_textfile, output_solver_textfile;

    // fill full_mesh_file and output options
    try {
        // CAREFUL : bulk_k and skeletal_k can be modified at the execution using -b and/or -s
        parse_execution_options(argc, argv, DIM, bulk_k, skeletal_k, full_mesh_file, 
                output_vtk_dir, output_vtk_meshfile, output_vtk_numsol_file, output_vtk_exactsol_file,
                output_tex_dir, output_tex_file, output_error_directory, 
                output_error_textfile, output_solver_textfile);
    } catch (const char* msg) {
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    unique_ptr<MyMesh::Mesh<DIM>> mesh(nullptr);
    try {
        // timer
        clock_t t(clock());
        bool build_edges(false); // do not build edges with Crouzeix-Raviart or HHO
        mesh = MyMesh::read_and_build_mesh<DIM>(full_mesh_file, build_edges);

        cout << " Mesh regularity :" << endl;
        cout << "     extremum mesh diameters ratio " << mesh->h_max()/mesh->h_min() << endl;
        cout << "     anisotropy max " << mesh->max_anisotropy() << " min   " << mesh->min_anisotropy() << endl << endl;

        t = clock() - t;
        mesh_read_time = ((double)t)/CLOCKS_PER_SEC;
    } catch (const char* msg) {
        cerr << "Could not read the mesh file " << full_mesh_file << " or build the mesh " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    // creates the polytopal element classes over the whole mesh
    MyDiscreteSpace::DiscreteSpace<DIM> ds(MyDiscreteSpace::DiscreteSpace<DIM>(mesh.get()));
    try {
        // creates the polytopal element instantiations with local nb of dof, 
        // reconstruction operator, stiffness and mass matrix
        ds.init<MyDiscreteSpace::HHO,MyDiscreteSpace::H1d>(mesh.get(), bulk_k, skeletal_k);

        // Add contributions to each local bilinear form and rhs
        ds.add_stiffness_contribution();

        // right-hand-side   \int f r_T v_T
        int specific_quadra_order(2 * skeletal_k + 2);  // optional, 6 by default, f is non-polynomial, need high quadra order
        ds.set_rhs(continuous_rhs, specific_quadra_order);

        // set that the BC is essential (Dirichlet)
        ds.set_BCtype(mesh.get(), MyDiscreteSpace::ESSENTIAL);
        // modify rhs to transform the pb into
        // a homogeneous Dirichlet BC pb
        ds.set_local_DirichletBC(exact_sol);

    } catch (const char* msg) {
        cerr << "Could not build the local contributions " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }
        

    // do we want static condensation ?
    bool static_cond = false; // can introduce troncature error
    // decide which BC are eliminated
    // Keeping the essential dof may have an impact on the 
    // ConjugateGradient resolution, so also on the result.
    // Safer to eliminate them
    bool essentialBC_elimination = true;
    bool naturalBC_elimination = false;
    // Initialize the global indexes and the map between the local and global indexes
    // Must be done after init or init_cells (for all cells) and after setting the BC
    MySolver::MatrixPattern<DIM> mp(mesh.get(), &ds,
        static_cond,
        essentialBC_elimination, naturalBC_elimination);


    // assemble and solve the global problem
    Eigen::VectorXd dof_values;
    try {
        // assemble local contributions into the global matrix
        // The elimination is performed (the dof with no global
        // index are eliminated).
        // Dirichlet BC are taken into account
        mp.assemble_local_contributions(mesh.get(), &ds);

        // timer
        clock_t t(clock());

#if DIM==2
        //solve the equation m_global_matrix * x = m_global_rhs
        // An important parameter of this class is the ordering 
        // method. It is used to reorder the columns 
        // (and eventually the rows) of the matrix to reduce 
        // the number of new elements that are created during 
        // numerical factorization. The cheapest method 
        //available is COLAMD.
        Eigen::SparseLU<Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int>> solver;
        // Compute the ordering permutation vector 
        // from the structural pattern of A
        solver.analyzePattern(mp.global_matrix());
        // Compute the numerical factorization 
        solver.factorize(mp.global_matrix()); 
        //Use the factors to solve the linear system 
        dof_values = solver.solve(mp.global_rhs()); 
#elif DIM==3
        // solve with iterative conjugate gradient
        Eigen::ConjugateGradient<Eigen::SparseMatrix<double>> solver;
        solver.compute(mp.global_matrix()); 
        dof_values = solver.solve(mp.global_rhs()); 

        cout << endl << " Conjugate Gradient #iterations:  " << solver.iterations();
        cout << ",  estimated error: " << solver.error() << endl << endl;

        if(!output_solver_textfile.empty()) { 
            // output_simu_solver appends in output_solver_textfile
            // the order of convergence, the mesh size, the time to solve the linear system, 
            // the number of cells, the size of the linear system,
            // the number of iterations of the solver and the estimated error of the solver
            output_simu_solver(output_error_directory, output_solver_textfile,
                skeletal_k + 1, mesh->h_mean(), (clock() - t) / CLOCKS_PER_SEC, 
                mesh->n_cells(), dof_values.size(),
                solver.iterations(), solver.error());
        }
#endif

        t = clock() - t;
        solver_time = ((double)t)/CLOCKS_PER_SEC;

        // timer
        t = clock();
        // reconstruct the local polynomials from the global dof values
        // first, deal with the global to local mapping, 
        // the static cond and the Dirichlet lifting,
        // then apply the reconstruction operator in each PE
        ds.reconstruction(mp.get_local_to_global_operator(), dof_values, mp.static_cond());

        t = clock() - t;
        reconstruction_time = ((double)t)/CLOCKS_PER_SEC;

    } catch (const char* msg) {
        cerr << "Could not assemble the local contributions " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    // visualization
    if(!output_vtk_numsol_file.empty()) {
        try {
            // timer
            clock_t t(clock());

            // creates the VtkVisu class
            // simplification of the visu file because the method is conforming, need to be precised
            // in the creation of VtkVisu instance and in ds.reconstruction_at_vertices
            MyVisu::VtkVisu<DIM> vtk(mesh.get());

            // the vtk visu needs the value at the vertices of mesh.
            // They are reconstructed from the numerical solution.
            // Contains the value of all component if vectorial variable
            vtk.add_var(ds.reconstruction_at_vertices(mesh.get()), "u"); // by default store the first var
                
            // value of the exact solution
            vtk.add_var(exact_sol, "exact_u");

            // write vtk file
            vtk.write_mesh(output_vtk_dir, output_vtk_meshfile);
            vtk.write(output_vtk_dir, output_vtk_numsol_file);

            t = clock() - t;
            visu_time = ((double)t)/CLOCKS_PER_SEC;

        } catch (const char* msg) {
            cerr << "Could not create the output file " << endl;
            cerr << msg << endl;
            return EXIT_FAILURE; 
        }
    }

    try {
        // timer
        clock_t t(clock());

#if DIM==2
        double mesh_size(mesh->h_max());
#elif DIM==3
        double mesh_size(mesh->h_mean());
#endif

        int norm_quadra_order(2 * skeletal_k + 1);  // quadra order of the norm computations 

        // I have (not frequently) troubles with quadrature = 3 in 2D
        if(DIM == 2 && norm_quadra_order == 3) { norm_quadra_order++; }; 

        // compute the L2 norm of the error, 
        // comparaison with the exact solution given as continuous fonction
        double relative_l2error(MyDiscreteSpace::relative_l2error(&ds, exact_sol, norm_quadra_order));

        // compute the H1 semi norm of the error, 
        // comparaison with the exact solution given as continuous fonction
        double relative_h1error(MyDiscreteSpace::relative_h1error(&ds, grad_exact_sol, norm_quadra_order));

        t = clock() - t;
        norm_time = ((double)t)/CLOCKS_PER_SEC;

        cout << " for mesh size (h_max in 2D, h_mean in 3D) " << mesh_size << " L2 error between the exact sol and the numerical sol " <<
        relative_l2error << endl;
        cout << " for mesh size (h_max in 2D, h_mean in 3D) " << mesh_size << " semi H1 error between the exact sol and the numerical sol " <<
        relative_h1error << endl;

        // timer
        total_time = clock() - total_time;
        total_time_s = ((double)total_time)/CLOCKS_PER_SEC;
        cout << endl << endl << " Total time (in s): " << total_time_s << "    with :" << endl;
        cout << "       mesh reader (in s):             " << mesh_read_time << "   in % : " << mesh_read_time / total_time_s *100 << endl;
        cout << "       init discrete space (in s):     " << ds.get_init_time() << "   in % : " << ds.get_init_time() / total_time_s *100 << endl;
        cout << "       build local matrices (in s):    " << ds.get_local_system_time() << "   in % : " << ds.get_local_system_time() / total_time_s *100 << endl;
        cout << "       init matrix pattern (in s):     " << mp.get_init_time() << "   in % : " << mp.get_init_time() / total_time_s *100 << endl;
        cout << "       assemble (in s):                " << mp.get_assemble_time() << "   in % : " << mp.get_assemble_time() / total_time_s *100 << endl;
        cout << "       solve (in s):                   " << solver_time << "   in % : " << solver_time / total_time_s *100 << endl;
        cout << "       reconstruct (in s):             " << reconstruction_time << "   in % : " << reconstruction_time / total_time_s *100 << endl;
        if(!output_vtk_meshfile.empty()) {
            cout << "       visualization (in s):           " << visu_time << "   in % : " << visu_time / total_time_s *100 << endl; }
        cout << "       norms computation (in s):       " << norm_time << "   in % : " << norm_time / total_time_s *100 << endl;
        cout << endl;




        if(!output_error_textfile.empty()) { 
            // output_simu_characteristic_num appends in output_error_textfile
            // the order of convergence, the mesh size, the time to solve the linear system, 
            // the total time of the execution, the number of cells, the size of the linear system,
            // the relative error in L2 norm and the relative error in semi-norm H1
            output_simu_characteristic_num(output_error_directory, output_error_textfile,
                skeletal_k + 1, mesh_size, solver_time, total_time_s, mesh->n_cells(), dof_values.size(), 
                relative_l2error, relative_h1error);
        }

    } catch (const char* msg) {
        cerr << "Could not compute errors " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }


    mesh.reset(nullptr); mesh = 0;

    cout << "end of code" << endl;

}
/** @} */
