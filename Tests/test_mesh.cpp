#include <iostream>
#include <vector>
#include <memory>
#include <Eigen/Dense>
#include "common_io.hpp"
#include "read_and_build_mesh.hpp"
#include "vtk_writer.hpp"
#include "tex_writer.hpp"
using namespace std;
// using namespace MyMesh;

#define DIM 2

int main(int argc, char** argv)
{


    string full_mesh_file, output_vtk_dir, output_vtk_meshfile, 
           output_vtk_numsol_file, output_vtk_exactsol_file,
           output_tex_dir, output_tex_file, 
           output_error_directory, output_error_textfile, output_solver_textfile;
    int unused_bulk_k(0), unused_skeletal_k(0);

    // fill full_mesh_file and output options
    try {
        parse_execution_options(argc, argv, DIM, unused_bulk_k, unused_skeletal_k, full_mesh_file, 
                output_vtk_dir, output_vtk_meshfile, output_vtk_numsol_file, output_vtk_exactsol_file,
                output_tex_dir, output_tex_file, output_error_directory, 
                output_error_textfile, output_solver_textfile);
    } catch (const char* msg) {
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    unique_ptr<MyMesh::Mesh<DIM>> mesh(nullptr);
    try {
        mesh = MyMesh::read_and_build_mesh<DIM>(full_mesh_file);
    } catch (const char* msg) {
        cerr << "Could not read the mesh file " << full_mesh_file << " or build the mesh " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    if(!output_vtk_meshfile.empty()) {
        // write vtu file with the mesh
        try {
            // creates the VtkVisu class
            MyVisu::VtkVisu<DIM> vtk(mesh.get());
            // visualization of the mesh
            vtk.write_mesh(output_vtk_dir, output_vtk_meshfile);
        } catch (const char* msg) {
            cerr << msg << endl;
            return EXIT_FAILURE; 
        }
    }
#if DIM==2
    if(!output_tex_file.empty()) {
        // write tex file with the mesh
        bool plot_centers = false;
        try {
            write_tex_mesh(mesh.get(), output_tex_dir, output_tex_file, plot_centers); 
        } catch (const char* msg) {
            cerr << msg << endl;
            return EXIT_FAILURE; 
        }
    }
#endif

    mesh.reset(nullptr); mesh = 0;

    cout << "end of code" << endl;
}
