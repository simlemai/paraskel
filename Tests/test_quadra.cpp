#include <iostream>
#include <vector>
#include <memory>
#include "common_io.hpp"
#include "mesh.hpp"
#include "read_and_build_mesh.hpp"
#include "hho.hpp"
#include "quadratures.hpp"
using namespace std;

#define DIM 3

int main(int argc, char** argv)
{
    string full_mesh_file, output_vtk_dir, output_vtk_meshfile, 
           output_vtk_numsol_file, output_vtk_exactsol_file,
           output_tex_dir, output_tex_file, 
           output_error_directory, output_error_textfile, output_solver_textfile;
    int unused_bulk_k(0), unused_skeletal_k(0);;

    // fill full_mesh_file and output options
    try {
        parse_execution_options(argc, argv, DIM, unused_bulk_k, unused_skeletal_k, full_mesh_file, 
                output_vtk_dir, output_vtk_meshfile, output_vtk_numsol_file, output_vtk_exactsol_file,
                output_tex_dir, output_tex_file, output_error_directory, 
                output_error_textfile, output_solver_textfile);
    } catch (const char* msg) {
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    unique_ptr<MyMesh::Mesh<DIM>> mesh(nullptr);
    try {
        mesh = MyMesh::read_and_build_mesh<DIM>(full_mesh_file);
    } catch (const char* msg) {
        cerr << "Could not read the mesh file " << full_mesh_file << " or build the mesh " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }


    // cells 1173 to 1360 are square-pyramids with gmsh_meshes/cube_hexagonal.msh
    size_t g_i(0); 
    size_t deg(5);

    cout << " -------------       For 3D cell " << g_i << endl;
    cout << " Vertices coordinates are " << endl;
    for(auto& v : mesh->cell(g_i)->get_vertices()) {
        cout << "         ";
        for(size_t i=0; i<DIM; i++) cout << "   " << v->coords()[i];
        cout << endl;
    }
    cout << " Centroid coordinates are " << endl;
    cout << "         ";
    for(size_t i=0; i<DIM; i++) cout << "   " << mesh->cell(g_i)->mass_center()[i];
    cout << endl;
    cout << " Measure is " << endl;
    cout << "         " << " " << mesh->cell(g_i)->measure() << endl;

    // ----------------------------------------------------------------------------
    // construct all the quadrature points and weights for the cell g_i
    // (Gauss Legendre method of degree deg)
    MyQuadra::QuadraturePoints<DIM> qps(MyQuadra::integrate(mesh->cell(g_i), deg));
    // ----------------------------------------------------------------------------


    cout << " --------------------------------- " << endl;
    cout << " Quadratures for degree  " << deg << endl << endl;
    for(auto& qp : qps.list()) {
        cout << "     pt    ";
        for(size_t i=0; i<DIM; i++) cout << "   " << qp->point()[i];
        cout << "    with weight " << qp->weight() << endl;
    }
    
    cout << " --------------------------------- " << endl;
    cout << " integrate for function x^4 is   ";
    double integ(0.0);
    for(auto& qp : qps.list()) {
        // phi = x^2 on qp
        const double phi(qp->point()[0] * qp->point()[0] * qp->point()[0] * qp->point()[0]);
        integ += qp->weight() * phi;
    }
    cout << integ << endl;

    cout << " --------------------------------- " << endl;
    cout << " integrate for function x^2 + y is   ";
    integ = 0.0;
    for(auto& qp : qps.list()) {
        // phi = x + y on qp
        const double phi(qp->point()[0] * qp->point()[0] + qp->point()[1]);
        integ += qp->weight() * phi;
    }
    cout << integ << endl;

    cout << " --------------------------------- " << endl;
    cout << " integrate for function x + y^2 is   ";
    integ = 0.0;
    for(auto& qp : qps.list()) {
        // phi = x + y on qp
        const double phi(qp->point()[0] + qp->point()[1] * qp->point()[1]);
        integ += qp->weight() * phi;
    }
    cout << integ << endl;

    cout << " --------------------------------- " << endl;
    cout << " integrate for function x^2 + y^2 is   ";
    integ = 0.0;
    for(auto& qp : qps.list()) {
        // phi = x + y on qp
        const double phi(qp->point()[0] * qp->point()[0] + qp->point()[1] * qp->point()[1]);
        integ += qp->weight() * phi;
    }
    cout << integ << endl;

    cout << " --------------------------------- " << endl;
    cout << " integrate for function y^4 is   ";
    integ = 0.0;
    for(auto& qp : qps.list()) {
        // phi = x + y on qp
        const double phi(qp->point()[1] * qp->point()[1] * qp->point()[1] * qp->point()[1]);
        integ += qp->weight() * phi;
    }
    cout << integ << endl;

    if(DIM == 3) {
        cout << " --------------------------------- " << endl;
        cout << " integrate for function x + z^2 is   ";
        integ = 0.0;
        for(auto& qp : qps.list()) {
            // phi = x + y on qp
            const double phi(qp->point()[0] + qp->point()[2] * qp->point()[2]);
            integ += qp->weight() * phi;
        }
        cout << integ << endl;

        cout << " --------------------------------- " << endl;
        cout << " integrate for function z^3 is   ";
        integ = 0.0;
        for(auto& qp : qps.list()) {
            // phi = x + y on qp
            const double phi(qp->point()[2] * qp->point()[2] * qp->point()[2]);
            integ += qp->weight() * phi;
        }
        cout << integ << endl;
    }


    mesh.reset(nullptr); mesh = 0;

    cout << "end of code" << endl;

}
