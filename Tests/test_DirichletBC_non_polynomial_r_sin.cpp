#include <iostream>
#include <vector>
#include <memory>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include<Eigen/SparseLU>
#include <time.h>
#include "common_io.hpp"
#include "mesh.hpp"
#include "read_and_build_mesh.hpp"
#include "vtk_writer.hpp"
#include "tex_writer.hpp"
#include "p1_crouzeix_raviart.hpp"
#include "pk_lagrange.hpp"
#include "discrete_space.hpp"
#include "matrix_pattern.hpp"
#include "norm.hpp"
#include <math.h>       // cos, pi

using namespace std;

#define DIM 2

    /**
     * Solve with P1 Crouzeix-Raviart or P1 Lagrange FE method
     *         - Laplacien(u) = f
     *                      u = u_0 at the boundary
     * 
     * Test with u(r, theta) = r^2/3 * sin(2/3 theta) in polar coordinates
     *        so f(x,y) = 0
    */


double exact_sol(const Eigen::ArrayXd pt) {  // also used to define u_0
    // convert cartesian to polar coordinates
    double r2( pow(pt[0], 2) + pow(pt[1], 2) );

    double theta;
    if(abs(pt[0]) > 1e-13) { // x is not 0 (middle of the domain)
        theta = atan( pt[1] / pt[0] );
        if(pt[0] < 0.0) theta += M_PI;
        // 0 <= theta < 2 * pi
        if(theta < 0.0) theta += 2.0 * M_PI;
        if(theta > 2.0 * M_PI) theta -= 2.0 * M_PI;
    } else if(pt[1] > - 1e-13) {
        theta = M_PI / 2.0;
    } else {
        theta = M_PI * 3.0 / 2.0;
    }

    return pow(r2, 1.0/3.0) * sin(2.0/3.0 * theta );
}

Eigen::RowVectorXd grad_exact_sol(const Eigen::ArrayXd pt) {
    // convert cartesian to polar coordinates
    double r2( pow(pt[0], 2) + pow(pt[1], 2) );

    double theta;
    if(abs(pt[0]) > 1e-7) { // x is not 0 (middle of the domain)
        theta = atan( pt[1] / pt[0] );
        if(pt[0] < 0.0) theta += M_PI;
        // 0 <= theta < 2 * pi
        if(theta < - 1e-8) theta += 2.0 * M_PI;
        if(theta > 2.0 * M_PI) theta -= 2.0 * M_PI;
    } else if(pt[1] > 0.0) {
        theta = M_PI / 2.0;
    } else {
        theta = M_PI * 3.0 / 2.0;
    }

    double grad_tan(1+pow(pt[1]/pt[0], 2.));
    Eigen::RowVectorXd grad(DIM);
    grad[0] = 2./3. * pt[0] * pow(r2, -2.0/3.0) * sin(2.0/3.0 * theta) + 
                pow(r2, 1.0/3.0) * (- 2.) * pt[1] * cos(2.0/3.0 * theta)
                / 3. / pow(pt[0], 2.) / grad_tan;  // \x
    grad[1] = 2./3. * pt[1] * pow(r2, -2.0/3.0) * sin(2.0/3.0 * theta) + 
                pow(r2, 1.0/3.0) * 2. * cos(2.0/3.0 * theta) / 3. / pt[0] / grad_tan; // \y
    return grad;
}


double continuous_rhs(const Eigen::ArrayXd pt) { 
    return 0.0;
}


int main(int argc, char** argv)
{
    // set ks
    // CAREFUL : can be modified by parse_execution_options (at the execution)
    int skeletal_k(1);

    // timer
    clock_t total_time(clock());
    double total_time_s;
    double mesh_read_time(0.0);
    double solver_time(0.0);
    double visu_time(0.0);

    string full_mesh_file, output_vtk_dir, output_vtk_meshfile, 
           output_vtk_numsol_file, output_vtk_exactsol_file,
           output_tex_dir, output_tex_file, 
           output_error_directory, output_error_textfile, output_solver_textfile;
    int unused_bulk_k(0);

    // fill full_mesh_file and output options
    try {
        // CAREFUL : skeletal_k can be modified at the execution using -s
        parse_execution_options(argc, argv, DIM, unused_bulk_k, skeletal_k, full_mesh_file, 
                output_vtk_dir, output_vtk_meshfile, output_vtk_numsol_file, output_vtk_exactsol_file,
                output_tex_dir, output_tex_file, output_error_directory, 
                output_error_textfile, output_solver_textfile);
    } catch (const char* msg) {
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    unique_ptr<MyMesh::Mesh<DIM>> mesh(nullptr);
    try {
        // timer
        clock_t t(clock());
        // bool build_edges(false); // do not build edges with Crouzeix-Raviart
        // mesh = MyMesh::read_and_build_mesh<DIM>(full_mesh_file, build_edges);
        mesh = MyMesh::read_and_build_mesh<DIM>(full_mesh_file);
        t = clock() - t;
        mesh_read_time = ((double)t)/CLOCKS_PER_SEC;
    } catch (const char* msg) {
        cerr << "Could not read the mesh file " << full_mesh_file << " or build the mesh " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    // creates the polytopal element classes over the whole mesh
    MyDiscreteSpace::DiscreteSpace<DIM> ds(MyDiscreteSpace::DiscreteSpace<DIM>(mesh.get()));
    try {
        // creates the polytopal element instantiations with local nb of dof, 
        // reconstruction operator, stiffness and mass matrix
        // ds.init<MyDiscreteSpace::P1CrouzeixRaviart,MyDiscreteSpace::H1>(mesh.get(), skeletal_k); // always 1
        ds.init<MyDiscreteSpace::PkLagrange,MyDiscreteSpace::H1>(mesh.get(), skeletal_k);

        // Add contributions to each local bilinear form and rhs
        ds.add_stiffness_contribution();

        // right-hand-side   \int f r_T v_T
        // with f(x,y) = 0
        int specific_quadra_order(1); // f is nul, do not need high quadra order
        ds.set_rhs(continuous_rhs, specific_quadra_order);

        // set that the BC is essential (Dirichlet)
        ds.set_BCtype(mesh.get(), MyDiscreteSpace::ESSENTIAL);
        // modify rhs to transform the pb into
        // a homogeneous Dirichlet BC pb
        ds.set_local_DirichletBC(exact_sol);

    } catch (const char* msg) {
        cerr << "Could not build the local contributions " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }
        

    // do we want static condensation ?
    bool static_cond = false;
    // decide which BC are eliminated
    // Keeping the essential dof may have an impact on the 
    // ConjugateGradient resolution, so also on the result.
    // Safer to eliminate them
    bool essentialBC_elimination = true;
    bool naturalBC_elimination = false;
    // Initialize the global indexes and the map between the local and global indexes
    // Must be done after init or init_cells (for all cells) and after setting the BC
    MySolver::MatrixPattern<DIM> mp(mesh.get(), &ds,
        static_cond,
        essentialBC_elimination, naturalBC_elimination);


    // assemble and solve the global problem
    Eigen::VectorXd dof_values;
    try {
        // assemble local contributions into the global matrix
        // The elimination is performed (the dof with no global
        // index are eliminated).
        // Dirichlet BC are taken into account
        mp.assemble_local_contributions(mesh.get(), &ds);

        // timer
        clock_t t(clock());

        //solve the equation m_global_matrix * x = m_global_rhs
        // An important parameter of this class is the ordering 
        // method. It is used to reorder the columns 
        // (and eventually the rows) of the matrix to reduce 
        // the number of new elements that are created during 
        // numerical factorization. The cheapest method 
        //available is COLAMD.
        Eigen::SparseLU<Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int>> solver;
        // Compute the ordering permutation vector 
        // from the structural pattern of A
        solver.analyzePattern(mp.global_matrix());
        // Compute the numerical factorization 
        solver.factorize(mp.global_matrix()); 
        //Use the factors to solve the linear system 
        dof_values = solver.solve(mp.global_rhs()); 

        // reconstruct polynomial from the dof values
        // includes the modification due to the Dirichlet lifting
        ds.reconstruction(mp.get_local_to_global_operator(), dof_values, mp.static_cond());

        t = clock() - t;
        solver_time = ((double)t)/CLOCKS_PER_SEC;

    } catch (const char* msg) {
        cerr << "Could not assemble the local contributions " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    // visualization
    if(!output_vtk_numsol_file.empty()) {
        try {
            // timer
            clock_t t(clock());

            // the vtk visu needs the value at the vertices of mesh.
            // by default extract the first var with non conforming method
            Eigen::MatrixXd num_sol_at_vertices(ds.reconstruction_at_vertices(mesh.get()));
                
            // creates the VtkVisu class  (by default non conforming method)
            MyVisu::VtkVisu<DIM> vtk(mesh.get());
            // Add to the visu the numerical solution
            vtk.add_var(num_sol_at_vertices, "numerical sol");
                
            // Add to the visu the exact solution
            vtk.add_var(exact_sol, "exact sol");

            // write vtk file
            vtk.write(output_vtk_dir, output_vtk_numsol_file);

            t = clock() - t;
            visu_time = ((double)t)/CLOCKS_PER_SEC;

        } catch (const char* msg) {
            cerr << "Could not create the output file " << endl;
            cerr << msg << endl;
            return EXIT_FAILURE; 
        }
    }

    try {
#if DIM==2
        double mesh_size(mesh->h_max());
#elif DIM==3
        double mesh_size(mesh->h_mean());
#endif

        int norm_quadra_order(2 * skeletal_k + 1);  // quadra order of the norm computations 
        // I have (not frequently) troubles with quadrature = 3 in 2D
        if(DIM == 2 && norm_quadra_order == 3) { norm_quadra_order++; };
        
        // compute the L2 norm of the error, 
        // comparaison with the exact solution given as continuous fonction
        double relative_l2error(MyDiscreteSpace::relative_l2error(&ds, exact_sol, norm_quadra_order));

        // compute the H1 semi norm of the error, 
        // comparaison with the exact solution given as continuous fonction
        double relative_h1error(MyDiscreteSpace::relative_h1error(&ds, grad_exact_sol, norm_quadra_order));


        cout << endl << " for mesh size " << mesh_size << " L2 error between the exact sol and the numerical sol " <<
        relative_l2error << endl;
        cout << " for mesh size " << mesh_size << " semi H1 error between the exact sol and the numerical sol " <<
        relative_h1error << endl;

        // timer
        total_time = clock() - total_time;
        total_time_s = ((double)total_time)/CLOCKS_PER_SEC;
        cout << endl << " Total time (in s): " << total_time_s << "    with :" << endl;
        cout << "       mesh reader (in s):             " << mesh_read_time << "   in % : " << mesh_read_time / total_time_s *100 << endl;
        cout << "       init discrete space (in s):     " << ds.get_init_time() << "   in % : " << ds.get_init_time() / total_time_s *100 << endl;
        cout << "       build local matrices (in s):    " << ds.get_local_system_time() << "   in % : " << ds.get_local_system_time() / total_time_s *100 << endl;
        cout << "       init matrix pattern (in s):     " << mp.get_init_time() << "   in % : " << mp.get_init_time() / total_time_s *100 << endl;
        cout << "       assemble (in s):                " << mp.get_assemble_time() << "   in % : " << mp.get_assemble_time() / total_time_s *100 << endl;
        cout << "       solve and reconstruct (in s):   " << solver_time << "   in % : " << solver_time / total_time_s *100 << endl;
        cout << "       visualization (in s):           " << visu_time << "   in % : " << visu_time / total_time_s *100 << endl;
        cout << endl;

        if(!output_error_textfile.empty()) { 
            // output_simu_characteristic_num appends in output_error_textfile
            // the order of convergence, the mesh size, the time to solve the linear system, 
            // the total time of the execution, the number of cells, the size of the linear system,
            // the relative error in L2 norm and the relative error in semi-norm H1
            output_simu_characteristic_num(output_error_directory, output_error_textfile,
                skeletal_k, mesh_size, solver_time, total_time_s, mesh->n_cells(), dof_values.size(), 
                relative_l2error, relative_h1error);
        }

    } catch (const char* msg) {
        cerr << "Could not compute errors " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }


    mesh.reset(nullptr); mesh = 0;

    cout << "end of code" << endl;

}
