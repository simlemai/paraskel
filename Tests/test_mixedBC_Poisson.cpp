#include <iostream>
#include <vector>
#include <memory>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include<Eigen/SparseLU>
#include <time.h>
#include "common_io.hpp"
#include "mesh.hpp"
#include "read_and_build_mesh.hpp"
#include "vtk_writer.hpp"
#include "tex_writer.hpp"
#include "p1_crouzeix_raviart.hpp"
#include "pk_lagrange.hpp"
#include "discrete_space.hpp"
#include "matrix_pattern.hpp"
#include "norm.hpp"
#include <math.h>       // cos
#define PI 3.14159265

using namespace std;

#define DIM 3

    /**
     * Solve with P1 Crouzeix-Raviart or P1 Lagrange FE method
     * the Poisson problem with mixed boundary conditions
     *         - Laplacien(u) = f
     *                Grad(u) . n = 0        at x = x_min and x = x_max
     *                      u = u_\partial   at y = y_min and y = y_max
     * 
     * in 2D              u(x,y) = cos(Pi x) * cos(Pi y) + y
     *                 so f(x,y) = 2 * Pi^2 * cos(Pi x) * cos(Pi y)
     * in 3D            u(x,y,z) = cos(Pi x) * cos(Pi y) + z
     *               so f(x,y,z) = 2 * Pi^2 * cos(Pi x) * cos(Pi y)
    */


double exact_sol(const Eigen::ArrayXd pt) {  // used in norm and to define u_\partial
#if DIM==2
    return cos(PI * pt[0]) * cos(PI * pt[1]) + pt[1];
#elif DIM==3
    return cos(PI * pt[0]) * cos(PI * pt[1]) + pt[2];
#endif
}

Eigen::RowVectorXd grad_exact_sol(const Eigen::ArrayXd pt) { // used only in norm
    Eigen::RowVectorXd grad(DIM);
    grad[0] = - PI * sin(PI * pt[0]) * cos(PI * pt[1]); // \x
    grad[1] = - PI * cos(PI * pt[0]) * sin(PI * pt[1]); // \y
#if DIM==2
    grad[1] += 1.; // \y  + pt[1]
#elif DIM==3
    grad[2] = 1.; // \z
#endif
    return grad;
}

double continuous_rhs(const Eigen::ArrayXd pt) { 
    return 2. * std::pow(PI, 2) * cos(PI * pt[0]) * cos(PI * pt[1]);
}


int main(int argc, char** argv)
{
    // set ks
    // CAREFUL : can be modified by parse_execution_options (at the execution)
    int skeletal_k(1);

    // timer
    clock_t total_time(clock());
    double total_time_s;
    double mesh_read_time(0.0);
    double solver_time(0.0);
    double visu_time(0.0);

    string full_mesh_file, output_vtk_dir, output_vtk_meshfile, 
           output_vtk_numsol_file, output_vtk_exactsol_file,
           output_tex_dir, output_tex_file, 
           output_error_directory, output_error_textfile, output_solver_textfile;
    int unused_bulk_k(0);

    // fill full_mesh_file and output options
    try {
        // CAREFUL : skeletal_k can be modified at the execution using -s
        parse_execution_options(argc, argv, DIM, unused_bulk_k, skeletal_k, full_mesh_file, 
                output_vtk_dir, output_vtk_meshfile, output_vtk_numsol_file, output_vtk_exactsol_file,
                output_tex_dir, output_tex_file, output_error_directory, 
                output_error_textfile, output_solver_textfile);
    } catch (const char* msg) {
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    unique_ptr<MyMesh::Mesh<DIM>> mesh(nullptr);
    try {
        // timer
        clock_t t(clock());
        mesh = MyMesh::read_and_build_mesh<DIM>(full_mesh_file);
        // bool build_edges(false); // do not build edges with Crouzeix-Raviart
        // mesh = MyMesh::read_and_build_mesh<DIM>(full_mesh_file, build_edges);

        cout << " Mesh regularity :" << endl;
        cout << "     extremum mesh diameters ratio " << mesh->h_max()/mesh->h_min() << endl;
        cout << "     anisotropy max " << mesh->max_anisotropy() << " min   " << mesh->min_anisotropy() << endl << endl;

        t = clock() - t;
        mesh_read_time = ((double)t)/CLOCKS_PER_SEC;
    } catch (const char* msg) {
        cerr << "Could not read the mesh file " << full_mesh_file << " or build the mesh " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    // creates the polytopal element classes over the whole mesh
    MyDiscreteSpace::DiscreteSpace<DIM> ds(MyDiscreteSpace::DiscreteSpace<DIM>(mesh.get()));
    try {
        // creates the polytopal element instantiations with local nb of dof, 
        // reconstruction operator, stiffness and mass matrix
        // ds.init<MyDiscreteSpace::P1CrouzeixRaviart,MyDiscreteSpace::H1>(mesh.get(), skeletal_k); // always 1
        ds.init<MyDiscreteSpace::PkLagrange,MyDiscreteSpace::H1>(mesh.get(), skeletal_k);

        // Add contributions to each local bilinear form and rhs
        ds.add_stiffness_contribution();

        // right-hand-side   \int f r_T v_T
        int specific_quadra_order(2 * skeletal_k + 2);  // optional, 6 by default, f is non-polynomial, need high quadra order
        ds.set_rhs(continuous_rhs, specific_quadra_order);

        // some BC are essential (Dirichlet) : at y = y_min and y = y_max
        // by default BC are init with natural flag (Neumann)
        mesh->find_domain_extremities(); // set the domain size to use ymin and ymax
        double eps(1.e-6);
        double min_y_BC(mesh->min_domain_extremities()[1] + eps);
        double max_y_BC(mesh->max_domain_extremities()[1] - eps);

#if DIM==2
        // the test is verified on the mass center of each boundary object 
        ds.set_BCtype(mesh.get(), MyDiscreteSpace::ESSENTIAL,
            [=](const Eigen::ArrayXd pt) { return ( pt[1] < min_y_BC || pt[1] > max_y_BC ); } 
            );
#elif DIM==3
        // the test is verified on the mass center of each boundary object 
        double min_z_BC(mesh->min_domain_extremities()[2] + eps);
        double max_z_BC(mesh->max_domain_extremities()[2] - eps);
        ds.set_BCtype(mesh.get(), MyDiscreteSpace::ESSENTIAL,
            [=](const Eigen::ArrayXd pt) { return ( pt[1] < min_y_BC || pt[1] > max_y_BC ||
                                                    pt[2] < min_z_BC || pt[2] > max_z_BC); }
            );
#endif

        // modify rhs to transform the pb into
        // a homogeneous Dirichlet BC pb
        ds.set_local_DirichletBC(exact_sol);

    } catch (const char* msg) {
        cerr << "Could not build the local contributions " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }
        

    // do we want static condensation ?
    bool static_cond = true;
    // decide which BC are eliminated
    // Keeping the essential dof may have an impact on the 
    // ConjugateGradient resolution, so also on the result.
    // Safer to eliminate them
    bool essentialBC_elimination = true;
    bool naturalBC_elimination = false;
    // Initialize the global indexes and the map between the local and global indexes
    // Must be done after init or init_cells (for all cells) and after setting the BC
    MySolver::MatrixPattern<DIM> mp(mesh.get(), &ds,
        static_cond,
        essentialBC_elimination, naturalBC_elimination);


    // assemble and solve the global problem
    Eigen::VectorXd dof_values;
    try {
        // assemble local contributions into the global matrix
        // The elimination is performed (the dof with no global
        // index are eliminated).
        // Dirichlet BC are taken into account
        mp.assemble_local_contributions(mesh.get(), &ds);

        // timer
        clock_t t(clock());

#if DIM==2
        //solve the equation m_global_matrix * x = m_global_rhs
        // An important parameter of this class is the ordering 
        // method. It is used to reorder the columns 
        // (and eventually the rows) of the matrix to reduce 
        // the number of new elements that are created during 
        // numerical factorization. The cheapest method 
        //available is COLAMD.
        Eigen::SparseLU<Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int>> solver;
        // Compute the ordering permutation vector 
        // from the structural pattern of A
        solver.analyzePattern(mp.global_matrix());
        // Compute the numerical factorization 
        solver.factorize(mp.global_matrix()); 
        //Use the factors to solve the linear system 
        dof_values = solver.solve(mp.global_rhs()); 

#elif DIM==3
            // solve with iterative conjugate gradient
            Eigen::ConjugateGradient<Eigen::SparseMatrix<double>> solver;
            solver.compute(mp.global_matrix()); 
            dof_values = solver.solve(mp.global_rhs()); 

            cout << endl << " Conjugate Gradient #iterations:  " << solver.iterations();
            cout << ",  estimated error: " << solver.error() << endl << endl;
#endif
        cout << endl << " The size of the linear system (nb of DOF which are not eliminated) is " << 
            dof_values.size()  << endl;

        // reconstruct polynomial from the dof values
        // includes the modification due to the Dirichlet lifting
        ds.reconstruction(mp.get_local_to_global_operator(), dof_values, mp.static_cond());

        t = clock() - t;
        solver_time = ((double)t)/CLOCKS_PER_SEC;

    } catch (const char* msg) {
        cerr << "Could not assemble the local contributions " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    // visualization
    if(!output_vtk_numsol_file.empty()) {
        try {
            // timer
            clock_t t(clock());
                
            // creates the VtkVisu class (by default non conforming method)
            MyVisu::VtkVisu<DIM> vtk(mesh.get());

            // Add to the visu the numerical solution
            // the vtk visu needs the value at the vertices of mesh.
             // by default store the first var with non conforming method
            vtk.add_var(ds.reconstruction_at_vertices(mesh.get()), "numerical sol");
                
            // Add to the visu the exact solution
            vtk.add_var(exact_sol, "exact sol");

            // write vtk file
            vtk.write(output_vtk_dir, output_vtk_numsol_file);

            t = clock() - t;
            visu_time = ((double)t)/CLOCKS_PER_SEC;

        } catch (const char* msg) {
            cerr << "Could not create the output file " << endl;
            cerr << msg << endl;
            return EXIT_FAILURE; 
        }
    }

    try {
#if DIM==2
        double mesh_size(mesh->h_max());
#elif DIM==3
        double mesh_size(mesh->h_mean());
#endif

        int norm_quadra_order(2 * skeletal_k + 1);  // quadra order of the norm computations 
        // I have (not frequently) troubles with quadrature = 3 in 2D
        if(DIM == 2 && norm_quadra_order == 3) { norm_quadra_order++; };  

        // compute the L2 norm of the error, 
        // comparaison with the exact solution given as continuous fonction
        double relative_l2error(MyDiscreteSpace::relative_l2error(&ds, exact_sol, norm_quadra_order));

        // compute the H1 semi norm of the error, 
        // comparaison with the exact solution given as continuous fonction
        double relative_h1error(MyDiscreteSpace::relative_h1error(&ds, grad_exact_sol, norm_quadra_order));

        cout << " for mesh size (mean diam) " << mesh_size << " L2 error between the exact sol and the numerical sol " <<
        relative_l2error << endl;
        cout << " for mesh size (mean diam) " << mesh_size << " semi H1 error between the exact sol and the numerical sol " <<
        relative_h1error << endl;

        // timer
        total_time = clock() - total_time;
        total_time_s = ((double)total_time)/CLOCKS_PER_SEC;
        cout << endl << " Total time (in s): " << total_time_s << "    with :" << endl;
        cout << "       mesh reader (in s):             " << mesh_read_time << "   in % : " << mesh_read_time / total_time_s *100 << endl;
        cout << "       init discrete space (in s):     " << ds.get_init_time() << "   in % : " << ds.get_init_time() / total_time_s *100 << endl;
        cout << "       build local matrices (in s):    " << ds.get_local_system_time() << "   in % : " << ds.get_local_system_time() / total_time_s *100 << endl;
        cout << "       init matrix pattern (in s):     " << mp.get_init_time() << "   in % : " << mp.get_init_time() / total_time_s *100 << endl;
        cout << "       assemble (in s):                " << mp.get_assemble_time() << "   in % : " << mp.get_assemble_time() / total_time_s *100 << endl;
        cout << "       solve and reconstruct (in s):   " << solver_time << "   in % : " << solver_time / total_time_s *100 << endl;
        if(!output_vtk_numsol_file.empty()) { 
            cout << "       visualization (in s):           " << visu_time << "   in % : " << visu_time / total_time_s *100 << endl;
        }
        cout << endl;

        if(!output_error_textfile.empty()) { 
            // append the characteristic numbers in file (mesh size, norm of error, ...)
            // careful, the number of dof is the size of the matrix, some dof might be eliminated !
            output_simu_characteristic_num(output_error_directory, output_error_textfile,
                skeletal_k, mesh_size, solver_time, total_time_s, mesh->n_cells(), dof_values.size(), 
                relative_l2error, relative_h1error);
        }

    } catch (const char* msg) {
        cerr << "Could not compute errors " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }


    mesh.reset(nullptr); mesh = 0;

    cout << "end of code" << endl;

}
