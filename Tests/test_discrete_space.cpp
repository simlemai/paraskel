#include <iostream>
#include <vector>
#include <memory>
#include "common_io.hpp"
#include "mesh.hpp"
#include "read_and_build_mesh.hpp"
#include "p1_crouzeix_raviart.hpp"
#include "pk_lagrange.hpp"
#include "discrete_space.hpp"
#include "matrix_pattern.hpp"
using namespace std;

#define DIM 3

int main(int argc, char** argv)
{
    // set ks
    // CAREFUL : can be modified by parse_execution_options (at the execution)
    int skeletal_k(1);

    string full_mesh_file, output_vtk_dir, output_vtk_meshfile, 
           output_vtk_numsol_file, output_vtk_exactsol_file,
           output_tex_dir, output_tex_file, 
           output_error_directory, output_error_textfile, output_solver_textfile;
    int unused_bulk_k(0);

    // fill full_mesh_file and output options
    try {
        // CAREFUL : skeletal_k can be modified at the execution using -s
        parse_execution_options(argc, argv, DIM, unused_bulk_k, skeletal_k, full_mesh_file, 
                output_vtk_dir, output_vtk_meshfile, output_vtk_numsol_file, output_vtk_exactsol_file,
                output_tex_dir, output_tex_file, output_error_directory, 
                output_error_textfile, output_solver_textfile);
    } catch (const char* msg) {
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    unique_ptr<MyMesh::Mesh<DIM>> mesh(nullptr);
    try {
        bool build_edges(false); // do not build edges with Crouzeix-Raviart
        mesh = MyMesh::read_and_build_mesh<DIM>(full_mesh_file, build_edges);
        // mesh = MyMesh::read_and_build_mesh<DIM>(full_mesh_file);
    } catch (const char* msg) {
        cerr << "Could not read the mesh file " << full_mesh_file << " or build the mesh " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    // creates the polytopal element classes over the whole mesh with k order in cells and ks order at squeletal unknowns
    MyDiscreteSpace::DiscreteSpace<DIM> ds(MyDiscreteSpace::DiscreteSpace<DIM>(mesh.get()));
    try {
        ds.init<MyDiscreteSpace::P1CrouzeixRaviart,MyDiscreteSpace::H1>(mesh.get(), skeletal_k);
        // ds.init<MyDiscreteSpace::PkLagrange,MyDiscreteSpace::H1>(mesh.get(), skeletal_k);
    } catch (const char* msg) {
        cerr << "Could not init the DiscreteSpace " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    // to print the local indexes, get one polytopal_element
    double norm_inf_error(0.);

    for(size_t g_i=0; g_i<mesh->n_cells(); g_i++) {
    // size_t g_i(15);
    // cout << " --------------------------------- " << endl;
    // cout << "       for cell " << g_i << endl;
    // cout << " --------------------------------- " << endl;
    MyDiscreteSpace::PolytopalElement<DIM>* pe(ds.polytopal_element(g_i));
    
    // cout << " size of local_to_global (n_cumulate_local_dof) " << pe->n_cumulate_local_dof() << endl << endl;

    // if(!pe->get_vertices_local_index().empty()) {
    //     for(size_t i=0; i<mesh->cell(g_i)->n_vertices(); i++) {
    //         cout<< "local index of vertex dof " << pe->get_vertices_local_index()[i] << endl;
    //     }
    // }
    // if(!pe->get_edges_local_index().empty()) {
    //     for(size_t i=0; i<mesh->cell(g_i)->n_edges(); i++) {
    //         cout<< "local index of edge dof " << pe->get_edges_local_index()[i] << endl;
    //     }
    // }
    // for(size_t i=0; i<mesh->cell(g_i)->n_faces(); i++) {
    //     cout<< "local index of face dof " << pe->get_faces_local_index()[i] << endl;
    // }
    // cout<< "local index of bulk dof " << pe->get_bulk_local_index() << endl << endl;
    
    // cout<< "nb of bulk dof " << pe->n_bulk_dof() << endl << endl;

    // // Initialize global indexes
    // // Must be done after init or init_cells (for all cells)
    // MySolver::MatrixPattern<DIM> mp(mesh.get(), &ds);
    // // to print, get the local to global operator
    // std::vector<std::vector<size_t>> local_to_global_operator(mp.get_local_to_global_operator());

    // if(!pe->get_vertices_local_index().empty()) {
    //     for(size_t i=0; i<mesh->cell(g_i)->n_vertices(); i++) {
    //         cout<< "global index of vertex dof " << mesh->cell(g_i)->get_vertices()[i]->discrete_unknown_global_index() << endl;
    //     }
    // }
    // if(!pe->get_edges_local_index().empty()) {
    //     for(size_t i=0; i<mesh->cell(g_i)->n_edges(); i++) {
    //         cout<< "global index of edge dof " << mesh->cell(g_i)->get_edges()[i]->discrete_unknown_global_index() << endl;
    //     }
    // }
    // for(size_t i=0; i<mesh->cell(g_i)->n_faces(); i++) {
    //     cout<< "global index of face dof " << mesh->cell(g_i)->get_faces()[i]->discrete_unknown_global_index() << endl;
    // }
    // cout<< "global index of bulk dof " << mesh->cell(g_i)->discrete_unknown_global_index() << endl << endl;

    // if(!pe->get_vertices_local_index().empty()) {
    //     for(size_t i=0; i<mesh->cell(g_i)->n_vertices(); i++) {
    //         cout<< "local to global of vertex dof " << local_to_global_operator[g_i][pe->get_vertices_local_index()[i]] << endl;
    //     }
    // }
    // if(!pe->get_edges_local_index().empty()) {
    //     for(size_t i=0; i<mesh->cell(g_i)->n_edges(); i++) {
    //         cout<< "local to global of edge dof " << local_to_global_operator[g_i][pe->get_edges_local_index()[i]] << endl;
    //     }
    // }
    // for(size_t i=0; i<mesh->cell(g_i)->n_faces(); i++) {
    //     cout<< "local to global of face dof " << local_to_global_operator[g_i][pe->get_faces_local_index()[i]] << endl;
    // }
    // cout<< "local to global of bulk dof " << local_to_global_operator[g_i][pe->get_bulk_local_index()] << endl << endl;

    // // print info about the vertices
    // std::vector<MyMesh::Vertex<2>*> vertices(mesh->cell(g_i)->get_vertices());
    // cout << " vertices coords " << endl;
    // for(size_t i=0; i<vertices.size(); i++) {
    //     cout << vertices[i]->coords().transpose() << endl;
    // }
    // cout << endl << " center of mass of cell " << mesh->cell(g_i)->mass_center().transpose() << endl;
    // cout << " diameter " << mesh->cell(g_i)->diam() << endl << endl;


    Eigen::MatrixXd mu(pe->get_reconstruction_operator());
    // std::cout << " m_reconstruction_operator " << std::endl << mu << std::endl << std::endl;

    // test
    Eigen::MatrixXd test(4,4);
    MyMesh::Cell<DIM>* c(mesh->cell(g_i));
    size_t cv(0);
    for(auto& f : c->get_faces()) { // Crouzeix-Raviart
        test(cv,0) = 1.;
        test(cv,1) = (f->mass_center()[0] - c->mass_center()[0])/c->diam()/0.5;
        test(cv,2) = (f->mass_center()[1] - c->mass_center()[1])/c->diam()/0.5;
        test(cv,3) = (f->mass_center()[2] - c->mass_center()[2])/c->diam()/0.5;
        cv++;
    }
    // for(auto& v : c->get_vertices()) { // Lagrange
    //     test(cv,0) = 1.;
    //     test(cv,1) = (v->coords()[0] - c->mass_center()[0])/c->diam()/0.5;
    //     test(cv,2) = (v->coords()[1] - c->mass_center()[1])/c->diam()/0.5;
    //     test(cv,3) = (v->coords()[2] - c->mass_center()[2])/c->diam()/0.5;
    //     cv++;
    // }
    Eigen::MatrixXd inv_test(test.lu().solve(Eigen::MatrixXd::Identity(4, 4)));

    double diff((inv_test - mu).norm());
    norm_inf_error = max(norm_inf_error, diff);

    // if(diff > 1.e-12) {
    //     cout << " test =? 0        " << diff << endl;
    //     cerr << "not good m_reconstruction_operator " << endl;
    //     return EXIT_FAILURE; 
    // }
    }

    mesh.reset(nullptr); mesh = 0;

    cout << "max error in test m_reconstruction_operator is " << norm_inf_error << endl;
    cout << "end of code" << endl;

}
