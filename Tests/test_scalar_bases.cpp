#include <iostream>
#include <vector>
#include <memory>
#include "common_io.hpp"
#include "mesh.hpp"
#include "read_and_build_mesh.hpp"
#include "hho.hpp"
#include "bases.hpp"
using namespace std;

#define DIM 2

int main(int argc, char** argv)
{
    string full_mesh_file, output_vtk_dir, output_vtk_meshfile, 
           output_vtk_numsol_file, output_vtk_exactsol_file,
           output_tex_dir, output_tex_file, 
           output_error_directory, output_error_textfile, output_solver_textfile;
    int unused_bulk_k(0), unused_skeletal_k(0);

    // fill full_mesh_file and output options
    try {
        parse_execution_options(argc, argv, DIM, unused_bulk_k, unused_skeletal_k, full_mesh_file, 
                output_vtk_dir, output_vtk_meshfile, output_vtk_numsol_file, output_vtk_exactsol_file,
                output_tex_dir, output_tex_file, output_error_directory, 
                output_error_textfile, output_solver_textfile);
    } catch (const char* msg) {
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    unique_ptr<MyMesh::Mesh<DIM>> mesh(nullptr);
    try {
        mesh = MyMesh::read_and_build_mesh<DIM>(full_mesh_file);
    } catch (const char* msg) {
        cerr << "Could not read the mesh file " << full_mesh_file << " or build the mesh " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    // construct the basis for different mesh elements
    try {
        // Cell
        size_t c_gi(0);
        MyMesh::Cell<DIM>* cpt(mesh->cell(c_gi));
        MyBasis::Basis<MyMesh::Cell<DIM>*>* mesh_basis(new MyBasis::ScaledMonomialScalarBasis<MyMesh::Cell<DIM>*>(cpt, 1));
        Eigen::VectorXd eval(mesh_basis->eval_functions(cpt->mass_center()).transpose());
        // gradients
        vector<Eigen::MatrixXd> eval_grad(mesh_basis->eval_gradients(cpt->mass_center()));

        cout << " center of mass of cell " << cpt->mass_center().transpose() << endl;
        cout << " diameter " << cpt->diam() << endl;
        cout << " eval at center of mass " << eval.transpose() << endl;
        cout << " eval_grad at center of mass " << endl;
                for(auto& grad : eval_grad) { cout << grad << endl;}
                cout << endl << endl;
        // eval and eval_grad at all vertices of cell
        for(size_t i = 0; i < cpt->n_vertices(); i++) {
            Eigen::VectorXd point(cpt->get_vertices()[i]->coords());
            eval = mesh_basis->eval_functions(point).transpose();
            eval_grad = mesh_basis->eval_gradients(point);
            cout << " for vertex " << point.transpose() << endl;
            cout << " eval is " << eval.transpose() << endl;
            cout << " eval_grad is " << endl;
                for(auto& grad : eval_grad) { cout << grad << endl;}
                cout << endl << endl;
        }
        
        delete mesh_basis; mesh_basis = 0;

    } catch (const char* msg) {
        cerr << "Could not build the base " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }
    try {
        // Faces of cell c_gi
        size_t c_gi(0);
        MyMesh::Cell<DIM>* cpt(mesh->cell(c_gi));
        for(auto& fpt : cpt->get_faces()) {
            size_t f_gi(fpt->global_index());
            
            cout << " ----------------------------------------------" << endl;
            cout << " for face " << f_gi << endl;
            cout << " center of mass of face " << fpt->mass_center().transpose() << endl;
            cout << " diameter " << fpt->diam() << endl;

            MyBasis::Basis<MyMesh::Face<DIM>*>*  mesh_basis(new MyBasis::ScaledMonomialScalarBasis<MyMesh::Face<DIM>*>(fpt, 1));
            Eigen::VectorXd eval(mesh_basis->eval_functions(fpt->mass_center()).transpose());
            cout << " eval at center of mass " << eval.transpose() << endl;
            // gradients
            vector<Eigen::MatrixXd> eval_grad(mesh_basis->eval_gradients(fpt->mass_center()));
            cout << " eval_grad at center of mass " << endl;
                    for(auto& grad : eval_grad) { cout << grad << endl;}
                    cout << endl << endl;

            // eval and eval_grad at all vertices of face
            for(size_t i = 0; i < fpt->n_vertices(); i++) {
                Eigen::VectorXd point(fpt->get_vertices()[i]->coords());
                eval = mesh_basis->eval_functions(point).transpose();
                cout << " for vertex " << point.transpose() << endl;
                cout << " eval is " << eval.transpose()  << endl ;
                eval_grad = mesh_basis->eval_gradients(point);
                cout << " eval_grad is " << endl;
                        for(auto& grad : eval_grad) { cout << grad << endl;}
                        cout << endl << endl;
            }

            delete mesh_basis; mesh_basis = 0;
        }

    } catch (const char* msg) {
        cerr << "Could not build the base " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }


    mesh.reset(nullptr); mesh = 0;

    cout << "end of code" << endl;

}
