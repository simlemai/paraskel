/**
  * @{
*/
#include <iostream>
#include <vector>
#include <memory>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SparseLU>
#include <time.h>
#include "common_io.hpp"
#include "mesh.hpp"
#include "read_and_build_mesh.hpp"
#include "vtk_writer.hpp"
#include "tex_writer.hpp"
#include "hho_discontinuous_pk.hpp"
#include "p1_crouzeix_raviart_p0.hpp"
#include "taylor_hood.hpp"
#include "discrete_space.hpp"
#include "matrix_pattern.hpp"
#include "norm.hpp"
#include <math.h>       // cos
#define PI 3.14159265

using namespace std;


    /**
     * @file Demo_Stokes.cpp
     * 
     * Solve the Stokes system.
     *     - mu lap(u) + grad(p) = f on Omega
     *                    div(u) = 0 on Omega
     *                         u = u_\partial on the boundary
     *                   mean(p) = 0 on Omega
     * 
     * A Lagrange multiplier is used to fix the constant : mean(p) = 0.
     * 
     * If u_0 != 0, the global system is transformed into homogeneous 
     * Dirichlet boundary condition using a shift u = u_i + u_b 
     * where u_i = 0 on the boundary. 
     * In this case, div(u) = 0 becomes div(u_i) + div(u_b) = 0, and the 
     * variational formulation using Lagrange multiplier becomes :
     * 
     * find u_i in H_0^1(Omega, R^d), p in L_0^2(Omega) and lambda in R such that
     *  mu * (grad(u_i), grad(v)) - (div(v), p) = (f, v)      for all v in H_0^1(Omega, R^d)
     *            - (div(u_i), q) + (q, lambda) = (div(u_b), q - mean(q))   for all q in L2(Omega)
     *                                  (p, mu) = 0       for all mu in R
     *                                      u_i = 0       on the boundary
    */

// Select the dimension
#define DIM 2  ///< dimension must be known at the compilation
// #define DIM 3  ///< dimension must be known at the compilation

double mu(1.);

// used to define u_\partial, in visu and norm computation
std::vector<double> exact_sol_u(const Eigen::ArrayXd pt) {  
    std::vector<double> u_sol(DIM);

#if DIM==2
    // in 2D, u = curl(h), with h = x2 (x-1)^2 y2 (y-1)^2
    u_sol[0] = 2 * std::pow(pt[0], 2) * std::pow(pt[0]-1., 2) 
                * pt[1] * (pt[1] - 1.) * (2 * pt[1] - 1.);
    u_sol[1] = - 2 * std::pow(pt[1], 2) * std::pow(pt[1]-1., 2) 
                * pt[0] * (pt[0] - 1.) * (2 * pt[0] - 1.);
    return u_sol;

#elif DIM==3
    u_sol[0] = cos(PI * pt[0]) * sin(PI * pt[1]) + std::pow(pt[2], 5);
    u_sol[1] = - sin(PI * pt[0]) * cos(PI * pt[1]) - std::pow(pt[2], 3);
    u_sol[2] = std::pow(pt[0], 2) + std::pow(pt[1], 2); 

    return u_sol;

#endif
}

//  used in f, visu and norm computation
double exact_sol_p(const Eigen::ArrayXd pt) {  
#if DIM==2
    double p_sol(
        std::pow(pt[0], 5) + std::pow(pt[1], 5) - 1./3.);
#elif DIM==3
    double p_sol(
        std::pow(pt[0], 5) + std::pow(pt[1], 5) + std::pow(pt[2], 5) - 1./2.);
#endif
    return p_sol;
}

// only used in norm computations
Eigen::MatrixXd grad_exact_sol_u(const Eigen::ArrayXd pt) { 
    Eigen::MatrixXd grad(Eigen::MatrixXd::Zero(DIM, DIM));

#if DIM==2
    grad(0, 0) = 4. * pt[0] * (pt[0] - 1.) * (2. * pt[0] - 1.) * 
                    pt[1] * (pt[1] - 1.) * (2. * pt[1] - 1.); // sol_u[0] \x
    grad(0, 1) = 2. * std::pow(pt[0], 2) * std::pow(pt[0] - 1., 2) * (
                    6. * std::pow(pt[1], 2) - 6. * pt[1] + 1. ); // sol_u[0] \y
    grad(1, 0) = - 2. * ( 6. * std::pow(pt[0], 2) - 6. * pt[0] + 1. ) *
                    std::pow(pt[1] - 1., 2) * std::pow(pt[1], 2); // sol_u[1] \x
    grad(1, 1) = - grad(0, 0); // sol_u[1] \y

#elif DIM==3
    grad(0, 0) = - PI * sin(PI * pt[0]) * sin(PI * pt[1]); // sol_u[0] \x
    grad(0, 1) = PI * cos(PI * pt[0]) * cos(PI * pt[1]); // sol_u[0] \y
    grad(0, 2) = 5. * std::pow(pt[2], 4); // sol_u[0] \z

    grad(1, 0) = - grad(0, 1); // sol_u[1] \x
    grad(1, 1) = - grad(0, 0); // sol_u[1] \y
    grad(1, 2) = - 3. * std::pow(pt[2], 2); // sol_u[1] \z

    grad(2, 0) = 2. * pt[0]; // sol_u[2] \x
    grad(2, 1) = 2. * pt[1]; // sol_u[2] \y
    grad(2, 2) = 0.; // sol_u[2] \z
#endif

    return grad;
}

// used in continuous_rhs and norm computation
Eigen::RowVectorXd grad_exact_sol_p(const Eigen::ArrayXd pt) { 
    Eigen::RowVectorXd grad(DIM);
    for(size_t i = 0; i < DIM; i++) {
        grad[i] = 5 * std::pow(pt[i], 4);
    }
    return grad;
}

// definition of f
std::vector<double> continuous_rhs(const Eigen::ArrayXd pt) { 
    std::vector<double> rhs(DIM);
    Eigen::RowVectorXd grad_p(grad_exact_sol_p(pt));

#if DIM==2
    double x2(std::pow(pt[0],2)), y2(std::pow(pt[1],2));

    rhs[0] = 
        // - mu lap(u_0)
        - 4. * mu * (
            (6. * pt[1] - 3.) * x2 * std::pow(pt[0] - 1.,2)
            + (6. * x2 - 6. * pt[0] + 1) * pt[1] * (pt[1] - 1.) * (2. * pt[1] - 1.)
        // + grad(p)[0]
        ) + grad_p[0];
    rhs[1] = 
        // - mu lap(u_1)
        4. * mu * (
            pt[0] * (pt[0] - 1.) * (2. * pt[0] - 1.) * (6. * y2 - 6. * pt[1] + 1)
            + y2 * std::pow(pt[1] - 1.,2) * (6. * pt[0] - 3.)
        // + grad(p)[1]
        ) + grad_p[1];

#elif DIM==3
    double k(2. * mu * std::pow(PI, 2));

    rhs[0] = k * cos(PI * pt[0]) * sin(PI * pt[1]) - 20. * std::pow(pt[2], 3) + grad_p[0];
    rhs[1] = - k * sin(PI * pt[0]) * cos(PI * pt[1]) + 6. * pt[2] + grad_p[1];
    rhs[2] = - 4. * mu + grad_p[2];
#endif

    return rhs;
}


int main(int argc, char** argv)
{
    // set kb and ks
    // CAREFUL : they can be modified by parse_execution_options (at the execution)
    int bulk_k(1), skeletal_k(2);
    int order_cv; // order of cv = skeletal_k or skeletal_k + 1 in HHO

    // timers
    clock_t total_time(clock());
    double mesh_read_time, solver_time, reconstruction_time, visu_time, norm_time, total_time_s;

    string full_mesh_file, output_vtk_dir, output_vtk_meshfile, 
           output_vtk_numsol_file, output_vtk_exactsol_file,
           output_tex_dir, output_tex_file, 
           output_error_directory, output_error_textfile, output_solver_textfile;

    /**
     * Interpret the informations given at the execution :
     * ./exec -m full_input_mesh_filename 
     *        -s skeletal_degree_value
     *        -b bulk_degree_value
     *        -v (optional: full_output_vtk_filename) 
     *        -t (optional: full_output_tex_filename)
     *        -e (optional: full_output_filename for error and solver info)
     * -h print help message,
     * -D print the dimension
     * 
     * only "-m full_input_mesh_filename" is mandatory,
     * the following formats of the meshfile are allowed 
     * (the extensions are important):
     *      in 2D : gmsh "*.msh" or the FVCA5 format "*.typ1" or "*.typ2"
     *      in 3D : gmsh "*.msh" or the FVCA6 format "*.typ6".
     * 
    */
    try {
        // CAREFUL : bulk_k and skeletal_k can be modified at the execution using -b and/or -s
        parse_execution_options(argc, argv, DIM, bulk_k, skeletal_k, full_mesh_file, 
                output_vtk_dir, output_vtk_meshfile, output_vtk_numsol_file, output_vtk_exactsol_file,
                output_tex_dir, output_tex_file, output_error_directory, 
                output_error_textfile, output_solver_textfile);
        // init the order of convergence at the skeletal degree,
        // is modified if HHO is used
        order_cv = skeletal_k; 
    } catch (const char* msg) {
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    unique_ptr<MyMesh::Mesh<DIM>> mesh(nullptr);
    try {
        clock_t t(clock()); // timer
        mesh = MyMesh::read_and_build_mesh<DIM>(full_mesh_file);
        /**
         * The edges objects exist only in 3D.
         * An option allows to read and build the mesh 
         * without building the edges (save time when edges 
         * are not needed for the polytopal element method).
         * Replace the previous command line by the two next ones.
        */
        // bool build_edges(false); // do not build edges with Crouzeix-Raviart or HHO, ...
        // mesh = MyMesh::read_and_build_mesh<DIM>(full_mesh_file, build_edges);

        cout << " Mesh regularity :" << endl;
        cout << "     extremum mesh diameters ratio " << mesh->h_max()/mesh->h_min() << endl;
        cout << "     anisotropy max " << mesh->max_anisotropy() << " min   " << mesh->min_anisotropy() << endl << endl;

        t = clock() - t; // timer
        mesh_read_time = ((double)t)/CLOCKS_PER_SEC; // timer
    } catch (const char* msg) {
        cerr << "Could not read the mesh file " << full_mesh_file << " or build the mesh " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    // creates the polytopal element classes over the whole mesh
    MyDiscreteSpace::DiscreteSpace<DIM> ds(MyDiscreteSpace::DiscreteSpace<DIM>(mesh.get()));
    size_t var_u(0); // the first variable is the velocity (always in Stokes)
    size_t var_p(1); // the second variable is the pressure (always in Stokes)
    try {
        /**
         * Creates the polytopal element instantiations
         * It must respect the format
         *    ds.init<PEType, u_continuous_space, p_continuous_space>(my_mesh, k);
         * It is possible to specify two orders (bulk_order, skeletal_order).
         * Up to now, the possible continuous spaces are L2, H1, and H1d.
        */
        // // solve Stokes with HHOdiscontinuousPk: the order of the method is ks + 1
        // // in HHO: kb = ks or kb = ks + 1
        // if(bulk_k != skeletal_k && bulk_k != skeletal_k + 1) {
        //     cout << " WARNING: the bulk and skeletal degrees are not coherent for HHO, bulk degree is modified. " << endl;
        //     bulk_k = skeletal_k + 1;
        // }
        // ds.init<MyDiscreteSpace::HHOdiscontinuousPk, MyDiscreteSpace::H1d, MyDiscreteSpace::L2>(
        //     mesh.get(), bulk_k, skeletal_k); 
        // order_cv = skeletal_k + 1;
        // cout << endl << " Stokes solved with HHO-discontinuous P(k-1) : the velocity is in HHO(" << 
        //                 bulk_k << ", " << skeletal_k << ")" <<
        //                 " and the pressure is in discontinuous P" << skeletal_k << endl;

        // OR with TaylorHood: the order of the method is ks
        ds.init<MyDiscreteSpace::TaylorHood, MyDiscreteSpace::H1d, MyDiscreteSpace::L2>(mesh.get(), skeletal_k); 
        cout << endl << " Problem solved with TaylorHood : the velocity is in P" << skeletal_k <<
                            " and the pressure is in P" << skeletal_k - 1 << endl;

        // // OR with P1CrouzeixRaviartP0: the order of the method is 1 (ks always = 1)
        // skeletal_k = 1; order_cv = skeletal_k; 
        // ds.init<MyDiscreteSpace::P1CrouzeixRaviartP0, MyDiscreteSpace::H1d, MyDiscreteSpace::L2>(mesh.get(), skeletal_k);
        // cout << endl << " Problem solved with P1CrouzeixRaviart P0 " << endl;

        cout << " The order of the method is " << order_cv << endl;

        // Add the stiffness contributions of the velocity variable 
        // to each local bilinear form and rhs
        ds.add_stiffness_contribution(var_u, mu); // mu(grad u, grad v)
        // Add the crossed term   - (div u, p)
        ds.add_divergence_contribution(var_u, var_p, -1.);

        /**
         * right-hand-side   \int f r_T v_T
         * The order of the quadrature must be specified
         * and on which variable (u here)
        */
        int specific_quadra_order(2 * skeletal_k + 2);
        ds.set_rhs(continuous_rhs, specific_quadra_order, var_u);

        // Set that the velocity BC is essential (Dirichlet)
        ds.set_BCtype(mesh.get(), var_u, MyDiscreteSpace::ESSENTIAL);
        /**
         * Give the value of u_\partial.
         * It modifies the rhs to transform the pb
         * into homogeneous Dirichlet
        */
        ds.set_local_DirichletBC(exact_sol_u, var_u);

    } catch (const char* msg) {
        cerr << "Could not build the local contributions " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }
        
    /**
     * Initialize the global linear system informations.
     * Must be done after init or init_cells (for all cells)
     * and after setting the BC informations.
     * By default, eliminate the bulk degrees of freedom
     * using static condensation 
     * and the essential degrees of freedom.
    */
    MySolver::MatrixPattern<DIM> mp(mesh.get(), &ds);
    // Replace the previous command line by the next ones
    // to decide which eliminations are performed
    // bool use_static_condensation = false; // elimination of the bulk dof using static condensation ?
    // // WARNING : Keeping the essential dof may have an important impact on the 
    // // ConjugateGradient resolution, so also on the result.
    // // It is safer to eliminate them
    // bool essentialBC_elimination = true; // elimination of the essential degrees of freedom ?
    // bool naturalBC_elimination = false; // elimination of the natural degrees of freedom ?
    // MySolver::MatrixPattern<DIM> mp(mesh.get(), &ds,
    //     use_static_condensation,
    //     essentialBC_elimination, naturalBC_elimination);


    /**
     * Assemble and solve the global problem
    */
    Eigen::VectorXd dof_values;
    try {
        /**
         * Assemble the local contributions into the global matrix
        */
        mp.assemble_local_contributions(mesh.get(), &ds);

        clock_t t(clock()); // timer

#if DIM==2
        /**
         * Solve the equation m_global_matrix * x = m_global_rhs using LU
         * An important parameter of this class is the ordering 
         * method. It is used to reorder the columns 
         * (and eventually the rows) of the matrix to reduce 
         * the number of new elements that are created during 
         * numerical factorization. The cheapest method 
         * available is COLAMD.
        */
        Eigen::SparseLU<Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int>> solver;
        // Compute the ordering permutation vector 
        // from the structural pattern of A
        solver.analyzePattern(mp.global_matrix());
        // Compute the numerical factorization 
        solver.factorize(mp.global_matrix()); 
        // Use the factors to solve the linear system 
        dof_values = solver.solve(mp.global_rhs()); 
        
        t = clock() - t; // timer
        solver_time = ((double)t)/CLOCKS_PER_SEC; // timer

#elif DIM==3
        /**
         * Solve the equation m_global_matrix * x = m_global_rhs 
         * using iterative conjugate gradient
        */
        Eigen::ConjugateGradient<Eigen::SparseMatrix<double>> solver;
        solver.compute(mp.global_matrix()); 
        dof_values = solver.solve(mp.global_rhs()); 

        cout << endl << " Conjugate Gradient #iterations:  " << solver.iterations();
        cout << ",  estimated error: " << solver.error() << endl << endl;

        t = clock() - t; // timer
        solver_time = ((double)t)/CLOCKS_PER_SEC; // timer

        /**
         * If you want to write in a file
         * some informations about the iterative solver
         * execute the code with
         *    -e (optional: full_output_filename for error and solver info)
        */
        if(!output_solver_textfile.empty()) { 
            // save in file the CG convergence info 
            // output_simu_solver appends in output_solver_textfile
            // the order of convergence, the mesh size, the time to solve the linear system, 
            // the number of cells, the size of the linear system,
            // the number of iterations of the solver and the estimated error of the solver
            output_simu_solver(output_error_directory, output_solver_textfile,
                order_cv, mesh->h_mean(), (clock() - t) / CLOCKS_PER_SEC, 
                mesh->n_cells(), dof_values.size(),
                solver.iterations(), solver.error());
        }
#endif

        cout << endl << " The size of the linear system (nb of DOF which are not eliminated) is " << 
            dof_values.size()  << endl;
        cout << " The Lagrange multipier is " << dof_values(dof_values.size() - 1) << ", should be 0" << endl << endl;

        t = clock() - t; // timer
        /**
         * Reconstruct the local polynomials from the global dof values:
         * first, deal with the global to local mapping, 
         * the static cond and the Dirichlet lifting,
         * then reconstruct the polynomial(s) in each PE
        */
        ds.reconstruction(mp.get_local_to_global_operator(), dof_values, mp.static_cond());

        t = clock() - t; // timer
        reconstruction_time = ((double)t)/CLOCKS_PER_SEC; // timer

    } catch (const char* msg) {
        cerr << "Could not assemble the local contributions " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    /** 
     * If you want to write a vtk file
     * for a visualization, execute the code with
     *      -v (optional: full_output_vtk_filename) 
    */
    if(!output_vtk_numsol_file.empty()) {
        try {
            clock_t t(clock()); // timer

            // instanciates the VtkVisu class (by default for a non-conforming method)
            bool conforming_method(false); 
            MyVisu::VtkVisu<DIM> vtk(mesh.get());
            /** 
             * An option allows to specify that 
             * the method is conforming,
             * it simplifies the file (it writes only one value by vertex
             * instead of one value by vertex and by cell).
             * It must be specified at the instanciation of the VtkVisu class
             * and when getting the value of the reconstruction at the vertices.
             * Replace the previous command line by the two next ones.
            */
            // conforming_method = true; 
            // MyVisu::VtkVisu<DIM> vtk(mesh.get(), conforming_method);

            // The vtk visu needs the value at 
            // the vertices of mesh of the concerned variable,
            // which is obtained using 
            // ds.reconstruction_at_vertices(mesh.get(), conforming_method, var_index);
            // Contains the value of all components if vectorial variable
            vtk.add_var(ds.reconstruction_at_vertices(mesh.get(), conforming_method, var_u), "velocity");
            vtk.add_var(ds.reconstruction_at_vertices(mesh.get(), conforming_method, var_p), "pressure");


            // Vizualisation of continuous functions (here the exact solutions)
            vtk.add_var(exact_sol_u, "exact velocity");
            vtk.add_var(exact_sol_p, "exact pressure");

            // write the vtk file with the values 
            // selected with "add_var"
            vtk.write(output_vtk_dir, output_vtk_numsol_file);

            // If you want to store the mesh 
            // (not necessary to visualize other values)
            // uncomment the following line
            // vtk.write_mesh(output_vtk_dir, output_vtk_meshfile);

            t = clock() - t; // timer
            visu_time = ((double)t)/CLOCKS_PER_SEC; // timer

        } catch (const char* msg) {
            cerr << "Could not create the output file " << endl;
            cerr << msg << endl;
            return EXIT_FAILURE; 
        }
    }

#if DIM==2
    /** 
     * If you want to write in a file the 2D mesh in tex format,
     * execute the code with
     *     -t (optional: full_output_tex_filename)
    */
    if(!output_tex_file.empty()) {
        // if you want to plot also the centers of the cells,
        // set plot_centers = true;
        bool plot_centers = false;
        try {
            write_tex_mesh(mesh.get(), output_tex_dir, output_tex_file, plot_centers); 
        } catch (const char* msg) {
            cerr << msg << endl;
            return EXIT_FAILURE; 
        }
    }
#endif

    /** 
     * Computations of norms of errors
    */
    try {
        clock_t t(clock()); // timer

#if DIM==2
        double mesh_size(mesh->h_max());
#elif DIM==3
        double mesh_size(mesh->h_mean());
#endif

        int norm_quadra_order(2 * skeletal_k + 2);
        // compute the relative and absolute L2 norms of the error for u and p, 
        // comparaison with the exact solutions given as continuous fonctions
        double rel_l2error_u(MyDiscreteSpace::relative_l2error(&ds, exact_sol_u, norm_quadra_order, var_u)); 
        double abs_l2error_u(MyDiscreteSpace::absolute_l2error(&ds, exact_sol_u, norm_quadra_order, var_u)); 
        double rel_l2error_p(MyDiscreteSpace::relative_l2error(&ds, exact_sol_p, norm_quadra_order, var_p)); 
        double abs_l2error_p(MyDiscreteSpace::absolute_l2error(&ds, exact_sol_p, norm_quadra_order, var_p)); 

        // compute the relative and absolute H1 semi norms of the error for u, 
        // comparaison with the exact solutions given as continuous fonctions
        double rel_h1error_u(MyDiscreteSpace::relative_h1error(&ds, grad_exact_sol_u, norm_quadra_order, var_u)); 
        double abs_h1error_u(MyDiscreteSpace::absolute_h1error(&ds, grad_exact_sol_u, norm_quadra_order, var_u)); 

        t = clock() - t; // timer
        norm_time = ((double)t)/CLOCKS_PER_SEC; // timer

        cout << endl << " for mesh size (h_max in 2D, h_mean in 3D) " << mesh_size << ", the relative error in L2 norm between the exact sol and the numerical sol for u is " <<
        rel_l2error_u << "  for p is " << rel_l2error_p << endl;
        cout << " for mesh size (h_max in 2D, h_mean in 3D) " << mesh_size << ", the relative error in H1 semi-norm between the exact sol and the numerical sol for u is " <<
        rel_h1error_u << endl;


        // print timers
        total_time = clock() - total_time;
        total_time_s = ((double)total_time)/CLOCKS_PER_SEC;
        cout << endl << endl << " Total time (in s): " << total_time_s << "    with :" << endl;
        cout << "       mesh reader (in s):             " << mesh_read_time << "   in % : " << mesh_read_time / total_time_s *100 << endl;
        cout << "       init discrete space (in s):     " << ds.get_init_time() << "   in % : " << ds.get_init_time() / total_time_s *100 << endl;
        cout << "       build local matrices (in s):    " << ds.get_local_system_time() << "   in % : " << ds.get_local_system_time() / total_time_s *100 << endl;
        cout << "       init matrix pattern (in s):     " << mp.get_init_time() << "   in % : " << mp.get_init_time() / total_time_s *100 << endl;
        cout << "       assemble (in s):                " << mp.get_assemble_time() << "   in % : " << mp.get_assemble_time() / total_time_s *100 << endl;
        cout << "       solve (in s):                   " << solver_time << "   in % : " << solver_time / total_time_s *100 << endl;
        cout << "       reconstruct (in s):             " << reconstruction_time << "   in % : " << reconstruction_time / total_time_s *100 << endl;
        if(!output_vtk_meshfile.empty()) {
            cout << "       visualization (in s):           " << visu_time << "   in % : " << visu_time / total_time_s *100 << endl; }
        cout << "       norms computation (in s):       " << norm_time << "   in % : " << norm_time / total_time_s *100 << endl;
        cout << endl;

        /**
         * If you want to write in a file
         * some informations about the simulation
         * execute the code with
         *    -e (optional: full_output_filename for error and solver info)
        */
        if(!output_error_textfile.empty()) { 
            // output_simu_characteristic_num appends in output_error_textfile
            // the order of convergence, the mesh size, the time to solve the linear system, 
            // the total time of the execution, the number of cells, the size of the linear system,
            // the relative error in L2 norm and the relative error in semi-norm H1
            // relative to u
            output_simu_characteristic_num(output_error_directory, output_error_textfile,
                order_cv, mesh_size, solver_time, total_time_s, mesh->n_cells(), dof_values.size(), 
                rel_l2error_u, rel_h1error_u);
            // relative to p
            output_simu_characteristic_num(output_error_directory, output_error_textfile,
                order_cv, mesh_size, solver_time, total_time_s, mesh->n_cells(), dof_values.size(), 
                rel_l2error_p, 0.);
        }

    } catch (const char* msg) {
        cerr << "Could not compute errors " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    // reset the mesh pointer
    mesh.reset(nullptr); mesh = 0;

    cout << "end of code" << endl;

}
/** @} */

