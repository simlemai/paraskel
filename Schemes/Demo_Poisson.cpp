/**
  * @{
*/
#include <iostream>
#include <vector>
#include <memory>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SparseLU>
#include <time.h>
#include "common_io.hpp"
#include "mesh.hpp"
#include "read_and_build_mesh.hpp"
#include "vtk_writer.hpp"
#include "tex_writer.hpp"
#include "p1_crouzeix_raviart.hpp"
#include "pk_lagrange.hpp"
#include "hho.hpp"
#include "enhanced_conforming_vem.hpp"
#include "conforming_vem.hpp"
#include "discrete_space.hpp"
#include "matrix_pattern.hpp"
#include "norm.hpp"
#include <math.h>       // cos
#define PI 3.14159265

using namespace std;


    /**
     * @file Demo_Poisson.cpp
     * 
     * Solve the poisson problem with non polynomial value for the exact solution    <br>
     *         - Laplacien(u) = f                                                    <br>
     *                      u = u_\partial at the boundary                           <br>
     * 
     * in 2D     u(x,y) = cos(Pi x) * cos(Pi y)                                      <br>
     *        so f(x,y) = 2 * Pi^2 * cos(Pi x) * cos(Pi y)                           <br>
     * in 3D   u(x,y,z) = cos(Pi x) * sin(Pi y) + z                                  <br>
     *        so f(x,y) = 2 * Pi^2 * cos(Pi x) * sin(Pi y)                           <br>
    */

// Select the dimension
#define DIM 2  ///< dimension must be known at the compilation
// #define DIM 3  ///< dimension must be known at the compilation

// definition of f
double continuous_rhs(const Eigen::ArrayXd pt) { 
#if DIM==2
    return 2. * std::pow(PI, 2) * cos(PI * pt[0]) * cos(PI * pt[1]);
#elif DIM==3
    return 2. * std::pow(PI, 2) * cos(PI * pt[0]) * sin(PI * pt[1]);
#endif
}

// used to define u_\partial and to compute norms of error
double exact_sol(const Eigen::ArrayXd pt) {
#if DIM==2
    return cos(PI * pt[0]) * cos(PI * pt[1]);
#elif DIM==3
    return cos(PI * pt[0]) * sin(PI * pt[1]) + pt[2];
#endif
}

// only used to compute norms of error
Eigen::RowVectorXd grad_exact_sol(const Eigen::ArrayXd pt) {
    Eigen::RowVectorXd grad(DIM);
#if DIM==2
    grad[0] = - PI * sin(PI * pt[0]) * cos(PI * pt[1]); // \x
    grad[1] = - PI * cos(PI * pt[0]) * sin(PI * pt[1]); // \y
#elif DIM==3
    grad[0] = - PI * sin(PI * pt[0]) * sin(PI * pt[1]); // \x
    grad[1] = PI * cos(PI * pt[0]) * cos(PI * pt[1]); // \y
    grad[2] = 1.; // \z
#endif
 
    return grad;
}


int main(int argc, char** argv)
{
    // set kb and ks
    // CAREFUL : they can be modified by parse_execution_options (at the execution)
    int bulk_k(0), skeletal_k(1);
    int order_cv; // order of cv = skeletal_k or skeletal_k + 1 in HHO

    // timers
    clock_t total_time(clock());
    double mesh_read_time, solver_time, reconstruction_time, visu_time, norm_time, total_time_s;

    string full_mesh_file, output_vtk_dir, output_vtk_meshfile, 
           output_vtk_numsol_file, output_vtk_exactsol_file,
           output_tex_dir, output_tex_file, 
           output_error_directory, output_error_textfile, output_solver_textfile;

    /**
     * Interpret the informations given at the execution :
     * ./exec -m full_input_mesh_filename 
     *        -s skeletal_degree_value
     *        -b bulk_degree_value
     *        -v (optional: full_output_vtk_filename) 
     *        -t (optional: full_output_tex_filename)
     *        -e (optional: full_output_filename for error and solver info)
     * -h print help message
     * -D print the dimension
     * 
     * only "-m full_input_mesh_filename" is mandatory,
     * the following formats of the meshfile are allowed 
     * (the extensions are important):
     *      in 2D : gmsh "*.msh" or the FVCA5 format "*.typ1" or "*.typ2"
     *      in 3D : gmsh "*.msh" or the FVCA6 format "*.typ6".
     * 
    */
    try {
        // CAREFUL : bulk_k and skeletal_k can be modified at the execution using -b and/or -s
        parse_execution_options(argc, argv, DIM, bulk_k, skeletal_k, full_mesh_file, 
                output_vtk_dir, output_vtk_meshfile, output_vtk_numsol_file, output_vtk_exactsol_file,
                output_tex_dir, output_tex_file, output_error_directory, 
                output_error_textfile, output_solver_textfile);
        // init the order of convergence at the skeletal degree,
        // is modified if HHO is used
        order_cv = skeletal_k; 
    } catch (const char* msg) {
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    unique_ptr<MyMesh::Mesh<DIM>> mesh(nullptr);
    try {
        clock_t t(clock()); // timer
        mesh = MyMesh::read_and_build_mesh<DIM>(full_mesh_file);
        /**
         * The edges objects exist only in 3D.
         * An option allows to read and build the mesh 
         * without building the edges (save time when edges 
         * are not needed for the polytopal element method).
         * Replace the previous command line by the two next ones.
        */
        // bool build_edges(false); // do not build edges with Crouzeix-Raviart or HHO, ...
        // mesh = MyMesh::read_and_build_mesh<DIM>(full_mesh_file, build_edges);

        cout << " Mesh regularity :" << endl;
        cout << "     extremum mesh diameters ratio " << mesh->h_max()/mesh->h_min() << endl;
        cout << "     anisotropy max " << mesh->max_anisotropy() << " min   " << mesh->min_anisotropy() << endl << endl;

        t = clock() - t; // timer
        mesh_read_time = ((double)t)/CLOCKS_PER_SEC; // timer
    } catch (const char* msg) {
        cerr << "Could not read the mesh file " << full_mesh_file << " or build the mesh " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    // creates the polytopal element classes over the whole mesh
    MyDiscreteSpace::DiscreteSpace<DIM> ds(MyDiscreteSpace::DiscreteSpace<DIM>(mesh.get()));
    try {
        /**
         * Creates the polytopal element instantiations
         * It must respect the format
         *    ds.init<MyDiscreteSpace::PEType, MyDiscreteSpace::continuous_spaces>(my_mesh, k);
         * It is possible to specify two orders (bulk_order, skeletal_order),
         * and several continuous spaces (see the demo solving Stokes).
         * Up to now, the possible continuous spaces are L2, H1, and H1d.
        */

        // Solve with PkLagrange: the order of the method is ks
        ds.init<MyDiscreteSpace::PkLagrange,MyDiscreteSpace::H1>(mesh.get(), skeletal_k); 
        cout << endl << " Poisson solved with PkLagrange " << endl;

        // // OR with P1CrouzeixRaviart: always k = 1, the order of the method is k
        // ds.init<MyDiscreteSpace::P1CrouzeixRaviart,MyDiscreteSpace::H1>(mesh.get(), 1); skeletal_k = 1;
        // cout << endl << " Poisson solved with P1CrouzeixRaviart " << endl;

        // // OR with conforming VEM: kb = ks - 1, the order of the method is ks
        // ds.init<MyDiscreteSpace::VEM,MyDiscreteSpace::H1>(mesh.get(), bulk_k, skeletal_k);
        // cout << endl << " Poisson solved with conforming VEM(" << bulk_k << ", " << skeletal_k << ")" << endl;

        // // OR with enhanced VEM: kb = ks - 1, the order of the method is ks
        // ds.init<MyDiscreteSpace::enhancedVEM,MyDiscreteSpace::H1>(mesh.get(), bulk_k, skeletal_k);
        // cout << endl << " Poisson solved with enhanced conforming VEM(" << bulk_k << ", " << skeletal_k << ")" << endl;
        
        // // OR with HHO: kb = ks or kb = ks + 1, the order of the method is ks + 1
        // if(bulk_k != skeletal_k && bulk_k != skeletal_k + 1) {
        //     cout << " WARNING: the bulk and skeletal degrees are not coherent for HHO, bulk degree is modified. " << endl;
        //     bulk_k = skeletal_k;
        // }
        // ds.init<MyDiscreteSpace::HHO,MyDiscreteSpace::H1>(mesh.get(), bulk_k, skeletal_k); order_cv = skeletal_k + 1;
        // cout << endl << " Poisson solved with HHO(" << bulk_k << ", " << skeletal_k << ")" << endl;

        /**
         * In HHO, it is possible to choose between 
         * the Lehrenfeld-Schöberl stabilization
         * or the equal-order stabilizations 
         * By default, choose Lehrenfeld-Schöberl when bulk_k - skeletal_k = 1
        */
        // ds.set_stabilization_type("ls_stabilization"); // Lehrenfeld-Schöberl stabilization
        // ds.set_stabilization_type("equal_order_stabilization"); // equal-order stabilization

        cout << " The order of the method is " << order_cv << endl;

        // Add contributions to each local bilinear form and rhs
        // - Laplacien(u)
        ds.add_stiffness_contribution();

        // right-hand-side   \int f r_T v_T
        ds.set_rhs(continuous_rhs); 
        /**
         * The order of the quadrature can be specified as follows,
         * by default the order is 6.
         * Replace the previous command line by the two next ones.
        */
        // int specific_quadra_order(2 * skeletal_k + 2);
        // ds.set_rhs(continuous_rhs, specific_quadra_order);


        /**
         * Boundary conditions:
         * By default, homogeneous natural (Neumann) BC is aplied everywhere.
        */
        // // if you let homogeneous Neumann BC, you must add a Lagrange multiplier
        // // to fix the mean u = 0 (only in 2D, in 3D mean u != 0)
        // ds.add_lagrange_multiplier();

        // OR set that the BC is essential (Dirichlet) everywhere
        ds.set_BCtype(mesh.get(), MyDiscreteSpace::ESSENTIAL);
        ds.set_local_DirichletBC(exact_sol); // give the value of u_\partial, transform the pb into homogeneous Dirichlet

//         // OR set essential BC only on a part of the domain 
//         // here the boundaries y = y_min and y = y_max (and z_min in 3D)
//         mesh->find_domain_extremities(); // find the domain size to use y_min and y_max
//         double eps(1.e-6);
//         double y_min(mesh->min_domain_extremities()[1] + eps);
//         double y_max(mesh->max_domain_extremities()[1] - eps);
// #if DIM==2
//         ds.set_BCtype(mesh.get(), MyDiscreteSpace::ESSENTIAL,
//             [=](const Eigen::ArrayXd pt) { return ( pt[1] < y_min || pt[1] > y_max ); } 
//             );
// #elif DIM==3
//         double z_min(mesh->min_domain_extremities()[2] + eps);
//         ds.set_BCtype(mesh.get(), MyDiscreteSpace::ESSENTIAL,
//             [=](const Eigen::ArrayXd pt) { return ( pt[1] < y_min || pt[1] > y_max ||
//                                                     pt[2] < z_min); }
//             );
// #endif
//         ds.set_local_DirichletBC(exact_sol); // give the value of u_\partial, transform the pb into homogeneous Dirichlet


    } catch (const char* msg) {
        cerr << "Could not build the local contributions " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }
        

    /**
     * Initialize the global linear system informations.
     * Must be done after init or init_cells (for all cells)
     * and after setting the BC informations.
     * By default, eliminate the bulk degrees of freedom
     * using static condensation 
     * and the essential degrees of freedom.
    */
    MySolver::MatrixPattern<DIM> mp(mesh.get(), &ds);
    // Replace the previous command line by the next ones
    // to decide which eliminations are performed
    // bool use_static_condensation = false; // elimination of the bulk dof using static condensation ?
    // // WARNING : Keeping the essential dof may have an important impact on the 
    // // ConjugateGradient resolution, so also on the result.
    // // It is safer to eliminate them
    // bool essentialBC_elimination = true; // elimination of the essential degrees of freedom ?
    // bool naturalBC_elimination = false; // elimination of the natural degrees of freedom ?
    // MySolver::MatrixPattern<DIM> mp(mesh.get(), &ds,
    //     use_static_condensation,
    //     essentialBC_elimination, naturalBC_elimination);


    /**
     * Assemble and solve the global problem
    */
    Eigen::VectorXd dof_values;
    try {
        /**
         * Assemble the local contributions into the global matrix
        */
        mp.assemble_local_contributions(mesh.get(), &ds);

        clock_t t(clock()); // timer
#if DIM==2
        /**
         * Solve the equation m_global_matrix * x = m_global_rhs using LU
         * An important parameter of this class is the ordering 
         * method. It is used to reorder the columns 
         * (and eventually the rows) of the matrix to reduce 
         * the number of new elements that are created during 
         * numerical factorization. The cheapest method 
         * available is COLAMD.
        */
        Eigen::SparseLU<Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int>> solver;
        // Compute the ordering permutation vector 
        // from the structural pattern of A
        solver.analyzePattern(mp.global_matrix());
        // Compute the numerical factorization 
        solver.factorize(mp.global_matrix()); 
        // Use the factors to solve the linear system 
        dof_values = solver.solve(mp.global_rhs()); 
        
        t = clock() - t; // timer
        solver_time = ((double)t)/CLOCKS_PER_SEC; // timer

#elif DIM==3
        /**
         * Solve the equation m_global_matrix * x = m_global_rhs 
         * using iterative conjugate gradient
        */
        Eigen::ConjugateGradient<Eigen::SparseMatrix<double>> solver;
        solver.compute(mp.global_matrix()); 
        dof_values = solver.solve(mp.global_rhs()); 

        cout << endl << " Conjugate Gradient #iterations:  " << solver.iterations();
        cout << ",  estimated error: " << solver.error() << endl << endl;

        t = clock() - t; // timer
        solver_time = ((double)t)/CLOCKS_PER_SEC; // timer

        /**
         * If you want to write in a file
         * some informations about the iterative solver
         * execute the code with
         *    -e (optional: full_output_filename for error and solver info)
        */
        if(!output_solver_textfile.empty()) { 
            // save in file the CG convergence info 
            // output_simu_solver appends in output_solver_textfile
            // the order of convergence, the mesh size, the time to solve the linear system, 
            // the number of cells, the size of the linear system,
            // the number of iterations of the solver and the estimated error of the solver
            output_simu_solver(output_error_directory, output_solver_textfile,
                order_cv, mesh->h_mean(), (clock() - t) / CLOCKS_PER_SEC, 
                mesh->n_cells(), dof_values.size(),
                solver.iterations(), solver.error());
        }
#endif

        cout << endl << " The size of the linear system (nb of DOF which are not eliminated) is " << 
            dof_values.size()  << endl;
        if(ds.lagrange_multiplier()) {
            cout << " The Lagrange multipier is " << dof_values(dof_values.size() - 1) << ", should be 0" << endl << endl;
        }

        t = clock() - t; // timer
        /**
         * Reconstruct the local polynomials from the global dof values:
         * first, deal with the global to local mapping, 
         * the static cond and the Dirichlet lifting,
         * then reconstruct the polynomial(s) in each PE
        */
        ds.reconstruction(mp.get_local_to_global_operator(), dof_values, mp.static_cond());

        t = clock() - t; // timer
        reconstruction_time = ((double)t)/CLOCKS_PER_SEC; // timer

    } catch (const char* msg) {
        cerr << "Could not assemble the local contributions " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    /** 
     * If you want to write a vtk file
     * for a visualization, execute the code with
     *      -v (optional: full_output_vtk_filename) 
    */
    if(!output_vtk_numsol_file.empty()) {
        try {
            clock_t t(clock()); // timer

            // instantiates the VtkVisu class (by default for a non-conforming method)
            MyVisu::VtkVisu<DIM> vtk(mesh.get());
            /** 
             * An option allows to specify that 
             * the method is conforming,
             * it simplifies the file (it writes only one value by vertex
             * instead of one value by vertex and by cell).
             * It must be specified at the instantiation of the VtkVisu class
             * and when getting the value of the reconstruction at the vertices.
             * Replace the previous command line by the two next ones.
            */
            // bool conforming_method(true); 
            // MyVisu::VtkVisu<DIM> vtk(mesh.get(), conforming_method);

            // The vtk visu needs the value at the vertices of mesh,
            // which is obtained using ds.reconstruction_at_vertices(mesh.get());
            // By default, get the value of the first variable with non-conforming method.
            vtk.add_var(ds.reconstruction_at_vertices(mesh.get()));
            // It will also give a default legend for this visualization,
            // to set your own legend
            // replace the previous command line by the following one 
            // vtk.add_var(ds.reconstruction_at_vertices(mesh.get()), "reconstructed sol");
            // To specify that the method is conforming (it must also be specified at 
            // the instantiation of the class),
            // replace the previous command line by the following one.
            // vtk.add_var(ds.reconstruction_at_vertices(mesh.get(), conforming_method));

            // Vizualisation of a continuous function (here the exact solution)
            vtk.add_var(exact_sol);
            // Same way to give a legend
            // vtk.add_var(exact_sol, "the exact solution");

            // write the vtk file with the values 
            // selected with "add_var"
            vtk.write(output_vtk_dir, output_vtk_numsol_file);

            // If you want to store the mesh 
            // (not necessary to visualize other values)
            // uncomment the following line
            // vtk.write_mesh(output_vtk_dir, output_vtk_meshfile);

            t = clock() - t; // timer
            visu_time = ((double)t)/CLOCKS_PER_SEC; // timer

        } catch (const char* msg) {
            cerr << "Could not create the output file " << endl;
            cerr << msg << endl;
            return EXIT_FAILURE; 
        }
    }

#if DIM==2
    /** 
     * If you want to write in a file the 2D mesh in tex format,
     * execute the code with
     *     -t (optional: full_output_tex_filename)
    */
    if(!output_tex_file.empty()) {
        // if you want to plot also the centers of the cells,
        // set plot_centers = true;
        bool plot_centers = false;
        try {
            write_tex_mesh(mesh.get(), output_tex_dir, output_tex_file, plot_centers); 
        } catch (const char* msg) {
            cerr << msg << endl;
            return EXIT_FAILURE; 
        }
    }
#endif

    /** 
     * Computations of norms of errors
    */
    try {
        clock_t t(clock()); // timer

#if DIM==2
        double mesh_size(mesh->h_max());
#elif DIM==3
        double mesh_size(mesh->h_mean());
#endif

        // compute the relative and absolute L2 norms of the error, 
        // comparaison with the exact solution given as continuous fonction
        // by default, the quadrature order is 10
        double relative_l2error(MyDiscreteSpace::relative_l2error(&ds, exact_sol));
        double absolute_l2error(MyDiscreteSpace::absolute_l2error(&ds, exact_sol));

        // compute the relative and absolute H1 semi norms of the error, 
        // comparaison with the exact solution given as continuous fonction
        // by default, the quadrature order is 10
        double relative_h1error(MyDiscreteSpace::relative_h1error(&ds, grad_exact_sol));
        double absolute_h1error(MyDiscreteSpace::absolute_h1error(&ds, grad_exact_sol));

        // // It is possible (and recommanded) to specify the order of quadrature
        // int norm_quadra_order(2 * skeletal_k + 2);
        // // compute the relative L2 norm and H1 semi norm of the error
        // // with a given quadrature order
        // double rel_l2error(MyDiscreteSpace::relative_l2error(&ds, exact_sol, norm_quadra_order));
        // double rel_h1error(MyDiscreteSpace::relative_h1error(&ds, grad_exact_sol, norm_quadra_order));

        t = clock() - t; // timer
        norm_time = ((double)t)/CLOCKS_PER_SEC; // timer

        cout << endl << " for mesh size (h_max in 2D, h_mean in 3D) " << mesh_size << ", the relative error in L2 norm between the exact sol and the numerical sol is " <<
        relative_l2error << endl;
        cout << "               and the absolute error in L2 norm is " << absolute_l2error << endl;
        cout << " for mesh size (h_max in 2D, h_mean in 3D) " << mesh_size << ", the relative error in H1 semi-norm between the exact sol and the numerical sol is " <<
        relative_h1error << endl;
        cout << "               and the absolute error in H1 semi-norm is " << absolute_h1error << endl;

        // print timers
        total_time = clock() - total_time; // timer
        total_time_s = ((double)total_time)/CLOCKS_PER_SEC; // timer
        cout << endl << " Total time (in s): " << total_time_s << "    with :" << endl;
        cout << "       mesh reader (in s):             " << mesh_read_time << "   in % : " << mesh_read_time / total_time_s *100 << endl;
        cout << "       init discrete space (in s):     " << ds.get_init_time() << "   in % : " << ds.get_init_time() / total_time_s *100 << endl;
        cout << "       build local matrices (in s):    " << ds.get_local_system_time() << "   in % : " << ds.get_local_system_time() / total_time_s *100 << endl;
        cout << "       init matrix pattern (in s):     " << mp.get_init_time() << "   in % : " << mp.get_init_time() / total_time_s *100 << endl;
        cout << "       assemble (in s):                " << mp.get_assemble_time() << "   in % : " << mp.get_assemble_time() / total_time_s *100 << endl;
        cout << "       solve (in s):                   " << solver_time << "   in % : " << solver_time / total_time_s *100 << endl;
        cout << "       reconstruct (in s):             " << reconstruction_time << "   in % : " << reconstruction_time / total_time_s *100 << endl;
        if(!output_vtk_meshfile.empty()) {
            cout << "       visualization (in s):           " << visu_time << "   in % : " << visu_time / total_time_s *100 << endl; }
        cout << "       norms computation (in s):       " << norm_time << "   in % : " << norm_time / total_time_s *100 << endl;
        cout << endl;

        /**
         * If you want to write in a file
         * some informations about the simulation
         * execute the code with
         *    -e (optional: full_output_filename for error and solver info)
        */
        if(!output_error_textfile.empty()) { 
            // output_simu_characteristic_num appends in output_error_textfile
            // the order of convergence, the mesh size, the time to solve the linear system, 
            // the total time of the execution, the number of cells, the size of the linear system,
            // the relative error in L2 norm and the relative error in semi-norm H1
            output_simu_characteristic_num(output_error_directory, output_error_textfile,
                order_cv, mesh_size, solver_time, total_time_s, mesh->n_cells(), dof_values.size(), 
                relative_l2error, relative_h1error);
        }

    } catch (const char* msg) {
        cerr << "Could not compute errors " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    // reset the mesh pointer
    mesh.reset(nullptr); mesh = 0;

    cout << "end of code" << endl;

}
/** @} */

