#include <iostream>
#include <vector>
#include <memory>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include<Eigen/SparseLU>
#include <time.h>
#include "common_io.hpp"
#include "mesh.hpp"
#include "read_and_build_mesh.hpp"
#include "vtk_writer.hpp"
#include "tex_writer.hpp"
#include "p1_crouzeix_raviart_p0.hpp"
#include "discrete_space.hpp"
#include "matrix_pattern.hpp"
#include "norm.hpp"
#include <math.h>       // cos
#define PI 3.14159265

using namespace std;

    /**
     * Solve the Stokes system.
     *     - mu lap(u) + grad(p) = f on Omega
     *                    div(u) = 0 on Omega
     *                         u = u_0 on the boundary
     *                   mean(p) = 0 on Omega
     * 
     * A Lagrange multiplier is used to fix the constant : mean(p) = 0.
     * 
     * If u_0 != 0, the global system is transformed into homogeneous 
     * Dirichlet boundary condition using a shift u = u_i + u_b 
     * where u_i = 0 on the boundary. 
     * In this case, div(u) = 0 becomes div(u_i) + div(u_b) = 0, and the 
     * variational formulation using Lagrange multiplier becomes :
     * 
     * find u_i in H_0^1(Omega, R^d), p in L_0^2(Omega) and lambda in R such that
     *  mu * (grad(u_i), grad(v)) - (div(v), p) = (f, v)      for all v in H_0^1(Omega, R^d)
     *            - (div(u_i), q) + (q, lambda) = (div(u_b), q - mean(q))   for all q in L2(Omega)
     *                                  (p, mu) = 0       for all mu in R
     *                                      u_i = 0       on the boundary
    */

#define DIM 2  ///< dimension must be known at the compilation 

double mu(1.);

std::vector<double> exact_sol_u(const Eigen::ArrayXd pt) {  // used to define u_0, in visu and norm computation
    std::vector<double> u_sol(DIM);

#if DIM==2
    // u = curl(h), with h = x2 (x-1)^2 y2 (y-1)^2
    u_sol[0] = 2 * std::pow(pt[0], 2) * std::pow(pt[0]-1., 2) 
                * pt[1] * (pt[1] - 1.) * (2 * pt[1] - 1.);
    u_sol[1] = - 2 * std::pow(pt[1], 2) * std::pow(pt[1]-1., 2) 
                * pt[0] * (pt[0] - 1.) * (2 * pt[0] - 1.);
    return u_sol;
#elif DIM==3
    // // u = grad(h), with h = 5 x6 - 90 x4 y2 + 120 x2 y4 - 16 y6 + 15 x4 z2
    // //   - 180 x2 y2 z2 + 120 y4 z2 + 15 x2 z4 - 90 y2 z4 + 5 z6
    // double x2(std::pow(pt[0],2)), x3(std::pow(pt[0],3)), x4(std::pow(pt[0],4)), x5(std::pow(pt[0],5));
    // double y2(std::pow(pt[1],2)), y3(std::pow(pt[1],3)), y4(std::pow(pt[1],4)), y5(std::pow(pt[1],5));
    // double z2(std::pow(pt[2],2)), z3(std::pow(pt[2],3)), z4(std::pow(pt[2],4)), z5(std::pow(pt[2],5));

    // u_sol[0] = 30. * x5 - 360. * x3 * y2 + 240. * pt[0] * y4 + 60.* x3 * z2
    //             - 360. * pt[0] * y2 * z2 + 30. * pt[0] * z4;
    // u_sol[1] = - 180. * x4 * pt[1] + 480. * x2 * y3 - 96. * y5 
    //             - 360. * x2 * pt[1] * z2 + 480. * y3 * z2 - 180. * pt[1] * z4;
    // u_sol[2] = 30. * x4 * pt[2] - 360. * x2 * y2 * pt[2] + 240. * y4 * pt[2]
    //             + 60. * x2 * z3 - 360. * y2 * z3 + 30. * z5;


    u_sol[0] = cos(PI * pt[0]) * sin(PI * pt[1]) + std::pow(pt[2], 5);
    u_sol[1] = - sin(PI * pt[0]) * cos(PI * pt[1]) - std::pow(pt[2], 3);
    u_sol[2] = std::pow(pt[0], 2) + std::pow(pt[1], 2); 

    return u_sol;
#endif
}

double exact_sol_p(const Eigen::ArrayXd pt) {  //  used in f, visu and norm computation

#if DIM==2
    double p_sol(std::pow(pt[0], 5) + std::pow(pt[1], 5) - 1./3.);
#elif DIM==3
    double p_sol(
        std::pow(pt[0], 5) + std::pow(pt[1], 5) + std::pow(pt[2], 5) - 1./2.);
#endif
    return p_sol;
}

Eigen::MatrixXd grad_exact_sol_u(const Eigen::ArrayXd pt) { // only used in norm computation
    Eigen::MatrixXd grad(Eigen::MatrixXd::Zero(DIM, DIM));

#if DIM==2
    grad(0, 0) = 4. * pt[0] * (pt[0] - 1.) * (2. * pt[0] - 1.) * 
                    pt[1] * (pt[1] - 1.) * (2. * pt[1] - 1.); // sol_u[0] \x
    grad(0, 1) = 2. * std::pow(pt[0], 2) * std::pow(pt[0] - 1., 2) * (
                    6. * std::pow(pt[1], 2) - 6. * pt[1] + 1. ); // sol_u[0] \y
    grad(1, 0) = - 2. * ( 6. * std::pow(pt[0], 2) - 6. * pt[0] + 1. ) *
                    std::pow(pt[1] - 1., 2) * std::pow(pt[1], 2); // sol_u[1] \x
    grad(1, 1) = - grad(0, 0); // sol_u[1] \y

    return grad;

#elif DIM==3
    // double x2(std::pow(pt[0],2)), x3(std::pow(pt[0],3)), x4(std::pow(pt[0],4));
    // double y2(std::pow(pt[1],2)), y3(std::pow(pt[1],3)), y4(std::pow(pt[1],4));
    // double z2(std::pow(pt[2],2)), z3(std::pow(pt[2],3)), z4(std::pow(pt[2],4));

    // // sol_u[0]
    // grad(0, 0) = 150. * x4 - 1080. * x2 * y2 + 240. * y4 + 180. * x2 * z2
    //             - 360. * y2 * z2 + 30. * z4; // sol_u[0] \x
    // grad(0, 1) = - 720. * x3 * pt[1] + 960. * pt[0] * y3 
    //             - 720. * pt[0] * pt[1] * z2; // sol_u[0] \y
    // grad(0, 2) = 120. * x3 * pt[2] - 720. * pt[0] * y2 * pt[2] 
    //             + 120. * pt[0] * z3; // sol_u[0] \z
    // // sol_u[1]
    // grad(1, 0) = - 720. * x3 * pt[1] + 960. * pt[0] * y3 
    //             - 720. * pt[0] * pt[1] * z2; // sol_u[1] \x
    // grad(1, 1) = - 180. * x4 + 1440. * x2 * y2 - 480. * y4
    //             - 360. * x2 * z2 + 1440. * y2 * z2 - 180. * z4; // sol_u[1] \y
    // grad(1, 2) = - 720. * x2 * pt[1] * pt[2] + 960. * y3 * pt[2] 
    //             - 720. * pt[1] * z3; // sol_u[1] \z

    // // sol_u[2]
    // grad(2, 0) = 120. * x3 * pt[2] - 720. * pt[0] * y2 * pt[2] 
    //             + 120. * pt[0] * z3; // sol_u[2] \x
    // grad(2, 1) = - 720. * x2 * pt[1] * pt[2] + 960. * y3 * pt[2]
    //             - 720. * pt[1] * z3; // sol_u[2] \y
    // grad(2, 2) = 30. * x4 - 360. * x2 * y2 + 240. * y4 + 180. * x2 * z2
    //             - 1080. * y2 * z2 + 150. * z4; // sol_u[2] \z

    grad(0, 0) = - PI * sin(PI * pt[0]) * sin(PI * pt[1]); // sol_u[0] \x
    grad(0, 1) = PI * cos(PI * pt[0]) * cos(PI * pt[1]); // sol_u[0] \y
    grad(0, 2) = 5. * std::pow(pt[2], 4); // sol_u[0] \z

    grad(1, 0) = - grad(0, 1); // sol_u[1] \x
    grad(1, 1) = - grad(0, 0); // sol_u[1] \y
    grad(1, 2) = - 3. * std::pow(pt[2], 2); // sol_u[1] \z

    grad(2, 0) = 2. * pt[0]; // sol_u[2] \x
    grad(2, 1) = 2. * pt[1]; // sol_u[2] \y
    grad(2, 2) = 0.; // sol_u[2] \z

    return grad;
#endif
}

Eigen::RowVectorXd grad_exact_sol_p(const Eigen::ArrayXd pt) { // used in continuous_rhs and norm computation
    Eigen::RowVectorXd grad(DIM);
    for(size_t i = 0; i < DIM; i++) {
        grad[i] = 5 * std::pow(pt[i], 4);
    }
    return grad;
}

std::vector<double> continuous_rhs(const Eigen::ArrayXd pt) { 
    std::vector<double> rhs(DIM);
    Eigen::RowVectorXd grad_p(grad_exact_sol_p(pt));

#if DIM==2
    double x2(std::pow(pt[0],2)), y2(std::pow(pt[1],2));

    rhs[0] = 
        // - mu lap(u_0)
        - 4. * mu * (
            (6. * pt[1] - 3.) * x2 * std::pow(pt[0] - 1.,2)
            + (6. * x2 - 6. * pt[0] + 1) * pt[1] * (pt[1] - 1.) * (2. * pt[1] - 1.)
        // + grad(p)[0]
        ) + grad_p[0];
    rhs[1] = 
        // - mu lap(u_1)
        4. * mu * (
            pt[0] * (pt[0] - 1.) * (2. * pt[0] - 1.) * (6. * y2 - 6. * pt[1] + 1)
            + y2 * std::pow(pt[1] - 1.,2) * (6. * pt[0] - 3.)
        // + grad(p)[1]
        ) + grad_p[1];

#elif DIM==3
    // rhs[0] = grad_p[0]; // - mu lap(u_0) = 0
    // rhs[1] = grad_p[1]; // - mu lap(u_1) = 0
    // rhs[2] = grad_p[2]; // - mu lap(u_2) = 0

    double k(2. * mu * std::pow(PI, 2));

    rhs[0] = k * cos(PI * pt[0]) * sin(PI * pt[1]) - 20. * std::pow(pt[2], 3) + grad_p[0];
    rhs[1] = - k * sin(PI * pt[0]) * cos(PI * pt[1]) + 6. * pt[2] + grad_p[1];
    rhs[2] = - 4. * mu + grad_p[2];

#endif

    return rhs;
}


int main(int argc, char** argv)
{
    // set ks
    // CAREFUL : can be modified by parse_execution_options (at the execution)
    int skeletal_k(1);

    // timer
    clock_t total_time(clock());
    double mesh_read_time, solver_time, reconstruction_time, visu_time, norm_time, total_time_s;

    string full_mesh_file, output_vtk_dir, output_vtk_meshfile, 
           output_vtk_numsol_file, output_vtk_exactsol_file,
           output_tex_dir, output_tex_file, 
           output_error_directory, output_error_textfile, output_solver_textfile;
    int unused_bulk_k(0);

    // fill full_mesh_file and output options
    try {
        // CAREFUL : unused_bulk_k and skeletal_k can be modified at the execution using -b and/or -s
        parse_execution_options(argc, argv, DIM, unused_bulk_k, skeletal_k, full_mesh_file, 
                output_vtk_dir, output_vtk_meshfile, output_vtk_numsol_file, output_vtk_exactsol_file,
                output_tex_dir, output_tex_file, output_error_directory, 
                output_error_textfile, output_solver_textfile);
    } catch (const char* msg) {
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    unique_ptr<MyMesh::Mesh<DIM>> mesh(nullptr);
    try {
        // timer
        clock_t t(clock());
        bool build_edges(false); // it is not necessary to build edges with Crouzeix-Raviart
        mesh = MyMesh::read_and_build_mesh<DIM>(full_mesh_file, build_edges);
        t = clock() - t;
        mesh_read_time = ((double)t)/CLOCKS_PER_SEC;
    } catch (const char* msg) {
        cerr << "Could not read the mesh file " << full_mesh_file << " or build the mesh " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    // creates the finite element classes over the whole mesh
    MyDiscreteSpace::DiscreteSpace<DIM> ds(MyDiscreteSpace::DiscreteSpace<DIM>(mesh.get()));
    size_t var_u(0); // the first variable is the velocity (always in Stokes)
    size_t var_p(1); // the second variable is the pressure (always in Stokes)
    try {
        cout << endl << " Problem solved with P1CrouzeixRaviart P0 " << endl;
        cout << " The order of the method is 1 " << endl;

        // creates the finite element instantiations with local nb of dof, 
        // reconstruction operator, stiffness matrix
        // k must be > 1, first continuous space must be for the velocity (Pk), second for the pressure (P(k-1)).
        ds.init<MyDiscreteSpace::P1CrouzeixRaviartP0, MyDiscreteSpace::H1d, MyDiscreteSpace::L2>(mesh.get(), skeletal_k); // always k = 1

        // Add the stiffness contributions of the velocity variable 
        // to each local bilinear form and rhs
        ds.add_stiffness_contribution(var_u, mu); // mu(grad u, grad v)
        // Add the crossed term   - (div u, p) which means
        // adding - (p, div(v)); - (div(u_i), q); 
        // we should add (div(u_b), q - mean(q)) to the rhs
        // but (div(u_b), q) is already added in the assembly
        // and  (int_Boundary Interpolate(u_b) . n) * mean(q) = 0 because div(u) = 0
        ds.add_divergence_contribution(var_u, var_p, -1.);

        // set that the velocity BC is essential (Dirichlet)
        ds.set_BCtype(mesh.get(), var_u, MyDiscreteSpace::ESSENTIAL);
        // init the Dirichlet lifting to transform the pb into
        // a homogeneous Dirichlet BC pb
        ds.set_local_DirichletBC(exact_sol_u, var_u);

        // right-hand-side   \int f r_T v_T
        int specific_quadra_order(5);  // optional, 6 by default 
        ds.set_rhs(continuous_rhs, specific_quadra_order, var_u); // = (f, v) so var_u defines the space H1d

    } catch (const char* msg) {
        cerr << "Could not build the local contributions " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    // Initialize the global indexes and the map between the local and global indexes
    // Must be done after init or init_cells (for all cells)
    bool use_static_condensation = true; 
    MySolver::MatrixPattern<DIM> mp(mesh.get(), &ds, use_static_condensation);

    // assemble and solve the global problem
    Eigen::VectorXd dof_values;
    try {
        // assemble local contributions into the global matrix
        // perform the static condensation if asked (eliminate 
        // all or some of the bulk unknowns)
        mp.assemble_local_contributions(mesh.get(), &ds);

        // timer
        clock_t t(clock());

#if DIM==2
        // solve the equation m_global_matrix * x = m_global_rhs
        // An important parameter of this class is the ordering 
        // method. It is used to reorder the columns 
        // (and eventually the rows) of the matrix to reduce 
        // the number of new elements that are created during 
        // numerical factorization. The cheapest method 
        // available is COLAMD.
        Eigen::SparseLU<Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int>> solver;
        // Compute the ordering permutation vector 
        // from the structural pattern of A
        solver.analyzePattern(mp.global_matrix());
        // Compute the numerical factorization 
        solver.factorize(mp.global_matrix()); 
        // Use the factors to solve the linear system 
        dof_values = solver.solve(mp.global_rhs()); 

        t = clock() - t;
        solver_time = ((double)t)/CLOCKS_PER_SEC;

#elif DIM==3
        // solve with iterative conjugate gradient
        Eigen::ConjugateGradient<Eigen::SparseMatrix<double>> solver;
        solver.compute(mp.global_matrix()); 
        dof_values = solver.solve(mp.global_rhs()); 

        t = clock() - t;
        solver_time = ((double)t)/CLOCKS_PER_SEC;

        cout << endl << " Conjugate Gradient #iterations:  " << solver.iterations();
        cout << ",  estimated error: " << solver.error() << endl << endl;

        // to write in a file some informations about the iterative solver, 
        // execute the code with
        //    -e (optional: full_output_filename for error and solver info)
        if(!output_solver_textfile.empty()) { 
            // save in file the CG convergence info 
            // output_simu_solver appends in output_solver_textfile
            // the order of convergence, the mesh size, the time to solve the linear system, 
            // the number of cells, the size of the linear system,
            // the number of iterations of the solver and the estimated error of the solver
            output_simu_solver(output_error_directory, output_solver_textfile,
                skeletal_k, mesh->h_mean(), (clock() - t) / CLOCKS_PER_SEC, mesh->n_cells(), 
                dof_values.size(), solver.iterations(), solver.error());
        }
#endif

        cout << endl << " The size of the linear system (nb of DOF which are not eliminated) is " << 
            dof_values.size()  << endl;

        if(ds.lagrange_multiplier()) {
            cout << " The Lagrange multipier is " << dof_values(dof_values.size() - 1) << ", should be 0" << endl << endl;
        }
        
        // timer
        t = clock();
        // reconstruct the local polynomials from the global dof values
        // first, deal with the global to local mapping, 
        // the static cond and the Dirichlet lifting,
        // then apply the reconstruction operator in each FE
        ds.reconstruction(mp.get_local_to_global_operator(), dof_values, mp.static_cond());

        t = clock() - t;
        reconstruction_time = ((double)t)/CLOCKS_PER_SEC;

    } catch (const char* msg) {
        cerr << "Could not assemble the local contributions " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }

    // visualization, execute the code with
    //      -v (optional: full_output_vtk_filename) 
    if(!output_vtk_meshfile.empty()) {
        try {
            // timer
            clock_t t(clock());

            // creates the VtkVisu class
            // If the method is conforming, simplification of the visu file, by precising it
            // in the creation of VtkVisu instance and in ds.reconstruction_at_vertices.
            // Can be set as true only if all variables are solved with conforming methods.
            bool conforming_method(false); 
            MyVisu::VtkVisu<DIM> vtk(mesh.get(), conforming_method);

            // the vtk visu needs the value at the vertices of mesh.
            // They are reconstructed from the numerical solution,
            // "bool conforming_method(true)" means that only one value at  
            // each vertex is computed (and not one value by vertex BY CELL).
            // Contains the value of all components if vectorial variable
            vtk.add_var(ds.reconstruction_at_vertices(mesh.get(), conforming_method, var_u), "velocity");
            vtk.add_var(ds.reconstruction_at_vertices(mesh.get(), conforming_method, var_p), "pressure");
                
            // value of the exact solution
            vtk.add_var(exact_sol_u, "exact velocity");
            vtk.add_var(exact_sol_p, "exact pressure");

            // write the vtk file with the values 
            // selected with "add_var"
            vtk.write(output_vtk_dir, output_vtk_numsol_file);

            t = clock() - t;
            visu_time = ((double)t)/CLOCKS_PER_SEC;

        } catch (const char* msg) {
            cerr << "Could not create the output file(s) " << endl;
            cerr << msg << endl;
            return EXIT_FAILURE; 
        }
    }
#if DIM==2
    // to write in a file the 2D mesh in tex format,
    // execute the code with
    //    -t (optional: full_output_tex_filename)
    if(!output_tex_file.empty()) {
        // write tex file with the mesh and with or without the center of the cells
        bool plot_centers = false;
        try {
            write_tex_mesh(mesh.get(), output_tex_dir, output_tex_file, plot_centers); 
        } catch (const char* msg) {
            cerr << msg << endl;
            return EXIT_FAILURE; 
        }
    }
#endif

    try {
        // timer
        clock_t t(clock());

#if DIM==2
        double mesh_size(mesh->h_max());
#elif DIM==3
        double mesh_size(mesh->h_mean());
#endif

        int norm_quadra_order(5);  // quadra order of the norm computations 

        // compute the relative error (L2 norm), 
        // comparaison with the exact solution given as continuous fonction
        double rel_l2error_u(MyDiscreteSpace::relative_l2error(&ds, exact_sol_u, norm_quadra_order, var_u)); 
        double rel_l2error_p(MyDiscreteSpace::relative_l2error(&ds, exact_sol_p, norm_quadra_order, var_p)); 

        // compute the relative error (H1 semi-norm),
        // comparaison with the gradient of the exact solution given as continuous fonction
        double rel_h1error_u(MyDiscreteSpace::relative_h1error(&ds, grad_exact_sol_u, norm_quadra_order, var_u)); 

        t = clock() - t;
        norm_time = ((double)t)/CLOCKS_PER_SEC;

        cout << endl << " for mesh size (h_max in 2D, h_mean in 3D) " << mesh_size << " relative L2 error between the exact sol and the numerical sol    u : " <<
        rel_l2error_u << "    p : " << rel_l2error_p << endl;
        cout << " for mesh size (h_max in 2D, h_mean in 3D) " << mesh_size << " relative semi H1 error between the exact sol and the numerical sol   u : " <<
        rel_h1error_u << endl;

        // timer
        total_time = clock() - total_time;
        total_time_s = ((double)total_time)/CLOCKS_PER_SEC;
        cout << endl << endl << " Total time (in s): " << total_time_s << "    with :" << endl;
        cout << "       mesh reader (in s):             " << mesh_read_time << "   in % : " << mesh_read_time / total_time_s *100 << endl;
        cout << "       init discrete space (in s):     " << ds.get_init_time() << "   in % : " << ds.get_init_time() / total_time_s *100 << endl;
        cout << "       build local matrices (in s):    " << ds.get_local_system_time() << "   in % : " << ds.get_local_system_time() / total_time_s *100 << endl;
        cout << "       init matrix pattern (in s):     " << mp.get_init_time() << "   in % : " << mp.get_init_time() / total_time_s *100 << endl;
        cout << "       assemble (in s):                " << mp.get_assemble_time() << "   in % : " << mp.get_assemble_time() / total_time_s *100 << endl;
        cout << "       solve (in s):                   " << solver_time << "   in % : " << solver_time / total_time_s *100 << endl;
        cout << "       reconstruct (in s):             " << reconstruction_time << "   in % : " << reconstruction_time / total_time_s *100 << endl;
        if(!output_vtk_meshfile.empty()) {
            cout << "       visualization (in s):           " << visu_time << "   in % : " << visu_time / total_time_s *100 << endl; }
        cout << "       norms computation (in s):       " << norm_time << "   in % : " << norm_time / total_time_s *100 << endl;
        cout << endl;

        // to write in a file some informations about the simulation, 
        // execute the code with
        //    -e (optional: full_output_filename for error and solver info)
        if(!output_error_textfile.empty()) { 
            // output_simu_characteristic_num appends in output_error_textfile
            // the order of convergence, the mesh size, the time to solve the linear system, 
            // the total time of the execution, the number of cells, the size of the linear system,
            // the relative error in L2 norm and the relative error in semi-norm H1
            // relative to u
            output_simu_characteristic_num(output_error_directory, output_error_textfile,
                skeletal_k, mesh_size, solver_time, total_time_s, mesh->n_cells(), dof_values.size(), 
                rel_l2error_u, rel_h1error_u);
            // relative to p
            output_simu_characteristic_num(output_error_directory, output_error_textfile,
                skeletal_k, mesh_size, solver_time, total_time_s, mesh->n_cells(), dof_values.size(), 
                rel_l2error_p, 0.);
        }

    } catch (const char* msg) {
        cerr << "Could not compute errors " << endl;
        cerr << msg << endl;
        return EXIT_FAILURE; 
    }


    mesh.reset(nullptr); mesh = 0;

    cout << "end of code" << endl;

}
